const imageGrid = document.querySelector(".image-grid");
const links = imageGrid.querySelectorAll("a");
const imgs = imageGrid.querySelectorAll("img");
const lightboxModal = document.getElementById("lightbox-modal");
const bsModal = new bootstrap.Modal(lightboxModal);
const modalBody = document.querySelector(".modal-body .container-fluid");

for (const link of links) {
	link.addEventListener("click", function (e) {
		e.preventDefault();
		const currentImg = link.querySelector("img");
		const lightboxCarousel = document.getElementById("lightboxCarousel");
		if (lightboxCarousel) {
			const parentCol = link.parentElement.parentElement;
			const index = [...parentCol.parentElement.children].indexOf(parentCol);
			const bsCarousel = new bootstrap.Carousel(lightboxCarousel);
			bsCarousel.to(index);
		} else {
			createCarousel(currentImg);
		}
		bsModal.show();
	});
}

function createCarousel(img) {
	const markup = `
    <div id="lightboxCarousel" class="carousel slide carousel-fade" data-bs-ride="carousel" data-bs-interval="false">
      <div class="carousel-inner">
        ${createSlides(img)}
      </div> 
      <button class="carousel-control-prev" type="button" data-bs-target="#lightboxCarousel" data-bs-slide="prev">
       <span class="carousel-control-prev-icon" aria-hidden="true"></span>
       <span class="visually-hidden">Previous</span>
      </button>
      <button class="carousel-control-next" type="button" data-bs-target="#lightboxCarousel" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
      </button>
    </div>
    `;

	modalBody.innerHTML = markup;
}

function createSlides(img) {
	let markup = "";
	const currentImgSrc = img.getAttribute("src");

	for (const img of imgs) {
		const imgSrc = img.getAttribute("src");
		const imgAlt = img.getAttribute("alt");
		const imgCaption = img.getAttribute("data-caption");

		markup += `
    <div class="carousel-item${currentImgSrc === imgSrc ? " active" : ""}">
      <img src=${imgSrc} alt=${imgAlt}>
      ${imgCaption ? createCaption(imgCaption) : ""}
    </div>
    `;
	}

	return markup;
}

function createCaption(caption) {
	return `<div class="carousel-caption">
     <p class="m-0">${caption}</p>
    </div>`;
}

$(document).ready(function () {
	var myPlayer = videojs("vid1");
	myPlayer.ready(function () {
		console.log("downloadvideo");

		setTimeout(function () {
			console.log("stop video");
			//myPlayer.play();
			myPlayer.pause();
		}, 10000);
	});
});

/*///////////////////////////////////////////////////////////////////*/
/* Simple HTML5 Audio player                                         */
/* With button Play/Pause                                            */
/* To hide the player, just remove `controls` attribute or quote it  */
/*///////////////////////////////////////////////////////////////////*/

// forked from http://jsfiddle.net/5r02pf9u/
// some links:
// http://html5tutorial.info/html5-audio.php
// http://html5tutorial.info/html5-video.php
//
// some links:
// https://html.spec.whatwg.org/#audio
// http://www.w3.org/html/wg/drafts/html/master/semantics.html#audio

// http://www.position-absolute.com/articles/introduction-to-the-html5-audio-tag-javascript-manipulation/
// also interesting comments:
//Audio.prototype.stop = function () {
//    this.pause();
//    this.currentTime = 0;
//}
// 'play', 'pause', 'play at 35 secondes', 'Volume to 0', 'Volume open'
// http://www.position-relative.net/creation/audiotag/

// pure JS only
//var blasterTrigger = document.querySelector(".blasterTrigger");
//blasterTrigger.addEventListener("click", function () {
//    document.getElementById("blast").play();

// JQuery
// var blast = document.querySelector("#blast");

// Play/Pause Button
$(".blasterTrigger").click(function () {
	$("#blast").each(function () {
		// html5 audio player - jquery toggle click play/pause?
		// http://stackoverflow.com/a/2988130
		if (this.paused === false) {
			this.pause();
			$(".blasterTrigger").text("Play");
			//        alert('music paused');
		} else {
			this.play();
			$(".blasterTrigger").text("||");
			//        alert('music playing');
		}
	});
});

// Play/Stop button ...
// not sure how to use the following function:
Audio.prototype.stop = function () {
	this.pause();
	this.currentTime = 0;
};

// ... Play/Stop button
$(".blasterTrigger2").click(function () {
	$("#blast").each(function () {
		// html5 audio player - jquery toggle click play/pause?
		// http://stackoverflow.com/a/2988130
		if (this.paused === false) {
			this.pause();
			//this.currentTime = 0;
			$(".blasterTrigger2").text("Play");
			//        alert('music paused');
		} else {
			this.play();
			$(".blasterTrigger2").text("Stop");
			//        alert('music playing');
		}
	});
});

/* // auto-play when page is loaded.
document.addEventListener("DOMContentLoaded", function (event) {
    document.getElementById("blast").play();
});
*/
