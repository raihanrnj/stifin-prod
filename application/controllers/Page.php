<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Page extends CI_Controller
{

    var $API = "";

    public function __construct()
    {
        parent::__construct();
        $this->API = "https://api.stifin.or.id/api/server/MyTopTen/WEB";
        $this->load->model('M_konsep');
        $this->load->model('M_landing');
        $this->load->model('M_ekosistem');
        $this->load->model('M_about');
        $this->load->model('M_download');
        $this->load->model('M_artikel');
        $this->load->model('M_detail_cabang');
        $this->load->model('M_detail_promotor');
        $this->load->library('curl');
        $this->load->helper('url');
        $this->load->helper('text');
    }

    public function view($page = 'landing')
    {
        if (!file_exists(APPPATH . 'views/view_user/' . $page . '.php')) {
            show_404();
        }

        $data['api_data'] = $this->curl->simple_get('https://api.stifin.or.id/api/server/MyTopTen/WEB');

        $data['title'] = ucfirst($page);
        $data['page'] = $page;

        $data['hero_section'] = $this->M_landing->get_data();
        $data['about_section'] = $this->M_landing->get_data_about();
        $data['about_promotor_section'] = $this->M_ekosistem->get_data_about_promotor();
        $data['keunggulan_section'] = $this->M_landing->get_data_keunggulan();
        $data['video_section'] = $this->M_landing->get_data_video();
        $data['testimoni_section'] = $this->M_landing->get_data_testimoni();
        $data['slider_section'] = $this->M_landing->get_data_slider();
        $data['yayasan_stifin'] = $this->M_about->get_data_about_yayasan();
        $data['stifin_institute'] = $this->M_about->get_data_stifin_institute();
        $data['stifin_rqs'] = $this->M_about->get_data_stifin_rqs();
        $data['konsep_stifin'] = $this->M_konsep->get_data_konsep();
        $data['sensing'] = $this->M_konsep->get_data_sensing();
        $data['thingking'] = $this->M_konsep->get_data_thingking();
        $data['intuiting'] = $this->M_konsep->get_data_intuiting();
        $data['feeling'] = $this->M_konsep->get_data_feeling();
        $data['insting'] = $this->M_konsep->get_data_insting();
        $data['merchandise'] = $this->M_about->get_data_merchandise();
        $data['artikel'] = $this->M_artikel->get_artikel_landing();
        $data['video'] = $this->M_download->get_data_video();
        $data['audio'] = $this->M_download->get_data_audio();
        $data['ebook'] = $this->M_download->get_data_ebook();
        $data['aplikasi'] = $this->M_download->get_data_aplikasi();
        $data['footer_section'] = $this->M_landing->get_data_footer();
        $data['partners'] = $this->M_landing->get_data_partners();
        $data['cabang_section'] = $this->M_landing->get_data_cabang();
        $data['promotor_section'] = $this->M_landing->get_data_promotor();
        $data['detail_cabang'] = $this->M_detail_cabang->get_data_about_detail_cabang();
        $data['potensi_usaha'] = $this->M_detail_cabang->get_data_potensi_usaha();
        $data['sumber_profit'] = $this->M_detail_cabang->get_data_sumber_profit();
        $data['alur_tes'] = $this->M_detail_cabang->get_data_alur_tes();
        $data['pilihan_cabang'] = $this->M_detail_cabang->get_data_pilihan_cabang();
        $data['popup'] = $this->M_detail_cabang->get_data_popup();
        $data['detail_promotor'] = $this->M_detail_promotor->get_data_detail_promotor();
        $data['dp_benefit'] = $this->M_detail_promotor->get_data_benefit_promotor();
        $data['trainer_stifin'] = $this->M_ekosistem->get_data_trainer();
        $data['people_trainer_stifin'] = $this->M_ekosistem->get_data_trainer_people();
        $data['solver_about'] = $this->M_ekosistem->get_data_solver_about();
        $data['solver_stifin'] = $this->M_ekosistem->get_data_solver_people();


        $this->load->view('view_templates/header', $data);
        $this->load->view('view_user/' . $page, $data);
        $this->load->view('view_templates/footer', $data);
    }

    public function artikel($slug = NULL)
    {

        // cek if slug is null
        if ($slug === NULL) {
            $data['artikel'] = $this->M_artikel->get_data_artikel();
            $data['title'] = 'Artikel';
            $data['page'] = 'artikel';
            $data['footer_section'] = $this->M_landing->get_data_footer();


            $this->load->view('view_templates/header', $data);
            $this->load->view('view_user/artikel', $data);
            $this->load->view('view_templates/footer', $data);
        } else {
            $data['artikel'] = $this->M_artikel->get_artikel($slug);
            $data['title'] = $data['artikel']['judul'];
            $data['page'] = 'artikel_detail';
            $data['footer_section'] = $this->M_landing->get_data_footer();


            $this->load->view('view_templates/header', $data);
            $this->load->view('view_user/artikel_detail', $data);
            $this->load->view('view_templates/footer', $data);
        }
    }
}
