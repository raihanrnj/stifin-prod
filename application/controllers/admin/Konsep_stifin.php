<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Konsep_stifin extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_konsep');
        $this->load->library('upload');
        $this->load->helper('text');
        if ($this->session->userdata('status') != "admin_login") {
            redirect(base_url() . 'admin/auth');
        }
    }

    public function index()
    {
        $data['page']             = "konsep_stifin";
        $data['judul']             = "Konsep STIFIn";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['konsep_stifin'] = $this->M_konsep->get_data_konsep();


        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/konsep_stifin/konsep_stifin', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function konsep_stifin_add()
    {
        $konten        = $this->input->post('konten');

        $this->form_validation->set_rules('konten', 'konten', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'konten'      => $konten,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_konsep->insert_data_konsep($data);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/konsep_stifin');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/konsep_stifin');
                }
            }
        }
    }

    public function konsep_stifin_update()
    {
        $id           = $this->input->post('id');
        $konten        = $this->input->post('konten');

        $this->form_validation->set_rules('konten', 'konten', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'konten'      => $konten,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_konsep->update_data_konsep($data, $data_id);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/konsep_stifin');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/konsep_stifin');
                }
            } else {
                $data = array(
                    'konten'      => $konten,
                );
                $this->M_konsep->update_data_konsep($data, $data_id);
                $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                redirect('admin/konsep_stifin');
            }
        }
    }

    public function konsep_stifin_delete($id)
    {
        $where = array('id' => $id);
        $this->M_konsep->delete_data_konsep($where, 'tb_konsep_stifin');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
        redirect('admin/konsep_stifin');
    }

    // Sensing
    public function sensing()
    {
        $data['page']             = "sensing";
        $data['judul']             = "Konsep STIFIn";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['sensing'] = $this->M_konsep->get_data_sensing();


        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/konsep_stifin/sensing', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function sensing_add()
    {
        $konten        = $this->input->post('konten');

        $this->form_validation->set_rules('konten', 'konten', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'konten'      => $konten,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_konsep->insert_data_sensing($data);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/konsep_stifin/sensing');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/konsep_stifin/sensing');
                }
            }
        }
    }

    public function sensing_update()
    {
        $id           = $this->input->post('id');
        $konten        = $this->input->post('konten');

        $this->form_validation->set_rules('konten', 'konten', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'konten'      => $konten,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_konsep->update_data_sensing($data, $data_id);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/konsep_stifin/sensing');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/konsep_stifin/sensing');
                }
            } else {
                $data = array(
                    'konten'      => $konten,
                );
                $this->M_konsep->update_data_sensing($data, $data_id);
                $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                redirect('admin/konsep_stifin/sensing');
            }
        }
    }

    public function sensing_delete($id)
    {
        $where = array('id' => $id);
        $this->M_konsep->delete_data_sensing($where, 'tb_sensing');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
        redirect('admin/konsep_stifin/sensing');
    }

    // Thingking
    public function thingking()
    {
        $data['page']             = "thingking";
        $data['judul']             = "Konsep STIFIn";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['thingking'] = $this->M_konsep->get_data_thingking();


        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/konsep_stifin/thingking', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function thingking_add()
    {
        $konten        = $this->input->post('konten');

        $this->form_validation->set_rules('konten', 'konten', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'konten'      => $konten,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_konsep->insert_data_thingking($data);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/konsep_stifin/thingking');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/konsep_stifin/thingking');
                }
            }
        }
    }

    public function thingking_update()
    {
        $id           = $this->input->post('id');
        $konten        = $this->input->post('konten');

        $this->form_validation->set_rules('konten', 'konten', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'konten'      => $konten,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_konsep->update_data_thingking($data, $data_id);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/konsep_stifin/thingking');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/konsep_stifin/thingking');
                }
            } else {
                $data = array(
                    'konten'      => $konten,
                );
                $this->M_konsep->update_data_thingking($data, $data_id);
                $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                redirect('admin/konsep_stifin/thingking');
            }
        }
    }

    public function thingking_delete($id)
    {
        $where = array('id' => $id);
        $this->M_konsep->delete_data_thingking($where, 'tb_thingking');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
        redirect('admin/konsep_stifin/thingking');
    }

    // Intuiting
    public function intuiting()
    {
        $data['page']             = "intuiting";
        $data['judul']             = "Konsep STIFIn";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['intuiting'] = $this->M_konsep->get_data_intuiting();


        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/konsep_stifin/intuiting', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function intuiting_add()
    {
        $konten        = $this->input->post('konten');

        $this->form_validation->set_rules('konten', 'konten', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'konten'      => $konten,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_konsep->insert_data_intuiting($data);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/konsep_stifin/intuiting');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/konsep_stifin/intuiting');
                }
            }
        }
    }

    public function intuiting_update()
    {
        $id           = $this->input->post('id');
        $konten        = $this->input->post('konten');

        $this->form_validation->set_rules('konten', 'konten', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'konten'      => $konten,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_konsep->update_data_intuiting($data, $data_id);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/konsep_stifin/intuiting');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/konsep_stifin/intuiting');
                }
            } else {
                $data = array(
                    'konten'      => $konten,
                );
                $this->M_konsep->update_data_intuiting($data, $data_id);
                $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                redirect('admin/konsep_stifin/intuiting');
            }
        }
    }

    public function intuiting_delete($id)
    {
        $where = array('id' => $id);
        $this->M_konsep->delete_data_intuiting($where, 'tb_intuiting');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
        redirect('admin/konsep_stifin/intuiting');
    }

    // Feeling
    public function feeling()
    {
        $data['page']             = "feeling";
        $data['judul']             = "Konsep STIFIn";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['feeling'] = $this->M_konsep->get_data_feeling();


        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/konsep_stifin/feeling', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function feeling_add()
    {
        $konten        = $this->input->post('konten');

        $this->form_validation->set_rules('konten', 'konten', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'konten'      => $konten,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_konsep->insert_data_feeling($data);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/konsep_stifin/feeling');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/konsep_stifin/feeling');
                }
            }
        }
    }

    public function feeling_update()
    {
        $id           = $this->input->post('id');
        $konten        = $this->input->post('konten');

        $this->form_validation->set_rules('konten', 'konten', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'konten'      => $konten,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_konsep->update_data_feeling($data, $data_id);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/konsep_stifin/feeling');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/konsep_stifin/feeling');
                }
            } else {
                $data = array(
                    'konten'      => $konten,
                );
                $this->M_konsep->update_data_feeling($data, $data_id);
                $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                redirect('admin/konsep_stifin/feeling');
            }
        }
    }

    public function feeling_delete($id)
    {
        $where = array('id' => $id);
        $this->M_konsep->delete_data_feeling($where, 'tb_feeling');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
        redirect('admin/konsep_stifin/feeling');
    }

    // Insting
    public function insting()
    {
        $data['page']             = "insting";
        $data['judul']             = "Konsep STIFIn";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['insting'] = $this->M_konsep->get_data_insting();


        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/konsep_stifin/insting', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function insting_add()
    {
        $konten        = $this->input->post('konten');

        $this->form_validation->set_rules('konten', 'konten', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'konten'      => $konten,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_konsep->insert_data_insting($data);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/konsep_stifin/insting');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/konsep_stifin/insting');
                }
            }
        }
    }

    public function insting_update()
    {
        $id           = $this->input->post('id');
        $konten        = $this->input->post('konten');

        $this->form_validation->set_rules('konten', 'konten', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'konten'      => $konten,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_konsep->update_data_insting($data, $data_id);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/konsep_stifin/insting');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/konsep_stifin/insting');
                }
            } else {
                $data = array(
                    'konten'      => $konten,
                );
                $this->M_konsep->update_data_insting($data, $data_id);
                $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                redirect('admin/konsep_stifin/insting');
            }
        }
    }

    public function insting_delete($id)
    {
        $where = array('id' => $id);
        $this->M_konsep->delete_data_insting($where, 'tb_insting');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
        redirect('admin/konsep_stifin/insting');
    }
}
