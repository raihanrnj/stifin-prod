<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Ekosistem extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_ekosistem');
        $this->load->library('upload');
        $this->load->helper('text');
        if ($this->session->userdata('status') != "admin_login") {
            redirect(base_url() . 'admin/auth');
        }
    }

    public function cabang()
    {
        $data['page']             = "cabang";
        $data['judul']             = "Download Aplikasi";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['cabang'] = $this->M_ekosistem->get_data_cabang();
        $data['data_provinsi'] = $this->M_ekosistem->get_data_provinsi();

        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/ekosistem/cabang', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function cabang_add()
    {
        $provinsi        = $this->input->post('provinsi');
        $nama_cabang        = $this->input->post('nama_cabang');
        $alamat        = $this->input->post('alamat');
        $jam_operasional        = $this->input->post('jam_operasional');
        $no_hp        = $this->input->post('no_hp');
        $maps        = $this->input->post('maps');
        $website        = $this->input->post('website');

        $this->form_validation->set_rules('provinsi', 'Provinsi', 'trim|required');
        $this->form_validation->set_rules('nama_cabang', 'nama_cabang', 'trim|required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');
        $this->form_validation->set_rules('jam_operasional', 'jam_operasional', 'trim|required');
        $this->form_validation->set_rules('no_hp', 'No HP', 'trim|required');
        $this->form_validation->set_rules('maps', 'Maps', 'trim|required');
        $this->form_validation->set_rules('website', 'Website', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data = array(
                'provinsi' => $provinsi,
                'nama_cabang' => $nama_cabang,
                'alamat' => $alamat,
                'jam_operasional' => $jam_operasional,
                'no_hp' => $no_hp,
                'maps' => $maps,
                'website' => $website,
            );
            $this->M_ekosistem->insert_data_cabang($data);
            $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
            redirect('admin/ekosistem/cabang');
        } else {
            $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
            redirect('admin/ekosistem/cabang');
        }
    }

    public function cabang_update()
    {
        $id = $this->input->post('id');
        $provinsi        = $this->input->post('provinsi');
        $nama_cabang        = $this->input->post('nama_cabang');
        $alamat        = $this->input->post('alamat');
        $jam_operasional        = $this->input->post('jam_operasional');
        $no_hp        = $this->input->post('no_hp');
        $maps        = $this->input->post('maps');
        $website        = $this->input->post('website');

        $this->form_validation->set_rules('provinsi', 'Provinsi', 'trim|required');
        $this->form_validation->set_rules('nama_cabang', 'nama_cabang', 'trim|required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');
        $this->form_validation->set_rules('jam_operasional', 'jam_operasional', 'trim|required');
        $this->form_validation->set_rules('no_hp', 'No HP', 'trim|required');
        $this->form_validation->set_rules('maps', 'Maps', 'trim|required');
        $this->form_validation->set_rules('website', 'Website', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);
            $data = array(
                'provinsi' => $provinsi,
                'nama_cabang' => $nama_cabang,
                'alamat' => $alamat,
                'jam_operasional' => $jam_operasional,
                'no_hp' => $no_hp,
                'maps' => $maps,
                'website' => $website,
            );
            $this->M_ekosistem->update_data_cabang($data, $data_id);
            $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
            redirect('admin/ekosistem/cabang');
        } else {
            $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
            redirect('admin/ekosistem/cabang');
        }
    }

    public function cabang_delete($id)
    {
        $where = array('id' => $id);
        $this->M_ekosistem->delete_data_cabang($where, 'tb_cabang');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di hapus</div>');
        redirect('admin/ekosistem/cabang');
    }

    // Workshop
    public function workshop()
    {
        $data['page']             = "workshop";
        $data['judul']             = "Download Aplikasi";
        $data['deskripsi']         = "Manage Data CRUD";
        $workshop       = $this->M_ekosistem->get_data_workshop();
        foreach ($workshop as $index => $row) {
            $tanggal_workshop = explode(' ', $row['tanggal_workshop']);

            $workshop[$index]['tanggal_workshop'] = [];
            $workshop[$index]['tanggal_workshop'][0] = '';
            $workshop[$index]['tanggal_workshop'][1] = '';
            if (isset($tanggal_workshop[0])) {
                $workshop[$index]['tanggal_workshop'][0] = $tanggal_workshop[0];
            }
            if (isset($tanggal_workshop[1])) {
                $workshop[$index]['tanggal_workshop'][1] = $tanggal_workshop[1];
            }
        }

        $data['workshop'] = $workshop;
        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/ekosistem/workshop', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function workshop_add()
    {
        $judul                   = $this->input->post('judul');
        $konten                  = $this->input->post('konten');
        $tanggal_workshop        = implode(" ", $this->input->post('tanggal_workshop'));
        $this->form_validation->set_rules('judul', 'judul', 'trim|required');
        $this->form_validation->set_rules('konten', 'konten', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data = array(
                'judul' => $judul,
                'konten' => $konten,
                'tanggal_workshop' => $tanggal_workshop
            );
            $this->M_ekosistem->insert_data_workshop($data);
            $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
            redirect('admin/ekosistem/workshop');
        } else {
            $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
            redirect('admin/ekosistem/workshop');
        }
    }

    public function workshop_update()
    {
        $id = $this->input->post('id');
        $judul        = $this->input->post('judul');
        $konten        = $this->input->post('konten');
        $tanggal_workshop        = implode(" ", $this->input->post('tanggal_workshop'));
        $this->form_validation->set_rules('judul', 'judul', 'trim|required');
        $this->form_validation->set_rules('konten', 'konten', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);
            $data = array(
                'judul' => $judul,
                'konten' => $konten,
                'tanggal_workshop' => $tanggal_workshop
            );
            $this->M_ekosistem->update_data_workshop($data, $data_id);
            $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
            redirect('admin/ekosistem/workshop');
        } else {
            $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
            redirect('admin/ekosistem/workshop');
        }
    }

    public function workshop_delete($id)
    {
        $where = array('id' => $id);
        $this->M_ekosistem->delete_data_workshop($where, 'tb_workshop');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di hapus</div>');
        redirect('admin/ekosistem/workshop');
    }


    // jenjang
    public function jenjang()
    {
        $data['page']             = "jenjang";
        $data['judul']             = "Download Aplikasi";
        $data['deskripsi']         = "Manage Data CRUD";
        $jenjang       = $this->M_ekosistem->get_data_jenjang();

        foreach ($jenjang as $index => $row) {
            $tahapan =  $row['tahapan'];

            $jenjang[$index]['tahapan'] = $tahapan;
            // $jenjang[$index]['tahapan'][0] = '';
            // $jenjang[$index]['tahapan'][1] = '';
            // if (isset($tahapan[0])) {
            //     $jenjang[$index]['tahapan'][0] = $tahapan[0];
            // }
            // if (isset($tahapan[1])) {
            //     $jenjang[$index]['tahapan'][1] = $tahapan[1];
            // }
        }

        $data['jenjang'] = $jenjang;
        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/ekosistem/jenjang', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function jenjang_add()
    {
        $judul                   = $this->input->post('judul');
        $konten                  = $this->input->post('konten');
        $tahapan        = $this->input->post('tahapan');
        $this->form_validation->set_rules('judul', 'judul', 'trim|required');
        $this->form_validation->set_rules('konten', 'konten', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data = array(
                'judul' => $judul,
                'konten' => $konten,
                'tahapan' => $tahapan
            );
            $this->M_ekosistem->insert_data_jenjang($data);
            $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
            redirect('admin/ekosistem/jenjang');
        } else {
            $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
            redirect('admin/ekosistem/jenjang');
        }
    }

    public function jenjang_update()
    {
        $id = $this->input->post('id');
        $judul        = $this->input->post('judul');
        $konten        = $this->input->post('konten');
        $tahapan        = $this->input->post('tahapan');
        $this->form_validation->set_rules('judul', 'judul', 'trim|required');
        $this->form_validation->set_rules('konten', 'konten', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);
            $data = array(
                'judul' => $judul,
                'konten' => $konten,
                'tahapan' => $tahapan
            );
            $this->M_ekosistem->update_data_jenjang($data, $data_id);
            $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
            redirect('admin/ekosistem/jenjang');
        } else {
            $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
            redirect('admin/ekosistem/jenjang');
        }
    }

    public function jenjang_delete($id)
    {
        $where = array('id' => $id);
        $this->M_ekosistem->delete_data_jenjang($where, 'tb_jenjang');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di hapus</div>');
        redirect('admin/ekosistem/jenjang');
    }


    // About Promotor Section
    public function about_promotor_section()
    {
        $data['page']             = "about_promotor_section";
        $data['judul']             = "Landing Page";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['about_promotor_section'] = $this->M_ekosistem->get_data_about_promotor();

        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/ekosistem/about_promotor_section', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function about_promotor_section_add()
    {
        $judul        = $this->input->post('judul');
        $deskripsi    = $this->input->post('deskripsi');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'deskripsi' => $deskripsi,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_ekosistem->insert_data_about_promotor($data);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/landing/about_promotor_section');
                } else {
                    $this->session->set_flashdata('notif', '        <div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/landing/about_promotor_section');
                }
            }
        }
    }

    public function about_promotor_section_update()
    {
        $id           = $this->input->post('id');
        $judul        = $this->input->post('judul');
        $deskripsi    = $this->input->post('deskripsi');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'deskripsi' => $deskripsi,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_ekosistem->update_data_about_promotor($data, $data_id);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                    redirect('admin/ekosistem/about_promotor_section');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/ekosistem/about_promotor_section');
                }
            } else {
                $data = array(
                    'judul'      => $judul,
                    'deskripsi' => $deskripsi,
                );
                $this->M_ekosistem->update_data_about_promotor($data, $data_id);
                $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                redirect('admin/landing/about_promotor_section');
            }
        }
    }

    public function about_promotor_section_delete($id)
    {
        $where = array('id' => $id);
        $this->M_ekosistem->delete_data_about_promotor($where, 'tb_about_us');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
        redirect('admin/landing/about_promotor_section');
    }

    // Trainer STIFIn
    public function trainer_stifin()
    {
        $data['page']              = "about_trainer_stifin";
        $data['judul']             = "Landing Page";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['trainer_stifin'] = $this->M_ekosistem->get_data_trainer();

        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/ekosistem/trainer_stifin/trainer_stifin', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function trainer_stifin_add()
    {
        $judul        = $this->input->post('judul');
        $deskripsi    = $this->input->post('deskripsi');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'deskripsi' => $deskripsi,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_ekosistem->insert_data_trainer($data);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/ekosistem/trainer_stifin');
                } else {
                    $this->session->set_flashdata('notif', '        <div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/ekosistem/trainer_stifin');
                }
            }
        }
    }

    public function trainer_stifin_update()
    {
        $id           = $this->input->post('id');
        $judul        = $this->input->post('judul');
        $deskripsi    = $this->input->post('deskripsi');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'deskripsi' => $deskripsi,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_ekosistem->update_data_trainer($data, $data_id);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                    redirect('admin/ekosistem/trainer_stifin');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/ekosistem/trainer_stifin');
                }
            } else {
                $data = array(
                    'judul'      => $judul,
                    'deskripsi' => $deskripsi,
                );
                $this->M_ekosistem->update_data_trainer($data, $data_id);
                $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                redirect('admin/ekosistem/trainer_stifin');
            }
        }
    }

    public function trainer_stifin_delete($id)
    {
        $where = array('id' => $id);
        $this->M_ekosistem->delete_data_trainer($where, 'tb_trainer_about');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
        redirect('admin/ekosistem/trainer_stifin');
    }

    // Roadmap Trainer
    public function trainer_roadmap()
    {
        $data['page']              = "roadmap_trainer_stifin";
        $data['judul']             = "Download Aplikasi";
        $data['deskripsi']         = "Manage Data CRUD";
        $trainer_roadmap       = $this->M_ekosistem->get_data_roadmap_trainer();

        $data['trainer_roadmap'] = $trainer_roadmap;
        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/ekosistem/trainer_stifin/roadmap_trainer', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function trainer_roadmap_add()
    {
        $konten                  = $this->input->post('konten');
        $this->form_validation->set_rules('konten', 'konten', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data = array(
                'konten' => $konten,
                'waktu'       => date('Y-m-d H:i:s'),
            );
            $this->M_ekosistem->insert_data_roadmap_trainer($data);
            $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di simpan</div>');
            redirect('admin/ekosistem/trainer_roadmap');
        } else {
            $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal di simpan</div>');
            redirect('admin/ekosistem/trainer_roadmap');
        }
    }

    public function trainer_roadmap_update()
    {
        $id = $this->input->post('id');
        $konten        = $this->input->post('konten');
        $this->form_validation->set_rules('konten', 'konten', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);
            $data = array(
                'konten' => $konten,
                'waktu'       => date('Y-m-d H:i:s'),
            );
            $this->M_ekosistem->update_data_roadmap_trainer($data, $data_id);
            $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
            redirect('admin/ekosistem/trainer_roadmap');
        } else {
            $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal di simpan</div>');
            redirect('admin/ekosistem/trainer_roadmap');
        }
    }

    public function trainer_roadmap_delete($id)
    {
        $where = array('id' => $id);
        $this->M_ekosistem->delete_data_roadmap_trainer($where, 'tb_trainer_roadmap');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di hapus</div>');
        redirect('admin/ekosistem/trainer_roadmap');
    }

    // Para Trainer
    public function trainer()
    {
        $data['page']              = "people_trainer_stifin";
        $data['judul']             = "Landing Page";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['people_trainer_stifin'] = $this->M_ekosistem->get_data_trainer_people();

        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/ekosistem/trainer_stifin/people_trainer_stifin', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function trainer_add()
    {
        $nama        = $this->input->post('nama');
        $branding        = $this->input->post('branding');
        $rating        = $this->input->post('rating');
        $deskripsi    = $this->input->post('deskripsi');

        $this->form_validation->set_rules('nama', 'nama', 'trim|required');
        $this->form_validation->set_rules('branding', 'branding', 'trim|required');
        $this->form_validation->set_rules('rating', 'rating', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '4048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'nama'        => $nama,
                        'branding'        => $branding,
                        'rating'        => $rating,
                        'deskripsi' => $deskripsi,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_ekosistem->insert_data_trainer_people($data);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/ekosistem/trainer');
                } else {
                    $this->session->set_flashdata('notif', '        <div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/ekosistem/trainer');
                }
            }
        }
    }

    public function trainer_update()
    {
        $id           = $this->input->post('id');
        $nama        = $this->input->post('nama');
        $branding        = $this->input->post('branding');
        $rating        = $this->input->post('rating');
        $deskripsi    = $this->input->post('deskripsi');

        $this->form_validation->set_rules('nama', 'nama', 'trim|required');
        $this->form_validation->set_rules('branding', 'branding', 'trim|required');
        $this->form_validation->set_rules('rating', 'rating', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'nama'        => $nama,
                        'branding'        => $branding,
                        'rating'        => $rating,
                        'deskripsi' => $deskripsi,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_ekosistem->update_data_trainer_people($data, $data_id);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                    redirect('admin/ekosistem/trainer');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/ekosistem/trainer');
                }
            } else {
                $data = array(
                    'nama'        => $nama,
                    'branding'        => $branding,
                    'rating'        => $rating,
                    'deskripsi' => $deskripsi,
                );
                $this->M_ekosistem->update_data_trainer_people($data, $data_id);
                $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                redirect('admin/ekosistem/trainer');
            }
        }
    }

    public function trainer_delete($id)
    {
        $where = array('id' => $id);
        $this->M_ekosistem->delete_data_trainer_people($where, 'tb_trainer_people');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
        redirect('admin/ekosistem/trainer');
    }


    // Solver about
    public function solver_about()
    {
        $data['page']              = "solver_about";
        $data['judul']             = "Landing Page";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['solver_about'] = $this->M_ekosistem->get_data_solver_about();

        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/ekosistem/solver_stifin/about_solver', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function solver_about_add()
    {
        $judul        = $this->input->post('judul');
        $deskripsi    = $this->input->post('deskripsi');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '4048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'deskripsi' => $deskripsi,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_ekosistem->insert_data_solver_about($data);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/ekosistem/solver_about');
                } else {
                    $this->session->set_flashdata('notif', '        <div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/ekosistem/solver_about');
                }
            }
        }
    }

    public function solver_about_update()
    {
        $id           = $this->input->post('id');
        $judul        = $this->input->post('judul');
        $deskripsi    = $this->input->post('deskripsi');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '4048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'deskripsi' => $deskripsi,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_ekosistem->update_data_solver_about($data, $data_id);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                    redirect('admin/ekosistem/solver_about');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/ekosistem/solver_about');
                }
            } else {
                $data = array(
                    'judul'      => $judul,
                    'deskripsi' => $deskripsi,
                );
                $this->M_ekosistem->update_data_solver_about($data, $data_id);
                $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                redirect('admin/ekosistem/solver_about');
            }
        }
    }

    public function solver_about_delete($id)
    {
        $where = array('id' => $id);
        $this->M_ekosistem->delete_data_solver_about($where, 'tb_solver_about');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
        redirect('admin/ekosistem/solver_about');
    }

    // Roadmap Solver
    public function solver_roadmap()
    {
        $data['page']              = "solver_roadmap";
        $data['judul']             = "Download Aplikasi";
        $data['deskripsi']         = "Manage Data CRUD";
        $solver_roadmap       = $this->M_ekosistem->get_data_roadmap_solver();

        $data['solver_roadmap'] = $solver_roadmap;
        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/ekosistem/solver_stifin/roadmap_solver', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function solver_roadmap_add()
    {
        $konten                  = $this->input->post('konten');
        $this->form_validation->set_rules('konten', 'konten', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data = array(
                'konten' => $konten,
                'waktu'       => date('Y-m-d H:i:s'),
            );
            $this->M_ekosistem->insert_data_roadmap_solver($data);
            $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di simpan</div>');
            redirect('admin/ekosistem/solver_roadmap');
        } else {
            $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal di simpan</div>');
            redirect('admin/ekosistem/solver_roadmap');
        }
    }

    public function solver_roadmap_update()
    {
        $id = $this->input->post('id');
        $konten        = $this->input->post('konten');
        $this->form_validation->set_rules('konten', 'konten', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);
            $data = array(
                'konten' => $konten,
                'waktu'       => date('Y-m-d H:i:s'),
            );
            $this->M_ekosistem->update_data_roadmap_solver($data, $data_id);
            $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
            redirect('admin/ekosistem/solver_roadmap');
        } else {
            $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal di simpan</div>');
            redirect('admin/ekosistem/solver_roadmap');
        }
    }

    public function solver_roadmap_delete($id)
    {
        $where = array('id' => $id);
        $this->M_ekosistem->delete_data_roadmap_solver($where, 'tb_solver_roadmap');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di hapus</div>');
        redirect('admin/ekosistem/solver_roadmap');
    }

     // Para Solver
     public function solver()
     {
         $data['page']              = "solver_people";
         $data['judul']             = "Landing Page";
         $data['deskripsi']         = "Manage Data CRUD";
         $data['solver_stifin'] = $this->M_ekosistem->get_data_solver_people();
 
         $this->load->view('view_admin/templates/header', $data);
         $this->load->view('view_admin/templates/asside', $data);
         $this->load->view('view_admin/ekosistem/solver_stifin/solver_stifin', $data);
         $this->load->view('view_admin/templates/footer', $data);
     }
 
     public function solver_add()
     {
         $nama        = $this->input->post('nama');
         $branding        = $this->input->post('branding');
         $rating        = $this->input->post('rating');
         $deskripsi    = $this->input->post('deskripsi');
 
         $this->form_validation->set_rules('nama', 'nama', 'trim|required');
         $this->form_validation->set_rules('branding', 'branding', 'trim|required');
         $this->form_validation->set_rules('rating', 'rating', 'trim|required');
         $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');
 
         if ($this->form_validation->run() !== FALSE) {
 
             $config['upload_path']   = './assets/upload/images';
             $config['allowed_types'] = 'jpg|png|jpeg';
             $config['max_size']      = '4048';
             $config['file_name']     = $_FILES['gambar']['name'];
 
             $this->upload->initialize($config);
 
             if (!empty($_FILES['gambar']['name'])) {
                 if ($this->upload->do_upload('gambar')) {
                     $gambar = $this->upload->data();
                     date_default_timezone_set("Asia/Jakarta");
                     $data = array(
                         'nama'        => $nama,
                         'branding'        => $branding,
                         'rating'        => $rating,
                         'deskripsi' => $deskripsi,
                         'gambar'      => $gambar['file_name'],
                     );
                     $this->M_ekosistem->insert_data_solver_people($data);
                     $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                     redirect('admin/ekosistem/solver');
                 } else {
                     $this->session->set_flashdata('notif', '        <div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                     redirect('admin/ekosistem/solver');
                 }
             }
         }
     }
 
     public function solver_update()
     {
         $id           = $this->input->post('id');
         $nama        = $this->input->post('nama');
         $branding        = $this->input->post('branding');
         $rating        = $this->input->post('rating');
         $deskripsi    = $this->input->post('deskripsi');
 
         $this->form_validation->set_rules('nama', 'nama', 'trim|required');
         $this->form_validation->set_rules('branding', 'branding', 'trim|required');
         $this->form_validation->set_rules('rating', 'rating', 'trim|required');
         $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');
 
         if ($this->form_validation->run() !== FALSE) {
             $data_id = array('id' => $id);
 
             $config['upload_path']   = './assets/upload/images';
             $config['allowed_types'] = 'jpg|png|jpeg';
             $config['max_size']      = '1048';
             $config['file_name']     = $_FILES['gambar']['name'];
 
             $this->upload->initialize($config);
 
             if (!empty($_FILES['gambar']['name'])) {
                 if ($this->upload->do_upload('gambar')) {
                     $gambar = $this->upload->data();
                     date_default_timezone_set("Asia/Jakarta");
                     $data = array(
                         'nama'        => $nama,
                         'branding'        => $branding,
                         'rating'        => $rating,
                         'deskripsi' => $deskripsi,
                         'gambar'      => $gambar['file_name'],
                     );
                     $this->M_ekosistem->update_data_solver_people($data, $data_id);
                     $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                     redirect('admin/ekosistem/solver');
                 } else {
                     $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                     redirect('admin/ekosistem/solver');
                 }
             } else {
                 $data = array(
                     'nama'        => $nama,
                     'branding'        => $branding,
                     'rating'        => $rating,
                     'deskripsi' => $deskripsi,
                 );
                 $this->M_ekosistem->update_data_solver_people($data, $data_id);
                 $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                 redirect('admin/ekosistem/solver');
             }
         }
     }
 
     public function solver_delete($id)
     {
         $where = array('id' => $id);
         $this->M_ekosistem->delete_data_solver_people($where, 'tb_solver_people');
         $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
         redirect('admin/ekosistem/solver');
     }
}
