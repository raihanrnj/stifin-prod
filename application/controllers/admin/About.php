<?php
defined('BASEPATH') or exit('No direct script access allowed');

class About extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_about');
        $this->load->library('upload');
        $this->load->helper('text');
        if ($this->session->userdata('status') != "admin_login") {
            redirect(base_url() . 'admin/auth');
        }
    }

    public function index()
    {
        $data['page']             = "yayasan_stifin";
        $data['judul']             = "Konsep STIFIn";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['yayasan_stifin'] = $this->M_about->get_data_about_yayasan();


        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/about/yayasan_stifin', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function yayasan_stifin_add()
    {
        $konten        = $this->input->post('konten');

        $this->form_validation->set_rules('konten', 'konten', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'konten'      => $konten,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_about->insert_data_about_yayasan($data);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/about');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/about');
                }
            }
        }
    }

    public function yayasan_stifin_update()
    {
        $id           = $this->input->post('id');
        $konten        = $this->input->post('konten');

        $this->form_validation->set_rules('konten', 'konten', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'konten'      => $konten,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_about->update_data_about_yayasan($data, $data_id);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/about');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/about');
                }
            } else {
                $data = array(
                    'konten'      => $konten,
                );
                $this->M_about->update_data_about_yayasan($data, $data_id);
                $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                redirect('admin/about');
            }
        }
    }

    public function yayasan_stifin_delete($id)
    {
        $where = array('id' => $id);
        $this->M_about->delete_data_about_yayasan($where, 'tb_yayasan_stifin');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
        redirect('admin/about');
    }

    // STIFIn Institute
    public function stifin_institute()
    {
        $data['page']             = "stifin_institute";
        $data['judul']             = "Konsep STIFIn";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['stifin_institute'] = $this->M_about->get_data_stifin_institute();


        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/about/stifin_institute', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function stifin_institute_add()
    {
        $konten        = $this->input->post('konten');

        $this->form_validation->set_rules('konten', 'konten', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'konten'      => $konten,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_about->insert_data_stifin_institute($data);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/about/stifin_institute');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/about/stifin_institute');
                }
            }
        }
    }

    public function stifin_institute_update()
    {
        $id           = $this->input->post('id');
        $konten        = $this->input->post('konten');

        $this->form_validation->set_rules('konten', 'konten', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'konten'      => $konten,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_about->update_data_stifin_institute($data, $data_id);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/about/stifin_institute');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/about/stifin_institute');
                }
            } else {
                $data = array(
                    'konten'      => $konten,
                );
                $this->M_about->update_data_stifin_institute($data, $data_id);
                $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                redirect('admin/about/stifin_institute');
            }
        }
    }

    public function stifin_institute_delete($id)
    {
        $where = array('id' => $id);
        $this->M_about->delete_data_stifin_institute($where, 'tb_stifin_institute');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
        redirect('admin/about/stifin_institute');
    }

    // STIFIn RQS
    public function stifin_rqs()
    {
        $data['page']             = "stifin_rqs";
        $data['judul']             = "Konsep STIFIn";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['stifin_rqs'] = $this->M_about->get_data_stifin_rqs();


        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/about/stifin_rqs', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function stifin_rqs_add()
    {
        $konten        = $this->input->post('konten');

        $this->form_validation->set_rules('konten', 'konten', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'konten'      => $konten,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_about->insert_data_stifin_rqs($data);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/about/stifin_rqs');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/about/stifin_rqs');
                }
            }
        }
    }

    public function stifin_rqs_update()
    {
        $id           = $this->input->post('id');
        $konten        = $this->input->post('konten');

        $this->form_validation->set_rules('konten', 'konten', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'konten'      => $konten,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_about->update_data_stifin_rqs($data, $data_id);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/about/stifin_rqs');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/about/stifin_rqs');
                }
            } else {
                $data = array(
                    'konten'      => $konten,
                );
                $this->M_about->update_data_stifin_rqs($data, $data_id);
                $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                redirect('admin/about/stifin_rqs');
            }
        }
    }

    public function stifin_rqs_delete($id)
    {
        $where = array('id' => $id);
        $this->M_about->delete_data_stifin_rqs($where, 'tb_stifin_rqs');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
        redirect('admin/about/stifin_rqs');
    }

    // STIFIn Merhandise
    public function merchandise()
    {
        $data['page']             = "merchandise";
        $data['judul']             = "Konsep STIFIn";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['merchandise'] = $this->M_about->get_data_merchandise();


        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/about/merchandise', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function merchandise_add()
    {
        $judul        = $this->input->post('judul');
        $deskripsi        = $this->input->post('deskripsi');

        $this->form_validation->set_rules('judul', 'judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'deskripsi'      => $deskripsi,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_about->insert_data_merchandise($data);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/about/merchandise');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/about/merchandise');
                }
            }
        }
    }

    public function merchandise_update()
    {
        $id           = $this->input->post('id');
        $judul        = $this->input->post('judul');
        $deskripsi        = $this->input->post('deskripsi');

        $this->form_validation->set_rules('judul', 'judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'deskripsi'      => $deskripsi,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_about->update_data_merchandise($data, $data_id);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/about/merchandise');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/about/merchandise');
                }
            } else {
                $data = array(
                    'judul'      => $judul,
                    'deskripsi'      => $deskripsi,
                );
                $this->M_about->update_data_merchandise($data, $data_id);
                $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                redirect('admin/about/merchandise');
            }
        }
    }

    public function merchandise_delete($id)
    {
        $where = array('id' => $id);
        $this->M_about->delete_data_merchandise($where, 'tb_merchandise');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
        redirect('admin/about/merchandise');
    }

    // Stifin Roadmap
    // Workshop
    public function sejarah()
    {
        $data['page']              = "sejarah";
        $data['judul']             = "Download Aplikasi";
        $data['deskripsi']         = "Manage Data CRUD";
        $sejarah       = $this->M_about->get_data_sejarah();

        $data['sejarah'] = $sejarah;
        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/about/stifin_sejarah', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function sejarah_add()
    {
        // $judul                   = $this->input->post('judul');
        $konten                  = $this->input->post('konten');
        // $tanggal_sejarah        = implode(" ", $this->input->post('tanggal_sejarah'));
        // $this->form_validation->set_rules('judul', 'judul', 'trim|required');
        $this->form_validation->set_rules('konten', 'konten', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data = array(
                // 'judul' => $judul,
                'konten' => $konten,
                // 'tanggal_sejarah' => $tanggal_sejarah
            );
            $this->M_about->insert_data_sejarah($data);
            $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di simpan</div>');
            redirect('admin/about/sejarah');
        } else {
            $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal di simpan</div>');
            redirect('admin/about/sejarah');
        }
    }

    public function sejarah_update()
    {
        $id = $this->input->post('id');
        // $judul        = $this->input->post('judul');
        $konten        = $this->input->post('konten');
        // $tanggal_sejarah        = implode(" ", $this->input->post('tanggal_sejarah'));
        // $this->form_validation->set_rules('judul', 'judul', 'trim|required');
        $this->form_validation->set_rules('konten', 'konten', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);
            $data = array(
                // 'judul' => $judul,
                'konten' => $konten,
                // 'tanggal_sejarah' => $tanggal_sejarah
            );
            $this->M_about->update_data_sejarah($data, $data_id);
            $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
            redirect('admin/about/sejarah');
        } else {
            $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal di simpan</div>');
            redirect('admin/about/sejarah');
        }
    }

    public function sejarah_delete($id)
    {
        $where = array('id' => $id);
        $this->M_about->delete_data_sejarah($where, 'tb_sejarah');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di hapus</div>');
        redirect('admin/about/sejarah');
    }
}
