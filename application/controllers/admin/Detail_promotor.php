<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Detail_promotor extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_detail_promotor');
        $this->load->library('upload');
        $this->load->helper('text');
        if ($this->session->userdata('status') != "admin_login") {
            redirect(base_url() . 'admin/auth');
        }
    }

    public function index()
    {
        $data['page']              = "detail_promotor";
        $data['judul']             = "Konsep STIFIn";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['detail_promotor'] = $this->M_detail_promotor->get_data_detail_promotor();


        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/promotor/about_promotor', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function dp_about_add()
    {
        $judul        = $this->input->post('judul');
        $deskripsi    = $this->input->post('deskripsi');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);
            $this->upload->overwrite = true;

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'deskripsi' => $deskripsi,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_detail_promotor->insert_data_detail_promotor($data);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/detail_promotor');
                } else {
                    $this->session->set_flashdata('notif', '        <div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/detail_promotor');
                }
            }
        }
    }

    public function dp_about_update()
    {
        $id           = $this->input->post('id');
        $judul        = $this->input->post('judul');
        $deskripsi    = $this->input->post('deskripsi');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'deskripsi' => $deskripsi,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_detail_promotor->update_data_detail_promotor($data, $data_id);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                    redirect('admin/detail_promotor');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/detail_promotor');
                }
            } else {
                $data = array(
                    'judul'      => $judul,
                    'deskripsi' => $deskripsi,
                );
                $this->M_detail_promotor->update_data_detail_promotor($data, $data_id);
                $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                redirect('admin/detail_promotor');
            }
        }
    }
    public function dp_about_delete($id)
    {
        $where = array('id' => $id);
        $this->M_detail_promotor->delete_data_detail_promotor($where, 'tb_dp_about');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
        redirect('admin/detail_promotor');
    }


    // Benefit
    public function dp_benefit()
    {
        $data['page']              = "dp_benefit";
        $data['judul']             = "Konsep STIFIn";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['dp_benefit'] = $this->M_detail_promotor->get_data_benefit_promotor();


        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/promotor/benefit_promotor', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function dp_benefit_add()
    {
        $judul        = $this->input->post('judul');
        $deskripsi    = $this->input->post('deskripsi');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);
            $this->upload->overwrite = true;

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'deskripsi' => $deskripsi,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_detail_promotor->insert_data_benefit_promotor($data);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/detail_promotor/dp_benefit');
                } else {
                    $this->session->set_flashdata('notif', '        <div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/detail_promotor/dp_benefit');
                }
            }
        }
    }

    public function dp_benefit_update()
    {
        $id           = $this->input->post('id');
        $judul        = $this->input->post('judul');
        $deskripsi    = $this->input->post('deskripsi');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'deskripsi' => $deskripsi,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_detail_promotor->update_data_benefit_promotor($data, $data_id);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                    redirect('admin/detail_promotor/dp_benefit');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/detail_promotor/dp_benefit');
                }
            } else {
                $data = array(
                    'judul'      => $judul,
                    'deskripsi' => $deskripsi,
                );
                $this->M_detail_promotor->update_data_benefit_promotor($data, $data_id);
                $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                redirect('admin/detail_promotor/dp_benefit');
            }
        }
    }
    public function dp_benefit_delete($id)
    {
        $where = array('id' => $id);
        $this->M_detail_promotor->delete_data_benefit_promotor($where, 'tb_dp_benefit');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
        redirect('admin/detail_promotor/dp_benefit');
    }

    public function dp_roadmap()
    {
        $data['page']              = "dp_roadmap";
        $data['judul']             = "Download Aplikasi";
        $data['deskripsi']         = "Manage Data CRUD";
        $dp_roadmap       = $this->M_detail_promotor->get_data_roadmap_promotor();

        $data['dp_roadmap'] = $dp_roadmap;
        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/promotor/roadmap_promotor', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function dp_roadmap_add()
    {
        $konten                  = $this->input->post('konten');
        $this->form_validation->set_rules('konten', 'konten', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data = array(
                'konten' => $konten,
                'waktu'       => date('Y-m-d H:i:s'),
            );
            $this->M_detail_promotor->insert_data_roadmap_promotor($data);
            $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di simpan</div>');
            redirect('admin/detail_promotor/dp_roadmap');
        } else {
            $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal di simpan</div>');
            redirect('admin/detail_promotor/dp_roadmap');
        }
    }

    public function dp_roadmap_update()
    {
        $id = $this->input->post('id');
        $konten        = $this->input->post('konten');
        $this->form_validation->set_rules('konten', 'konten', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);
            $data = array(
                'konten' => $konten,
                'waktu'       => date('Y-m-d H:i:s'),
            );
            $this->M_detail_promotor->update_data_roadmap_promotor($data, $data_id);
            $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
            redirect('admin/detail_promotor/dp_roadmap');
        } else {
            $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal di simpan</div>');
            redirect('admin/detail_promotor/dp_roadmap');
        }
    }

    public function dp_roadmap_delete($id)
    {
        $where = array('id' => $id);
        $this->M_detail_promotor->delete_data_roadmap_promotor($where, 'tb_dp_roadmap');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di hapus</div>');
        redirect('admin/detail_promotor/dp_roadmap');
    }

}
