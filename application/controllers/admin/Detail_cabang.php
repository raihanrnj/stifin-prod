<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Detail_cabang extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_detail_cabang');
        $this->load->library('upload');
        $this->load->helper('text');
        if ($this->session->userdata('status') != "admin_login") {
            redirect(base_url() . 'admin/auth');
        }
    }

    public function index()
    {
        $data['page']             = "detail_cabang";
        $data['judul']             = "Konsep STIFIn";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['detail_cabang'] = $this->M_detail_cabang->get_data_about_detail_cabang();


        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/cabang/about_cabang', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function dc_about_add()
    {
        $judul        = $this->input->post('judul');
        $deskripsi    = $this->input->post('deskripsi');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);
            $this->upload->overwrite = true;

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'deskripsi' => $deskripsi,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_detail_cabang->insert_data_about_detail_cabang($data);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/detail_cabang');
                } else {
                    $this->session->set_flashdata('notif', '        <div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/detail_cabang');
                }
            }
        }
    }

    public function dc_about_update()
    {
        $id           = $this->input->post('id');
        $judul        = $this->input->post('judul');
        $deskripsi    = $this->input->post('deskripsi');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'deskripsi' => $deskripsi,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_detail_cabang->update_data_about_detail_cabang($data, $data_id);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                    redirect('admin/detail_cabang');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/detail_cabang');
                }
            } else {
                $data = array(
                    'judul'      => $judul,
                    'deskripsi' => $deskripsi,
                );
                $this->M_detail_cabang->update_data_about_detail_cabang($data, $data_id);
                $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                redirect('admin/detail_cabang');
            }
        }
    }
    public function dc_about_delete($id)
    {
        $where = array('id' => $id);
        $this->M_detail_cabang->delete_data_about_detail_cabang($where, 'tb_dc_about_cabang');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
        redirect('admin/detail_cabang');
    }


    // Potensi Usaha
    public function dc_potensi_usaha()
    {
        $data['page']             = "potensi_usaha";
        $data['judul']             = "Konsep STIFIn";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['potensi_usaha'] = $this->M_detail_cabang->get_data_potensi_usaha();


        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/cabang/potensi_usaha', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function dc_potensi_usaha_add()
    {
        $judul        = $this->input->post('judul');
        $deskripsi    = $this->input->post('deskripsi');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);
            $this->upload->overwrite = true;

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'deskripsi' => $deskripsi,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_detail_cabang->insert_data_potensi_usaha($data);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/detail_cabang/dc_potensi_usaha');
                } else {
                    $this->session->set_flashdata('notif', '        <div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/detail_cabang/dc_potensi_usaha');
                }
            }
        }
    }

    public function dc_potensi_usaha_update()
    {
        $id           = $this->input->post('id');
        $judul        = $this->input->post('judul');
        $deskripsi    = $this->input->post('deskripsi');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'deskripsi' => $deskripsi,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_detail_cabang->update_data_potensi_usaha($data, $data_id);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                    redirect('admin/detail_cabang/dc_potensi_usaha');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/detail_cabang/dc_potensi_usaha');
                }
            } else {
                $data = array(
                    'judul'      => $judul,
                    'deskripsi' => $deskripsi,
                );
                $this->M_detail_cabang->update_data_potensi_usaha($data, $data_id);
                $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                redirect('admin/detail_cabang/dc_potensi_usaha');
            }
        }
    }
    public function dc_potensi_usaha_delete($id)
    {
        $where = array('id' => $id);
        $this->M_detail_cabang->delete_data_potensi_usaha($where, 'tb_dc_potensi');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
        redirect('admin/detail_cabang/dc_potensi_usaha');
    }


    //  Sumber Profit
    public function dc_sumber_profit()
    {
        $data['page']             = "sumber_profit";
        $data['judul']             = "Konsep STIFIn";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['sumber_profit'] = $this->M_detail_cabang->get_data_sumber_profit();


        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/cabang/sumber_profit', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function dc_sumber_profit_add()
    {
        $icon        = $this->input->post('icon');
        $judul        = $this->input->post('judul');
        $deskripsi    = $this->input->post('deskripsi');

        $this->form_validation->set_rules('icon', 'Icon', 'trim|required');
        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi');

        if ($this->form_validation->run() !== FALSE) {
            $data = array(
                'icon'      => $icon,
                'judul'      => $judul,
                'deskripsi' => $deskripsi,
            );
            $this->M_detail_cabang->insert_data_sumber_profit($data);
            $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
            redirect('admin/detail_cabang/dc_sumber_profit');
        } else {
            $this->session->set_flashdata('notif', '        <div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
            redirect('admin/detail_cabang/dc_sumber_profit');
        }
    }

    public function dc_sumber_profit_update()
    {
        $id           = $this->input->post('id');
        $icon        = $this->input->post('icon');
        $judul        = $this->input->post('judul');
        $deskripsi    = $this->input->post('deskripsi');

        $this->form_validation->set_rules('icon', 'Icon', 'trim|required');
        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'icon'      => $icon,
                        'judul'      => $judul,
                        'deskripsi' => $deskripsi,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_detail_cabang->update_data_sumber_profit($data, $data_id);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                    redirect('admin/detail_cabang/dc_sumber_profit');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/detail_cabang/dc_sumber_profit');
                }
            } else {
                $data = array(
                    'icon'      => $icon,
                    'judul'      => $judul,
                    'deskripsi' => $deskripsi,
                );
                $this->M_detail_cabang->update_data_sumber_profit($data, $data_id);
                $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                redirect('admin/detail_cabang/dc_sumber_profit');
            }
        }
    }
    public function dc_sumber_profit_delete($id)
    {
        $where = array('id' => $id);
        $this->M_detail_cabang->delete_data_sumber_profit($where, 'tb_dc_sumber_profit');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
        redirect('admin/detail_cabang/dc_sumber_profit');
    }

    // Alur Tes STIFIn
    public function dc_alur_tes()
    {
        $data['page']             = "alur_tes";
        $data['judul']             = "Landing Page";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['alur_tes'] = $this->M_detail_cabang->get_data_alur_tes();

        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/cabang/alur_tes', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function dc_alur_tes_add()
    {
        $icon        = $this->input->post('icon');
        $judul        = $this->input->post('judul');
        $deskripsi        = $this->input->post('deskripsi');

        $this->form_validation->set_rules('icon', 'Icon', 'trim|required');
        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data = array(
                'icon'      => $icon,
                'judul'      => $judul,
                'deskripsi' => $deskripsi,
            );
            $this->M_detail_cabang->insert_data_alur_tes($data, $data);
            $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
            redirect('admin/detail_cabang/dc_alur_tes');
        } else {
            $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
            redirect('admin/detail_cabang/dc_alur_tes');
        }
    }

    public function dc_alur_tes_update()
    {
        $id = $this->input->post('id');
        $icon        = $this->input->post('icon');
        $judul        = $this->input->post('judul');
        $deskripsi        = $this->input->post('deskripsi');

        $this->form_validation->set_rules('icon', 'Icon', 'trim|required');
        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);
            $data = array(
                'icon'      => $icon,
                'judul'      => $judul,
                'deskripsi' => $deskripsi,
            );
            $this->M_detail_cabang->update_data_alur_tes($data, $data_id);
            $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
            redirect('admin/detail_cabang/dc_alur_tes');
        } else {
            $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
            redirect('admin/detail_cabang/dc_alur_tes');
        }
    }

    public function dc_alur_tes_delete($id)
    {
        $where = array('id' => $id);
        $this->M_detail_cabang->delete_data_alur_tes($where, 'tb_dc_alur_tes');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
        redirect('admin/detail_cabang/dc_alur_tes');
    }


    // Pilihan Cabang
    public function dc_pilihan_cabang()
    {
        $data['page']              = "pilihan_cabang";
        $data['judul']             = "Landing Page";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['pilihan_cabang'] = $this->M_detail_cabang->get_data_pilihan_cabang();

        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/cabang/pilihan_cabang', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function dc_pilihan_cabang_add()
    {
        $icon        = $this->input->post('icon');
        $judul        = $this->input->post('judul');
        $deskripsi        = $this->input->post('deskripsi');

        $this->form_validation->set_rules('icon', 'Icon', 'trim|required');
        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data = array(
                'icon'      => $icon,
                'judul'      => $judul,
                'deskripsi' => $deskripsi,
            );
            $this->M_detail_cabang->insert_data_pilihan_cabang($data, $data);
            $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
            redirect('admin/detail_cabang/dc_pilihan_cabang');
        } else {
            $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
            redirect('admin/detail_cabang/dc_pilihan_cabang');
        }
    }

    public function dc_pilihan_cabang_update()
    {
        $id = $this->input->post('id');
        $icon        = $this->input->post('icon');
        $judul        = $this->input->post('judul');
        $deskripsi        = $this->input->post('deskripsi');

        $this->form_validation->set_rules('icon', 'Icon', 'trim|required');
        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);
            $data = array(
                'icon'      => $icon,
                'judul'      => $judul,
                'deskripsi' => $deskripsi,
            );
            $this->M_detail_cabang->update_data_pilihan_cabang($data, $data_id);
            $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
            redirect('admin/detail_cabang/dc_pilihan_cabang');
        } else {
            $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
            redirect('admin/detail_cabang/dc_pilihan_cabang');
        }
    }

    public function dc_pilihan_cabang_delete($id)
    {
        $where = array('id' => $id);
        $this->M_detail_cabang->delete_data_pilihan_cabang($where, 'tb_dc_pilihan_cabang');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
        redirect('admin/detail_cabang/dc_pilihan_cabang');
    }

    //  Sumber Pop Up
    public function dc_popup()
    {
        $data['page']             = "popup";
        $data['judul']             = "Konsep STIFIn";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['popup'] = $this->M_detail_cabang->get_data_popup();


        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/cabang/popup', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function dc_popup_add()
    {
        $config['upload_path']   = './assets/upload/images';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size']      = '4048';
        $config['file_name']     = $_FILES['gambar']['name'];

        $this->upload->initialize($config);

        if (!empty($_FILES['gambar']['name'])) {
            if ($this->upload->do_upload('gambar')) {
                $gambar = $this->upload->data();
                date_default_timezone_set("Asia/Jakarta");
                $data = array(
                    'gambar'      => $gambar['file_name'],
                    'waktu'       => date('Y-m-d H:i:s'),
                );
                $this->M_detail_cabang->insert_data_popup($data);
                $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                redirect('admin/detail_cabang/dc_popup');
            } else {
                $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                redirect('admin/detail_cabang/dc_popup');
            }
        }
    }


    public function dc_popup_update()
    {
        $id           = $this->input->post('id');
        $this->form_validation->set_rules('id', 'ID', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '4048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'waktu'       => date('Y-m-d H:i:s'),
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_detail_cabang->update_data_popup($data, $data_id);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                    redirect('admin/detail_cabang/dc_popup');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/detail_cabang/dc_popup');
                }
            }
        }
    }
    public function dc_popup_delete($id)
    {
        $where = array('id' => $id);
        $this->M_detail_cabang->delete_data_popup($where, 'tb_dc_popup');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
        redirect('admin/detail_cabang/dc_popup');
    }
}
