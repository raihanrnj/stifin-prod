<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('M_login');
        $this->load->library('form_validation');
    }

    function index()
    {
        $this->load->view('view_admin/login');
    }
    function login()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $this->form_validation->set_rules('username', 'Username', 'trim|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');
        if ($this->form_validation->run() != false) {
            $where = array(
                'username' => $username,
                'password' => md5($password)
            );
            $data = $this->M_login->edit_data($where, 'tb_admin');
            $d = $this->M_login->edit_data($where, 'tb_admin')->row();
            $cek = $data->num_rows();
            if ($cek > 0) {
                $session = array(
                    'id' => $d->id,
                    'nama' => $d->nama,
                    'status' => 'admin_login'
                );
                $this->session->set_userdata($session);
                redirect('admin/landing');
            } else {
                $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Username atau password salah</div>');
                redirect('admin/auth');
            }
        } else {
            $this->load->view('view_admin/login');
        }
    }


    function logout()
    {
        $this->session->sess_destroy();
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Berhasil Logout</div>');
        redirect('admin/auth');
    }
}
