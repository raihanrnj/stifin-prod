<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Artikel extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_artikel');
        $this->load->library('upload');
        $this->load->helper('text');
        if ($this->session->userdata('status') != "admin_login") {
            redirect(base_url() . 'admin/auth');
        }
    }

    public function index()
    {
        $data['page']             = "artikel";
        $data['judul']             = "Konsep STIFIn";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['artikel'] = $this->M_artikel->get_data_artikel();


        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/artikel/artikel', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function artikel_add()
    {
        $judul        = $this->input->post('judul');
        $slug        = $this->input->post('slug');
        $konten        = $this->input->post('konten');

        $this->form_validation->set_rules('judul', 'judul', 'trim|required');
        $this->form_validation->set_rules('slug', 'slug', 'trim|required');
        $this->form_validation->set_rules('konten', 'konten', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'slug'      => $slug,
                        'konten'      => $konten,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_artikel->insert_data_artikel($data);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/artikel');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/artikel');
                }
            }
        }
    }

    public function artikel_update()
    {
        $id           = $this->input->post('id');
        $judul        = $this->input->post('judul');
        $slug        = $this->input->post('slug');
        $konten        = $this->input->post('konten');

        $this->form_validation->set_rules('judul', 'judul', 'trim|required');
        $this->form_validation->set_rules('slug', 'slug', 'trim|required');
        $this->form_validation->set_rules('konten', 'konten', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'slug'      => $slug,
                        'konten'      => $konten,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_artikel->update_data_artikel($data, $data_id);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/artikel');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/artikel');
                }
            } else {
                $data = array(
                    'judul'      => $judul,
                    'slug'      => $slug,
                    'konten'      => $konten,
                );
                $this->M_artikel->update_data_artikel($data, $data_id);
                $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                redirect('admin/artikel');
            }
        }
    }

    public function artikel_delete($id)
    {
        $where = array('id' => $id);
        $this->M_artikel->delete_data_artikel($where, 'tb_artikel');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
        redirect('admin/artikel');
    }
}
