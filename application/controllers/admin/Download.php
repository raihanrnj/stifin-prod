<?php
defined('BASEPATH') or exit('No direct script access allowed');

function curl_get_file_contents($URL)
    {
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $URL);
        $contents = curl_exec($c);
        curl_close($c);
    
        if ($contents) return $contents;
        else return FALSE;
    }
    
class Download extends CI_Controller
{
    

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_download');
        $this->load->library('upload');
        $this->load->helper('download');
        // if ($this->session->userdata('status') != "admin_login") {
        //     redirect(base_url() . 'admin/auth');
        // }
    }
    


    public function index()
    {
        $data['page']             = "download_aplikasi";
        $data['judul']             = "Download Aplikasi";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['aplikasi'] = $this->M_download->get_data_aplikasi();


        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/download/download_aplikasi', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function aplikasi_add()
    {
        $judul        = $this->input->post('judul');
        $deskripsi    = $this->input->post('deskripsi');
        $icon    = $this->input->post('icon');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('icon', 'Icon', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/upload/file';
            $config['allowed_types'] = 'exe|zip|rar';
            $config['max_size']      = '80000';
            $config['file_name']     = $_FILES['file']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['file']['name'])) {
                if ($this->upload->do_upload('file')) {
                    $file = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'deskripsi' => $deskripsi,
                        'icon' => $icon,
                        'file'      => $file['file_name'],
                    );
                    $this->M_download->insert_data_aplikasi($data);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/download');
                } else {
                    $this->session->set_flashdata('notif', '        <div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/download');
                }
            }
        }
    }

    public function aplikasi_update()
    {
        $id           = $this->input->post('id');
        $judul        = $this->input->post('judul');
        $deskripsi    = $this->input->post('deskripsi');
        $icon    = $this->input->post('icon');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);

            $config['upload_path']   = './assets/upload/file';
            $config['allowed_types'] = 'exe|zip|rar';
            $config['max_size']      = '80000';
            $config['file_name']     = $_FILES['file']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['file']['name'])) {
                if ($this->upload->do_upload('file')) {
                    $file = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'deskripsi' => $deskripsi,
                        'icon' => $icon,
                        'file'      => $file['file_name'],
                    );
                    $this->M_download->update_data_aplikasi($data, $data_id);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                    redirect('admin/download');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/download');
                }
            } else {
                $data = array(
                    'judul'      => $judul,
                    'deskripsi' => $deskripsi,
                );
                $this->M_download->update_data_aplikasi($data, $data_id);
                $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                redirect('admin/download');
            }
        }
    }
    public function aplikasi_delete($id)
    {
        $where = array('id' => $id);
        $this->M_download->delete_data_aplikasi($where, 'tb_download_aplikasi');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
        redirect('admin/download');
    }

    public function aplikasi_download($filename = NULL){
        $data = curl_get_file_contents(base_url('assets/upload/file/'.$filename));
        force_download($filename, $data);
    }





















































    // Video Section
    public function video()
    {
        $data['page']             = "download_video";
        $data['judul']             = "Landing Page";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['video'] = $this->M_download->get_data_video();

        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/download/download_video', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function video_add()
    {
        $link        = $this->input->post('link');

        $this->form_validation->set_rules('link', 'Link', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data = array(
                'link_embed'      => $link
            );
            $this->M_download->insert_data_video($data, $data);
            $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di tambahkan</div>');
            redirect('admin/download/video');
        } else {
            $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
            redirect('admin/download/video');
        }
    }

    public function video_update()
    {
        $id = $this->input->post('id');
        $link        = $this->input->post('link');

        $this->form_validation->set_rules('link', 'Link', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);
            $data = array(
                'link_embed'      => $link
            );
            $this->M_download->update_data_video($data, $data_id);
            $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
            redirect('admin/download/video');
        } else {
            $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
            redirect('admin/download/video');
        }
    }

    public function video_delete($id)
    {
        $where = array('id' => $id);
        $this->M_download->delete_data_video($where, 'tb_download_video');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
        redirect('admin/download/video');
    }


    //download audio
    public function audio()
    {
        $data['page']             = "download_audio";
        $data['judul']             = "Konsep STIFIn";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['audio'] = $this->M_download->get_data_audio();


        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/download/download_audio', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function audio_add()
    {
        $judul        = $this->input->post('judul');

        $this->form_validation->set_rules('judul', 'judul', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/upload/audio';
            $config['allowed_types'] = 'mp3|wav|ogg';
            $config['max_size']      = '15000';
            $config['file_name']     = $_FILES['audio']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['audio']['name'])) {
                if ($this->upload->do_upload('audio')) {
                    $audio = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'audio'      => $audio['file_name'],
                    );
                    $this->M_download->insert_data_audio($data);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/download/audio');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/download/audio');
                }
            }
        }
    }

    public function audio_update()
    {
        $id           = $this->input->post('id');
        $judul        = $this->input->post('judul');

        $this->form_validation->set_rules('judul', 'judul', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '15000';
            $config['file_name']     = $_FILES['audio']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['audio']['name'])) {
                if ($this->upload->do_upload('audio')) {
                    $audio = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'audio'      => $audio['file_name'],
                    );
                    $this->M_download->update_data_audio($data, $data_id);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/download/audio');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/download/audio');
                }
            } else {
                $data = array(
                    'judul'      => $judul,
                );
                $this->M_download->update_data_audio($data, $data_id);
                $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                redirect('admin/download/audio');
            }
        }
    }

    public function audio_delete($id)
    {
        $where = array('id' => $id);
        $this->M_download->delete_data_audio($where, 'tb_download_audio');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
        redirect('admin/download/audio');
    }

    //download ebook
    public function ebook()
    {
        $data['page']             = "download_ebook";
        $data['judul']             = "Konsep STIFIn";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['ebook'] = $this->M_download->get_data_ebook();


        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/download/download_ebook', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function ebook_add()
    {
        $judul        = $this->input->post('judul');
        $deskripsi    = $this->input->post('deskripsi');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/upload/materi';
            $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|ppt|pptx';
            $config['max_size']      = '800000';
            $config['file_name']     = $_FILES['file']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['file']['name'])) {
                if ($this->upload->do_upload('file')) {
                    $file = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'deskripsi' => $deskripsi,
                        'file'      => $file['file_name'],
                    );
                    $this->M_download->insert_data_ebook($data);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/download/ebook');
                } else {
                    $this->session->set_flashdata('notif', '        <div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/download/ebook');
                }
            }
        }
    }

    public function ebook_update()
    {
        $id           = $this->input->post('id');
        $judul        = $this->input->post('judul');
        $deskripsi    = $this->input->post('deskripsi');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);

            $config['upload_path']   = './assets/upload/materi';
            $config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|ppt|pptx';
            $config['max_size']      = '20000';
            $config['file_name']     = $_FILES['file']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['file']['name'])) {
                if ($this->upload->do_upload('file')) {
                    $file = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'deskripsi' => $deskripsi,
                        'file'      => $file['file_name'],
                    );
                    $this->M_download->update_data_ebook($data, $data_id);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                    redirect('admin/download/ebook');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/download/ebook');
                }
            } else {
                $data = array(
                    'judul'      => $judul,
                    'deskripsi' => $deskripsi,
                );
                $this->M_download->update_data_ebook($data, $data_id);
                $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                redirect('admin/download/ebook');
            }
        }
    }
    public function ebook_delete($id)
    {
        $where = array('id' => $id);
        $this->M_download->delete_data_ebook($where, 'tb_download_materi');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
        redirect('admin/download/ebook');
    }
    
  

    public function ebook_download($filename = NULL){
        $data = curl_get_file_contents(base_url('assets/upload/materi/'.$filename));
        force_download($filename, $data);
    }
}
