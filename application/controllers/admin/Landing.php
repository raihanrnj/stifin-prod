<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Landing extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        // load model disini
        $this->load->model('M_landing');
        $this->load->library('upload');
        $this->load->helper('text');
        if ($this->session->userdata('status') != "admin_login") {
            redirect(base_url() . 'admin/auth');
        }
    }

    public function index()
    {
        $data['page']             = "landing";
        $data['judul']             = "Beranda";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['hero_section'] = $this->M_landing->get_data();

        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/home/hero_section', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function hero_section_add()
    {
        $judul        = $this->input->post('judul');
        $deskripsi    = $this->input->post('deskripsi');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);
            $this->upload->overwrite = true;

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'deskripsi' => $deskripsi,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_landing->insert($data);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/landing');
                } else {
                    $this->session->set_flashdata('notif', '        <div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/landing');
                }
            }
        }
    }

    public function hero_section_update()
    {
        $id           = $this->input->post('id');
        $judul        = $this->input->post('judul');
        $deskripsi    = $this->input->post('deskripsi');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'deskripsi' => $deskripsi,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_landing->update($data, $data_id);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                    redirect('admin/landing');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/landing');
                }
            } else {
                $data = array(
                    'judul'      => $judul,
                    'deskripsi' => $deskripsi,
                );
                $this->M_landing->update($data, $data_id);
                $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                redirect('admin/landing');
            }
        }
    }
    public function hero_section_delete($id)
    {
        $where = array('id' => $id);
        $this->M_landing->delete($where, 'tb_hero_section');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
        redirect('admin/landing');
    }


    // About Section
    public function about_section()
    {
        $data['page']             = "about_section";
        $data['judul']             = "Landing Page";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['about_section'] = $this->M_landing->get_data_about();

        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/home/about_section', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function about_section_add()
    {
        $judul        = $this->input->post('judul');
        $deskripsi    = $this->input->post('deskripsi');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'deskripsi' => $deskripsi,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_landing->insert_data_about($data);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/landing/about_section');
                } else {
                    $this->session->set_flashdata('notif', '        <div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/landing/about_section');
                }
            }
        }
    }

    public function about_section_update()
    {
        $id           = $this->input->post('id');
        $judul        = $this->input->post('judul');
        $deskripsi    = $this->input->post('deskripsi');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'deskripsi' => $deskripsi,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_landing->update_data_about($data, $data_id);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                    redirect('admin/landing/about_section');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/landing/about_section');
                }
            } else {
                $data = array(
                    'judul'      => $judul,
                    'deskripsi' => $deskripsi,
                );
                $this->M_landing->update_data_about($data, $data_id);
                $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                redirect('admin/landing/about_section');
            }
        }
    }

    public function about_section_delete($id)
    {
        $where = array('id' => $id);
        $this->M_landing->delete_data_about($where, 'tb_about_us');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
        redirect('admin/landing/about_section');
    }

    
    // Keunggulan Section
    public function keunggulan_section()
    {
        $data['page']             = "keunggulan_section";
        $data['judul']             = "Landing Page";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['keunggulan_section'] = $this->M_landing->get_data_keunggulan();

        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/home/keunggulan_section', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function keunggulan_section_add()
    {
        $judul        = $this->input->post('judul');
        $deskripsi    = $this->input->post('deskripsi');
        $icon    = $this->input->post('icon');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');
        $this->form_validation->set_rules('icon', 'Icon', 'trim|required');

            if ($this->form_validation->run() !== FALSE) {
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'deskripsi' => $deskripsi,
                        'icon'      => $icon,
                    );
                    $this->M_landing->insert_data_keunggulan($data);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/landing/keunggulan_section');
                } else {
                    $this->session->set_flashdata('notif', '        <div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/landing/keunggulan_section');

        }
    }

    public function keunggulan_section_update()
    {
        $id           = $this->input->post('id');
        $judul        = $this->input->post('judul');
        $deskripsi    = $this->input->post('deskripsi');
        $icon    = $this->input->post('icon');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');
        $this->form_validation->set_rules('icon', 'Icon', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            date_default_timezone_set("Asia/Jakarta");
            $data = array(
                'judul'      => $judul,
                'deskripsi' => $deskripsi,
                'icon'      => $icon,
            );

            $data_id = array('id' => $id);
            $this->M_landing->update_data_keunggulan($data,$data_id);
            $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
            redirect('admin/landing/keunggulan_section');
        } else {
            $this->session->set_flashdata('notif', '        <div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
            redirect('admin/landing/keunggulan_section');
        }
    }

    public function keunggulan_section_delete($id)
    {
        $where = array('id' => $id);
        $this->M_landing->delete_data_keunggulan($where, 'tb_keunggulan');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
        redirect('admin/landing/keunggulan_section');
    }

    // Video Section
    public function video_section()
    {
        $data['page']             = "video_section";
        $data['judul']             = "Landing Page";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['video_section'] = $this->M_landing->get_data_video();

        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/home/video_section', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function video_section_add()
    {
        $link        = $this->input->post('link');

        $this->form_validation->set_rules('link', 'Link', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data = array(
                'link_embed'      => $link
            );
            $this->M_landing->insert_data_video($data, $data);
            $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
            redirect('admin/landing/video_section');
        } else {
            $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
            redirect('admin/landing/video_section');
        }
    }

    public function video_section_update()
    {
        $id = $this->input->post('id');
        $link        = $this->input->post('link');

        $this->form_validation->set_rules('link', 'Link', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);
            $data = array(
                'link_embed'      => $link
            );
            $this->M_landing->update_data_video($data, $data_id);
            $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
            redirect('admin/landing/video_section');
        } else {
            $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
            redirect('admin/landing/video_section');
        }
    }

    public function video_section_delete($id)
    {
        $where = array('id' => $id);
        $this->M_landing->delete_data_video($where, 'tb_video');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
        redirect('admin/landing/video_section');
    }

    // Testimoni Section
    public function testimoni_section()
    {
        $data['page']             = "testimoni_section";
        $data['judul']             = "Landing Page";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['testimoni_section'] = $this->M_landing->get_data_testimoni();

        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/home/testimoni_section', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function testimoni_section_add()
    {
        $nama        = $this->input->post('nama');
        $pekerjaan    = $this->input->post('pekerjaan');
        $testimoni    = $this->input->post('testimoni');

        $this->form_validation->set_rules('nama', 'Nama', 'trim|required');
        $this->form_validation->set_rules('pekerjaan', 'Pekerjaan', 'trim|required');
        $this->form_validation->set_rules('testimoni', 'Testimoni', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'nama'      => $nama,
                        'pekerjaan' => $pekerjaan,
                        'testimoni' => $testimoni,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_landing->insert_data_testimoni($data);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/landing/testimoni_section');
                } else {
                    $this->session->set_flashdata('notif', '        <div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/landing/testimoni_section');
                }
            }
        }
    }

    public function testimoni_section_update()
    {
        $id           = $this->input->post('id');
        $nama        = $this->input->post('nama');
        $pekerjaan    = $this->input->post('pekerjaan');
        $testimoni    = $this->input->post('testimoni');

        $this->form_validation->set_rules('nama', 'Nama', 'trim|required');
        $this->form_validation->set_rules('pekerjaan', 'Pekerjaan', 'trim|required');
        $this->form_validation->set_rules('testimoni', 'Testimoni', 'trim|required');


        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'nama'      => $nama,
                        'pekerjaan' => $pekerjaan,
                        'testimoni' => $testimoni,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_landing->update_data_testimoni($data, $data_id);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                    redirect('admin/landing/testimoni_section');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/landing/testimoni_section');
                }
            } else {
                $data = array(
                    'nama'      => $nama,
                    'pekerjaan' => $pekerjaan,
                    'testimoni' => $testimoni
                );
                $this->M_landing->update_data_testimoni($data, $data_id);
                $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                redirect('admin/landing/testimoni_section');
            }
        }
    }

    public function testimoni_section_delete($id)
    {
        $where = array('id' => $id);
        $this->M_landing->delete_data_testimoni($where, 'tb_testimonial');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
        redirect('admin/landing/testimoni_section');
    }


     // Slider Section
     public function slider_section()
     {
         $data['page']             = "slider_section";
         $data['judul']             = "Landing Page";
         $data['deskripsi']         = "Manage Data CRUD";
         $data['slider_section'] = $this->M_landing->get_data_slider();
 
         $this->load->view('view_admin/templates/header', $data);
         $this->load->view('view_admin/templates/asside', $data);
         $this->load->view('view_admin/home/slider_section', $data);
         $this->load->view('view_admin/templates/footer', $data);
     }
 
     public function slider_section_add()
     {
         $teks        = $this->input->post('teks');
 
         $this->form_validation->set_rules('teks', 'Teks', 'trim|required');
 
         if ($this->form_validation->run() !== FALSE) {
 
             $config['upload_path']   = './assets/upload/images';
             $config['allowed_types'] = 'jpg|png|jpeg';
             $config['max_size']      = '8048';
             $config['file_name']     = $_FILES['gambar']['name'];
 
             $this->upload->initialize($config);
 
             if (!empty($_FILES['gambar']['name'])) {
                 if ($this->upload->do_upload('gambar')) {
                     $gambar = $this->upload->data();
                     date_default_timezone_set("Asia/Jakarta");
                     $data = array(
                         'teks'      => $teks,
                         'gambar'      => $gambar['file_name'],
                     );
                     $this->M_landing->insert_data_slider($data);
                     $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                     redirect('admin/landing/slider_section');
                 } else {
                     $this->session->set_flashdata('notif', '        <div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                     redirect('admin/landing/slider_section');
                 }
             }
         }
     }
 
     public function slider_section_update()
     {
         $id           = $this->input->post('id');
         $teks        = $this->input->post('teks');
 
         $this->form_validation->set_rules('teks', 'Teks', 'trim|required');
 
         if ($this->form_validation->run() !== FALSE) {
             $data_id = array('id' => $id);
 
             $config['upload_path']   = './assets/upload/images';
             $config['allowed_types'] = 'jpg|png|jpeg';
             $config['max_size']      = '8048';
             $config['file_name']     = $_FILES['gambar']['name'];
 
             $this->upload->initialize($config);
 
             if (!empty($_FILES['gambar']['name'])) {
                 if ($this->upload->do_upload('gambar')) {
                     $gambar = $this->upload->data();
                     date_default_timezone_set("Asia/Jakarta");
                     $data = array(
                         'teks'      => $teks,
                         'gambar'      => $gambar['file_name'],
                     );
                     $this->M_landing->update_data_slider($data, $data_id);
                     $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                     redirect('admin/landing/slider_section');
                 } else {
                     $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                     redirect('admin/landing/slider_section');
                 }
             } else {
                 $data = array(
                     'teks'      => $teks,
                 );
                 $this->M_landing->update_data_slider($data, $data_id);
                 $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                 redirect('admin/landing/slider_section');
             }
         }
     }
 
     public function slider_section_delete($id)
     {
         $where = array('id' => $id);
         $this->M_landing->delete_data_slider($where, 'tb_slider');
         $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
         redirect('admin/landing/slider_section');
     }

     
    // Footer Section
    public function footer_section()
    {
        $data['page']             = "footer_section";
        $data['judul']             = "Landing Page";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['footer_section'] = $this->M_landing->get_data_footer();

        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/home/footer_section', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function footer_section_add()
    {
        $tagline        = $this->input->post('tagline');
        $alamat    = $this->input->post('alamat');
        $telp        = $this->input->post('telp');
        $email    = $this->input->post('email');
        $tw        = $this->input->post('tw');
        $fb    = $this->input->post('fb');
        $yt        = $this->input->post('yt');
        $lk    = $this->input->post('lk');
        $ig        = $this->input->post('ig');
        

        $this->form_validation->set_rules('tagline', 'Tagline', 'trim|required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');
        $this->form_validation->set_rules('telp', 'Telp', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required');
        $this->form_validation->set_rules('tw', 'Tw', 'trim|required');
        $this->form_validation->set_rules('fb', 'Fb', 'trim|required');
        $this->form_validation->set_rules('yt', 'Yt', 'trim|required');
        $this->form_validation->set_rules('lk', 'Lk', 'trim|required');
        $this->form_validation->set_rules('ig', 'Ig', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            date_default_timezone_set("Asia/Jakarta");
            $data = array(
                'tagline'      => $tagline,
                'alamat' => $alamat,
                'telp'      => $telp,
                'email'      => $email,
                'tw'      => $tw,
                'fb'      => $fb,
                'yt'      => $yt,
                'lk'      => $lk,
                'ig'      => $ig,
            );
            $this->M_landing->insert_data_footer($data);
            $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
            redirect('admin/landing/footer_section');
        } else {
            $this->session->set_flashdata('notif', '        <div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
            redirect('admin/landing/footer_section');

}
    }

    public function footer_section_update()
    {
        $id           = $this->input->post('id');
        $tagline        = $this->input->post('tagline');
        $alamat    = $this->input->post('alamat');
        $telp        = $this->input->post('telp');
        $email    = $this->input->post('email');
        $tw        = $this->input->post('tw');
        $fb    = $this->input->post('fb');
        $yt        = $this->input->post('yt');
        $lk    = $this->input->post('lk');
        $ig        = $this->input->post('ig');
        

        $this->form_validation->set_rules('tagline', 'Tagline', 'trim|required');
        $this->form_validation->set_rules('alamat', 'Alamat', 'trim|required');
        $this->form_validation->set_rules('telp', 'Telp', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required');
        $this->form_validation->set_rules('tw', 'Tw', 'trim|required');
        $this->form_validation->set_rules('fb', 'Fb', 'trim|required');
        $this->form_validation->set_rules('yt', 'Yt', 'trim|required');
        $this->form_validation->set_rules('lk', 'Lk', 'trim|required');
        $this->form_validation->set_rules('ig', 'Ig', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            
            date_default_timezone_set("Asia/Jakarta");
            $data = array(
                'tagline'      => $tagline,
                'alamat' => $alamat,
                'telp'      => $telp,
                'email'      => $email,
                'tw'      => $tw,
                'fb'      => $fb,
                'yt'      => $yt,
                'lk'      => $lk,
                'ig'      => $ig
            );
            $data_id = array('id' => $id);

            $this->M_landing->update_data_footer($data,$data_id);
            $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
            redirect('admin/landing/footer_section');
        } else {
            $this->session->set_flashdata('notif', '        <div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
            redirect('admin/landing/footer_section');


        }
    }

    public function footer_section_delete($id)
    {
        $where = array('id' => $id);
        $this->M_landing->delete_data_footer($where, 'tb_footer');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
        redirect('admin/landing/footer_section');
    }

    public function partners()
    {
        $data['page']             = "partners";
        $data['judul']             = "Konsep STIFIn";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['partners'] = $this->M_landing->get_data_partners();


        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/partners/partners', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function partners_add()
    {
        $nama        = $this->input->post('nama');

        $this->form_validation->set_rules('nama', 'nama', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['logo']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['logo']['name'])) {
                if ($this->upload->do_upload('logo')) {
                    $logo = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'nama'      => $nama,
                        'logo'      => $logo['file_name'],
                    );
                    $this->M_landing->insert_data_partners($data);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/landing/partners');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/landing/partners');
                }
            }
        }
    }

    public function partners_update()
    {
        $id           = $this->input->post('id');
        $nama        = $this->input->post('nama');

        $this->form_validation->set_rules('nama', 'nama', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['logo']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['logo']['name'])) {
                if ($this->upload->do_upload('logo')) {
                    $logo = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'nama'      => $nama,
                        'logo'      => $logo['file_name'],
                    );
                    $this->M_landing->update_data_partners($data, $data_id);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/landing/partners');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/landing/partners');
                }
            } else {
                $data = array(
                    'nama'      => $nama,
                );
                $this->M_landing->update_data_partners($data, $data_id);
                $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                redirect('admin/landing/partners');
            }
        }
    }

    public function partners_delete($id)
    {
        $where = array('id' => $id);
        $this->M_landing->delete_data_partners($where, 'tb_partners');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
        redirect('admin/landing/partners');
    }

    // Cabang Section
    public function cabang_section()
    {
        $data['page']             = "cabang_section";
        $data['judul']             = "Landing Page";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['cabang_section'] = $this->M_landing->get_data_cabang();

        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/home/cabang_section', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function cabang_section_add()
    {
        $judul        = $this->input->post('judul');
        $deskripsi    = $this->input->post('deskripsi');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'deskripsi' => $deskripsi,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_landing->insert_data_cabang($data);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/landing/cabang_section');
                } else {
                    $this->session->set_flashdata('notif', '        <div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/landing/cabang_section');
                }
            }
        }
    }

    public function cabang_section_update()
    {
        $id           = $this->input->post('id');
        $judul        = $this->input->post('judul');
        $deskripsi    = $this->input->post('deskripsi');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'deskripsi' => $deskripsi,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_landing->update_data_cabang($data, $data_id);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                    redirect('admin/landing/cabang_section');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/landing/cabang_section');
                }
            } else {
                $data = array(
                    'judul'      => $judul,
                    'deskripsi' => $deskripsi,
                );
                $this->M_landing->update_data_cabang($data, $data_id);
                $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                redirect('admin/landing/cabang_section');
            }
        }
    }

    public function cabang_section_delete($id)
    {
        $where = array('id' => $id);
        $this->M_landing->delete_data_cabang($where, 'tb_cabang_section');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
        redirect('admin/landing/cabang_section');
    }


    // Cabang Section
    public function promotor_section()
    {
        $data['page']             = "promotor_section";
        $data['judul']             = "Landing Page";
        $data['deskripsi']         = "Manage Data CRUD";
        $data['promotor_section'] = $this->M_landing->get_data_promotor();

        $this->load->view('view_admin/templates/header', $data);
        $this->load->view('view_admin/templates/asside', $data);
        $this->load->view('view_admin/home/promotor_section', $data);
        $this->load->view('view_admin/templates/footer', $data);
    }

    public function promotor_section_add()
    {
        $judul        = $this->input->post('judul');
        $deskripsi    = $this->input->post('deskripsi');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'deskripsi' => $deskripsi,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_landing->insert_data_promotor($data);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil disimpan</div>');
                    redirect('admin/landing/promotor_section');
                } else {
                    $this->session->set_flashdata('notif', '        <div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/landing/promotor_section');
                }
            }
        }
    }

    public function promotor_section_update()
    {
        $id           = $this->input->post('id');
        $judul        = $this->input->post('judul');
        $deskripsi    = $this->input->post('deskripsi');

        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskripsi', 'trim|required');

        if ($this->form_validation->run() !== FALSE) {
            $data_id = array('id' => $id);

            $config['upload_path']   = './assets/upload/images';
            $config['allowed_types'] = 'jpg|png|jpeg';
            $config['max_size']      = '1048';
            $config['file_name']     = $_FILES['gambar']['name'];

            $this->upload->initialize($config);

            if (!empty($_FILES['gambar']['name'])) {
                if ($this->upload->do_upload('gambar')) {
                    $gambar = $this->upload->data();
                    date_default_timezone_set("Asia/Jakarta");
                    $data = array(
                        'judul'      => $judul,
                        'deskripsi' => $deskripsi,
                        'gambar'      => $gambar['file_name'],
                    );
                    $this->M_landing->update_data_promotor($data, $data_id);
                    $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                    redirect('admin/landing/promotor_section');
                } else {
                    $this->session->set_flashdata('notif', '<div class="alert alert-danger alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-warning"></i> Data Gagal disimpan</div>');
                    redirect('admin/landing/promotor_section');
                }
            } else {
                $data = array(
                    'judul'      => $judul,
                    'deskripsi' => $deskripsi,
                );
                $this->M_landing->update_data_promotor($data, $data_id);
                $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil di edit</div>');
                redirect('admin/landing/promotor_section');
            }
        }
    }

    public function promotor_section_delete($id)
    {
        $where = array('id' => $id);
        $this->M_landing->delete_data_promotor($where, 'tb_promotor_section');
        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button><i class="icon fa fa-check"></i> Data berhasil dihapus</div>');
        redirect('admin/landing/promotor_section');
    }
}
