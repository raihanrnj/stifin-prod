<?php
defined('BASEPATH') or exit('No direct script access allowed');

class U_home extends CI_Controller
{

	public function __construct()
	{
		parent::__construct();
		// $this->load->model('M_user_welcome');
		// $this->load->model('M_berita');
	}

	public function index()
	{
		$data['page'] = "Landing Page";
		$this->load->view('view_templates/header');
		$this->load->view('view_user/landing');
		$this->load->view('view_templates/footer');
	}
}
