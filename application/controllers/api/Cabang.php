<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Cabang extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('M_ekosistem');
  }

  public function index()
  {
    $data = $this->M_ekosistem->get_data_cabang();
    $data_result = [];

    foreach ($data as $record) {
      $data_result[$record['provinsi']]['provinsi'] = $record['provinsi'];
      $data_result[$record['provinsi']]['data'][] = $record;
    }
    $data_result = array_values($data_result);

    //result data
    $result['data'] = $data_result;

    echo json_encode($result);
  }
}
