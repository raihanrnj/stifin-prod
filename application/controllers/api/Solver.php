<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Solver extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('M_ekosistem');
  }

  public function index()
  {
    //default
    $page = 1;
    $limit = 6;

    //costumize value by params
    if (isset($_GET['page'])) {
      $page = $_GET['page'];
    }
    if (isset($_GET['limit'])) {
      $limit = $_GET['limit'];
    }

    $start = ($page - 1) * $limit;

    $total_data = $this->M_ekosistem->get_data_roadmap_solver_allcount();
    $data       = $this->M_ekosistem->get_data_roadmap_solver_api($limit, $start);
    $data_result = [];

    foreach ($data as $i => $record) {
      if (!is_null($data[$i]['waktu'])) {
        $data[$i]['waktu'] = $data[$i]['waktu'];
      } else {
        $data[$i]['waktu'] = "";
      }
    }

    $record['waktu'] = $data;
    //result data
    $result['data'] = $data;
    $result['total'] = $total_data;
    $result['page'] = $page;
    $result['pageCount'] = ceil($total_data / $limit);

    echo json_encode($result);
  }
}
