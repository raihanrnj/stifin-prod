<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Sejarah extends CI_Controller
{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('M_about');
  }

  public function index()
  {
    //default
    $page = 1;
    $limit = 6;

    //costumize value by params
    if (isset($_GET['page'])) {
      $page = $_GET['page'];
    }
    if (isset($_GET['limit'])) {
      $limit = $_GET['limit'];
    }

    $start = ($page - 1) * $limit;

    $total_data = $this->M_about->get_data_sejarah_allcount();
    $data       = $this->M_about->get_data_sejarah_api($limit, $start);
    $data_result = [];

    foreach ($data as $i => $record) {
      if (!is_null($data[$i]['tanggal_sejarah'])) {
        $data[$i]['tanggal_sejarah'] = date_format(date_create($data[$i]['tanggal_sejarah']), "d F Y, H:i");
      } else {
        $data[$i]['tanggal_sejarah'] = "";
      }
    }

    $record['tanggal_sejarah'] = $data;
    //result data
    $result['data'] = $data;
    $result['total'] = $total_data;
    $result['page'] = $page;
    $result['pageCount'] = ceil($total_data / $limit);

    echo json_encode($result);
  }
}
