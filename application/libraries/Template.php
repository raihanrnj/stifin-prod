<?php
	class Template {
		protected $_ci;

		function __construct() {
			$this->_ci = &get_instance(); //Untuk Memanggil function load, dll dari CI. ex: $this->load, $this->model, dll
		}

		function views($template = NULL, $data = NULL) {
			if ($template != NULL) {
				$data['header'] 					= $this->_ci->load->view('view_admin/templates/header', $data, TRUE);
				$data['asside'] 					= $this->_ci->load->view('view_admin/templates/asside', $data, TRUE);
				$data['footer'] 					= $this->_ci->load->view('view_admin/templates/footer', $data, TRUE);
			}
		}
	}
?>