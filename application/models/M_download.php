<?php

class M_download extends CI_Model
{

    // Aplikasi
    function get_data_aplikasi()
    {
        $query = $this->db->get('tb_download_aplikasi');
        return $query->result_array();
    }

    public function insert_data_aplikasi($data)
    {
        $this->db->insert('tb_download_aplikasi', $data);
        return TRUE;
    }

    public function update_data_aplikasi($data, $data_id)
    {
        $this->db->update('tb_download_aplikasi', $data, $data_id);
        return TRUE;
    }

    function delete_data_aplikasi($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    // Video
    function get_data_video()
    {
        $query = $this->db->get('tb_download_video');
        return $query->result_array();
    }

    public function insert_data_video($data)
    {
        $this->db->insert('tb_download_video', $data);
        return TRUE;
    }

    public function update_data_video($data, $data_id)
    {
        $this->db->update('tb_download_video', $data, $data_id);
        return TRUE;
    }

    function delete_data_video($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    // Audio
    function get_data_audio()
    {
        $query = $this->db->get('tb_download_audio');
        return $query->result_array();
    }

    public function insert_data_audio($data)
    {
        $this->db->insert('tb_download_audio', $data);
        return TRUE;
    }

    public function update_data_audio($data, $data_id)
    {
        $this->db->update('tb_download_audio', $data, $data_id);
        return TRUE;
    }

    function delete_data_audio($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    // Ebook
    function get_data_ebook()
    {
        $query = $this->db->get('tb_download_materi');
        return $query->result_array();
    }

    public function insert_data_ebook($data)
    {
        $this->db->insert('tb_download_materi', $data);
        return TRUE;
    }

    public function update_data_ebook($data, $data_id)
    {
        $this->db->update('tb_download_materi', $data, $data_id);
        return TRUE;
    }

    function delete_data_ebook($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }
}
