<?php

class M_about extends CI_Model
{

    function get_data_about_yayasan()
    {
        $query = $this->db->get('tb_yayasan_stifin');
        return $query->result_array();
    }

    public function insert_data_about_yayasan($data)
    {
        $this->db->insert('tb_yayasan_stifin', $data);
        return TRUE;
    }

    public function update_data_about_yayasan($data, $data_id)
    {
        $this->db->update('tb_yayasan_stifin', $data, $data_id);
        return TRUE;
    }

    function delete_data_about_yayasan($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    // STIFIn Institute
    function get_data_stifin_institute()
    {
        $query = $this->db->get('tb_stifin_institute');
        return $query->result_array();
    }

    public function insert_data_stifin_institute($data)
    {
        $this->db->insert('tb_stifin_institute', $data);
        return TRUE;
    }

    public function update_data_stifin_institute($data, $data_id)
    {
        $this->db->update('tb_stifin_institute', $data, $data_id);
        return TRUE;
    }

    function delete_data_stifin_institute($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    // STIFIn RQS
    function get_data_stifin_rqs()
    {
        $query = $this->db->get('tb_stifin_rqs');
        return $query->result_array();
    }

    public function insert_data_stifin_rqs($data)
    {
        $this->db->insert('tb_stifin_rqs', $data);
        return TRUE;
    }

    public function update_data_stifin_rqs($data, $data_id)
    {
        $this->db->update('tb_stifin_rqs', $data, $data_id);
        return TRUE;
    }

    function delete_data_stifin_rqs($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

     // STIFIn RQS
     function get_data_merchandise()
     {
         $query = $this->db->get('tb_merchandise');
         return $query->result_array();
     }
 
     public function insert_data_merchandise($data)
     {
         $this->db->insert('tb_merchandise', $data);
         return TRUE;
     }
 
     public function update_data_merchandise($data, $data_id)
     {
         $this->db->update('tb_merchandise', $data, $data_id);
         return TRUE;
     }
 
     function delete_data_merchandise($where, $table)
     {
         $this->db->where($where);
         $this->db->delete($table);
     }

     //roadmap
    function get_data_sejarah()
    {
        $query = $this->db->get('tb_sejarah');
        return $query->result_array();
    }

    public function insert_data_sejarah($data)
    {
        $this->db->insert('tb_sejarah', $data);
        return TRUE;
    }

    public function update_data_sejarah($data, $data_id)
    {
        $this->db->update('tb_sejarah', $data, $data_id);
        return TRUE;
    }

    function delete_data_sejarah($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    function get_data_sejarah_api($limit = 10, $start = 0)
    {
        $this->db->limit($limit, $start);
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get('tb_sejarah');
        return $query->result_array();
    }
    function get_data_sejarah_allcount()
    {
        return $this->db->count_all('tb_sejarah');
    }
    function get_data_sejarah_allcount_result()
    {
        return $this->db->count_all_results('tb_sejarah');
    }
 
}
