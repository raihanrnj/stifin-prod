<?php

class M_landing extends CI_Model
{

    public $table = 'tb_hero_section';
    public $id = 'id';


    public function insert($data)
    {
        $this->db->insert('tb_hero_section', $data);
        return TRUE;
    }

    function get_data()
    {
        $query = $this->db->get('tb_hero_section');
        return $query->result_array();
    }

    public function update($data, $data_id)
    {
        $this->db->update('tb_hero_section', $data, $data_id);
        return TRUE;
    }

    function delete($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    // about section
    function get_data_about()
    {
        $query = $this->db->get('tb_about_us');
        return $query->result_array();
    }

    public function insert_data_about($data)
    {
        $this->db->insert('tb_about_us', $data);
        return TRUE;
    }

    public function update_data_about($data, $data_id)
    {
        $this->db->update('tb_about_us', $data, $data_id);
        return TRUE;
    }
    function delete_data_about($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    //keunggulan
    function get_data_keunggulan()
    {
        $query = $this->db->get('tb_keunggulan');
        return $query->result_array();
    }
    public function insert_data_keunggulan($data)
    {
        $this->db->insert('tb_keunggulan', $data);
        return TRUE;
    }

    public function update_data_keunggulan($data, $data_id)
    {
        $this->db->update('tb_keunggulan', $data, $data_id);
        return TRUE;
    }

    function delete_data_keunggulan($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    // Video section
    function get_data_video()
    {
        $query = $this->db->get('tb_video');
        return $query->result_array();
    }

    public function insert_data_video($data)
    {
        $this->db->insert('tb_video', $data);
        return TRUE;
    }

    public function update_data_video($data, $data_id)
    {
        $this->db->update('tb_video', $data, $data_id);
        return TRUE;
    }

    function delete_data_video($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    // Testimoni section
    function get_data_testimoni()
    {
        $query = $this->db->get('tb_testimonial');
        return $query->result_array();
    }

    public function insert_data_testimoni($data)
    {
        $this->db->insert('tb_testimonial', $data);
        return TRUE;
    }

    public function update_data_testimoni($data, $data_id)
    {
        $this->db->update('tb_testimonial', $data, $data_id);
        return TRUE;
    }

    function delete_data_testimoni($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

     // Slider section
     function get_data_slider()
     {
         $query = $this->db->get('tb_slider');
         return $query->result_array();
     }
 
     public function insert_data_slider($data)
     {
         $this->db->insert('tb_slider', $data);
         return TRUE;
     }
 
     public function update_data_slider($data, $data_id)
     {
         $this->db->update('tb_slider', $data, $data_id);
         return TRUE;
     }
 
     function delete_data_slider($where, $table)
     {
         $this->db->where($where);
         $this->db->delete($table);
     }

       // footer section
    function get_data_footer()
    {
        $query = $this->db->get('tb_footer');
        return $query->result_array();
    }

    public function insert_data_footer($data)
    {
        $this->db->insert('tb_footer', $data);
        return TRUE;
    }

    public function update_data_footer($data, $data_id)
    {
        $this->db->update('tb_footer', $data, $data_id);
        return TRUE;
    }
    function delete_data_footer($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    // partners
    function get_data_partners()
    {
        $query = $this->db->get('tb_partners');
        return $query->result_array();
    }

    public function insert_data_partners($data)
    {
        $this->db->insert('tb_partners', $data);
        return TRUE;
    }

    public function update_data_partners($data, $data_id)
    {
        $this->db->update('tb_partners', $data, $data_id);
        return TRUE;
    }

    function delete_data_partners($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    //cabang section
    function get_data_cabang()
    {
        $query = $this->db->get('tb_cabang_section');
        return $query->result_array();
    }

    public function insert_data_cabang($data)
    {
        $this->db->insert('tb_cabang_section', $data);
        return TRUE;
    }

    public function update_data_cabang($data, $data_id)
    {
        $this->db->update('tb_cabang_section', $data, $data_id);
        return TRUE;
    }
    function delete_data_cabang($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    //promotor section
    function get_data_promotor()
    {
        $query = $this->db->get('tb_promotor_section');
        return $query->result_array();
    }

    public function insert_data_promotor($data)
    {
        $this->db->insert('tb_promotor_section', $data);
        return TRUE;
    }

    public function update_data_promotor($data, $data_id)
    {
        $this->db->update('tb_promotor_section', $data, $data_id);
        return TRUE;
    }
    function delete_data_promotor($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }
}
