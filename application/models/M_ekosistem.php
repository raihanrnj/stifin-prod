<?php

class M_ekosistem extends CI_Model
{
    function get_data_cabang()
    {
        $query = $this->db->get('tb_cabang');
        return $query->result_array();
    }

    function get_data_provinsi()
    {
        $query = $this->db->select('*')->from('tb_provinsi')->get();
        return $query->result();
    }

    public function insert_data_cabang($data)
    {
        $this->db->insert('tb_cabang', $data);
        return TRUE;
    }

    public function update_data_cabang($data, $data_id)
    {
        $this->db->update('tb_cabang', $data, $data_id);
        return TRUE;
    }

    function delete_data_cabang($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    // Workshop
    function get_data_workshop()
    {
        $query = $this->db->get('tb_workshop');
        return $query->result_array();
    }

    public function insert_data_workshop($data)
    {
        $this->db->insert('tb_workshop', $data);
        return TRUE;
    }

    public function update_data_workshop($data, $data_id)
    {
        $this->db->update('tb_workshop', $data, $data_id);
        return TRUE;
    }

    function delete_data_workshop($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    function get_data_workshop_api($limit = 10, $start = 0)
    {
        $this->db->limit($limit, $start);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('tb_workshop');
        return $query->result_array();
    }
    function get_data_workshop_allcount()
    {
        return $this->db->count_all('tb_workshop');
    }
    function get_data_workshop_allcount_result()
    {
        return $this->db->count_all_results('tb_workshop');
    }

    // Jenjang
    function get_data_jenjang()
    {
        $query = $this->db->get('tb_jenjang');
        return $query->result_array();
    }

    public function insert_data_jenjang($data)
    {
        $this->db->insert('tb_jenjang', $data);
        return TRUE;
    }

    public function update_data_jenjang($data, $data_id)
    {
        $this->db->update('tb_jenjang', $data, $data_id);
        return TRUE;
    }

    function delete_data_jenjang($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    function get_data_jenjang_api($limit = 10, $start = 0)
    {
        $this->db->limit($limit, $start);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('tb_jenjang');
        return $query->result_array();
    }

    function get_data_jenjang_allcount()
    {
        return $this->db->count_all('tb_jenjang');
    }

    function get_data_jenjang_allcount_result()
    {
        return $this->db->count_all_results('tb_jenjang');
    }

    // about promotor section
    function get_data_about_promotor()
    {
        $query = $this->db->get('tb_about_promotor');
        return $query->result_array();
    }

    public function insert_data_about_promotor($data)
    {
        $this->db->insert('tb_about_promotor', $data);
        return TRUE;
    }

    public function update_data_about_promotor($data, $data_id)
    {
        $this->db->update('tb_about_promotor', $data, $data_id);
        return TRUE;
    }
    function delete_data_about_promotor($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    // get about trainer stifin
    function get_data_trainer()
    {
        $query = $this->db->get('tb_trainer_about');
        return $query->result_array();
    }

    public function insert_data_trainer($data)
    {
        $this->db->insert('tb_trainer_about', $data);
        return TRUE;
    }

    public function update_data_trainer($data, $data_id)
    {
        $this->db->update('tb_trainer_about', $data, $data_id);
        return TRUE;
    }

    function delete_data_trainer($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    // Roadmap Trainer
    function get_data_roadmap_trainer()
    {
        $query = $this->db->get('tb_trainer_roadmap');
        return $query->result_array();
    }

    public function insert_data_roadmap_trainer($data)
    {
        $this->db->insert('tb_trainer_roadmap', $data);
        return TRUE;
    }

    public function update_data_roadmap_trainer($data, $data_id)
    {
        $this->db->update('tb_trainer_roadmap', $data, $data_id);
        return TRUE;
    }

    function delete_data_roadmap_trainer($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    function get_data_roadmap_trainer_api($limit = 10, $start = 0)
    {
        $this->db->limit($limit, $start);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('tb_trainer_roadmap');
        return $query->result_array();
    }
    function get_data_roadmap_trainer_allcount()
    {
        return $this->db->count_all('tb_trainer_roadmap');
    }
    function get_data_roadmap_trainer_allcount_result()
    {
        return $this->db->count_all_results('tb_trainer_roadmap');
    }

    // People Trainer
    function get_data_trainer_people()
    {
        $query = $this->db->get('tb_trainer_people');
        return $query->result_array();
    }

    public function insert_data_trainer_people($data)
    {
        $this->db->insert('tb_trainer_people', $data);
        return TRUE;
    }

    public function update_data_trainer_people($data, $data_id)
    {
        $this->db->update('tb_trainer_people', $data, $data_id);
        return TRUE;
    }

    function delete_data_trainer_people($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }


    // Solver Stifin About
    function get_data_solver_about()
    {
        $query = $this->db->get('tb_solver_about');
        return $query->result_array();
    }

    public function insert_data_solver_about($data)
    {
        $this->db->insert('tb_solver_about', $data);
        return TRUE;
    }

    public function update_data_solver_about($data, $data_id)
    {
        $this->db->update('tb_solver_about', $data, $data_id);
        return TRUE;
    }

    function delete_data_solver_about($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    // Roadmap Solver
    function get_data_roadmap_solver()
    {
        $query = $this->db->get('tb_solver_roadmap');
        return $query->result_array();
    }

    public function insert_data_roadmap_solver($data)
    {
        $this->db->insert('tb_solver_roadmap', $data);
        return TRUE;
    }

    public function update_data_roadmap_solver($data, $data_id)
    {
        $this->db->update('tb_solver_roadmap', $data, $data_id);
        return TRUE;
    }

    function delete_data_roadmap_solver($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    function get_data_roadmap_solver_api($limit = 10, $start = 0)
    {
        $this->db->limit($limit, $start);
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('tb_solver_roadmap');
        return $query->result_array();
    }
    function get_data_roadmap_solver_allcount()
    {
        return $this->db->count_all('tb_solver_roadmap');
    }
    function get_data_roadmap_solver_allcount_result()
    {
        return $this->db->count_all_results('tb_solver_roadmap');
    }

    // People Solver
    function get_data_solver_people()
    {
        $query = $this->db->get('tb_solver_people');
        return $query->result_array();
    }

    public function insert_data_solver_people($data)
    {
        $this->db->insert('tb_solver_people', $data);
        return TRUE;
    }

    public function update_data_solver_people($data, $data_id)
    {
        $this->db->update('tb_solver_people', $data, $data_id);
        return TRUE;
    }

    function delete_data_solver_people($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }
}
