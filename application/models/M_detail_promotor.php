<?php

class M_detail_promotor extends CI_Model
{

    function get_data_detail_promotor()
    {
        $query = $this->db->get('tb_dp_about');
        return $query->result_array();
    }

    public function insert_data_detail_promotor($data)
    {
        $this->db->insert('tb_dp_about', $data);
        return TRUE;
    }

    public function update_data_detail_promotor($data, $data_id)
    {
        $this->db->update('tb_dp_about', $data, $data_id);
        return TRUE;
    }

    function delete_data_detail_promotor($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    // Benefit
    function get_data_benefit_promotor()
    {
        $query = $this->db->get('tb_dp_benefit');
        return $query->result_array();
    }

    public function insert_data_benefit_promotor($data)
    {
        $this->db->insert('tb_dp_benefit', $data);
        return TRUE;
    }

    public function update_data_benefit_promotor($data, $data_id)
    {
        $this->db->update('tb_dp_benefit', $data, $data_id);
        return TRUE;
    }

    function delete_data_benefit_promotor($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    // Roadmap
    function get_data_roadmap_promotor()
    {
        $query = $this->db->get('tb_dp_roadmap');
        return $query->result_array();
    }

    public function insert_data_roadmap_promotor($data)
    {
        $this->db->insert('tb_dp_roadmap', $data);
        return TRUE;
    }

    public function update_data_roadmap_promotor($data, $data_id)
    {
        $this->db->update('tb_dp_roadmap', $data, $data_id);
        return TRUE;
    }

    function delete_data_roadmap_promotor($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    function get_data_roadmap_promotor_api($limit = 10, $start = 0)
    {
        $this->db->limit($limit, $start);
        $this->db->order_by('id', 'ASC');
        $query = $this->db->get('tb_dp_roadmap');
        return $query->result_array();
    }
    function get_data_roadmap_promotor_allcount()
    {
        return $this->db->count_all('tb_dp_roadmap');
    }
    function get_data_roadmap_promotor_allcount_result()
    {
        return $this->db->count_all_results('tb_dp_roadmap');
    }
}
