<?php

class M_artikel extends CI_Model
{

    function get_data_artikel()
    {
        $this->db->order_by('id', 'DESC');
        $query = $this->db->get('tb_artikel');
        return $query->result_array();
    }

    public function insert_data_artikel($data)
    {
        $this->db->insert('tb_artikel', $data);
        return TRUE;
    }

    public function update_data_artikel($data, $data_id)
    {
        $this->db->update('tb_artikel', $data, $data_id);
        return TRUE;
    }

    function delete_data_artikel($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }
    
    function get_artikel($slug)
    {
        $query = $this->db->get_where('tb_artikel', array('slug' => $slug));
        return $query->row_array();
    }

    // create function get ada atrikel limit 3 short by latest
    function get_artikel_landing()
    {
        $this->db->order_by('id', 'DESC');
        $this->db->limit(3);
        $query = $this->db->get('tb_artikel');
        return $query->result_array();
    }
}
