<?php

class M_konsep extends CI_Model
{

    function get_data_konsep()
    {
        $query = $this->db->get('tb_konsep_stifin');
        return $query->result_array();
    }

    public function insert_data_konsep($data)
    {
        $this->db->insert('tb_konsep_stifin', $data);
        return TRUE;
    }

    public function update_data_konsep($data, $data_id)
    {
        $this->db->update('tb_konsep_stifin', $data, $data_id);
        return TRUE;
    }

    function delete_data_konsep($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    //sensing
    function get_data_sensing()
    {
        $query = $this->db->get('tb_sensing');
        return $query->result_array();
    }

    public function insert_data_sensing($data)
    {
        $this->db->insert('tb_sensing', $data);
        return TRUE;
    }

    public function update_data_sensing($data, $data_id)
    {
        $this->db->update('tb_sensing', $data, $data_id);
        return TRUE;
    }

    function delete_data_sensing($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    //thingking
    function get_data_thingking()
    {
        $query = $this->db->get('tb_thingking');
        return $query->result_array();
    }

    public function insert_data_thingking($data)
    {
        $this->db->insert('tb_thingking', $data);
        return TRUE;
    }

    public function update_data_thingking($data, $data_id)
    {
        $this->db->update('tb_thingking', $data, $data_id);
        return TRUE;
    }

    function delete_data_thingking($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    //Intuiting
    function get_data_intuiting()
    {
        $query = $this->db->get('tb_intuiting');
        return $query->result_array();
    }

    public function insert_data_intuiting($data)
    {
        $this->db->insert('tb_intuiting', $data);
        return TRUE;
    }

    public function update_data_intuiting($data, $data_id)
    {
        $this->db->update('tb_intuiting', $data, $data_id);
        return TRUE;
    }

    function delete_data_intuiting($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    //Feeling
    function get_data_feeling()
    {
        $query = $this->db->get('tb_feeling');
        return $query->result_array();
    }

    public function insert_data_feeling($data)
    {
        $this->db->insert('tb_feeling', $data);
        return TRUE;
    }

    public function update_data_feeling($data, $data_id)
    {
        $this->db->update('tb_feeling', $data, $data_id);
        return TRUE;
    }

    function delete_data_feeling($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }

    //Insting
    function get_data_insting()
    {
        $query = $this->db->get('tb_insting');
        return $query->result_array();
    }

    public function insert_data_insting($data)
    {
        $this->db->insert('tb_insting', $data);
        return TRUE;
    }

    public function update_data_insting($data, $data_id)
    {
        $this->db->update('tb_insting', $data, $data_id);
        return TRUE;
    }

    function delete_data_insting($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    }
}
