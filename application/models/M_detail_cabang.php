<?php

class M_detail_cabang extends CI_Model
{

    function get_data_about_detail_cabang()
    {
        $query = $this->db->get('tb_dc_about_cabang');
        return $query->result_array();
    }

    public function insert_data_about_detail_cabang($data)
    {
        $this->db->insert('tb_dc_about_cabang', $data);
        return TRUE;
    }

    public function update_data_about_detail_cabang($data, $data_id)
    {
        $this->db->update('tb_dc_about_cabang', $data, $data_id);
        return TRUE;
    }

    function delete_data_about_detail_cabang($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    } 

    //Potensi Usaha
    function get_data_potensi_usaha()
    {
        $query = $this->db->get('tb_dc_potensi');
        return $query->result_array();
    }

    public function insert_data_potensi_usaha($data)
    {
        $this->db->insert('tb_dc_potensi', $data);
        return TRUE;
    }

    public function update_data_potensi_usaha($data, $data_id)
    {
        $this->db->update('tb_dc_potensi', $data, $data_id);
        return TRUE;
    }

    function delete_data_potensi_usaha($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    } 

    //Sumber Profit
    function get_data_sumber_profit()
    {
        $query = $this->db->get('tb_dc_sumber_profit');
        return $query->result_array();
    }

    public function insert_data_sumber_profit($data)
    {
        $this->db->insert('tb_dc_sumber_profit', $data);
        return TRUE;
    }

    public function update_data_sumber_profit($data, $data_id)
    {
        $this->db->update('tb_dc_sumber_profit', $data, $data_id);
        return TRUE;
    }

    function delete_data_sumber_profit($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    } 

    //Alur Tes
    function get_data_alur_tes()
    {
        $query = $this->db->get('tb_dc_alur_tes');
        return $query->result_array();
    }

    public function insert_data_alur_tes($data)
    {
        $this->db->insert('tb_dc_alur_tes', $data);
        return TRUE;
    }

    public function update_data_alur_tes($data, $data_id)
    {
        $this->db->update('tb_dc_alur_tes', $data, $data_id);
        return TRUE;
    }

    function delete_data_alur_tes($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    } 

    //Pilihan Cabang
    function get_data_pilihan_cabang()
    {
        $query = $this->db->get('tb_dc_pilihan_cabang');
        return $query->result_array();
    }

    public function insert_data_pilihan_cabang($data)
    {
        $this->db->insert('tb_dc_pilihan_cabang', $data);
        return TRUE;
    }

    public function update_data_pilihan_cabang($data, $data_id)
    {
        $this->db->update('tb_dc_pilihan_cabang', $data, $data_id);
        return TRUE;
    }

    function delete_data_pilihan_cabang($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    } 

    //Popup
    function get_data_popup()
    {
        $query = $this->db->get('tb_dc_popup');
        return $query->result_array();
    }

    public function insert_data_popup($data)
    {
        $this->db->insert('tb_dc_popup', $data);
        return TRUE;
    }

    public function update_data_popup($data, $data_id)
    {
        $this->db->update('tb_dc_popup', $data, $data_id);
        return TRUE;
    }

    function delete_data_popup($where, $table)
    {
        $this->db->where($where);
        $this->db->delete($table);
    } 
}
