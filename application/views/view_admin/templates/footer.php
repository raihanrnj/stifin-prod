 <!-- Main Footer -->
 <footer class="main-footer">
     <!-- To the right -->
     <div class="pull-right hidden-xs"></div>
     <!-- Default to the left -->
     <strong>Copyright &copy; <?php $year = date("Y"); echo $year; ?> <a href="https://stifin.com" target="__blank">STIFIn</a>.</strong> All rights reserved.
 </footer>

 <!-- <div class="control-sidebar-bg"></div> -->
 </div>
 <!-- ./wrapper -->

 <!-- REQUIRED JS SCRIPTS -->

 <!-- jQuery 3 -->
 <script src="<?= base_url() ?>assets/admin_assets/bower_components/jquery/dist/jquery.min.js"></script>
 <!-- Bootstrap 3.3.7 -->
 <script src="<?= base_url() ?>assets/admin_assets/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
 <!-- AdminLTE App -->
 <script src="<?= base_url() ?>assets/admin_assets/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
 <script src="<?= base_url() ?>assets/admin_assets/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
 <script src="<?= base_url() ?>assets/admin_assets/dist/js/adminlte.min.js"></script>
 <!-- Bootstrap WYSIHTML5 -->
 <script src="<?= base_url() ?>assets/admin_assets/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
 <script src="<?= base_url() ?>assets/admin_assets/bower_components/ckeditor/ckeditor.js"></script>
 <script src="<?= base_url() ?>assets/admin_assets/custom/custom.js"></script>


 <!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
 </body>

 </html>