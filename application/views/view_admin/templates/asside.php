<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= base_url() ?>assets/admin_assets/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image" />
            </div>
            <div class="pull-left info">
                <p>Administrator</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MENU</li>
            <!-- Optionally, you can add icons to the links -->
            <!-- <li>
                <a href="<?= base_url() ?>admin/home"><i class="fa fa-link"></i> <span>Landing Page</span></a>
            </li> -->
            <li class="treeview <?php
                                if (trim($page == 'landing') || $page == 'about_section' || $page == 'video_section' || $page == 'testimoni_section' || $page == 'slider_section' || $page == 'partners' || $page == 'cabang_section' || $page == 'promotor_section' || $page == 'keunggulan_section' || $page == 'footer_section') {
                                    echo "active";
                                } else {
                                    echo "";
                                } ?>">
                <a href="#"><i class="fa fa-home" aria-hidden="true"></i> <span>Landing Page</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li <?php
                        if ($page == 'landing') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/landing">Hero Section</a></li>
                    <li <?php
                        if ($page == 'about_section') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/landing/about_section">About</a></li>
                    <li <?php
                        if ($page == 'keunggulan_section') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/landing/keunggulan_section">Keunggulan</a></li>
                    <li <?php
                        if ($page == 'video_section') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/landing/video_section">Video</a></li>
                    <li <?php
                        if ($page == 'testimoni_section') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/landing/testimoni_section">Testimoni</a></li>
                    <li <?php
                        if ($page == 'cabang_section') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>>
                        <a href="<?= base_url() ?>admin/landing/cabang_section">Cabang</a>
                    </li>
                    <li <?php
                        if ($page == 'promotor_section') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>>
                        <a href="<?= base_url() ?>admin/landing/promotor_section">Promotor</a>
                    </li>
                    <li <?php
                        if ($page == 'slider_section') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>>
                        <a href="<?= base_url() ?>admin/landing/slider_section">Slider</a>
                    </li>
                    <li <?php
                        if ($page == 'partners') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>>
                        <a href="<?= base_url() ?>admin/landing/partners">Partners</a>
                    </li>
                    <li <?php
                        if ($page == 'footer_section') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/landing/footer_section">Footer</a></li>
                </ul>
            </li>
            <li class="treeview <?php
                                if (trim($page == 'konsep_stifin') || $page == 'sensing' || $page == 'thingking' || $page == 'intuiting' || $page == 'feeling' || $page == 'insting') {
                                    echo "active";
                                } else {
                                    echo "";
                                } ?>">
                <a href="#"><i class="fa fa-sitemap" aria-hidden="true"></i> <span>Konsep STIFIn</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li <?php
                        if ($page == 'konsep_stifin') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/konsep_stifin">Konsep STIFIn</a></li>
                    <li <?php
                        if ($page == 'sensing') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/konsep_stifin/sensing">Sensing</a></li>
                    <li <?php
                        if ($page == 'thingking') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/konsep_stifin/thingking">Thingking</a></li>
                    <li <?php
                        if ($page == 'intuiting') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/konsep_stifin/intuiting">Intuiting</a></li>
                    <li <?php
                        if ($page == 'feeling') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/konsep_stifin/feeling">Feeling</a></li>
                    <li <?php
                        if ($page == 'insting') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/konsep_stifin/insting">Insting</a></li>
                </ul>
            </li>
            <li class="treeview <?php
                                if (trim($page == 'cabang') || $page == 'workshop') {
                                    echo "active";
                                } else {
                                    echo "";
                                } ?>">
                <a href="#"><i class="fa fa-dot-circle-o" aria-hidden="true"></i> <span>Ekosistem STIFIn</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li <?php
                        if ($page == 'cabang') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/ekosistem/cabang">Cabang</a></li>
                    <li <?php
                        if ($page == 'workshop') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/ekosistem/workshop">Workshop</a></li>
                    <li <?php
                        if ($page == 'about_promotor_section') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/ekosistem/about_promotor_section">About Promotor</a></li>
                    <li <?php
                        if ($page == 'jenjang') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/ekosistem/jenjang">Jenjang Promotor</a></li>
                </ul>
            </li>
            <li class="treeview <?php
                                if (trim($page == 'download_aplikasi') || $page == 'download_video' || $page == 'download_audio' || $page == 'download_ebook') {
                                    echo "active";
                                } else {
                                    echo "";
                                } ?>">
                <a href="#"><i class="fa fa-download" aria-hidden="true"></i> <span>Download</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li <?php
                        if ($page == 'download_aplikasi') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/download">Aplikasi</a></li>
                    <li <?php
                        if ($page == 'download_video') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/download/video">Video</a></li>
                    <li <?php
                        if ($page == 'download_audio') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/download/audio">Audio</a></li>
                    <li <?php
                        if ($page == 'download_ebook') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/download/ebook">Ebook</a></li>
                </ul>
            </li>
            <li <?php
                if ($page == 'artikel') {
                    echo "class='active'";
                } else {
                    echo "";
                }
                ?>><a href="<?= base_url() ?>admin/artikel "><i class="fa fa-book"></i> <span>Artikel</span></a>
            </li>
            <li class="treeview <?php
                                if (trim($page == 'yayasan_stifin') || $page == 'stifin_institute' || $page == 'stifin_rqs' || $page == 'merchandise' || $page == 'sejarah') {
                                    echo "active";
                                } else {
                                    echo "";
                                } ?>">
                <a href="#"><i class="fa fa-user" aria-hidden="true"></i><span>About</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li <?php
                        if ($page == 'yayasan_stifin') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/about">Yayasan STIFIn</a></li>
                    <li <?php
                        if ($page == 'sejarah') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/about/sejarah">Roadmap STIFIn</a></li>
                    <li <?php
                        if ($page == 'stifin_institute') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/about/stifin_institute">STIFIn Institute</a></li>
                    <li <?php
                        if ($page == 'stifin_rqs') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/about/stifin_rqs">Rumah Qur'an STIFIn</a></li>
                    <li <?php
                        if ($page == 'merchandise') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/about/merchandise">Merchandise</a></li>
                </ul>
            </li>
            <li class="treeview <?php
                                if (trim($page == 'detail_cabang') || $page == 'potensi_usaha' || $page == 'sumber_profit' || $page == 'alur_tes' || $page == 'pilihan_cabang' || $page == 'popup') {
                                    echo "active";
                                } else {
                                    echo "";
                                } ?>">
                <a href="#"><i class="fa fa-address-card" aria-hidden="true"></i></i><span>Detail Cabang</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li <?php
                        if ($page == 'detail_cabang') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/detail_cabang">About Cabang</a></li>
                    <li <?php
                        if ($page == 'potensi_usaha') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/detail_cabang/dc_potensi_usaha">Potensi Usaha</a></li>
                    <li <?php
                        if ($page == 'sumber_profit') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/detail_cabang/dc_sumber_profit">Sumber_profit</a></li>
                    <li <?php
                        if ($page == 'alur_tes') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/detail_cabang/dc_alur_tes">Alur Tes</a></li>
                    <li <?php
                        if ($page == 'pilihan_cabang') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/detail_cabang/dc_pilihan_cabang">Pilihan Cabang</a></li>
                    <li <?php
                        if ($page == 'popup') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/detail_cabang/dc_popup">Image Popup</a></li>
                </ul>
            <li class="treeview <?php
                                if (trim($page == 'detail_promotor') || $page == 'dp_benefit' || $page == 'dp_roadmap') {
                                    echo "active";
                                } else {
                                    echo "";
                                } ?>">
                <a href="#"><i class="fa fa-users" aria-hidden="true"></i></i><span>Detail Promotor</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li <?php
                        if ($page == 'detail_promotor') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/detail_promotor">About Promotor</a></li>
                    <li <?php
                        if ($page == 'dp_benefit') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/detail_promotor/dp_benefit">Benefit Promotor</a></li>
                    <li <?php
                        if ($page == 'dp_roadmap') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/detail_promotor/dp_roadmap">Roadmap Promotor</a></li>
                </ul>
            </li>
            <li class="treeview <?php
                                if (trim($page == 'about_trainer_stifin') || $page == 'roadmap_trainer_stifin' || $page == 'people_trainer_stifin') {
                                    echo "active";
                                } else {
                                    echo "";
                                } ?>">
                <a href="#"><i class="fa fa-pencil-square" aria-hidden="true"></i></i><span>Trainer STIFIn</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li <?php
                        if ($page == 'about_trainer_stifin') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/ekosistem/trainer_stifin">About Trainer</a></li>
                    <li <?php
                        if ($page == 'roadmap_trainer_stifin') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/ekosistem/trainer_roadmap">Roadmap Trainer</a></li>
                    <li <?php
                        if ($page == 'people_trainer_stifin') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/ekosistem/trainer">Para Trainer</a></li>
                </ul>
            </li>
            <li class="treeview <?php
                                if (trim($page == 'solver_about') || $page == 'solver_roadmap' || $page == 'solver_people') {
                                    echo "active";
                                } else {
                                    echo "";
                                } ?>">
                <a href="#"><i class="fa fa-check-square" aria-hidden="true"></i></i><span>Solver STIFIn</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li <?php
                        if ($page == 'solver_about') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/ekosistem/solver_about">About Solver</a></li>
                    <li <?php
                        if ($page == 'solver_roadmap') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/ekosistem/solver_roadmap">Roadmap Solver</a></li>
                    <li <?php
                        if ($page == 'solver_people') {
                            echo "class='active'";
                        } else {
                            echo "";
                        }
                        ?>><a href="<?= base_url() ?>admin/ekosistem/solver">Para Solver</a></li>
                </ul>
            </li>
        </ul>
        <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>