<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Footer Section</h1>
        <ol class="breadcrumb">
            <li>
                <a href="#"><i class="fa fa-dashboard"></i> Home</a>
            </li>
            <li class="active">Footer Section</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?= $this->session->flashdata('notif') ?>
        <?php
            if (empty($footer_section)) {
                echo "<button type='button' style='margin-bottom: 20px; margin-top: 20px;' class='btn btn-info' data-toggle='modal' data-target='#modal_tambah_data'><i class='fa fa-fw fa-plus'></i> Tambah Data</button>";
            }else{
                echo "</br>";
            }
        ?>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-info">
                    <!-- /.box-header -->
                    <div class="box-body" style="overflow: scroll;">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr> 
                                    <th>No</th>
                                    <th>Tagline</th>
                                    <th>Alamat</th>
                                    <th>Nomer Telp</th>
                                    <th>Email</th>
                                    <th>Twitter</th>
                                    <th>Facebook</th>
                                    <th>Youtube</th>
                                    <th>Linkedin</th>
                                    <th>Instagram</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = $this->uri->segment('4') + 1;
                                foreach ($footer_section as $data) : ?>
                                    <tr>
                                        <td class="td-midle"><?= $no++ ?></td>
                                        <td class="td-midle"><?= $data['tagline'] ?></td>
                                        <td class="td-midle"><?= word_limiter($data['alamat'], 20) ?></td>
                                        <td class="td-midle"><?= $data['telp'] ?></td>
                                        <td class="td-midle"><?= $data['email'] ?></td>
                                        <td class="td-midle"><?= $data['tw'] ?></td>
                                        <td class="td-midle"><?= $data['fb'] ?></td>
                                        <td class="td-midle"><?= $data['yt'] ?></td>
                                        <td class="td-midle"><?= $data['lk'] ?></td>
                                        <td class="td-midle"><?= $data['ig'] ?></td>
                                        
                                        <td class="td-midle">
                                            <a class="btn btn-warning btn-sm mr-md" href="" data-toggle="modal" data-target="#modal_update_data<?= $data['id'] ?>"><i class="fa fa-fw fa-edit"></i></a>
                                            <a class="btn btn-danger btn-sm" href="" data-toggle="modal" data-target="#modal_delete_data<?= $data['id'] ?>"><i class="fa fa-fw fa-trash"></i></i></a>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
    <div class="modal fade" id="modal_tambah_data">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Tambah Data</h4>
                </div>
                <?php echo form_open_multipart('admin/landing/footer_section_add', 'method="post"', 'role="form"', 'enctype="multipart/form-data"'); ?>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="tagline">Tagline</label>
                        <input name="tagline" type="text" class="form-control" id="tagline" placeholder="Masukan Tagline">
                    </div>
                    <div class="form-group">
                        <label>Alamat</label>
                        <textarea name="alamat" class="form-control" rows="3" id='alamat' placeholder="Masukan Alamat"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="telp">No Telp</label>
                        <input name="telp" type="text" class="form-control" id="telp" placeholder="Masukan No Telp">
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input name="email" type="text" class="form-control" id="email" placeholder="Masukan Email">
                    </div>
                    <div class="form-group">
                        <label for="tw">Twitter</label>
                        <input name="tw" type="text" class="form-control" id="tw" placeholder="Masukan Link Twitter">
                    </div>
                    <div class="form-group">
                        <label for="fb">Facebook</label>
                        <input name="fb" type="text" class="form-control" id="fb" placeholder="Masukan Link FB">
                    </div>
                    <div class="form-group">
                        <label for="yt">Youtube</label>
                        <input name="yt" type="text" class="form-control" id="telp" placeholder="Masukan Link youtube">
                    </div>
                    <div class="form-group">
                        <label for="lk">Linkedin</label>
                        <input name="lk" type="text" class="form-control" id="lk" placeholder="Masukan Link Linkedin">
                    </div>
                    <div class="form-group">
                        <label for="ig">Instagram</label>
                        <input name="ig" type="text" class="form-control" id="ig" placeholder="Masukan Link Instagram">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <?php
    $no = $this->uri->segment('4') + 1;
    foreach ($footer_section as $data) : ?>
        <div class="modal fade" id="modal_update_data<?= $data['id'] ?>">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Update Data</h4>
                    </div>
                    <?php echo form_open_multipart('admin/landing/footer_section_update', 'method="post"', 'role="form"', 'enctype="multipart/form-data"'); ?>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="tagline">Tagline</label>
                            <input name="id" value="<?= $data['id']; ?>" type="hidden" class="form-control" id="id">
                            <input name="tagline" value="<?= $data['tagline']; ?>" type="text" class="form-control" id="tagline" placeholder="Masukan Tagline">
                        </div>
                        <div class="form-group">
                            <label>Alamat</label>
                            <textarea name="alamat" class="form-control" rows="3" id="alamat" placeholder="Masukan Alamat"><?= $data['alamat']; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="telp">No. Telp</label>
                            <input name="telp" value="<?= $data['telp']; ?>" type="text" class="form-control" id="telp" placeholder="Masukan No. Telp">
                        </div>
                        <div class="form-group">
                            <label for="email">Email</label>
                            <input name="email" value="<?= $data['email']; ?>" type="text" class="form-control" id="email" placeholder="Masukan Email">
                        </div>
                        <div class="form-group">
                            <label for="tw">Twitter</label>
                            <input name="tw" value="<?= $data['tw']; ?>" type="text" class="form-control" id="tw" placeholder="Masukan Link Twitter">
                        </div>
                        <div class="form-group">
                            <label for="fb">Facebook</label>
                            <input name="fb" value="<?= $data['fb']; ?>" type="text" class="form-control" id="fb" placeholder="Masukan Link Facebook">
                        </div>
                        <div class="form-group">
                            <label for="yt">Youtube</label>
                            <input name="yt" value="<?= $data['yt']; ?>" type="text" class="form-control" id="yt" placeholder="Masukan Link Youtube">
                        </div>
                        <div class="form-group">
                            <label for="lk">Linkedin</label>
                            <input name="lk" value="<?= $data['lk']; ?>" type="text" class="form-control" id="lk" placeholder="Masukan Link Linkedin">
                        </div>
                        <div class="form-group">
                            <label for="ig">Instagram</label>
                            <input name="ig" value="<?= $data['ig']; ?>" type="text" class="form-control" id="ig" placeholder="Masukan Link Instagram">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    <?php endforeach ?>
    <!-- /.modal -->

    <?php
    $no = $this->uri->segment('4') + 1;
    foreach ($footer_section as $data) : ?>
        <div class="modal fade" id="modal_delete_data<?= $data['id'] ?>">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Hapus Data Yang Dipilih ?</h4>
                    </div>
                    <div class="modal-body">
                        <div class="text-center">
                            <button type="button" style="width: 45%;" class="btn btn-default mr-md" data-dismiss="modal">Close</button>
                            <a href="<?= base_url() ?>admin/landing/footer_section_delete/<?= $data['id'] ?>" style="width: 45%;" class="btn btn-danger">Hapus</a>
                        </div>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    <?php endforeach ?>
</div>
<!-- /.content-wrapper -->