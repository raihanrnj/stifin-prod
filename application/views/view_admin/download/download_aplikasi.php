<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Download Aplikasi</h1>
        <ol class="breadcrumb">
            <li>
                <a href="#"><i class="fa fa-dashboard"></i> Home</a>
            </li>
            <li class="active">Download Aplikasi</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?= $this->session->flashdata('notif') ?>
        <button type="button" style="margin-bottom: 20px; margin-top: 20px;" class="btn btn-info" data-toggle="modal" data-target="#modal_tambah_data">
            <i class="fa fa-fw fa-plus"></i> Tambah Data
        </button>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-info">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Aplikasi</th>
                                    <th>Deskripsi</th>
                                    <th>Icon</th>
                                    <th>File</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = $this->uri->segment('4') + 1;
                                foreach ($aplikasi as $data) : ?>
                                    <tr>
                                        <td class="td-midle"><?= $no++ ?></td>
                                        <td class="td-midle"><?= $data['judul'] ?></td>
                                        <td class="td-midle"><?= $data['deskripsi'] ?></td>
                                        <td class="td-midle"><?= $data['icon'] ?></td>
                                        <td class="td-midle"><?= $data['file'] ?></td>
                                        <!-- <td><img class="img-fluid" style="width: 50px;" src="<?= base_url() ?>assets/upload/images/<?= $data['gambar']; ?>" alt=""></td> -->
                                        <td class="td-midle">
                                            <a class="btn btn-warning btn-sm mr-md" href="" data-toggle="modal" data-target="#modal_update_data<?= $data['id'] ?>"><i class="fa fa-fw fa-edit"></i></a>
                                            <a class="btn btn-danger btn-sm" href="" data-toggle="modal" data-target="#modal_delete_data<?= $data['id'] ?>"><i class="fa fa-fw fa-trash"></i></i></a>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
    <div class="modal fade" id="modal_tambah_data">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Tambah Data</h4>
                </div>
                <?php echo form_open_multipart('admin/download/aplikasi_add', 'method="post"', 'role="form"', 'enctype="multipart/form-data"'); ?>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="judul">Judul</label>
                        <input name="judul" type="text" class="form-control" id="judul" placeholder="Masukan Judul">
                    </div>
                    <div class="form-group">
                        <label>Deskripsi</label>
                        <textarea name="deskripsi" class="form-control" rows="3" placeholder="Masukan Deskripsi"></textarea>
                    </div>
                    <div class="form-group">
                        <label>icon</label>
                        <input name="icon" type="text" class="form-control" id="icon" placeholder="Masukan icon dari web font awesome">
                    </div>
                    <div class="form-group">
                        <label for="judul">Masukan File PDF</label>
                        <input name="file" class="form-control" type="file" id="formFile">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <?php
    $no = $this->uri->segment('4') + 1;
    foreach ($aplikasi as $data) : ?>
        <div class="modal fade" id="modal_update_data<?= $data['id'] ?>">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Update Data</h4>
                    </div>
                    <?php echo form_open_multipart('admin/download/aplikasi_update', 'method="post"', 'role="form"', 'enctype="multipart/form-data"'); ?>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="judul">Judul</label>
                            <input name="id" value="<?= $data['id']; ?>" type="hidden" class="form-control" id="id">
                            <input name="judul" value="<?= $data['judul']; ?>" type="text" class="form-control" id="judul" placeholder="Masukan Judul">
                        </div>
                        <div class="form-group">
                            <label>Deskripsi</label>
                            <textarea name="deskripsi" class="form-control" rows="3" placeholder="Masukan Deskripsi"><?= $data['deskripsi']; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label>Icon</label>
                            <input name="icon" value="<?= $data['icon']; ?>" type="text" class="form-control" id="icon" placeholder="Masukan Icon dari web font awesome">
                            
                        </div>
                        <div class="form-group">
                            <label for="judul">Masukan File PDF</label>
                            <input name="file" value="<?= $data['file']; ?>" class="form-control" type="file" id="formFile">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    <?php endforeach ?>
    <!-- /.modal -->

    <?php
    $no = $this->uri->segment('4') + 1;
    foreach ($aplikasi as $data) : ?>
        <div class="modal fade" id="modal_delete_data<?= $data['id'] ?>">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Hapus Data Yang Dipilih ?</h4>
                    </div>
                    <div class="modal-body">
                        <div class="text-center">
                            <button type="button" style="width: 45%;" class="btn btn-default mr-md" data-dismiss="modal">Close</button>
                            <a href="<?= base_url() ?>admin/download/aplikasi_delete/<?= $data['id'] ?>" style="width: 45%;" class="btn btn-danger">Hapus</a>
                        </div>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    <?php endforeach ?>
</div>
<!-- /.content-wrapper -->