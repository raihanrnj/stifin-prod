<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Solver STIFIn</h1>
        <ol class="breadcrumb">
            <li>
                <a href="#"><i class="fa fa-dashboard"></i> Home</a>
            </li>
            <li class="active">Solver STIFIn</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?= $this->session->flashdata('notif') ?>
        <button type='button' style='margin-bottom: 20px; margin-top: 20px;' class='btn btn-info' data-toggle='modal' data-target='#modal_tambah_data'><i class='fa fa-fw fa-plus'></i> Tambah Data</button>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-info">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Personal Branding</th>
                                    <th>Rating</th>
                                    <th>Deskripsi</th>
                                    <th>Gambar</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = $this->uri->segment('4') + 1;
                                foreach ($solver_stifin as $data) : ?>
                                    <tr>
                                        <td class="td-midle"><?= $no++ ?></td>
                                        <td class="td-midle"><?= $data['nama'] ?></td>
                                        <td class="td-midle"><?= $data['branding'] ?></td>
                                        <td class="td-midle"><?= $data['rating'] ?></td>
                                        <td class="td-midle"><?= word_limiter($data['deskripsi'], 10) ?></td>
                                        <td><img class="img-fluid" style="width: 50px;" src="<?= base_url() ?>assets/upload/images/<?= $data['gambar']; ?>" alt=""></td>
                                        <td class="td-midle">
                                            <a class="btn btn-warning btn-sm mr-md" href="" data-toggle="modal" data-target="#modal_update_data<?= $data['id'] ?>"><i class="fa fa-fw fa-edit"></i></a>
                                            <a class="btn btn-danger btn-sm" href="" data-toggle="modal" data-target="#modal_delete_data<?= $data['id'] ?>"><i class="fa fa-fw fa-trash"></i></i></a>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
    <div class="modal fade" id="modal_tambah_data">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Tambah Data</h4>
                </div>
                <?php echo form_open_multipart('admin/ekosistem/solver_add', 'method="post"', 'role="form"', 'enctype="multipart/form-data"'); ?>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="nama">Nama</label>
                        <input name="nama" type="text" class="form-control" id="nama" placeholder="Masukan Nama">
                    </div>
                    <div class="form-group">
                        <label for="branding">Personal Branding</label>
                        <input name="branding" type="text" class="form-control" id="branding" placeholder="Masukan Personal Branding">
                    </div>
                    <div class="form-group">
                        <label for="rating">Rating</label><br>
                        <fieldset class="rating">
                            <input type="radio" id="star5" name="rating" value="5" /><label class="full" for="star5" title="Awesome - 5 stars"></label>
                            <input type="radio" id="star4half" name="rating" value="4 and a half" /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
                            <input type="radio" id="star4" name="rating" value="4" /><label class="full" for="star4" title="Pretty good - 4 stars"></label>
                            <input type="radio" id="star3half" name="rating" value="3 and a half" /><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
                            <input type="radio" id="star3" name="rating" value="3" /><label class="full" for="star3" title="Meh - 3 stars"></label>
                            <input type="radio" id="star2half" name="rating" value="2 and a half" /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
                            <input type="radio" id="star2" name="rating" value="2" /><label class="full" for="star2" title="Kinda bad - 2 stars"></label>
                            <input type="radio" id="star1half" name="rating" value="1 and a half" /><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
                            <input type="radio" id="star1" name="rating" value="1" /><label class="full" for="star1" title="Sucks big time - 1 star"></label>
                            <input type="radio" id="starhalf" name="rating" value="half" /><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
                        </fieldset>
                    </div><br>
                    <br>
                    <div class="form-group">
                        <label>Deskripsi</label>
                        <textarea name="deskripsi" class="textareacs form-control" rows="3" placeholder="Masukan Deskripsi"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="judul">Masukan Gambar</label>
                        <input name="gambar" class="form-control" type="file" id="formFile">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <?php
    $no = $this->uri->segment('4') + 1;
    foreach ($solver_stifin as $index => $data) : ?>
        <div class="modal fade" id="modal_update_data<?= $data['id'] ?>">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Update Data</h4>
                    </div>
                    <?php echo form_open_multipart('admin/ekosistem/solver_update', 'method="post"', 'role="form"', 'enctype="multipart/form-data"'); ?>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <input name="id" value="<?= $data['id'] ?>" type="hidden" class="form-control" id="nama" placeholder="Masukan Nama">
                            <input name="nama" value="<?= $data['nama'] ?>" type="text" class="form-control" id="nama" placeholder="Masukan Nama">
                        </div>
                        <div class="form-group">
                            <label for="branding">Personal Branding</label>
                            <input name="branding" value="<?= $data['branding'] ?>" type="text" class="form-control" id="branding" placeholder="Masukan Personal Branding">
                        </div>
                        <div class="form-group">
                            <label for="rating">Rating</label><br>
                            <fieldset class="rating">
                                <input type="radio" id="star5-<?= $index ?>" name="rating" value="5" <?php if ($data['rating'] == "5") echo 'checked'; ?> /><label class="full" for="star5-<?= $index ?>" title="Awesome - 5 stars"></label>
                                <input type="radio" id="star4half-<?= $index ?>" name="rating" value="4 and a half" <?php if ($data['rating'] == "4 and a half") echo 'checked'; ?> /><label class="half" for="star4half-<?= $index ?>" title="Pretty good - 4.5 stars"></label>
                                <input type="radio" id="star4-<?= $index ?>" name="rating" value="4" <?php if ($data['rating'] == "4") echo 'checked'; ?> /><label class="full" for="star4-<?= $index ?>" title="Pretty good - 4 stars"></label>
                                <input type="radio" id="star3half-<?= $index ?>" name="rating" value="3 and a half" <?php if ($data['rating'] == "3 and a half") echo 'checked'; ?> /><label class="half" for="star3half-<?= $index ?>" title="Meh - 3.5 stars"></label>
                                <input type="radio" id="star3-<?= $index ?>" name="rating" value="3" <?php if ($data['rating'] == "3") echo 'checked'; ?> /><label class="full" for="star3-<?= $index ?>" title="Meh - 3 stars"></label>
                                <input type="radio" id="star2half-<?= $index ?>" name="rating" value="2 and a half" <?php if ($data['rating'] == "2 and a half") echo 'checked'; ?> /><label class="half" for="star2half-<?= $index ?>" title="Kinda bad - 2.5 stars"></label>
                                <input type="radio" id="star2-<?= $index ?>" name="rating" value="2" <?php if ($data['rating'] == "2") echo 'checked'; ?> /><label class="full" for="star2-<?= $index ?>" title="Kinda bad - 2 stars"></label>
                                <input type="radio" id="star1half-<?= $index ?>" name="rating" value="1 and a half" <?php if ($data['rating'] == "1 and a half") echo 'checked'; ?> /><label class="half" for="star1half-<?= $index ?>" title="Meh - 1.5 stars"></label>
                                <input type="radio" id="star1-<?= $index ?>" name="rating" value="1" <?php if ($data['rating'] == "1") echo 'checked'; ?> /><label class="full" for="star1-<?= $index ?>" title="Sucks big time - 1 star"></label>
                                <input type="radio" id="starhalf-<?= $index ?>" name="rating" value="half" <?php if ($data['rating'] == "half") echo 'checked'; ?> /><label class="half" for="starhalf-<?= $index ?>" title="Sucks big time - 0.5 stars"></label>
                            </fieldset>
                        </div><br>
                        <br>
                        <div class="form-group">
                            <label>Deskripsi</label>
                            <textarea name="deskripsi" class="textareacs form-control" rows="3" placeholder="Masukan Deskripsi"><?= $data['deskripsi']; ?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="judul">Masukan Gambar</label>
                            <input name="gambar" value="<?= $data['gambar']; ?>" class="form-control" type="file" id="formFile">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    <?php endforeach ?>
    <!-- /.modal -->

    <?php
    $no = $this->uri->segment('4') + 1;
    foreach ($solver_stifin as $data) : ?>
        <div class="modal fade" id="modal_delete_data<?= $data['id'] ?>">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Hapus Data Yang Dipilih ?</h4>
                    </div>
                    <div class="modal-body">
                        <div class="text-center">
                            <button type="button" style="width: 45%;" class="btn btn-default mr-md" data-dismiss="modal">Close</button>
                            <a href="<?= base_url() ?>admin/ekosistem/solver_delete/<?= $data['id'] ?>" style="width: 45%;" class="btn btn-danger">Hapus</a>
                        </div>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    <?php endforeach ?>
</div>
<!-- /.content-wrapper -->