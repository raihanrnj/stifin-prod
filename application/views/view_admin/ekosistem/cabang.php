<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Data Cabang</h1>
        <ol class="breadcrumb">
            <li>
                <a href="#"><i class="fa fa-dashboard"></i> Home</a>
            </li>
            <li class="active">Data Cabang</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?= $this->session->flashdata('notif') ?>
        <button type="button" style="margin-bottom: 20px; margin-top: 20px;" class="btn btn-info" data-toggle="modal" data-target="#modal_tambah_data">
            <i class="fa fa-fw fa-plus"></i> Tambah Data
        </button>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-info">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Provinsi</th>
                                    <th>Nama Cabang</th>
                                    <th>Alamat</th>
                                    <th>Jam Operasional</th>
                                    <th>No HP</th>
                                    <th>Maps</th>
                                    <th>Website</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = $this->uri->segment('4') + 1;
                                foreach ($cabang as $data) : ?>
                                    <tr>
                                        <td class="td-midle"><?= $no++ ?></td>
                                        <td class="td-midle"><?= $data['provinsi'] ?></td>
                                        <td class="td-midle"><?= $data['nama_cabang'] ?></td>
                                        <td class="td-midle"><?= $data['alamat'] ?></td>
                                        <td class="td-midle"><?= $data['jam_operasional'] ?></td>
                                        <td class="td-midle"><?= $data['no_hp'] ?></td>
                                        <td class="td-midle"><?= $data['maps'] ?></td>
                                        <td class="td-midle"><?= $data['website'] ?></td>
                                        <td class="td-midle">
                                            <a class="btn btn-warning btn-sm mr-md" href="" data-toggle="modal" data-target="#modal_update_data<?= $data['id'] ?>"><i class="fa fa-fw fa-edit"></i></a>
                                            <a class="btn btn-danger btn-sm" href="" data-toggle="modal" data-target="#modal_delete_data<?= $data['id'] ?>"><i class="fa fa-fw fa-trash"></i></i></a>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
    <div class="modal fade" id="modal_tambah_data">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Tambah Data</h4>
                </div>
                <?php echo form_open_multipart('admin/ekosistem/cabang_add', 'method="post"', 'role="form"', 'enctype="multipart/form-data"'); ?>
                <div class="modal-body">
                    <div class="form-group">
                        <label>Provinsi</label>
                        <select name="provinsi" class="form-control">
                            <option value="">Pilih Provinsi</option>
                            <?php foreach ($data_provinsi as $data) { ?>
                                <option value="<?= $data->nama; ?>"><?= $data->nama; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="nama_cabang">Nama Cabang</label>
                        <input name="nama_cabang" type="text" class="form-control" id="nama_cabang" placeholder="Masukan nama cabang">
                    </div>
                    <div class="form-group">
                        <label>Alamat</label>
                        <textarea name="alamat" class="form-control" rows="3" placeholder="Masukan alamat"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="jam_operasional">Jam Operasional</label>
                        <input name="jam_operasional" type="text" class="form-control" id="jam_operasional" placeholder="Masukan jam operasional">
                    </div>
                    <div class="form-group">
                        <label for="no_hp">No Handphone</label>
                        <input name="no_hp" type="text" class="form-control" id="no_hp" placeholder="Masukan nomor handphone">
                    </div>
                    <div class="form-group">
                        <label for="maps">Maps link</label>
                        <input name="maps" type="text" class="form-control" id="maps" placeholder="Masukan maps link handphone">
                    </div>
                    <div class="form-group">
                        <label for="website">Website</label>
                        <input name="website" type="text" class="form-control" id="website" placeholder="Masukan website">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <?php
    $no = $this->uri->segment('4') + 1;
    foreach ($cabang as $e) : ?>
        <div class="modal fade" id="modal_update_data<?= $e['id'] ?>">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Update Data</h4>
                    </div>
                    <?php echo form_open_multipart('admin/ekosistem/cabang_update', 'method="post"', 'role="form"', 'enctype="multipart/form-data"'); ?>
                    <div class="modal-body">
                    <div class="form-group">
                        <label>Provinsi</label>
                        <select name="provinsi" class="form-control">
                            <option value="<?= $e['provinsi']; ?>"><?= $e['provinsi']; ?></option>
                            <?php foreach ($data_provinsi as $data) { ?>
                                <option value="<?= $data->nama; ?>"><?= $data->nama; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="nama_cabang">Nama Cabang</label>
                        <input name="id" value="<?= $e['id']; ?>" type="hidden" class="form-control" id="id">
                        <input name="nama_cabang" value="<?= $e['nama_cabang']; ?>" type="text" class="form-control" id="nama_cabang" placeholder="Masukan nama cabang">
                    </div>
                    <div class="form-group">
                        <label>Alamat</label>
                        <textarea name="alamat" value="<?= $e['alamat']; ?>" class="form-control" rows="3" placeholder="Masukan alamat"><?= $e['alamat']; ?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="jam_operasional">Jam Operasional</label>
                        <input name="jam_operasional" value="<?= $e['jam_operasional']; ?>" type="text" class="form-control" id="jam_operasional" placeholder="Masukan jam operasional">
                    </div>
                    <div class="form-group">
                        <label for="no_hp">No Handphone</label>
                        <input name="no_hp" type="text" value="<?= $e['no_hp']; ?>" class="form-control" id="no_hp" placeholder="Masukan nomor handphone">
                    </div>
                    <div class="form-group">
                        <label for="maps">Maps link</label>
                        <input name="maps" type="text" value="<?= $e['maps']; ?>" class="form-control" id="maps" placeholder="Masukan maps link handphone">
                    </div>
                    <div class="form-group">
                        <label for="website">Website</label>
                        <input name="website" type="text" value="<?= $e['website']; ?>" class="form-control" id="website" placeholder="Masukan website">
                    </div>
                </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                    </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    <?php endforeach ?>
    <!-- /.modal -->

    <?php
    $no = $this->uri->segment('4') + 1;
    foreach ($cabang as $data) : ?>
        <div class="modal fade" id="modal_delete_data<?= $data['id'] ?>">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Hapus Data Yang Dipilih ?</h4>
                    </div>
                    <div class="modal-body">
                        <div class="text-center">
                            <button type="button" style="width: 45%;" class="btn btn-default mr-md" data-dismiss="modal">Close</button>
                            <a href="<?= base_url() ?>admin/ekosistem/cabang_delete/<?= $data['id'] ?>" style="width: 45%;" class="btn btn-danger">Hapus</a>
                        </div>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    <?php endforeach ?>
</div>
<!-- /.content-wrapper -->