<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Data jenjang</h1>
        <ol class="breadcrumb">
            <li>
                <a href="#"><i class="fa fa-dashboard"></i> Home</a>
            </li>
            <li class="active">Data jenjang</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <?= $this->session->flashdata('notif') ?>
        <button type="button" style="margin-bottom: 20px; margin-top: 20px;" class="btn btn-info" data-toggle="modal" data-target="#modal_tambah_data">
            <i class="fa fa-fw fa-plus"></i> Tambah Data
        </button>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-info">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Judul</th>
                                    <th>Konten</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = $this->uri->segment('4') + 1;
                                foreach ($jenjang as $data) : ?>
                                    <tr>
                                        <td class="td-midle"><?= $no++ ?></td>
                                        <td class="td-midle"><?= $data['judul'] ?></td>
                                        <td class="td-midle"><?= word_limiter(strip_tags($data['konten']), 30) ?></td>
                                        <td class="td-midle">
                                            <a class="btn btn-warning btn-sm mr-md" href="" data-toggle="modal" data-target="#modal_update_data<?= $data['id'] ?>"><i class="fa fa-fw fa-edit"></i></a>
                                            <a class="btn btn-danger btn-sm" href="" data-toggle="modal" data-target="#modal_delete_data<?= $data['id'] ?>"><i class="fa fa-fw fa-trash"></i></i></a>
                                        </td>
                                    </tr>
                                <?php endforeach ?>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
    <!-- /.content -->
    <div class="modal fade" id="modal_tambah_data">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Tambah Data</h4>
                </div>
                <?php echo form_open_multipart('admin/ekosistem/jenjang_add', 'method="post"', 'role="form"', 'enctype="multipart/form-data"'); ?>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="judul">Judul</label>
                        <input name="judul" type="text" class="form-control" id="judul" placeholder="Masukan judul jenjang">
                    </div>
                    <div class="form-group">
                        <label>Konten</label>
                        <textarea name="konten" class="textareacs form-control" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
                    </div>
                    <label for="tahapan">Tahapan</label>
                        <input name="tahapan" type="text" class="form-control" id="tahapan" placeholder="Masukan tahapan ke berapa">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <?php
    $no = $this->uri->segment('4') + 1;
    foreach ($jenjang as $e) : ?>
        <div class="modal fade" id="modal_update_data<?= $e['id'] ?>">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Update Data</h4>
                    </div>
                    <?php echo form_open_multipart('admin/ekosistem/jenjang_update', 'method="post"', 'role="form"', 'enctype="multipart/form-data"'); ?>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="judul">Judul</label>
                            <input name="id" value="<?= $e['id'] ?>" type="hidden" class="form-control" id="id">
                            <input name="judul" type="text" value="<?= $e['judul'] ?>" class="form-control" id="judul" placeholder="Masukan judul jenjang">
                        </div>
                        <div class="form-group">
                            <label>Konten</label>
                            <textarea name="konten" class="textareacs form-control" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"><?= $e['konten'] ?></textarea>
                        </div>
                        <div class="form-group">
                            <label for="tahapan">Tahapan</label>
                            <input name="tahapan" type="text" value="<?= $e['tahapan'] ?>" class="form-control" id="tahapan" placeholder="Masukan tahapan ke berapa">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                    </form>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </div>
        </div>
    <?php endforeach ?>
    <!-- /.modal -->
    <?php
    $no = $this->uri->segment('4') + 1;
    foreach ($jenjang as $data) : ?>
        <div class="modal fade" id="modal_delete_data<?= $data['id'] ?>">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Hapus Data Yang Dipilih ?</h4>
                    </div>
                    <div class="modal-body">
                        <div class="text-center">
                            <button type="button" style="width: 45%;" class="btn btn-default mr-md" data-dismiss="modal">Close</button>
                            <a href="<?= base_url() ?>admin/ekosistem/jenjang_delete/<?= $data['id'] ?>" style="width: 45%;" class="btn btn-danger">Hapus</a>
                        </div>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    <?php endforeach ?>
</div>
<!-- /.content-wrapper -->