<div class="align-items-center p-3 my-3 rounded shadow-sm" >
    <div class="container">
        <div class="lh-1">
            <h1 class="h3 mt-2 mb-4 text-green-cus lh-1">Konsep STIFIn - Thinking</h1>
        </div>
    </div>
</div>

<!-- About Start -->
<div class="container-xxl mb-5 py-4 article-cs">
    <div class="container">
        <div class="row g-4 align-items-center">
            <div class="col-lg-12 wow " data-wow-delay="0.5s">
                <div class="h-100">
                <?php
                    foreach ($thingking as $f) : ?>
                        <div class="col-lg-8 wow fadeInUp mb-4" data-wow-delay="0.1s">
                            <img class="img-fluid-konsep" src="<?= base_url() ?>assets/upload/images/<?= $f['gambar']; ?>" alt="" />
                        </div>
                        <p>
                            <?= $f['konten']; ?></p>
                    <?php endforeach ?>
                </div>
            </div>
            <!-- FAQs Start -->
            <div class="container-xxl">

                <div class=" mx-auto wow fadeInUp" data-wow-delay="0.1s">
                    <h5 class="">FAQs Konsep STIFIn</h5>
                    <p class="mb-4">Dijawab oleh penemu STIFIn</p>
                </div>
                <div class="row">
                    <div class="col-lg-10">
                        <div class="accordion" id="accordionExample">
                            <div class="accordion-item wow fadeInUp" data-wow-delay="0.1s">
                                <h2 class="accordion-header" id="headingOne">
                                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        STIFIn menggunakan konsep universal dan bukan dari Islam? Apa sudah mendapat legalitas dari MUI?
                                    </button>
                                </h2>
                                <div id="collapseOne" class="accordion-collapse collapse show" aria-labelledby="headingOne" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        Konsep STIFIn memang merupakan konsep universal bukan berasal dari dan diperuntukkan pada agama tertentu. Oleh karena itu, legalitas dari MUI belum diperlukan. Legalitas yang diperlukan justru berupa legalitas ilmiah. Untuk itu, dalam rangka memperkuat riset internal yang sudah kami lakukan (lebih dari 10 tahun) juga diperlukan riset independen.
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item wow fadeInUp" data-wow-delay="0.2s">
                                <h2 class="accordion-header" id="headingTwo">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                        Apakah STIFIn bias digunakan untuk menyusun kompetensi berikut dengan KPI nya?
                                    </button>
                                </h2>
                                <div id="collapseTwo" class="accordion-collapse collapse" aria-labelledby="headingTwo" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        Dalam menentukan job title, kompetensi, dan KPI jika mempertimbangkan STIFIn akan menjadi lebih sempurna. Contoh: job title, kompetensi, dan KPI untuk orang keuangan sebaiknya menggunakan kualifikasi kinerja sebagaimana tipe Si yang harus efisien, cepat, dan teliti. Rujukan STIFIn tersebut selain mempermudah memilih orangnya juga mempermudah dalam pembinaan dan evaluasinya.
                                    </div>
                                </div>
                            </div>
                            <div class="accordion-item wow fadeInUp" data-wow-delay="0.3s">
                                <h2 class="accordion-header" id="headingThree">
                                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                        Koq masih banyak yang belum diriset tetapi sudah dibisniskan?
                                    </button>
                                </h2>
                                <div id="collapseThree" class="accordion-collapse collapse" aria-labelledby="headingThree" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        Hal terpenting bahwa STIFIn telah memulai meriset secara internal lebih dari 10 tahun dan langsung menguji cobakan kepada klien-klien sampai akhirnya bisa seakurat seperti sekarang. Untuk mengukuhkan bahwa hasil Tes STIFIn akurat kami menggunakan tim riset independen (lihat FAQ pada no.2 di atas). Berarti keperluan primer untuk berbisnis sudah kami penuhi yaitu kami menjual tes yang sudah terbukti akurasinya. Bahwa kemudian banyak hal lain yang masih perlu diriset itu lebih merupakan keperluan sekunder. Dan tentu saja memerlukan partisipasi banyak pihak.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- About End -->