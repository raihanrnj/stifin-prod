<div class="align-items-center p-3 my-3 rounded shadow-sm" >
    <div class="container">
        <div class="lh-1">
            <h1 class="h3 mt-2 mb-4 text-green-cus lh-1">Gallery Video</h1>
        </div>
    </div>
</div>
<div class="container-xxl mb-5 py-5">
    <div class="container">
        <section class="image-grid">
            <div class="container-xxl">
                <div class="row gy-4">
                    <?php foreach ($video as $vd) : ?>
                        <div class="col-12 col-sm-6 col-md-4">
                            <div class="videowrapper">
                                <iframe width="560" height="315" src="https://www.youtube.com/embed/<?= $vd['link_embed'] ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                            </div>
                        </div>
                    <?php endforeach ?>
                </div>
            </div>
        </section>
    </div>
</div>