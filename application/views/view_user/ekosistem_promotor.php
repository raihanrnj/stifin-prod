<div class="align-items-center p-3 my-3 rounded shadow-sm">
  <div class="container">
    <div class="lh-1">
      <h1 class="h3 mt-2 mb-4 text-green-cus lh-1">Promotor</h1>
    </div>
  </div>
</div>
<!-- About Start -->
<div class="container-xxl bg-light">
    <div class="container py-5 mb-3">
        <div class="row g-5 align-items-center mt-2 mb-5">
            <?php
            foreach ($about_promotor_section as $m) : ?>
                <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.1s">
                    <img class="img-fluid" src="<?= base_url() ?>assets/upload/images/<?= $m['gambar']; ?>" alt="">
                </div>
                <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.5s">
                    <div class="h-100 article-cs">
                        <h1 class="display-6 mb-4"><?= $m['judul'] ?></h1>
                        <p><?= $m['deskripsi'] ?></p>
                    <?php endforeach ?>
                   
                    </div>
                </div>
        </div>
    </div>
</div>
<!-- About End -->

<!-- sejarah -->

<div class="container-xxl mb-5 pb-5" x-data="{ posts: [] }" x-init="posts = await (await axios.get('<?= base_url() ?>/api/jenjang')).data">
<h2 style="margin-left:5%;margin-top:5%;">Jenjang Pendidikan Promotor</h2>
    <div class="container timeline-line pb-5">
        <template x-data="{number:1}" x-for="(color, index) in posts.data">
            <div x-bind:class="((index % 2) == 0)?'row g-5 justify-content-start my-1': 'row g-5 justify-content-end my-1'">
                <div class="col-md-6">
                    <div class="card timeline-card">
                        <div class="card-body p-4">
                            <!-- <h5 x-text="color.judul" class="mb-3">20 Maret 2023, 12:30 - 14:30</h5> -->
                            <content x-html="color.konten">
                            </content>
                        </div>
                    </div>
                </div>
            </div>
        </template>
    </div>
    <div class="text-center" x-data="{ page: 1 }">
        <template x-if="posts.pageCount !== page">
            <button x-on:click="page++; posts.data = posts.data.concat(((await axios.get('<?= base_url() ?>/api/jenjang?page='+page)).data).data);" class="btn btn-outline-primary btn-sm py-2 px-4 mt-3">Load More</button>
        </template>
    </div>
  
    <a class="btn btn-outline-primary btn-sm py-2 px-4 mt-3" href="https://api.whatsapp.com/send?phone=6281283101051&text=Hallo%20Kak,%20Saya%20mau%20join%20Promotor" style="margin-left:5%;margin-top:5%;">Tertarik Menjadi Promotor</a>


    <template x-if="(posts.data.length == 0)">
        <h5 class="text-center mt-5">Data Kosong</h5>
    </template>
</div>


<!-- Cabang -->
<div class="container-xxl mb-5 py-5" x-data="{ search: '', posts: [], resultSearch: [] }">
<h2 style="margin-left:5%;margin-top:5%;">Daftar Promotor STIFIn</h2>
  <div class="d-flex justify-content-end">
    <div class="col-md-3 col-6">
      <div class="input-group mb-3">
        <span class="input-group-text bg-white border-end-0" id="basic-addon1">
          <svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M18.9998 19L13.8028 13.803M13.8028 13.803C15.2094 12.3964 15.9996 10.4887 15.9996 8.49949C15.9996 6.51029 15.2094 4.60256 13.8028 3.19599C12.3962 1.78941 10.4885 0.999207 8.49931 0.999207C6.51011 0.999207 4.60238 1.78941 3.19581 3.19599C1.78923 4.60256 0.999023 6.51029 0.999023 8.49949C0.999023 10.4887 1.78923 12.3964 3.19581 13.803C4.60238 15.2096 6.51011 15.9998 8.49931 15.9998C10.4885 15.9998 12.3962 15.2096 13.8028 13.803Z" stroke="#4B5563" stroke-width="1.5" stroke-linecap="round" stroke-linejoin="round" />
          </svg>
        </span>
        <input placeholder="Cari Area" x-model="search" type="text" class="form-control" aria-label="Username" aria-describedby="basic-addon1">
      </div>
    </div>
  </div>

  <template x-if="search  === ''">
    <div class="accordion accordion-flush" id="accordionFlushExample" x-init="posts = await (await axios.get('https://api.stifin.id/promotor/promotorlist')).data">
      <template x-for="(color, index) in posts.data">
        <div class="accordion-item">
          <h2 class="accordion-header rounded" id="flush-headingTwo">
            <button x-text="color.Area" class="accordion-button collapsed rounded" type="button" data-bs-toggle="collapse" x-bind:data-bs-target="'#flush-collapseTwo' + index" aria-expanded="false" aria-controls="flush-collapseTwo">
              -
            </button>
          </h2>

          <div x-bind:id="'flush-collapseTwo' + index" class="accordion-collapse collapse border border-2" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
            <div class="row justify-content-start px-2 warp-collapse">
              <template x-for="(subdata, index) in color.List">
                <div class="col-lg-6">
                  <content class="card border-2 border-top border-0 rounded-top py-3">
                    <div class="card-body row text-dark">
                      <div class="col-md-3 col-6">
                        <strong x-text="subdata.Pemilik"></strong>
                      </div>
                      <div class="col-md-3 col-6">
                        <span x-text="subdata.KodeID"></span>
                      </div>
                      <div class="col-md-3 col-6 mt-md-0 mt-3">
                        <span x-text="subdata.Area"></span>
                      </div>
                      <div class="col-md-3 col-6 mt-md-0 mt-3">
                        <span x-text="subdata.Phone"></span>
                      </div>
                    </div>
                  </content>
                </div>
              </template>
            </div>
          </div>
        </div>
      </template>
    </div>
  </template>

  <template x-if="search  !== ''">
    <div class="accordion accordion-flush" id="accordionFlushExample" x-effect="resultSearch = await (await axios.get(`https://api.stifin.id/promotor/promotorArea/`+search)).data">
      <template x-if="resultSearch?.data.length > 0" x-effect="console.log(resultSearch)">
        <div class="accordion-item">
          <h2 class="accordion-header rounded" id="flush-headingTwo">
            <button x-text="resultSearch.data[0].Area" class="accordion-button rounded" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
              -
            </button>
          </h2>

          <div id="flush-collapseTwo" class="accordion-collapse collapse show border border-2" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
            <div class="row justify-content-start px-2 warp-collapse">
              <template x-for="(color, index) in resultSearch.data">
                <div class="col-lg-6">
                  <content class="card border-2 border-top border-0 rounded-top py-3">
                    <div class="card-body row text-dark">
                      <div class="col-md-3 col-6">
                        <strong x-text="color.Pemilik"></strong>
                      </div>
                      <div class="col-md-3 col-6">
                        <span x-text="color.KodeID"></span>
                      </div>
                      <div class="col-md-3 col-6">
                        <span x-text="color.Area"></span>
                      </div>
                      <div class="col-md-3 col-6">
                        <span x-text="color.Phone"></span>
                      </div>
                    </div>
                  </content>
                </div>

              </template>
            </div>
          </div>
        </div>
      </template>
      <template x-if="resultSearch?.data == false">
        <h3 class="">Not Found</h3>
      </template>
    </div>
  </template>
</div>
</div>
<!-- Cabang End -->
