<div class="align-items-center p-3 mt-3 rounded shadow-sm">
    <div class="container">
        <div class="lh-1">
            <h1 class="h3 mt-2 mb-4 text-green lh-1">Artikel Terbaru STIFIn</h1>
        </div>
    </div>
</div>
<!-- Post Start -->
<div class="container-xxl mb-5">
    <div class="container py-5">
        <div class="row g-4">
        <?php foreach ($artikel as $ar) : ?>
                <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
                    <div class="service-items shadow-sm p-0">
                        <img style="width:100%" src="<?= base_url() ?>assets/upload/images/<?= $ar['gambar']; ?>" alt="">
                        <div class="p-card-post">
                            <h5 class="mb-3"><?= $ar['judul'] ?></h5>
                            <p><?= word_limiter(strip_tags($ar['konten']), 18) ?></p>
                            <!-- <a href="<?= base_url('artikel/detail/') . $ar['id_artikel'] ?>">Read More <i class="fa fa-arrow-right ms-2"></i></a> -->
                            <a href="<?= base_url() ?>page/artikel/<?= $ar['slug'] ?> ">Read More <i class="fa fa-arrow-right ms-2"></i></a>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</div>
<!-- Post End -->