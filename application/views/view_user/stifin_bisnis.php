<div class="align-items-center p-3 my-3 rounded shadow-sm" >
    <div class="container">
        <div class="lh-1">
            <h1 class="h3 mt-2 mb-4 text-green-cus lh-1">STIFIn Bisnis</h1>
        </div>
    </div>
</div>

<!-- About Start -->
<div class="container-xxl mb-5 py-4 article-cs">
    <div class="container">
        <div class="row g-4 align-items-center">
            <div class="col-lg-12 wow " data-wow-delay="0.5s">
                <div class="h-100">
                    <div class="col-lg-8 wow mb-5" data-wow-delay="0.1s">
                        <img class="img-fluid" src="<?= base_url() ?>assets/img/Picture2.jpg" alt="" />
                    </div>
                    <h5 class="mb-3">STIFIn Bisnis</h5>
                    <p>
                        STIFIn Bisnis merupakan unit usaha Yayasan STIFIn untuk memperluas infrastruktur Cabang STIFIn baik di Indonesia ataupun di luar negeri.
                    </p>
                    <p>
                        STIFIn Bisnis hadir menciptakan bisnis investasi dengan berbasis untung tanpa rugi. Dari tahun 2009 hingga saat ini, volume pengetesan melonjak naik 2x lipat pada tahun 2019 dibandingkan 2018. Tes STIFIn karena simpel, akurat, dan aplikatif menjadikan STIFIn sebagai market leader dalam dunia tes personaliti dan kecerdasan.
                    </p>
                    <p>
                        Keunggulan bisnis STIFIn lainnya adalah infrastruktur bisnis STIFIn sudah kuat, terbukti STIFIn telah berhasil membina 33 Cabang, 3 diantaranya berada di luar negeri dan memiliki 2.292 Promotor yang telah tersebar di Indonesia dan mancanegara.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- About End -->