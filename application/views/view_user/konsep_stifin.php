<div class="align-items-center p-3 mt-3 rounded shadow-sm" >
    <div class="container">
        <div class="lh-1">
            <h1 class="h3 mt-2 mb-4 text-green-cus lh-1">Konsep STIFIn</h1>
        </div>
    </div>
</div>
    <!-- slider -->
    <div class="container-xxl mb-5 bg-light py-5 text-center">
        <div class="text-center mx-auto mt-3 wow fadeInUp" data-wow-delay="0.1s" style="max-width: 500px">
            <!-- <h1 class="display-6">Testimonials</h1>
            <p class="text-primary fs-5 mb-4">Kata Mereka Tentang STIFIn</p> -->
        </div>
        <div class="container">
            <div class="row">
                <!-- <h2>Automatic Slideshow</h2>
                <p>Change image every 2 seconds:</p> -->
                <div class="slideshow-container">
                    <?php
                    foreach ($slider_section as $y) : ?>
                        <div class="mySlides fade-slide"> 
                        <div class="numbertext">1 / 3</div>
                        <img src="<?= base_url() ?>assets/upload/images/<?= $y['gambar']; ?>" style="width:60%">
                        <div class="text-slide"><?= $y['teks'] ?></div>
                        </div>
                    <?php endforeach ?>
                </div>
                <br>

                <div style="text-align:center">
                    <span class="dot-slider"></span>
                    <span class="dot-slider"></span>
                    <span class="dot-slider"></span>
                </div>
            </div>
        </div>
    </div>




<!-- About Start -->
<div class="container-xxl py-4 article-cs">
    <div class="container">
        <div class="row g-5 align-items-center">
            <div class="col-lg-12 wow " data-wow-delay="0.5s">
                <div class="h-100 article-cs">
                    <?php
                    foreach ($konsep_stifin as $b) : ?>
                        <!-- <div class="col-lg-8 wow fadeInUp mb-4" data-wow-delay="0.1s">
                            <img class="img-fluid-konsep" src="<= base_url() ?>assets/upload/images/<= $b['gambar']; ?>" alt="" />
                        </div> -->
                        <p>
                            <?= $b['konten']; ?></p>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- About End -->