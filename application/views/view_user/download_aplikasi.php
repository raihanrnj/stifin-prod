<div class="align-items-center p-3 my-3 rounded shadow-sm" >
    <div class="container">
        <div class="lh-1">
            <h1 class="h3 mt-2 mb-4 text-green-cus lh-1">Download Aplikasi</h1>
        </div>
    </div>
</div>
<!-- Post Start -->
<div class="container-xxl mb-5">
    <div class="container py-5">
        <div class="row g-4">
            <?php foreach ($aplikasi as $data) : ?>
                <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
                    <div class="shadow-sm p-0" style="height: 100%  ;">
                        <!-- <img style="width:100%" src="<?= base_url() ?>assets/img/exe_image.png" alt=""> -->
                        <i class="fa <?= $data['icon'] ?> fa-3x fa-col" aria-hidden="true" style="font-size: 5em;margin-left: 30%;"></i>
                        <div class="p-card-post">
                            <h5 class="mb-3"><?= $data['judul'] ?></h5>
                            <p><?= $data['deskripsi'] ?></p>
                            <a class="btn btn-outline-primary btn-sm py-2 px-4 mt-3" href="<?= base_url() ?>admin/download/aplikasi_download/<?= $data['file'] ?> ">Download <i class="fa fa-arrow-down ms-2"></i></a>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</div>
<!-- Post End -->