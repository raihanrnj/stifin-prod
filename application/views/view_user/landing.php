<!-- Header Start -->
<div class="container-fluid hero-header py-5" style="height: 40em;" id="myDIV">
    <div class="container py-5" style="padding-top: 5.5rem !important;">
        <div class="row g-5 align-items-center">
            <div class="col-lg-7 animated fadeIn">
                <?php foreach ($hero_section as $data) : ?>
                    <h1 class="display-4 mb-3 animated slideInDown fn-landing"><?= $data['judul'] ?></h1>
                    <p class="animated slideInDown" style="color:rgb(255, 255, 255);animation-duration: 3s;"><?= $data['deskripsi'] ?></p>
                    <a href="<?= base_url() ?>page/view/konsep_stifin" class="btn btn-primary py-2 px-4 mt-3 animated slideInDown">Explore More</a>
            </div>
            <div class="col-lg-5 animated fadeIn">
                <!-- <img class="img-fluid animated pulse infinite" style="animation-duration: 3s;" src="<= base_url() ?>assets/upload/images/<= $data['gambar']; ?>" alt=""> -->
            </div>
        <?php endforeach ?>
        </div>
    </div>
</div>
<!-- Header End -->

<!-- About Start -->
<div class="container-xxl bg-white">
    <div class="container py-5 mb-3">
        <div class="row g-5 align-items-center mt-2 mb-5">
            <?php
            foreach ($about_section as $m) : ?>
                <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.1s">
                    <img class="img-fluid" src="<?= base_url() ?>assets/upload/images/<?= $m['gambar']; ?>" alt="">
                </div>
                <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.5s">
                    <div class="h-100 article-cs">
                        <h1 class="display-6 mb-4"><?= $m['judul'] ?></h1>
                        <p><?= $m['deskripsi'] ?></p>
                    <?php endforeach ?>
                    <a class="btn btn-primary py-2 px-4 mt-3" href="<?= base_url() ?>page/view/konsep_stifin">Read More</a>
                    </div>
                </div>
        </div>
    </div>
</div>
<!-- About End -->

<!-- Keunggulan STIFIn -->
<div class="container-xxl bg-light py-5 text-center">
    <div class="text-center mx-auto wow fadeInUp" data-wow-delay="0.1s" style="max-width: 500px">
        <h1 class="display-6">Keunggulan STIFIn</h1>
        <p class="text-primary fs-5 mb-5">Apa yang membuat STIFIn berbeda</p>
    </div>

    <div class="container">
        <div class="row article-cs">
            <?php
            foreach ($keunggulan_section as $x) : ?>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <div class="box-part shadow-sm text-center">
                        <i class="fa <?= $x['icon'] ?> fa-3x fa-col" aria-hidden="true"></i>
                        <div class="title mt-4">
                            <h4><?= $x['judul'] ?></h4>
                        </div>
                        <div class="text-keunggulan">
                            <p><?= $x['deskripsi'] ?></p>
                        </div>
                        <a href="<?= base_url() ?>page/view/konsep_stifin">Learn More</a>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</div>
<!-- Keunggulan End -->

<!-- Video Start -->
<div class="container-xxl bg-white py-5 mb-5 text-center">
    <div class="text-center mx-auto wow fadeInUp mt-4" data-wow-delay="0.1s" style="max-width: 800px">
        <h1 class="display-6">Sebenarnya Apa Itu Konsep STIFIn</h1>
        <p class="text-primary fs-5 mb-5">Anda akan mengenal lebih dekat Konsep STIFIn setelah menonton video ini</p>
    </div>
    <?php
    foreach ($video_section as $x) : ?>
        <center>
            <div class="col-12 col-sm-12 col-md-8 wow fadeInUp" data-wow-delay="0.1s">
                <div class="videowrapper">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/<?= $x['link_embed'] ?>" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe>
                </div>
            </div>
        </center>
    <?php endforeach ?>
</div>
<!-- Video End -->

<!-- Counting Start -->
<section id="statistic" class="statistic-section one-page-section" x-data="{ dataJml: [] }" x-init="dataJml = await (await axios.get('https://api.stifin.id/promotor/promotorJml')).data">
    <div class="container">
        <div class="row text-center">
            <div class="col-xs-12 col-md-4">
                <div class="counter">
                    <i class="fa fa-sitemap fa-2x stats-icon"></i>
                    <h2 class="timer count-title count-number" x-text="dataJml.JmlCab.toLocaleString(undefined, { minimumFractionDigits: 0, maximumFractionDigits: 3 })">0</h2>
                    <div class="stats-line-black"></div>
                    <p class="stats-text">Cabang</p>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="counter">
                    <i class="fa fa-users fa-2x stats-icon"></i>
                    <h2 class="timer count-title count-number" x-text="dataJml.JmlPro.toLocaleString(undefined, { minimumFractionDigits: 0, maximumFractionDigits: 3 })">0</h2>
                    <div class="stats-line-black"></div>
                    <p class="stats-text">Promotor</p>
                </div>
            </div>
            <div class="col-xs-12 col-md-4">
                <div class="counter">
                    <i class="fa fa-puzzle-piece fa-2x stats-icon"></i>
                    <h2 class="timer count-title count-number" x-text="dataJml.JmlTes.toLocaleString(undefined, { minimumFractionDigits: 0, maximumFractionDigits: 3 })">0</h2>
                    <div class="stats-line-black"></div>
                    <p class="stats-text">Total Tes</p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Counting Start End -->

<!-- Statistik Start -->
<div class="container-xxl py-5 text-center">
    <div class="text-center mx-auto wow fadeInUp" data-wow-delay="0.1s" style="max-width: 500px">
        <h1 class="display-6">Statistik</h1>
        <p class="text-primary fs-5 mb-5">Rangkuman Statistik STIFIn</p>
    </div>
    <div class="container table-responsive" x-data="{ posts: [] }" x-init="posts = await (await axios.get('https://api.stifin.or.id/api/server/MyTopTen/WEB')).data">
        <div class="row g-4">
            <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
                <div class="bg-white p-3 shadow-sm border border-1 border-light">
                    <h5 class="mb-4 mt-2 text-dark">Top Five Transaksi ID</h5>
                    <div class="divider-card"></div>
                    <table class="table table-striped table-hover">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Jumlah</th>
                            </tr>
                        </thead>
                        <tbody>
                            <template x-data="{number:1}" x-for="(color, index) in posts.dataProID">
                                <tr>
                                    <th x-text="index+1" scope="row"></th>
                                    <td x-text="color.Nama"></td>
                                    <td x-text="color.Jml"></td>
                                </tr>
                            </template>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
                <div class="bg-white p-3 shadow-sm border border-1 border-light">
                    <h5 class="mb-4 mt-2 text-dark">Top Ten Cabang</h5>
                    <div class="divider-card"></div>
                    <table class="table table-striped table-hover">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Jumlah</th>
                            </tr>
                        </thead>
                        <tbody>
                            <template x-data="{number:1}" x-for="(color, index) in posts.dataCab">
                                <tr>
                                    <th x-text="index+1" scope="row"></th>
                                    <td x-text="color.Nama"></td>
                                    <td x-text="color.Jml"></td>
                                </tr>
                            </template>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
                <div class="bg-white p-3 shadow-sm border border-1 border-light">
                    <h5 class="mb-4 mt-2 text-dark">Top Ten Promotor</h5>
                    <div class="divider-card"></div>
                    <table class="table table-striped table-hover">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nama</th>
                                <th scope="col">Jumlah</th>
                            </tr>
                        </thead>
                        <tbody>
                            <template x-data="{number:1}" x-for="(color, index) in posts.dataPro">
                                <tr>
                                    <th x-text="index+1" scope="row"></th>
                                    <td x-text="color.Nama"></td>
                                    <td x-text="color.Jml"></td>
                                </tr>
                            </template>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Statistik Ened -->

<!-- Cabang Start -->
<div class="container-xxl bg-light">
    <div class="container py-5">
        <div class="row g-5 align-items-center mt-2 mb-5">
            <?php
            foreach ($cabang_section as $ca) : ?>
                <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.1s">
                    <img class="img-fluid" src="<?= base_url() ?>assets/upload/images/<?= $ca['gambar']; ?>" alt="">
                </div>
                <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.5s">
                    <div class="h-100 article-cs">
                        <h1 class="display-6 mb-4"><?= $ca['judul'] ?></h1>
                        <p><?= $ca['deskripsi'] ?></p>
                    <?php endforeach ?>
                    <a class="btn btn-primary py-2 px-4 mt-3" href="<?= base_url() ?>page/view/detail_cabang">Read More</a>
                    </div>
                </div>
        </div>
    </div>
</div>
<!-- Cabang End -->

<!-- Promotor Start -->
<div class="container-xxl bg-white">
    <div class="container py-5 mb-3">
        <div class="row g-5 align-items-center mt-2 mb-5">
            <?php
            foreach ($promotor_section as $pro) : ?>
                <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.5s">
                    <div class="h-100 article-cs">
                        <h1 class="display-6 mb-4"><?= $pro['judul'] ?></h1>
                        <p><?= $pro['deskripsi'] ?></p>
                    <?php endforeach ?>
                    <a class="btn btn-primary py-2 px-4 mt-3" href="<?= base_url() ?>page/view/detail_promotor">Read More</a>
                    </div>
                </div>
                <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.1s">
                    <img class="img-fluid" src="<?= base_url() ?>assets/upload/images/<?= $pro['gambar']; ?>" alt="">
                </div>
        </div>
    </div>
</div>
<!-- Promotor End -->

<!-- Testimoni Start -->
<div class="container-xxl bg-light py-5 text-center">
    <div class="text-center mx-auto mt-3 wow fadeInUp" data-wow-delay="0.1s" style="max-width: 500px">
        <h1 class="display-6">Testimonials</h1>
        <p class="text-primary fs-5 mb-5">Kata Mereka Tentang STIFIn</p>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-12 testimonials testimonial-carousel owl-theme owl-carousel">
                <?php
                foreach ($testimoni_section as $y) : ?>
                    <div class="p-4 bg-white shadow-sm m-1">
                        <p class="text-muted text-start pb-4"><?= $y['testimoni'] ?></p>
                        <hr>
                        <div class="d-flex align-items-center pt-3">
                            <div class="author-img" style="margin-right: 20px;"> <img src="<?= base_url() ?>assets/upload/images/<?= $y['gambar']; ?>"> </div>
                            <div class="text-start">
                                <h5 class="m-0"><?= $y['nama'] ?></h5>
                                <p class="m-0 small font-medium text-muted"><?= $y['pekerjaan'] ?></p>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
        </div>
    </div>
</div>
<!-- Testimoni End -->

<!-- Client Start -->
<div class="container-xxl bg-white py-5 mb-4 mt-2 text-center">
    <div class="text-center mx-auto mt-3 wow fadeInUp" data-wow-delay="0.1s" style="max-width: 500px">
        <h1 class="display-6">Our Happy Clients</h1>
        <p class="text-primary fs-5 mb-4">Meet Our Happy Clients</p>
    </div>
    <div class="container">
        <section class="customer-logos slider">
            <?php foreach ($partners as $x) : ?>
                <div class="slide">
                    <img src="<?= base_url() ?>assets/upload/images/<?= $x['logo']; ?>" />
                </div>
            <?php endforeach ?>
        </section>
    </div>
</div>
<!-- Client End -->

<!-- Post Start -->
<div class="container-xxl py-5">
    <div class="container py-5">
        <div class="text-center mx-auto wow fadeInUp" data-wow-delay="0.1s" style="max-width: 500px;">
            <h1 class="display-6">Recent Post</h1>
            <p class="text-primary fs-5 mb-5">Dapatkan Informasi Terbaru Dari STIFIn</p>
        </div>
        <div class="wow fadeInUp text-center">
            <div class="row g-4">
                <?php foreach ($artikel as $ar) : ?>
                    <div class="col-lg-4 col-md-6 text-start" data-wow-delay="0.1s">
                        <div class="service-items shadow-sm p-0">
                            <img style="width:100%" src="<?= base_url() ?>assets/upload/images/<?= $ar['gambar']; ?>" alt="">
                            <div class="p-card-post">
                                <h5 class="mb-3"><?= $ar['judul'] ?></h5>
                                <p><?= word_limiter(strip_tags($ar['konten']), 18) ?></p>
                                <a href="<?= base_url() ?>page/artikel/<?= $ar['slug'] ?> ">Read More <i class="fa fa-arrow-right ms-2"></i></a>
                            </div>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
            <a class="btn btn-primary py-2 px-4 mt-5" href="<?= base_url() ?>page/artikel">Read More Article</a>
        </div>
    </div>
</div>
<!-- Post End -->