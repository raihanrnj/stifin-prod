<img src="<?= base_url() ?>assets/upload/images/BG_LP_PROMOTORS.png" alt="" class="image-header">
<!--- Apa Itu Promotor Start -->
<div class="container-xxl bg-white">
    <div class="container py-5 mb-3">
        <div class="row g-5 align-items-center mt-2 mb-5">
            <?php foreach ($detail_promotor as $detp) : ?>
                <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.1s">
                    <img class="img-fluid" src="<?= base_url() ?>assets/upload/images/<?= $detp['gambar']; ?>" alt="">
                </div>
                <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.5s">
                    <div class="h-100 article-cs">
                        <h1 class="display-6 mb-4"><?= $detp['judul'] ?></h1>
                        <p><?= $detp['deskripsi'] ?></p>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</div>
<!--- Apa Itu Promotor End -->

<!-- Benefit Promotor Start -->
<div class="container-xxl bg-light">
    <div class="container pb-3 mb-3">
        <div class="row g-5 align-items-center mt-2 mb-5">
            <?php foreach ($dp_benefit as $dpben) : ?>
                <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.5s">
                    <div class="h-100 article-cs">
                        <h1 class="display-6 mb-4"><?= $dpben['judul'] ?></h1>
                        <p><?= $dpben['deskripsi'] ?></p>
                    </div>
                </div>
                <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.1s">
                    <img class="img-fluid" src="<?= base_url() ?>assets/upload/images/<?= $dpben['gambar']; ?>" alt="">
                </div>
            <?php endforeach ?>
        </div>
    </div>
</div>
<!-- Benefit Promotor End -->

<div class="container-xxl my-5 pt-4">
    <h3 class="mb-4 d-flex flex-column align-items-center justify-content-between">Roadmap Promotor STIFIn</h3>
</div>

<!-- promotor -->
<div class="container-xxl mb-5 pb-5" x-data="{ posts: [] }" x-init="posts = await (await axios.get('<?= base_url() ?>/api/promotor')).data">
    <div class="container timeline-line pb-5">
        <template x-data="{number:1}" x-for="(mydata, index) in posts.data">
            <div x-bind:class="((index % 2) == 0)?'row g-5 justify-content-start my-1': 'row g-5 justify-content-end my-1'">
                <div class="col-md-6">
                    <div class="card timeline-card">
                        <div class="card-body p-4">
                            <content x-html="mydata.konten">
                            </content>
                        </div>
                    </div>
                </div>
            </div>
        </template>
    </div>
    <div class="text-center" x-data="{ page: 1 }">
        <template x-if="posts.pageCount !== page">
            <button x-on:click="page++; posts.data = posts.data.concat(((await axios.get('<?= base_url() ?>/api/promotor?page='+page)).data).data);" class="btn btn-outline-primary btn-sm py-2 px-4 mt-3">Load More</button>
        </template>
    </div>

    <template x-if="(posts.data.length == 0)">
        <h5 class="text-center mt-5">Data Kosong</h5>
    </template>
</div>
<!-- promotor End -->
<div class="container-xxl bg-white py-5 mt-4 text-center">
    <div class="text-center mx-auto wow fadeInUp" data-wow-delay="0.1s" style="max-width: 500px">
        <h1 class="display-6">Alur Tes STIFIn</h1>
        <p class="text-primary fs-5 mb-5 pb-4">Berikut adalah langkah-langkah tes STIFIn</p>
    </div>
    <div class="container mt-5">
        <div class="row g-0">
            <?php foreach ($alur_tes as $pot) : ?>
                <div class="col-md-2 mb-2" style="text-align: center;">
                    <i class="fa <?= $pot['icon'] ?> fa-2x text-success"></i>
                    <h5 class="mt-3"><?= $pot['judul'] ?></h5>
                    <p class="text-dark"><?= $pot['deskripsi'] ?></p>
                </div>
                <div class="col-sm-auto d-xl-flex mx-auto justify-content-xl-center align-items-xl-center"><i class="fa fa-long-arrow-right fa-lg text-dark"></i></div>
            <?php endforeach ?>
        </div>
    </div>
</div>

<div class="container my-5 text-white" style="background-color: #009879;">
    <div class="p-5 d-flex flex-column align-items-center justify-content-between">
        <h2 class="mb-4 mt-2 font-weight-bold text-light">Tertarik Menjadi Promotor STIFIn?</h2>
        <a class="btn btn-light py-2 px-4 mt-3" href="https://api.whatsapp.com/send?phone=6281283101051">Hubungi STIFIn</a>
    </div>
</div>
