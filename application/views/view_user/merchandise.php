<div class="align-items-center p-3 my-3 rounded shadow-sm" >
    <div class="container">
        <div class="lh-1">
            <h1 class="h3 mt-2 mb-4 text-green-cus lh-1">Merchandise</h1>
        </div>
    </div>
</div>
<div class="container-xxl mb-5">
    <div class="container py-5">
        <div class="row g-4">
            <?php foreach ($merchandise as $m) : ?>
                <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
                    <div class="shadow-sm p-0" style="height: 100%  ;">
                        <img style="width:100%" src="<?= base_url() ?>assets/upload/images/<?= $m['gambar']; ?>" alt="">
                        <div class="p-card-post">
                            <h5 class="mb-3"><?= $m['judul'] ?></h5>
                            <p><?= $m['deskripsi'] ?></p>
                            <a class="btn btn-outline-primary btn-sm py-2 px-4 mt-3" href="https://api.whatsapp.com/send?phone=6281283101051&text=Hallo%20Kak,%20Saya%20mau%20order%20<?= $m['judul'] ?>">Beli Sekarang</a>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>