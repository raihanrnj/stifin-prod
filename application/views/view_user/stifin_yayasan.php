<div class="align-items-center p-3 my-3 rounded shadow-sm">
    <div class="container">
        <div class="lh-1">
            <h1 class="h3 mt-2 mb-4 text-green-cus lh-1">Yayasan STIFIn</h1>
        </div>
    </div>
</div>

<!-- About Start -->
<div class="container-xxl mb-5 py-4 article-cs">
    <div class="container">
        <div class="row g-4 align-items-center">
            <div class="col-lg-12 wow " data-wow-delay="0.5s">
                <div class="h-100">
                    <?php
                    foreach ($yayasan_stifin as $c) : ?>
                        <div class="col-lg-8 wow fadeInUp mb-4" data-wow-delay="0.1s">
                            <img class="img-fluid-konsep" src="<?= base_url() ?>assets/upload/images/<?= $c['gambar']; ?>" alt="" style="max-width: 30% !important;" />
                        </div>
                        <p>
                            <?= $c['konten']; ?></p>
                    <?php endforeach ?>
                </div>
            </div>
            <h2>Roadmap Sejarah STIFIn</h2>
        </div>
    </div>
</div>
<!-- About End -->

<!-- sejarah -->
<div class="container-xxl mb-5 pb-5" x-data="{ posts: [] }" x-init="posts = await (await axios.get('<?= base_url() ?>/api/sejarah')).data">
    <div class="container timeline-line pb-5">
        <template x-data="{number:1}" x-for="(color, index) in posts.data">
            <div x-bind:class="((index % 2) == 0)?'row g-5 justify-content-start my-1': 'row g-5 justify-content-end my-1'">
                <div class="col-md-6">
                    <div class="card timeline-card">
                        <div class="card-body p-4">
                            <!-- <h5 x-text="color.judul" class="mb-3">20 Maret 2023, 12:30 - 14:30</h5> -->
                            <content x-html="color.konten">
                            </content>
                        </div>
                    </div>
                </div>
            </div>
        </template>
    </div>
    <div class="text-center" x-data="{ page: 1 }">
        <template x-if="posts.pageCount !== page">
            <button x-on:click="page++; posts.data = posts.data.concat(((await axios.get('<?= base_url() ?>/api/sejarah?page='+page)).data).data);" class="btn btn-outline-primary btn-sm py-2 px-4 mt-3">Load More</button>
        </template>
    </div>

    <template x-if="(posts.data.length == 0)">
        <h5 class="text-center mt-5">Data Kosong</h5>
    </template>
</div>
<!-- sejarah End -->