<div class="align-items-center p-3 my-3 rounded shadow-sm" >
    <div class="container">
        <div class="lh-1">
            <h1 class="h3 mt-2 mb-4 text-green-cus lh-1">Kontak</h1>
        </div>
    </div>
</div>

<!-- About Start -->
<div class="container-xxl mb-5 py-4 article-cs">
    <div class="container">
        <div class="row g-4 align-items-center">
            <div class="col-lg-12 wow " data-wow-delay="0.5s">
                <div class="h-100">
                    <h4 class="mb-2">STIFIn Genetic Intelligence</h4>
                    <p>
                        Kenali diri anda lebih dalam dengan STIFIn
                    </p>
                    <div class="mt-5">
                        <p><i class="fas fa-building"></i> Griya STIFIn LT. I, Bumi Makmur IV, Jl. Poksai H3 Pondok Gede, Bekasi.</p>
                        <p><i class="fas fa-phone-alt"></i> 021 844 350</p>
                        <p><i class="fas fa-envelope"></i> admin@stifin.com</p>
                    </div>
                    <br>
                    <div class="h-100 d-lg-inline-flex align-items-center d-none">
                        <a class="btn btn-square rounded-circle bg-light text-primary me-2" href=""><i class="fab fa-facebook-f"></i></a>
                        <a class="btn btn-square rounded-circle bg-light text-primary me-2" href=""><i class="fab fa-twitter"></i></a>
                        <a class="btn btn-square rounded-circle bg-light text-primary me-2" href=""><i class="fab fa-linkedin-in"></i></a>
                        <a class="btn btn-square rounded-circle bg-light text-primary me-0" href=""><i class="fab fa-youtube"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- About End -->