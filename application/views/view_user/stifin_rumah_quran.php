<div class="align-items-center p-3 my-3 rounded shadow-sm" >
    <div class="container">
        <div class="lh-1">
            <h1 class="h3 mt-2 mb-4 text-green-cus lh-1"> Rumah Qur'an STIFIn</h1>
        </div>
    </div>
</div>

<!-- About Start -->
<div class="container-xxl mb-5 py-4 article-cs">
    <div class="container">
        <div class="row g-4 align-items-center">
            <div class="col-lg-12 wow " data-wow-delay="0.5s">
                <div class="h-100">
                    <?php
                    foreach ($stifin_rqs as $e) : ?>
                        <div class="col-lg-8 wow fadeInUp mb-4" data-wow-delay="0.1s">
                            <img class="img-fluid-konsep" style="max-width: 30% !important;" src="<?= base_url() ?>assets/upload/images/<?= $e['gambar']; ?>" alt="" />
                        </div>
                        <p>
                            <?= $e['konten']; ?></p>
                    <?php endforeach ?>
                    <a class="btn btn-outline-primary btn-sm py-2 px-4 mt-3" href="https://rq.stifin.id/">Web Rumah Qur'an STIFIn</a>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- About End -->