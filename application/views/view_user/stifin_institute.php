<div class="align-items-center p-3 my-3 rounded shadow-sm" >
    <div class="container">
        <div class="lh-1">
            <h1 class="h3 mt-2 mb-4 text-green-cus lh-1">STIFIn Institute</h1>
        </div>
    </div>
</div>

<!-- About Start -->
<div class="container-xxl mb-5 py-4 article-cs">
    <div class="container">
        <div class="row g-4 align-items-center">
            <div class="col-lg-12 wow " data-wow-delay="0.5s">
                <div class="h-100">
                    <?php
                    foreach ($stifin_institute as $d) : ?>
                        <div class="col-lg-8 wow fadeInUp mb-4" data-wow-delay="0.1s">
                            <img class="img-fluid-konsep" src="<?= base_url() ?>assets/upload/images/<?= $d['gambar']; ?>" alt="" style="max-width: 30% !important;" />
                        </div>
                        <p>
                            <?= $d['konten']; ?></p>
                    <?php endforeach ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- About End -->