<div class="container-xxl bg-light">
    <div class="container py-5">
        <div class="row g-5 align-items-center mt-2 mb-3">
            <?php foreach ($solver_about as $m) : ?>
                <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.1s">
                    <img class="img-fluid" src="<?= base_url() ?>assets/upload/images/<?= $m['gambar']; ?>" alt="">
                </div>
                <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.5s">
                    <div class="h-100 article-cs">
                        <h1 class="display-6 mb-4"><?= $m['judul'] ?></h1>
                        <p><?= $m['deskripsi'] ?></p>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</div>
<div class="container-xxl my-5 pt-4">
    <h3 class="mb-4 d-flex flex-column align-items-center justify-content-between">Roadmap Solver STIFIn</h3>
</div>
<div class="container-xxl mb-5 pb-5" x-data="{ posts: [] }" x-init="posts = await (await axios.get('<?= base_url() ?>/api/solver')).data">
    <div class="container timeline-line pb-5">
        <template x-data="{number:1}" x-for="(mydata, index) in posts.data">
            <div x-bind:class="((index % 2) == 0)?'row g-5 justify-content-start my-1': 'row g-5 justify-content-end my-1'">
                <div class="col-md-6">
                    <div class="card timeline-card">
                        <div class="card-body p-4">
                            <content x-html="mydata.konten">
                            </content>
                        </div>
                    </div>
                </div>
            </div>
        </template>
    </div>
    <div class="text-center" x-data="{ page: 1 }">
        <template x-if="posts.pageCount !== page && posts?.data">
            <button x-on:click="page++; posts.data = posts.data.concat(((await axios.get('<?= base_url() ?>/api/solver?page='+page)).data).data);" class="btn btn-outline-primary btn-sm py-2 px-4 mt-3">Load More</button>
        </template>
    </div>

    <template x-if="(posts?.data.length == 0)">
        <h5 class="text-center mt-5">Data Kosong</h5>
    </template>
</div>

<div class="container-xxl bg-white">
    <div class="text-center mx-auto wow fadeInUp" data-wow-delay="0.1s" style="max-width: 500px">
        <h1 class="display-6">Solver STIFIn</h1>
        <p class="text-primary fs-5">Kenalan dengan para Solver STIFIn berpengalaman</p>
    </div>
    <div class="container py-5">
        <div class="row g-5 align-items-center mt-2 mb-3">
            <div class="row">
                <?php foreach ($solver_stifin as $index => $datas) : ?>
                    <div class="col-lg-3 mb-4">
                        <div class="row no-gutters">
                            <div class="col-md-12">
                                <img src="<?= base_url() ?>assets/upload/images/<?= $datas['gambar']; ?>" class="img-fluid" alt="">
                            </div>
                            <div class="col-md-12">
                                <div class="pt-2">
                                    <h5 class="mt-4 font-weight-medium mb-0"><?= $datas['nama']; ?></h5>
                                    <p class="mb-0"><?= $datas['branding']; ?></p>
                                    <div class="mb-1">
                                        <fieldset class="rating">
                                            <input type="radio" id="star5" name="rating-<?= $index ?>" value="5" <?php if ($datas['rating'] == "5") echo 'checked'; ?> /><label class="full" for="star5" title="Awesome - 5 stars"></label>
                                            <input type="radio" id="star4half" name="rating-<?= $index ?>" value="4 and a half" <?php if ($datas['rating'] == "4 and a half") echo 'checked'; ?> /><label class="half" for="star4half" title="Pretty good - 4.5 stars"></label>
                                            <input type="radio" id="star4" name="rating-<?= $index ?>" value="4" <?php if ($datas['rating'] == "4") echo 'checked'; ?> /><label class="full" for="star4" title="Pretty good - 4 stars"></label>
                                            <input type="radio" id="star3half" name="rating-<?= $index ?>" value="3 and a half" <?php if ($datas['rating'] == "3 and a half") echo 'checked'; ?> /><label class="half" for="star3half" title="Meh - 3.5 stars"></label>
                                            <input type="radio" id="star3" name="rating-<?= $index ?>" value="3" <?php if ($datas['rating'] == "3") echo 'checked'; ?> /><label class="full" for="star3" title="Meh - 3 stars"></label>
                                            <input type="radio" id="star2half" name="rating-<?= $index ?>" value="2 and a half" <?php if ($datas['rating'] == "2 and a half") echo 'checked'; ?> /><label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label>
                                            <input type="radio" id="star2" name="rating-<?= $index ?>" value="2" <?php if ($datas['rating'] == "2") echo 'checked'; ?> /><label class="full" for="star2" title="Kinda bad - 2 stars"></label>
                                            <input type="radio" id="star1half" name="rating-<?= $index ?>" value="1 and a half" <?php if ($datas['rating'] == "1 and a half") echo 'checked'; ?> /><label class="half" for="star1half" title="Meh - 1.5 stars"></label>
                                            <input type="radio" id="star1" name="rating-<?= $index ?>" value="1" <?php if ($datas['rating'] == "1") echo 'checked'; ?> /><label class="full" for="star1" title="Sucks big time - 1 star"></label>
                                            <input type="radio" id="starhalf" name="rating-<?= $index ?>" value="half" <?php if ($datas['rating'] == "half") echo 'checked'; ?> /><label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label>
                                        </fieldset>
                                    </div>
                                    <br>
                                    <div class="mt-3 text-red"><?= $datas['deskripsi']; ?></div>
                                </div>
                            </div>
                        </div>
                        <!-- Row -->
                    </div>
                <?php endforeach ?>
            </div>
        </div>
    </div>
</div>
<div class="container mb-5 text-white" style="background-color: #009879;">
    <div class="p-5 d-flex flex-column align-items-center justify-content-between">
        <h2 class="mb-4 mt-2 font-weight-bold text-center text-light">Tertarik Menjadi Solver STIFIn?</h2>
        <a class="btn btn-light py-2 px-4 mt-3" href="https://api.whatsapp.com/send?phone=6281283101051">Hubungi STIFIn</a>
    </div>
</div>