<div class="align-items-center p-3 my-3 rounded shadow-sm" >
    <div class="container">
        <div class="lh-1">
            <h1 class="h3 mt-2 mb-4 text-green-cus lh-1">About Us</h1>
        </div>
    </div>
</div>

<!-- About Start -->
<div class="container-xxl mb-5 py-4 article-cs">
    <div class="container">
        <div class="row g-4 align-items-center">
            <div class="col-lg-12 wow " data-wow-delay="0.5s">
                <div class="h-100">
                    <div class="col-lg-8 wow mb-5" data-wow-delay="0.1s">
                        <img class="img-fluid" src="<?= base_url() ?>assets/img/stf-img.png" alt="" />
                    </div>
                    <h5 class="mb-3">Tentang STIFIn</h5>
                    <p>
                        Tes STIFIn dilakukan dengan cara men-scan kesepuluh ujung jari Anda, mengambil waktu tidak lebih dari 1 menit.
                    <p>
                        Sidik jari yang membawa informasi tentang komposisi susunan syaraf tersebut kemudian dianalisa dan dihubungkan dengan belahan otak tertentu yang dominan berperan sebagai sistem-operasi dan sekaligus menjadi mesin kecerdasan anda.
                    </p>
                    <p>
                        Lebih jauh lagi dari susunan syaraf tersebut masih dapat diprediksi letak dominasi mesin kecerdasan Anda ada di irisan otak berwarna putih atau di irisan otak berwarna abu-abu, sehingga mesin kecerdasan anda memiliki kemudi introvert (i) atau ekstrovert (e).
                    </p>
                    <p>
                        Mesin kecerdasan dengan kemudi i atau e inilah yang kemudian disebut sebagai personaliti. Mesin kecerdasan dan personaliti ini keduanya genetik tidak pernah berubah sepanjang hidup anda. Namun demikian terdapat banyak lagi personaliti-personaliti lain yang tidak genetik dan dapat berubah.
                    </p>
                    <p>
                        Salah satu contohnya seperti sifat (traits) Introvert (disingkat I besar) dan Ekstrovert (disingkat E besar). I dan E sebagai sifat (traits) memang dapat berubah, sedangkan i dan e sebagai kemudi mesin kecerdasan adalah genetik dan tidak dapat berubah. Tes STIFIn mampu menyimpulkan mana yang genetik dengan terlebih dahulu menyingkirkan semua variabel yang bisa berubah (karena tidak genetik).
                    </p>
                    <p>
                        Konsep STIFIn diperkenalkan oleh Farid Poniman dengan mengkompilasi dari teori-teori psikologi, neuroscience, dan ilmu SDM. Prinsip besarnya mengacu kepada konsep kecerdasan tunggal dari Carl Gustaav Jung.
                    </p>
                    <p>
                        Tes ini memiliki reliabilitas yang tinggi. Tes yang dilakukan mulai dari usia anak tiga tahun hasilnya akan sama jika diulang kembali pada usia berapapun.
                    </p>

                    <br>
                    <h5 class="mb-4">Kontak STIFIn</h5>
                    <h6>Office :</h6>
                    <p>Griya STIFIn LT. I, Bumi Makmur IV, Jl. Poksai H3 Pondok Gede, Bekasi, Phone: 021 844 350</p>
                    <h6>Leaders:</h6>
                    <p>Andrei : 0856 9153 6170</p>
                    <h6>Marketting:</h6>
                    <p>Dedy : 0812 2088 2800</p>
                    <h6>IT Support :</h6>
                    <p>Nurul dan Aeni : 0821 1621 6565</p>
                    <br>

                </div>
            </div>
        </div>
    </div>
</div>
<!-- About End -->