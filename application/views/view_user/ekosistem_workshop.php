<div class="align-items-center p-3 my-3 rounded shadow-sm">
  <div class="container">
    <div class="lh-1">
      <h1 class="h3 mt-2 mb-4 text-green-cus lh-1">Workshop</h1>
    </div>
  </div>
</div>
<!-- Workshop -->
<div class="container-xxl mb-5 py-5" x-data="{ posts: [] }" x-init="posts = await (await axios.get('<?= base_url() ?>/api/workshop')).data">
  <div class="container timeline-line pb-5">
    <template x-data="{number:1}" x-for="(color, index) in posts.data">
      <div x-bind:class="((index % 2) == 0)?'row g-5 justify-content-start my-1': 'row g-5 justify-content-end my-1'">
        <div class="col-md-6">
          <div class="card timeline-card">
            <div class="card-body p-4">
              <h5 x-text="color.judul" class="mb-3">20 Maret 2023, 12:30 - 14:30</h5>
              <h6 x-text="color.tanggal_workshop" class="mb-3">20 Maret 2023, 12:30 - 14:30</h6>
              <content x-html="color.konten">
              </content>
              <a class="btn btn-outline-primary btn-sm py-2 px-4 mt-3" style="margin-top: 0px !important;" href="https://api.whatsapp.com/send?phone=6281283101051&text=Hallo%20Kak,%20Saya%20mau%20join%20workshop">Join Workshop</a>
            </div>
          </div>
        </div>
      </div>
    </template>
  </div>
  <div class="text-center" x-data="{ page: 1 }">
    <template x-if="posts.pageCount !== page">
      <button x-on:click="page++; posts.data = posts.data.concat(((await axios.get('<?= base_url() ?>/api/workshop?page='+page)).data).data);" class="btn btn-outline-primary btn-sm py-2 px-4 mt-3">Load More</button>
    </template>
  </div>

  <template x-if="(posts.data.length == 0)">
    <h2 class="text-center">Comming Soon</h2>
  </template>
</div>
<!-- Workshop End -->