<div class="align-items-center p-3 my-3 rounded shadow-sm" >
    <div class="container">
        <div class="lh-1">
            <h1 class="h3 mt-2 mb-4 text-green-cus lh-1">Download Audio</h1>
        </div>
    </div>
</div>
<!-- Post Start -->
<div class="container-xxl mb-5">
    <div class="container py-5">
        <div class="row g-4">
            <?php foreach ($audio as $a) : ?>
                <div class="col-lg-4 col-md-6 wow fadeInUp" data-wow-delay="0.1s">
                    <div class="shadow-sm p-0">
                        <div class="p-card-post">
                            <h6 class="mb-3"><?= $a['judul'] ?></h6>
                            <audio id="blast" src="<?= base_url() ?>assets/upload/audio/<?= $a['audio'] ?>" controls></audio>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
</div>
<!-- Post End -->