<div class="align-items-center p-3 my-3 rounded shadow-sm" >
    <div class="container">
        <div class="lh-1">
            <h1 class="h3 mt-2 mb-4 text-green-cus lh-1">Gallery Photo</h1>
        </div>
    </div>
</div>
<div class="container-xxl mb-5 py-5">
    <div class="container">
        <section class="image-grid">
            <div class="container-xxl">
                <div class="row gy-4">
                    <div class="col-12 col-sm-6 col-md-4">
                        <figure>
                            <a class="d-block" href="">
                                <img width="1920" height="1280" src="https://assets.codepen.io/162656/ireland1.jpg" class="img-fluid" alt="Ring of Kerry, County Kerry, Ireland" data-caption="Ring of Kerry, County Kerry, Ireland">
                            </a>
                        </figure>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4">
                        <figure>
                            <a class="d-block" href="">
                                <img width="1920" height="1280" src="https://assets.codepen.io/162656/ireland2.jpg" class="img-fluid" alt="Fintown, Ireland" data-caption="Fintown, Ireland">
                            </a>
                        </figure>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4">
                        <figure>
                            <a class="d-block" href="">
                                <img width="1920" height="1280" src="https://assets.codepen.io/162656/ireland3.jpg" class="img-fluid" alt="Anne Street, Dublin, Ireland" data-caption="Anne Street, Dublin, Ireland">
                            </a>
                        </figure>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4">
                        <figure>
                            <a class="d-block" href="">
                                <img width="1920" height="1280" src="https://assets.codepen.io/162656/ireland4.jpg" class="img-fluid" alt="Doonagore Castle, Doolin, Ireland" data-caption="Doonagore Castle, Doolin, Ireland">
                            </a>
                        </figure>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4">
                        <figure>
                            <a class="d-block" href="">
                                <img width="1920" height="1280" src="https://assets.codepen.io/162656/ireland5.jpg" class="img-fluid" alt="Connemara National Park, Letterfrack, Ireland" data-caption="Connemara National Park, Letterfrack, Ireland">
                            </a>
                        </figure>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4">
                        <figure>
                            <a class="d-block" href="">
                                <img width="1920" height="1280" src="https://assets.codepen.io/162656/ireland6.jpg" class="img-fluid" alt="Galway, Ireland" data-caption="Galway, Ireland">
                            </a>
                        </figure>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4">
                        <figure>
                            <a class="d-block" href="">
                                <img width="1920" height="1280" src="https://assets.codepen.io/162656/ireland7.jpg" class="img-fluid" alt="Connemara National Park, Letterfrack, Ireland" data-caption="Connemara National Park, Letterfrack, Ireland">
                            </a>
                        </figure>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4">
                        <figure>
                            <a class="d-block" href="">
                                <img width="1920" height="1280" src="https://assets.codepen.io/162656/ireland8.jpg" class="img-fluid" alt="The Forty Foot, Dublin 18, Ireland" data-caption="The Forty Foot, Dublin 18, Ireland">
                            </a>
                        </figure>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4">
                        <figure>
                            <a class="d-block" href="">
                                <img width="1920" height="1280" src="https://assets.codepen.io/162656/ireland9.jpg" class="img-fluid" alt="Coliemore Harbour, Dublin, Ireland" data-caption="Coliemore Harbour, Dublin, Ireland">
                            </a>
                        </figure>
                    </div>
                </div>
            </div>
        </section>

        <div class="modal lightbox-modal" id="lightbox-modal" tabindex="-1">
            <div class="modal-dialog modal-fullscreen">
                <div class="modal-content">
                    <button type="button" class="btn-close btn-close-white" data-bs-dismiss="modal" aria-label="Close"></button>
                    <div class="modal-body">
                        <div class="container-fluid p-0">
                            <!-- JS content here -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>