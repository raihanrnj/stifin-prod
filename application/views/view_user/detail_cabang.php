<img src="<?= base_url() ?>assets/upload/images/BG_LP_CABANG.png" alt="" class="image-header">
<!--- Apa Itu Promotor Start -->
<div class="container-xxl bg-light">
    <div class="container py-5">
        <div class="row g-5 align-items-center mt-2 mb-3">
            <?php foreach ($detail_cabang as $m) : ?>
                <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.1s">
                    <img class="img-fluid" src="<?= base_url() ?>assets/upload/images/<?= $m['gambar']; ?>" alt="">
                </div>
                <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.5s">
                    <div class="h-100 article-cs">
                        <h1 class="display-6 mb-4"><?= $m['judul'] ?></h1>
                        <p><?= $m['deskripsi'] ?></p>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</div>
<!--- Apa Itu Cabang End -->

<!-- Benefit Cabang Start -->
<div class="container-xxl bg-white">
    <div class="container mb-5 mb-3">
        <div class="row g-5 align-items-center mt-2 mb-5">
            <?php foreach ($potensi_usaha as $pot) : ?>
                <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.5s">
                    <div class="h-100 article-cs">
                        <h1 class="display-6 mb-4"><?= $pot['judul'] ?></h1>
                        <p><?= $pot['deskripsi'] ?></p>
                    </div>
                </div>
                <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.1s">
                    <img class="img-fluid" src="<?= base_url() ?>assets/upload/images/<?= $pot['gambar']; ?>" alt="">
                </div>
            <?php endforeach ?>
        </div>
    </div>
</div>
<!-- Benefit Cabang End -->

<!-- Keunggulan STIFIn -->
<div class="container-xxl bg-light py-5 text-center">
    <div class="text-center mx-auto wow fadeInUp" data-wow-delay="0.1s" style="max-width: 500px">
        <h1 class="display-6">Sumber Profit</h1>
        <p class="text-primary fs-5 mb-5">Apa yang membuat STIFIn berbeda</p>
    </div>

    <div class="container pb-3">
        <div class="row article-cs">
            <?php foreach ($sumber_profit as $sum) : ?>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 mb-3">
                    <div class="service-items shadow-sm p-4 text-center bg-body" style="min-height: 100%;">
                        <i class="fa <?= $sum['icon'] ?> fa-3x fa-col text-success" aria-hidden="true"></i>
                        <div class="title mt-4">
                            <h5><?= $sum['judul'] ?></h5>
                        </div>
                        <div class="text-keunggulan">
                            <p><?= $sum['deskripsi'] ?></p>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</div>
<!-- Keunggulan End -->

<div class="container-xxl bg-white py-5 mt-4 text-center">
    <div class="text-center mx-auto wow fadeInUp" data-wow-delay="0.1s" style="max-width: 500px">
        <h1 class="display-6">Alur Tes STIFIn</h1>
        <p class="text-primary fs-5 mb-5 pb-4">Berikut adalah langkah-langkah tes STIFIn</p>
    </div>
    <div class="container mt-5">
        <div class="row g-0">
            <?php
            $jumlah_data = count($alur_tes); // Menentukan jumlah data
            $i = 1; // Inisialisasi variabel iterasi
            foreach ($alur_tes as $pot) :
            ?>
                <div class="col-md-2 mb-2" style="text-align: center;">
                    <i class="fa <?= $pot['icon'] ?> fa-2x text-success"></i>
                    <h5 class="mt-3"><?= $pot['judul'] ?></h5>
                    <p class="text-dark"><?= $pot['deskripsi'] ?></p>
                </div>
                <?php
                // Cek apakah iterasi saat ini adalah iterasi terakhir
                if ($i == $jumlah_data) {
                ?>
                    <div class="arah col-sm-auto d-xl-flex mx-auto justify-content-xl-center align-items-xl-center hide-arrow"><i class="fa fa-long-arrow-right fa-lg text-dark"></i></div>
                <?php
                } else { // Jika bukan iterasi terakhir, tampilkan icon panah 
                ?>
                    <div class="arah col-sm-auto d-xl-flex mx-auto justify-content-xl-center align-items-xl-center"><i class="fa fa-long-arrow-right fa-lg text-dark"></i></div>
                <?php
                }
                $i++; // Update variabel iterasi
                ?>
            <?php endforeach ?>
        </div>
    </div>

</div>

<!-- Pilihan Cabang STIFIn -->
<div class="container-xxl bg-light py-5">
    <div class="text-center mx-auto wow fadeInUp" data-wow-delay="0.1s" style="max-width: 500px">
        <h1 class="display-6">Pilihan Cabang STIFIn</h1>
        <p class="text-primary fs-5 mb-5">Pilihan yang dapat anda pilih</p>
    </div>

    <div class="container pb-3">
        <div class="row article-cs">
            <?php foreach ($pilihan_cabang as $pil) : ?>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 mb-4">
                    <div class="service-items shadow-sm p-4 bg-body" style="min-height: 100%;">
                        <div class="text-center mt-2">
                            <i class="fa <?= $pil['icon'] ?> fa-3x fa-col text-success" aria-hidden="true"></i>
                            <div class="title mt-4">
                                <h5><?= $pil['judul'] ?></h5>
                            </div>
                        </div>
                        <div class="text-keunggulan">
                            <p><?= $pil['deskripsi'] ?></p>
                        </div>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</div>
<!-- Pilihan Cabang STIFIn End -->

<div class="container my-5 text-white" style="background-color: #009879;">
    <div class="p-5 d-flex flex-column align-items-center justify-content-between">
        <h2 class="mb-4 mt-2 font-weight-bold text-center text-light">Tertarik Menjadi Cabang STIFIn?</h2>
        <a class="btn btn-light py-2 px-4 mt-3" href="https://api.whatsapp.com/send?phone=6281283101051">Hubungi STIFIn</a>
    </div>
</div>

<div class="modal fade" id="modal_cabang" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <!-- <div class="modal-dialog modal-dialog-scrollable modal-md"> -->
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Update Cabang STIFIn</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <!-- <img class="img-fluid" src="<?= base_url() ?>assets/upload/images/41.png" alt=""> -->
                <div class="container">
                    <div class="row">
                        <div class="slideshow-container">
                            <?php
                            foreach ($popup as $po) : ?>
                                <div class="mySlides fade-slide">
                                    <div class="numbertext">1 / 2</div>
                                    <img src="<?= base_url() ?>assets/upload/images/<?= $po['gambar']; ?>" class="img-fluid">
                                </div>
                            <?php endforeach ?>
                        </div>
                        <br>
                        <div style="text-align:center" class="mt-3">
                            <span class="dot-slider"></span>
                            <span class="dot-slider"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Tutup</button>
                <a href="https://api.whatsapp.com/send?phone=6281283101051" target="_blank" type="button" class="btn btn-primary">Selengkapnya</a>
            </div>
        </div>
    </div>
</div>