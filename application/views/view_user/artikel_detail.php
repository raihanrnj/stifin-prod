<!-- About Start -->
<div class="align-items-center p-3 my-2 rounded shadow-sm" >
    <div class="container">
        <div class="lh-1">
            <p class=" mb-2 text-green"><a href="<?= base_url() ?>page/artikel"><b style="color: green;">Artikel</b></a> / <?= $artikel['judul']; ?></p>
        </div>
    </div>
</div>

<div class="container-xxl mb-5 py-4 article-cs">
    <div class="container">
        <div class="g-4 align-items-center">
            <div class="row">
                <div class="col-lg-1"></div>
                <div class="col-lg-10 wow " data-wow-delay="0.5s">
                    <div class="h-10">
                        <div class="col-lg-10" data-wow-delay="0.1s">
                            <img class="img-fluid-konsep" src="<?= base_url() ?>assets/upload/images/<?= $artikel['gambar']; ?>" alt="" style="max-width: 100% !important;" />
                            <h2 class="mt-5"><?= $artikel['judul']; ?></h2>
                        </div>
                        <p class="mt-4"><?= $artikel['konten']; ?></p>
                    </div>
                </div>
                <div class="col-lg-1"></div>
            </div>
        </div>
    </div>
</div>
<!-- About End -->