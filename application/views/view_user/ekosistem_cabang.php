<div class="align-items-center p-3 my-3 rounded shadow-sm">
  <div class="container">
    <div class="lh-1">
      <h1 class="h3 mt-2 mb-4 text-green-cus lh-1">Cabang STIFIn</h1>
    </div>
  </div>
</div>
<!-- Cabang -->
<!--- Apa Itu Promotor Start -->
<div class="container-xxl bg-light">
    <div class="container py-5">
        <div class="row g-5 align-items-center mt-2 mb-3">
            <?php foreach ($detail_cabang as $m) : ?>
                <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.1s">
                    <img class="img-fluid" src="<?= base_url() ?>assets/upload/images/<?= $m['gambar']; ?>" alt="">
                </div>
                <div class="col-lg-6 wow fadeInUp" data-wow-delay="0.5s">
                    <div class="h-100 article-cs">
                        <h1 class="display-6 mb-4"><?= $m['judul'] ?></h1>
                        <p><?= $m['deskripsi'] ?></p>
                    </div>
                </div>
            <?php endforeach ?>
        </div>
    </div>
</div>
<!--- Apa Itu Promotor End -->

<div class="container-xxl mb-5 py-5" x-data="{ search: '', posts: [], resultSearch: [] }">
<h2 style="margin-left:5%;margin-top:5%;margin-bottom:5%;">Daftar Cabang STIFIn</h2>
  <div class="accordion accordion-flush" id="accordionFlushExample" x-init="posts =  await (await axios.get('<?= base_url() ?>/api/cabang')).data">
    <template x-for="(color, index) in posts.data">
      <div class="accordion-item">
        <h2 class="accordion-header rounded" id="flush-headingTwo">
          <button x-text="color.provinsi" class="accordion-button collapsed rounded" type="button" data-bs-toggle="collapse" x-bind:data-bs-target="'#flush-collapseTwo' + index" aria-expanded="false" aria-controls="flush-collapseTwo">
            Jawa Timur
          </button>
        </h2>
        <div x-bind:id="'flush-collapseTwo' + index" class="accordion-collapse collapse border border-2" aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
          <div class="row justify-content-start px-2 warp-collapse">
            <template x-for="(subdata, index) in color.data">
              <div class="col-lg-6">
                <content class="card border-2 border-top border-0 rounded-top py-3">
                  <div class="card-body d-lg-flex">
                    <div class="col-lg-8">
                      <h6 class="mb-3" x-text="subdata.nama_cabang"></h6>
                      <p x-text="subdata.alamat"></p>
                      <span>Open - Close : <span x-text="subdata.jam_operasional"></span></span>
                    </div>
                    <div class="col-lg-4 mt-lg-0 mt-4 d-flex justify-content-lg-center align-items-center">
                      <div class="d-flex gap-2">
                        <a x-bind:href="'tel:' + subdata.no_hp" class="rounded-circle p-2 bg-light d-block">
                          <div class="position-relative icon-container">
                            <div class="icon-wrap">
                              <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M0.6875 4.0625C0.6875 10.2755 5.7245 15.3125 11.9375 15.3125H13.625C14.0726 15.3125 14.5018 15.1347 14.8182 14.8182C15.1347 14.5018 15.3125 14.0726 15.3125 13.625V12.596C15.3125 12.209 15.0493 11.8715 14.6735 11.7778L11.3563 10.9482C11.0263 10.8657 10.6798 10.9895 10.4765 11.261L9.749 12.2308C9.5375 12.5128 9.17225 12.6372 8.8415 12.5157C7.61366 12.0643 6.49861 11.3514 5.57359 10.4264C4.64856 9.50139 3.93566 8.38634 3.48425 7.1585C3.36275 6.82775 3.48725 6.4625 3.76925 6.251L4.739 5.5235C5.01125 5.32025 5.13425 4.973 5.05175 4.64375L4.22225 1.3265C4.1766 1.14402 4.07129 0.982027 3.92304 0.866253C3.77479 0.750478 3.5921 0.687562 3.404 0.6875H2.375C1.92745 0.6875 1.49822 0.86529 1.18176 1.18176C0.86529 1.49822 0.6875 1.92745 0.6875 2.375V4.0625Z" stroke="black" stroke-linecap="round" stroke-linejoin="round" />
                              </svg>
                            </div>
                          </div>
                        </a>
                        <a x-bind:href="subdata.maps" target="_blank" class="rounded-circle p-2 bg-light d-block">
                          <div class="position-relative icon-container">
                            <div class="icon-wrap">
                              <svg width="14" height="16" viewBox="0 0 14 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M9.25 6.875C9.25 7.47174 9.01295 8.04403 8.59099 8.46599C8.16903 8.88795 7.59674 9.125 7 9.125C6.40326 9.125 5.83097 8.88795 5.40901 8.46599C4.98705 8.04403 4.75 7.47174 4.75 6.875C4.75 6.27826 4.98705 5.70597 5.40901 5.28401C5.83097 4.86205 6.40326 4.625 7 4.625C7.59674 4.625 8.16903 4.86205 8.59099 5.28401C9.01295 5.70597 9.25 6.27826 9.25 6.875Z" stroke="black" stroke-linecap="round" stroke-linejoin="round" />
                                <path d="M12.625 6.875C12.625 12.2315 7 15.3125 7 15.3125C7 15.3125 1.375 12.2315 1.375 6.875C1.375 5.38316 1.96763 3.95242 3.02252 2.89752C4.07742 1.84263 5.50816 1.25 7 1.25C8.49184 1.25 9.92258 1.84263 10.9775 2.89752C12.0324 3.95242 12.625 5.38316 12.625 6.875Z" stroke="black" stroke-linecap="round" stroke-linejoin="round" />
                              </svg>
                            </div>
                          </div>
                        </a>
                        <a x-bind:href="subdata.website" target="_blank" class="rounded-circle p-2 bg-light d-block">
                          <div class="position-relative icon-container">
                            <div class="icon-wrap">
                              <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M8 14.75C9.49625 14.7499 10.9501 14.2529 12.1333 13.337C13.3164 12.4211 14.1619 11.1382 14.537 9.68975M8 14.75C6.50376 14.7499 5.04991 14.2529 3.86673 13.337C2.68356 12.4211 1.83806 11.1382 1.463 9.68975M8 14.75C9.86375 14.75 11.375 11.7275 11.375 8C11.375 4.2725 9.86375 1.25 8 1.25M8 14.75C6.13625 14.75 4.625 11.7275 4.625 8C4.625 4.2725 6.13625 1.25 8 1.25M14.537 9.68975C14.6758 9.14975 14.75 8.5835 14.75 8C14.7519 6.83908 14.4529 5.69749 13.8823 4.6865M14.537 9.68975C12.5369 10.7985 10.2869 11.3786 8 11.375C5.71311 11.3786 3.46314 10.7985 1.463 9.68975M1.463 9.68975C1.32106 9.13773 1.2495 8.56998 1.25 8C1.25 6.79625 1.565 5.66525 2.11775 4.6865M8 1.25C9.19718 1.2495 10.3729 1.56752 11.4066 2.17142C12.4403 2.77533 13.2948 3.64338 13.8823 4.6865M8 1.25C6.80282 1.2495 5.62706 1.56752 4.59336 2.17142C3.55966 2.77533 2.70523 3.64338 2.11775 4.6865M13.8823 4.6865C12.2493 6.10095 10.1604 6.87811 8 6.875C5.7515 6.875 3.695 6.05 2.11775 4.6865" stroke="black" stroke-linecap="round" stroke-linejoin="round" />
                              </svg>
                            </div>
                          </div>
                        </a>
                      </div>
                    </div>
                  </div>
                </content>
              </div>
            </template>
          </div>
        </div>
      </div>
    </template>
  </div>
  <template x-if="(posts.data.length == 0)">
    <h2 class="text-center">Comming Soon</h2>
  </template>
</div>
</div>
<!-- Cabang End -->