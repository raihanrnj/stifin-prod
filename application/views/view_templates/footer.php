<!-- Footer Start -->
<a class="whats-app" href="https://api.whatsapp.com/send?phone=6281283101051&text=Hallo%20Kak.." target="_blank">
    <i class="fa fa-whatsapp my-float"></i>
</a>
<div class="container-fluid bg-green footer pt-5 wow fadeIn" data-wow-delay="0.1s">
    <?php
    foreach ($footer_section as $x) : ?>
        <div class="container py-5 color-white">
            <div class="row g-5">
                <div class="col-md-6">
                    <!-- <h1 class="text-primary mb-3">STIFIn</h1> -->
                    <img class="m-0 img-fluid-logo" src="<?= base_url() ?>assets/img/logo_stifin.png" alt="">
                    <br>
                    <span><?= $x['tagline'] ?></span>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h5 class="mb-4 color-white">Get In Touch</h5>
                    <p><i class="fa fa-map me-3"></i><?= $x['alamat'] ?></p>
                    <p><i class="fa fa-phone me-3"></i><?= $x['telp'] ?></p>
                    <p><i class="fa fa-envelope me-3"></i><?= $x['email'] ?></p>
                </div>
                <div class="col-lg-3 col-md-6">
                    <h5 class="mb-4 color-white">Follow Us</h5>
                    <div class="d-flex">
                        <a class="btn btn-square rounded-circle me-1" href="<?= $x['tw'] ?>"><i class="fab fa-twitter"></i></a>
                        <a class="btn btn-square rounded-circle me-1" href="<?= $x['fb'] ?>"><i class="fab fa-facebook-f"></i></a>
                        <a class="btn btn-square rounded-circle me-1" href="<?= $x['yt'] ?>"><i class="fab fa-youtube"></i></a>
                        <a class="btn btn-square rounded-circle me-1" href="<?= $x['lk'] ?>"><i class="fab fa-linkedin-in"></i></a>
                        <a class="btn btn-square rounded-circle me-1" href="<?= $x['ig'] ?>"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid copyright">
            <div class="container color-white">
                <div class="row">
                    <div class="col-md-6 text-center text-md-start mb-3 mb-md-0">
                        &copy; <a href="#">STIFIn.com</a>, All Right Reserved.
                    </div>
                    <div class="col-md-6 text-center text-md-end">
                        <!--/*** This template is free as long as you keep the footer author’s credit link/attribution link/backlink. If you'd like to use the template without the footer author’s credit link/attribution link/backlink, you can purchase the Credit Removal License from "https://htmlcodex.com/credit-removal". Thank you for your support. ***/-->
                        Designed By <a href="https://raihanrnj.medium.com">STIFIn Dev</a> Distributed With <a href="https://stifin.com">Heart</a>
                    </div>
                </div>
            </div>
        </div>
    <?php endforeach ?>
</div>
<!-- Footer End -->


<!-- Back to Top -->
<a href="#" class="btn btn-lg btn-primary btn-lg-square rounded-circle back-to-top"><i class="bi bi-arrow-up"></i></a>


<!-- JavaScript Libraries -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
<script src="<?= base_url() ?>assets/lib/wow/wow.min.js"></script>
<script src="<?= base_url() ?>assets/lib/easing/easing.min.js"></script>
<script src="<?= base_url() ?>assets/lib/waypoints/waypoints.min.js"></script>
<script src="<?= base_url() ?>assets/lib/owlcarousel/owl.carousel.min.js"></script>
<script src="<?= base_url() ?>assets/lib/counterup/counterup.min.js"></script>
<script src='https://vjs.zencdn.net/5.4.6/video.min.js'></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/videojs-youtube/2.6.0/Youtube.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.6.0/slick.js"></script>

<!-- Template Javascript -->
<script src="<?= base_url() ?>assets/js/main.js"></script>
<script src="<?= base_url() ?>assets/js/custom.js"></script>
<script>
    $('.testimonials').owlCarousel({
        loop: false,
        margin: 20,
        nav: false,
        dots: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 2
            }
        }
    })
    $(".customer-logos").slick({
        slidesToShow: 6,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 1500,
        arrows: false,
        dots: false,
        pauseOnHover: false,
        responsive: [{
                breakpoint: 768,
                settings: {
                    slidesToShow: 4,
                },
            },
            {
                breakpoint: 520,
                settings: {
                    slidesToShow: 3,
                },
            },
        ],
    });
    $('.count-number').counterUp({
        delay: 10,
        time: 1000
    });
</script>
<?php
if ($page == 'detail_cabang') : ?>
    <script>
        $(document).ready(function() {
            $('#modal_cabang').modal('show');
        });
    </script>
<?php endif ?>
</body>

</html>