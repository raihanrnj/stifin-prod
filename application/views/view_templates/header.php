<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>STIFIn Official Website</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicon -->
    <link href="<?= base_url() ?>assets/img/favicon_stifin.png" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;500&family=Roboto:wght@500;700&display=swap" rel="stylesheet">

    <!-- Icon Font Stylesheet -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Libraries Stylesheet -->
    <link href="<?= base_url() ?>assets/lib/animate/animate.min.css" rel="stylesheet" />
    <link href="<?= base_url() ?>assets/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="<?= base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">

    <script defer src="https://unpkg.com/axios@1.3.4/dist/axios.min.js"></script>
    <script defer src="https://unpkg.com/alpinejs-money@latest/dist/money.min.js"></script>

    <script defer src="https://unpkg.com/alpinejs@3.x.x/dist/cdn.min.js"></script>


    <!-- Template Stylesheet -->
    <link href="<?= base_url() ?>assets/css/style.css?v=1" rel="stylesheet">
    <link href="<?= base_url() ?>assets/css/custom.css?v=1" rel="stylesheet">
    <link rel='stylesheet' href='https://vjs.zencdn.net/5.4.6/video-js.min.css'>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">

</head>

<body>
    <!-- Spinner Start -->
    <div id="spinner" class="show bg-white position-fixed translate-middle w-100 vh-100 top-50 start-50 d-flex align-items-center justify-content-center">
        <div class="spinner-grow text-primary" role="status"></div>
    </div>
    <!-- Spinner End -->


    <!-- Navbar Start -->
    <nav class="navbar navbar-expand-lg bg-white bg-green navbar-light sticky-top p-0 px-4 px-lg-5">
        <div class="container">
            <a href="<?= base_url() ?>" class="navbar-brand d-flex align-items-center">
                <!-- <h2 class="m-0 text-primary">STIFIn</h2> -->
                <img class="m-0 img-fluid-logo" src="<?= base_url() ?>assets/img/logo_stifin.png" alt="">
            </a>
            <button type="button" class="navbar-toggler" data-bs-toggle="collapse" data-bs-target="#navbarCollapse">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <div class="navbar-nav ms-auto py-4 py-lg-0">
                    <li class="nav-item active">
                        <a class="nav-link" href="<?= base_url() ?>">Home</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="#" id="menu" data-bs-toggle="dropdown" class="nav-link dropdown-toggle" data-bs-display="static">Konsep STIFIn</a>
                        <ul class="dropdown-menu shadow-sm m-0">
                            <li class="dropdown-submenu">
                                <a class="dropdown-item" href="<?= base_url() ?>page/view/konsep_stifin">Konsep STIFIn</a>
                            </li>
                            <li class="dropdown-submenu">
                                <a href="#" data-bs-toggle="dropdown" class="dropdown-item dropdown-toggle">5 MK STIFIn</a>
                                <ul class="dropdown-menu shadow-sm m-0">
                                    <li>
                                        <a href="<?= base_url() ?>page/view/sensing" class="dropdown-item">Sensing</a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url() ?>page/view/thinking" class="dropdown-item">Thinking</a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url() ?>page/view/intuiting" class="dropdown-item">Intuiting</a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url() ?>page/view/feeling" class="dropdown-item">Feeling</a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url() ?>page/view/insting" class="dropdown-item">Insting</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="#" id="menu" data-bs-toggle="dropdown" class="nav-link dropdown-toggle" data-bs-display="static">Ekosistem STIFIn</a>
                        <ul class="dropdown-menu shadow-sm m-0">
                            <li class="">
                                <a class="dropdown-item" href="<?= base_url() ?>page/view/ekosistem_cabang">Cabang STIFIn</a>
                            </li>
                            <li class="">
                                <a class="dropdown-item" href="<?= base_url() ?>page/view/ekosistem_promotor">Promotor STIFIn</a>
                            </li>
                            <li class="">
                                <a class="dropdown-item" href="<?= base_url() ?>page/view/ekosistem_trainer">Trainer STIFIn</a>
                            </li>
                            <li class="">
                                <a class="dropdown-item" href="<?= base_url() ?>page/view/ekosistem_solver">Solver STIFIn</a>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="<?= base_url() ?>page/view/ekosistem_workshop">Workshop STIFIn</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="#" id="menu" data-bs-toggle="dropdown" class="nav-link dropdown-toggle" data-bs-display="static">Download</a>
                        <ul class="dropdown-menu shadow-sm m-0">
                            <li class="">
                                <a class="dropdown-item" href="<?= base_url() ?>page/view/download_aplikasi">Aplikasi</a>
                            </li>
                            <li class="dropdown-submenu">
                                <a href="#" data-bs-toggle="dropdown" class="dropdown-item dropdown-toggle">Materi</a>
                                <ul class="dropdown-menu shadow-sm m-0">
                                    <li>
                                        <a href="<?= base_url() ?>page/view/download_video" class="dropdown-item">Video</a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url() ?>page/view/download_audio" class="dropdown-item">Audio</a>
                                    </li>
                                    <li>
                                        <a href="<?= base_url() ?>page/view/download_materi" class="dropdown-item">Ebook</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li class="nav-item active">
                        <a class="nav-link" href="<?= base_url() ?>page/artikel">Artikel</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a href="#" id="menu" data-bs-toggle="dropdown" class="nav-link dropdown-toggle" data-bs-display="static">About</a>
                        <ul class="dropdown-menu shadow-sm m-0">
                            <li class="dropdown-submenu">
                                <a class="dropdown-item" href="<?= base_url() ?>page/view/stifin_yayasan">Yayasan STIFIn</a>
                            </li>
                            <li class="dropdown-submenu">
                                <a class="dropdown-item" href="<?= base_url() ?>page/view/stifin_institute">STIFIn Institute</a>
                            </li>
                            <li class="dropdown-submenu">
                                <a class="dropdown-item" href="<?= base_url() ?>page/view/stifin_rumah_quran">Rumah Qur'an STIFIn</a>
                            </li>
                            <li class="dropdown-submenu">
                                <a class="dropdown-item" href="<?= base_url() ?>page/view/merchandise">Merchandise</a>
                            </li>
                        </ul>
                    </li>
                </div>
            </div>
        </div>
    </nav>
    <!-- Navbar End -->