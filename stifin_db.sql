-- phpMyAdmin SQL Dump
-- version 4.9.11
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 09, 2023 at 11:41 AM
-- Server version: 5.7.41
-- PHP Version: 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `stifin_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tb_about_promotor`
--

CREATE TABLE `tb_about_promotor` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `deskripsi` text,
  `gambar` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_about_promotor`
--

INSERT INTO `tb_about_promotor` (`id`, `judul`, `deskripsi`, `gambar`) VALUES
(4, 'Apa Sih Promotor STIFIn?', 'Promotor STIFIn merupakan salah satu profesi STIFIn yang sangat populer dengan dibekali pemahaman terkait konsep STIFIn sehingga sangat mumpuni ketika berhubungan dengan klien Tes STIFIn. Posisi Promotor STIFIn berada dibawah naungan Cabang STIFIn sehingga untuk pengisian ulang atau refill voucher atau trouble yang berhubungan dengan aplikasi promotor maka berhak menghubungi Cabang yang dinaungi. Tugas Utama Promotor adalah mengetes dan menjelaskan hasil Tes STIFIn kepada klien. Adapun tools yang digunakan promotor untuk melayani klien yaitu laptop dan scanner.', 'Artikel_STIFIn_(2)3.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_about_us`
--

CREATE TABLE `tb_about_us` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `deskripsi` text,
  `gambar` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_about_us`
--

INSERT INTO `tb_about_us` (`id`, `judul`, `deskripsi`, `gambar`) VALUES
(4, 'About Us', 'STIFIn merupakan akronim dari 5 Mesin Kecerdasan Sensing, Thinking, Intuiting, Feeling dan Insting yang menjelaskan tentang konsep untuk mengindentifikasi mesin kecerdasan dan karakter manusia berdasarkan sistem operasi otak yang dominan dengan cara  memindai sidik jari yang disebut dengan Tes STIFIn.  Tes STIFIn dilakukan dengan cara men-scan kesepuluh ujung jari Anda, mengambil waktu tidak lebih dari 1 menit. Sidik jari yang membawa informasi tentang komposisi susunan syaraf tersebut kemudian dianalisa dan dihubungkan dengan belahan otak tertentu yang dominan berperan sebagai sistem operasi (mesin kecerdasan) dan personaliti genetik Anda.\r\n', '3.png');

-- --------------------------------------------------------

--
-- Table structure for table `tb_admin`
--

CREATE TABLE `tb_admin` (
  `id` int(11) NOT NULL,
  `username` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_admin`
--

INSERT INTO `tb_admin` (`id`, `username`, `password`, `nama`, `status`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Heri Irawan', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_artikel`
--

CREATE TABLE `tb_artikel` (
  `id` int(11) NOT NULL,
  `slug` varchar(255) DEFAULT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `gambar` varchar(255) DEFAULT NULL,
  `konten` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_artikel`
--

INSERT INTO `tb_artikel` (`id`, `slug`, `judul`, `gambar`, `konten`) VALUES
(3, 'STIFIn-Personality', 'STIFIn Personality', '32.png', '<p></p>\r\n\r\n<p>Wacanda Bisnis adalah program Radio SmartFM 95,9 FM Jakarta. Dengan format acara yang santai, program ini tetap berbobot dalam menampilkan sisi bisnis. Host program ini adalah Abrar Din Ilyas, Gunawan Lukito dan Ola Nurlija. Bintang tamu pada edisi Jumat 22 januari 2016 jam 20.00 WIB adalah Bapak Farid Poniman, beliau adalah Penemu Konsep dan Tes STIFIn. Dalam acara Wacanda Bisnis Bapak Farid Poniman menjelaskan tentang STIFIn Personality. Secara singkat berikut resumenya :</p><p><br>Konsep STIFIn berdasarkan 5 Belahan Otak, setiap orang mempunyai 1 belahan otak yang dominan, dalam STIFIn disebut dengan Mesin Kecerdasan, sehingga ada 5 Mesin Kecerdasan yaitu Sensing, Thinking, Intuiting, Feeling dan Insting.</p><h3><br><b></b>Mesin Kecerdasan Sensing<b></b></h3><p><br>Sensing memiliki dominan belahan otak limbik kiri bawah. Orang Sensing gampangnya orang yang ngikut kepada FAKTA, apa yang terlihat, sehingga dia bicara hal-hal yang sensorik banget, faktual banget, konkrit banget. Kalau menyuruh orang Sensing ini bekerja , kita gak bisa berikan janji muluk-muluk yang tidak keliatan, langsung down to earth, duitnya segini, insentifnya segini, kamu kalau kerja ini dapetnya segini, buat orang Sensing begitu yang ia senangi karena ia jenis orang yang Tangible.</p><h3><br>Mesin Kecerdasan Thinking</h3><p><br>Thinking memiliki dominan belahan otak neokortek kiri atas. Orang Thinking menggunakan LOGIKA, apa-apa dilogikan oleh dia, itu mengapa kalau duit sudah masuk ke kantongnya, sulit untuk keluar karena penuh pertimbangan, buat apa, gunanya apa, analitis banget. Orang Bankers, auditor, bagus untuk orang Thinking, tapi secara umum orang-orang Eksekutif, Corporate bagus untuk orang Thinking.</p><h3><br>Mesin Kecerdasan Intuiting</h3><p><br>Intuiting memiliki dominan belahan otak neokortek kanan atas. Intuiting adalah orang yang kreatif, konseptor, abstrak, dia kebalikan dari orang Sensing, dia justru adalah Intangible, dia kualitatif. Jika orang Sensing mengejar Volume, dia ngejar margin, dia mau ada kualitas yang jauh lebih tinggi.</p><h3><br>Mesin Kecerdasan Feeling</h3><p><br>Feeling memiliki dominan belahan otak limbik kanan bawah. Orang yang pakai PERASAAN, faktor-faktor perasaan sangat dominan pada orang feeling, dia akan menjadi hebat ketika perasaannya terpicu, tersentuh, muncul mood, bangkit semangatnya, bangkit spiritnya. Profesi yang cocok untuk orang Feeling yang paling umum adalah Public Speaker, Sales, P.R, tapi yang paling spesifik adalah yang pandai berjanji belum tentu ditepati itu adalah Politisi. Termasuk orang ini memiliki kecenderungan banyak korban pasangan lawan jenisnya, pemberi harapan palsu, play boy cap kodok, tipe perayu.</p><h3><br>Mesin Kecerdasan Insting</h3><p><br>Insting memiliki dominan belahan otak tengah. Orang yang cenderung serba bisa, kita liat dia musik bisa, nari dia bisa, masak dia bisa, olah raga dia bisa, inilah orang tipe Generalis, dia akan sulit ditarik untuk menjadi Spesialis, sementara 4 tipe yang lainnya itu tipe Spesialis, yang akan sulit di tarik ke Generalis.</p>\r\n\r\n<p></p><strong></strong>'),
(4, 'Kunci-Sukses-Feeling-adalah-MEMIMPIN-DIRI', 'Kunci Sukses Feeling adalah MEMIMPIN DIRI', 'Artikel_STIFIn_.png', '<b></b>\r\n\r\n<p>Dalam sebuah seminar, seorang ayah bertanya “Apa maksudnya dengan Kunci Sukses Feeling adalah MEMIMPIN DIRI ??” Nah..Kunci sukses Feeling introvert dan Feeling extrovet akan dijabarkan pada artikel berikut.</p><p><br>Tipe Mesin Kecerdasan Feeling berfikir dan merasa menggunakan HATI, hati hubungannya dengan EMOSI dan PERASAAN. Mereka yang Personality Genetik Feeling introvert, akan fokus pada Kecerdasan Emosi (emotional quotient), artinya orang Feeling introvert akan fokus pada emosi yang digerakkan oleh dirinya sendiri.</p><p><br>Maka bilamana dia sudah mampu menggembleng dirinya, akan menjadi pribadi yang RAMAH, BIJAKSANA, PEMIMPIN SEJATI, ngangenin, kehadirannya di rindukan orang lain. Tapi bilamana dia “belum cerdas” dalam menggembleng dirinya, akan tampak pendiam, judes, kata-katanya menyengat, mudah tersinggung dan akhirnya dendam.</p><p><br>Mereka yang Personality Genetik Feeling extrovert, akan fokus pada Kecerdasan Sosial (Social Quotient), artinya tipe Feeling extrovert akan lebih fokus pada masalah emosi yang berhubungan dengan orang lain di sekitarnya.</p><p><br>Maka, jika Personality Genetik Feeling extrovert yang sudah tergembleng, akan lebih senang menjadi MENTOR bagi orang lain, lebih senang menjalin persahabatan lalu MEMOTIVASI sahabatnya agar maju menjadi orang sukses, sementara, dia sendiri akan menikmati kesuksesannya di belakang layar. Untuk itu, dia tampak BIJAKSANA dan SABAR.</p><p><br>Namun, bila tipe Feeling extrovert belum cerdas dalam menggembleng dirinya. Yang nampak adalah dia tak disukai lingkungannya karena menebar aura negatif, senang menyalahkan, suka nyuruh-nyuruh orang, lalu gak bertanggung jawab, malas bergerak, senang dengan hasil instant.</p><p><br>Kunci sukses Feeling adalah MEMIMPIN DIRI nya, sebelum memimpin orang lain</p><p>Dia harus mengusahakan sekuat tenaga mengolah emosi hatinya menjadi aura positif yang menebar cinta dan kasih sayang kepada lingkungannya. Dia harus mampu mendengar dan mengolah kata menjadi motivasi dan inspirasi bagi banyak orang. Dia harus mampu menahan rasa dengki, marah, kesal, sakit hati, cemburu, dan lain-lain yang semuanya justru akan menjadi petaka baginya.</p><p><br>Kesimpulannya, orang Feeling, Pimpin diri untuk BIJAKSINI sehingga bisa menjadi BIJAKSANA.</p>\r\n\r\n<b></b><br>'),
(5, 'Tips-Tidur-Siang-Bagi-Sensing', 'Tips Tidur Siang Bagi Sensing', 'Artikel_STIFIn_(2).png', '<p>\r\n\r\n</p><p>Sepertinya hal ini adalah fakta yang paling sering ditemukan saat sesi konsultasi tentang tipe anak yang Sensing. Yakni, anaknya susah di kondisikan untuk tidur di siang hari.</p><p><br>Padahal, sang ibu sudah mengeluarkan jurus andalannya. Yaitu:</p><p><br>Langkah pertama, menggiring anak masuk ke kamar.</p><p>Kedua, mematikan lampu kamar.</p><p>Ketiga, menemani anak tiduran di kasur.</p><p>Keempat, ibunya yang ketiduran .. anaknya keluar kamar lagi</p><p>Oalahhh.. ternyata jurus ampuh ajah ga mampu membuat anak Sensing tidur siang. Yang ada malah ibunya yang tidur siang. Klo gitu coba deh jurus yang belum dikatakan ampuh ini, namun banyak teruji hasilnya.</p><p><br>Kita berpegang pada informasi dasar dahulu yah. Dasarannya adalah anak Sensing baterainya besar, maka jika waktu yang ditentukan untuk tidur sudah tiba namun baterainya masih full, dijamin bakal ga mau tidur deh. Untuk itu perlu diadakan aktivitas bergerak yang menyenangkan terlebih dahulu sebelum dikondisikan untuk tidur siang.</p><p><br>Klo anak Sensing sudah banyak bergerak, coba deh perhatikan ketika ada waktu jeda antara kegiatan yang satu dengan yang berikutnya, anak Sensing bisa tiba-tiba hilang dari peredaran alias ketiduran karena capek. Tidurnya bisa dimana saja. Bisa pas naik ke mobil dan ga beberapa lama kemudian diajak ngobrol, eh udah tidur. Naik motor juga bisa begitu. Sedang menunggu makanan yang akan datang juga sama.</p><p><br>Oke, Untuk mudahnya, kira-kira beginilah tipsnya :</p><p><br>Lakukanlah aktivitas yang berkeringat atau bergerak lebih banyak sebelum waktu tidur siang.</p><p>Waktu tidur siang jangan kaku di satu waktu, misal jam 12 siang adalah waktu tidur siang, bisa disesuaikan dengan kondisi.</p><p>Kenali kondisi jeda dari aktivitas yang satu ke yang berikutnya.</p><p>Manfaatkan kondisi jeda tersebut, untuk merecharge baterainya anak Sensing.</p><p>Saat kondisi jeda datang, arahkan ke situasi yang nyaman dan aman untuk tidur.</p><p>Selamat menidurkan yang sudah waktunya tidur yah ..</p><p>Tetaplah sabar dalam mendampingi anak-anak dan terus bersyukur atas segala anugerah yang Seruu ini.</p>\r\n\r\n<br><p></p>'),
(6, 'Menyembuhkan-Luka-Bathin-Setiap-Mesin-Kecerdasan', 'Menyembuhkan Luka Bathin Setiap Mesin Kecerdasan', 'Artikel_STIFIn_(3).png', '<p>\r\n\r\n</p><p>Jika kamu pernah sakit hati dengan seseorang, dalam hal hubungan asmara ataupun pertemanan biasa, tapi sampai saat ini masih juga menjadi luka bathin yang tersimpan rapat. Berikut Tips menyembuhkan luka-luka batin yang dialami oleh setiap Mesin Kecerdasan.</p><h3>Mesin Kecerdasan Feeling</h3><p>Potensi terbesar luka batin ada pada orang Feeling. Luka tersebut hanya bisa disembuhkan dengan cinta baru yang jauh lebih indah dan lebih menyejukkan.</p><h3>Mesin Kecerdasan Sensing</h3><p>Sensing tidak mengalami luka batin namun kejadiannya membekas kuat pada ingatannya. Perlu diyakinkan dengan serangkaian bukti yang nyata terjadi di depan mata bahwa yang bersangkutan sudah menjadi pribadi yang berubah.</p><h3>Mesin Kecerdasan Thinking</h3><p>Thinking sebenarnya tidak mengalami luka batin namun logikanya sudah menyimpulkan bahwa tidak ada manfaatnya menjalin hubungan dengan orang tersebut. Maka bersiaplah menyampaikan alasan dan argumentasi yang paling rasional bahwa kehadirannya sangat dibutuhkan.</p><h3>Mesin Kecerdasan Intuiting</h3><p>Intuiting lebih kuatir dengan berbagai prasangka buruk yang belum tentu terjadi namun seolah sudah terjadi. Sehingga orang Intuiting perlu diperkaya imajinasinya dengan indahnya cinta dan betapa nikmatnya kehidupan di masa yang akan datang.</p><h3>Mesin Kecerdasan Insting</h3><p>Insting sebenarnya paling easy going. Namun jika sudah terjadi luka batin, berarti orang Insting sudah trauma pada kejadian tersebut. Perlu didampingi dan dibimbing secara intensif tanpa tekanan, untuk kemudian perlahan-lahan dilatih untuk menangani sendiri traumanya sampai dia pada akhirnya tidak lagi terbebani dengan luka batinnya.</p>\r\n\r\n<br><p></p>'),
(7, 'STIFIn-Bukan-Pembenaran-Atas-Kekurangan-Diri', 'STIFIn Bukan Pembenaran Atas Kekurangan Diri', 'Artikel_STIFIn_(5).png', '<p>\r\n\r\n</p><h3>JANGAN JADIKAN “STIFIn” SEBAGAI PEMBENARAN ATAS KEKURANGAN DIRI</h3><p><br>Beberapa tahun terakhir ini saya mendapatkan kesempatan luar biasa untuk mempelajari ilmu pengembangan diri yang disebut STIFIn. Perkenalan pertama kali saya dengan STIFIn ketika mengikuti TFT 1 Pak Jamil Azzaini di Jogja tahun 2009 adalah salah satu lembaran penting dalam kehidupan saya. Kemudian saya merasa “gue banget”, menemukan “missing puzzle” dalam kehidupan, dan merasa lebih mudah untuk menemukan jalur terbaik kehidupan. Yang membuat saya yakin untuk banting setir dari dunia desain bangunan menjadi (bergeser sedikit) ke dunia desain kehidupan. (Beda tipis kan? Hehehe.)</p><p><br>Seiring dengan perjalanan waktu, selain ilmu yang semakin luas saya dapatkan, saya pun berkenalan dengan banyak teman baru, yang sebagian besar diantaranya bahkan telah menjadi seperti saudara dengan hubungan yang sangat dekat. (Tapi sayangnya kok nggak ada yang jadi istri ya? ?#?eh? ?#?kode?.)</p><p><br>Beberapa hari lalu menjadi hari yang istimewa ketika ketemu dan terlibat “perbincangan seru dengan seorang sahabat lama”. Artinya : sudah kenal lama, sudah seperti sahabat, tapi berbincang ya baru kemarin itu. Seru kan?</p><p><br>Dan siang itu saya merasa dihajar habis-habisan terutama ketika sahabat saya itu cerita betapa “seru”-nya status-status saya di facebook, terutama pada musim pilpres beberapa waktu lalu. Sambil beliau menyampaikan pendapatnya, seolah di depan saya berkelebat berbagai penjelasan Pak Farid Poniman tentang Karakter Mesin Kecerdasan Insting. Semuanya “Gue Banget”! Cuma sayangnya “gue banget” yang versi negatif. Hahahaha. Betapa saya sangat responsif, temperamental, dan di saat yang sama juga on-off nya sangat tinggi. Habis ngamuk-ngamuk tetiba becanda, eh setelah ketawa-ketawa kemudian emosi lagi. Trus habis itu nyesel. Tapi besoknya terulang lagi. Parah dah. Hahaha.</p><p><br>Setelah ketawa-ketawa dengerin cerita beliau, tetiba saya tertohok oleh sebuah fakta : lho, saya udah belajar STIFIn sejak 2009 tapi ternyata masih begitu-begitu aja perilaku saya ya? Masih Insting banget versi on the spot eh versi negatifnya ya? Masih belum bisa move on dari Level Personality. Padahal level Personality adalah level terendah. Sangat jauh dari gambaran terbaik seorang Insting. Aduh.</p><p><br>Maka pada saat itu saya ber istighfar dan kemudian dalam hati berterima kasih kepada sahabat saya ini. Yang mengibaratkan dirinya sebagai seseorang yang berada di “teras rumah STIFIn”. Yang pernah masuk ke dalam rumah STIFIn tapi kemudian merasa lebih nyaman duduk menyendiri di teras dibanding ngobrol berinteraksi di dalam rumah. Tapi juga tidak seperti sebagian yang lain, yang meninggalkan rumah STIFIn dengan penuh kekecewaan atau bahkan kemarahan.</p><p><br>Beliau bercerita bahwa potret seperti saya ini hanyalah salah satu dari sekian banyak potret-potret para peggiat STIFIn. Siapakah pegiat STIFIn itu? Yaitu mereka yang aktif mengkampanyekan STIFIn. Serunya, beliau menyatakan bahwa potret-potret tersebut menggambarkan hal yang sama. Yaitu masih di Level Personality. Yang menggambarkan setiap Mesin Kecerdasan dengan semua kutub positif dan negatifnya.</p><p><br>Yang gampang banget ngomong :</p><p><br>“ya kan saya Thinking, emang susah lah terima pendapat orang lain.”</p><p>“Sensing kan gitu. Saya nggak bisa ngerjain kalo ga ada contohnya.”</p><p>“Lebay gpp lah. Secara aku Feeling gitu loohh…”</p><p>“Emang susah ngikutin aturan kalo Intuiting kayak saya gini.”</p><p>“Bawaan Insting kali ya? Jadi emang saya ngga bisa fokus.”</p><p><br>…dan beliau menyampaikan ketidak nyamanannya berinteraksi dengan orang-orang seperti itu (salah satunya : Saya!). Yang belajar banyak tentang STIFIn namun kemudian hanya untuk mendapatkan pembenaran atas kekurangannya. Yang seru membahas kelemahan masing-masing namun hanya sebagai lucu-lucuan, tanpa adanya satu aksi konkret untuk memperbaiki diri.</p><p><br>Deg! Seolah jantung berhenti mendengar cerita beliau. Terutama karena saya saat ini sedang mendapat tugas untuk mengelola STIFIn Institute, yang menjadi salah satu garda terdepan dalam menyebarluaskan STIFIn ke seluruh Indonesia. Maka kemudian saya kembali ber istighfar, memohon ampun kepada Allah atas sering hadirnya perasaan merasa sudah hebat. Merasa sudah banyak paham tentang STIFIn. Ternyata tentang diri saya pun saya belum paham. Dan ternyata saya belum praktekkan. Kalo dilihat dari Taksonomi Bloom, ternyata saya baru sebatas ingat atau hafal saja. Masih jauh dari menerapkan, menganalisa, dst. Saya lebih sering merasa hebat dibandingkan menjadi hebat.</p><p><br>Alhamdulilah, Allah masih sayang sama saya. Masih bersedia mengirimkan seseorang untuk menampar saya. (Cuma kenapa namparnya kuenceng banget, ya Allah… hehehe…)</p><p><br>Bismillah, memulai hari dan memulai minggu dengan niat untuk menjadi seorang Insting yang selalu berusaha untuk menaikkan level kepribadian saya. Menjadi seorang Insting yang sederhana, berfokus pada kepentingan yang lebih besar, mementingkan orang lain, cepat tuntas, merangkul potensi-potensi yang ada, mempermudah, memperlancar, tidak membebani, mengalir, pragmatis berdaya guna, dan berperan aktif dalam program kemanusiaan. Semoga Allah mudahkan langkah kaki dan upaya saya serta sahabat-sahabat saya para pegiat STIFIn untuk menjadi hebat. Aamiinn.</p>\r\n\r\n<br><p></p>'),
(8, 'Mendapatkan-Bahagia-Dengan-4-Ta-Harta-Tahta-Cinta-Kata', 'Mendapatkan Bahagia Dengan 4 Ta, Harta, Tahta, Cinta, Kata', 'Artikel_STIFIn_(4).png', '<p>\r\n\r\n</p><p><b>Sensing berucap</b> : Bahagia itu kalau bisa punya duit yang banyak. Tanpa duit susah mau ngapa ngapain. Mati emang gak bawa duit, tapi sebelum mati kan butuh duit.</p><p><br><b>Insting berucap </b>: Kebahagiaan bukanlah pada Harta. Bahagia ya Bahagia aja. Tuh banyak orang Kaya gak bahagia hidupnya. Sholatnya jadi lupa, karena sibuk nyari duit. Banyak harta nanti repot ciiiiiinn..menjawab pertanyaan malaikat di kubur.</p><p><br><b>Feeling berucap</b> : Punya Harta kalau Gak saling mencintai untuk apa. Yang penting itu dalam Hatinya ada kita satu satunya. Gak selingkuh dan minta poligami. Banyak pasangan kaya, tuh artis kurang apa duitnya, cerai juga. Karena telah hilang rasa cinta mereka. Yang penting Hati bukan Harta, ini mirip lagunya.. Kun Anta Tazdada Jamala – Humood AlKhudher.</p><p><br><b>Thinking berucap</b> : Jabatan tinggi akan mendatangkan penghasilan yang banyak. Duit banyak tidak dipandang dan dihormati orang kalau kita tidak punya pangkat dan jabatan.</p><p><br><b>Intuiting berucap</b> : Harta bisa habis. Tapi Ilmu bisa menjaga Harta. Cari ilmu dulu, nanti duit akan datang sendiri. Orang berilmu itu lebih tinggi posisinya di mata Allah dari pada orang ber Harta.</p><p><br>Hmm..kebayangkan</p><p><br>Betapa “serunya” rumah tangga, ketika kita memiliki Mesin Kecerdasan yang berbeda dengan pasangan saat berbicara tentang Harta, Tahta, Kata (Ilmu), Cinta dan Bahagia. Hal seperti ini aja, masalah beda pendapat tentang Harta, Tahta, Kata (Ilmu), Cinta, Bahagia, jika masing-masing tidak mampu memposisikan “makanats” (artinya keberadaan, dalam STIFIn disebut Mesin Kecerdasan) pasangannya. Bisa “barabe” (hancur) tuh rumah tangga.</p><p><br>Kalau gitu bagus donk, kalau Mesin Kecerdasan saya sama dengan pasangan ?..nah..klo hal ini lain lagi babnya bro and bri…karena tidak selamanya yang sama itu bagus, coba laki-laki sama laki-laki nikah, bagus tidaaak.. ?</p><p><br>Tips Memilih Jodoh</p><p><br>Tips selingan : Jika ingin menikah, carilah perbedaan, jangan cari yang banyak kesamaan sifatnya. Biasanya, karena merasa banyak kesamaan sifat, disangka pasangan cocok ideal. Padahal kutub magnet yang sama, mereka jadi gak akur, tuh buktinya tolak tolakan. Begitu kutubnya beda, jadi akur dan saling mendekat berpelukan, nempel gak mau lepas. Ternyata kutub magnet lebih smart dari kaum LGBT.</p><p><br>Perbedaan Adalah Sunnatullah Untuk Menjadi Akur</p><p><br>Nah, kalau kutub magnet yang berbeda aja bisa akur, saling mendekat dan berpelukan, masa iya..kita sama pasangan yang berbeda sifat, malah menjadi tolak tolakan. Berarti SunnatullahNya, perbedaan itu menjadikan kita akur. Tapi, faktanya keributan yang ada dirumah tangga, justru dikarenakan adanya perbedaan pandangan dan pengalaman hidup, pendapat dan persepsi.</p><p><br>Perbedaan itu terjadi karena beda Mesin Kecerdasannya. Mesin Kecerdasan itu adalah salah satu dari 5 bagian otak manusia yang paling dominan, yang paling aktif bekerja selama 24 jam/hari mengendalikan manusia. Otak Dominan tersebut menghasilkan output dalam bentuk Pandangan Hidup, Persepsi, Pendapat, Ucapan, Sikap dan Prilaku serta Prioritas Hidup yang berbeda satu sama lainnya.</p><p><br>Untuk itulah Konsep STIFIn yang ditemukan oleh guru kami ayah Farid Poniman, hadir sebagai Konsep yang membuat kita menjadi lebih mudah memahami perbedaan, tidak memaksakan kehendak pribadi kepada orang lain. Sehingga menjadi dasar untuk lebih mudah menghormati, memahami sikap dan tindakan, serta pilihan dan pandangan hidup pasangan kita ataupun orang lain.</p><p><br>Dulu sebelum kenal STIFIn, saya selalu memaksa teman yang saya kenal, agar berwirausaha kalau dia memilih profesi PNS atau Karyawan, maaf ya teman-teman yang PNS. Tapi, Sejak kenal STIFIn saya sekarang bisa jalan normal lagi, tulang punggung tidak sakit lagi dan pusing dikepala hilang…loh..kok jadi klinik Tong Fang. Setelah mengenal Konsep STIFIn saya jadi sadar, ternyata saya tidak bisa memaksakan semua orang untuk memilih jalan hidup yang sama dengan saya.</p><p><br>Ini ada cerita fiktif :</p><p><br>Intuiting berkata : Ilmu itu lebih penting dari duit. Si Intuiting ini selalu fokus nyari ilmu, baru dapatkan duit dengan ilmunya.</p><p><br>Sensing berkata : Gak ada duit, mana bisa menuntut ilmu, bayar seminar dan pelatihan. Sensing berpendapat cari duit dulu baru Ilmu.</p><p><br>Untungnya si Sensing, gak seperti itu, dulu dia manut aja (karena ditaklukkan sama Intuiting kali ya.), lagi susah susahnya duit, karena si Intuiting mau Tobat Profesi, dari Pemilik Usaha Warnet dan Jasa Service Komputer, ingin putar haluan jadi Trainer. Jadi si Intuiting ini udah tobat profesi duluan, sebelum kenal istilah Tobat Profesi dari STIFIn.</p><p><br>Intuiting ngomong ke Sensing, eh kita ada duit gak ya, mau ikut seminar ini nih. Pas pula Sensing lagi ngefans sama Kek Jamil Azzaini, jadi jebollah proposal untuk pergi ke Jakarta buat Wanna Be Trainer Batch 12, disana dikenalin dengan STIFIn dan berlanjut beberapa kali Workshop STIFIn. Sampe-sampe kita ngutang waktu itu untuk WSL3 yang nilainya 12 juta. Ups….</p><p><br>Tapi, sekarang..dia bilang..</p><p>Untung ya..ayah dulu ikut yang gituan..</p><p>Bisa ketemu dunia baru yang berbeda, komunitas baru.</p><p><br>Yaa..untung kamu juga dulu manut aja..</p><p>Trus dijawab.. Namanya awak patuh sama suami. Ciee cieee..cuit cuiiiit</p><p><br>Coba bayangin, kalau kejadiannya tidak Happy Ending seperti itu, tapi seperti ini :</p><p><br>Apa yah..uang kita terbatas, pengeluaran banyak, pemasukan minim, ini mau pergi seminar WBT, “Wanna Be Bokek” itu namanya, ngabisin duit aja. Emang bisa dapat duit langsung setelah pulang dari seminar, gak kan ?..Gak..gak gak..gak bisa ayah pergi seminar..dari mana duitnya, duit kita gak ada.</p><p><br>Lalu, dijawab enteng sama Intuiting, yang anti kemapanan..”Dari Allah” duitnya.</p><p><br>Sensing langsung jawab, emang bisa duit itu turun gitu aja dari langit kayak Allah nurunin hujan tanpa kerja keras. (ini bahasa emak saya dulu, ternyata setelah saya Tes STIFIn, Sensing hasilnya, hehe..)</p><p><br>Kalau saya terusin bisa panjang nih..daya hayal saya dalam mengarang bisa gak habis..Mau bacanya..sampe habis.. ??</p><p><br>Jadi Intinya..</p><p><br>Kenali Mesin Kecerdasan pasangan :</p><p><br>1. Agar kita lebih mudah menerima, memahami dan bersungguh sungguh mewujudkan apa yang menjadi Prioritas Hidupnya, menerima sikap dan cara berpikirnya.</p><p><br>2. Agar kita tidak Ego Sendiri, ingin prioritas hidup kita sendiri yang dipenuhi, sikap kita sendiri yang ditenggang rasai dan ingin dimengertii, tanpa memikirkan kalau pasangan punya prioritas yang berbeda, sikap dan cara pandang yang berbeda yang butuh juga dimengertii.</p><p><br>3. Saya kan Sensing kata istrinya, kamu harus ngerti donk, cari duit yang banyak sono. Ini juga sikap yang gak benar, namanya bersembunyi dibalik STIFIn. Pasangan anda pasti mengerti tanpa perlu anda ngomong begitu, jika dia udah mengenal Konsep STIFIn, jadi buruan di kenalin dengan mengikuti Workshop STIFIn.</p><p><br>4. Saya kan Feeling, hanya bisa memberikan Cinta, jadi jangan harapkan Harta. Ini namanya menjadikan STIFIn sebagai alasan pembenaran dari kelemahan diri karena tidak mau dan mampu berupaya mencari harta.</p><p><br>“Bagi saya yang penting kita Bahagia kata istri Insting, hidup pas pasan tidak masalah, asalkan kita bahagia. Kata suaminya yang Sensing. Yaaa gak gitu juga keless, emang bisa beli beras pake mata uang Bahagia, haji koim itu walaupun udah haji, kalau berasnya di beli tetap pake Rupiah”</p><p><br>“Bagi Saya Feeling, yang pentingkan saya mencintai kamu, sayang kamu, sepenuh hatiku milik kamu, kalau kamu mau Harta yang banyak, yaa..jangan berharap sama saya donk, saya kan feeling hanya bisa memberikan cinta dan kasih sayang. Kalau mau harta, sono tuh nikah sama Datuk Maringgih.</p><p><br>Jadi baiknya..</p><p><br>Okey, saya buat kamu Bahagia selalu dirumah dan dimanapun kita bersama berada..Tapi kamu buat saya bahagia dengan penghasilan yang banyak ya, kata Sensing pada Insting.</p><p><br>Okey, sekarang kamu nuntut ilmu yang banyak, nanti dengan itu hasilkan uang yang banyak ya, kata Sensing pada Intuiting (ini kata istri saya).</p><p><br>Okey, saya berikan kamu Cinta, dengan itu kamu harus semangat ya, pantang menyerah mencari nafkah yang banyak untuk keluarga kita, agar kita bisa beli rumah dan mobil sendiri. Kata Sensing pada Feeling.</p><p><br>Okey, saya berikan kamu kesempatan berkarir sampai jabatan tertinggi, saya bertanggung jawab dirumah, tapi hasilkan duit yang banyak ya, tanpa korupsi, ingat tanpa korupsi. Kata Sensing pada Thinking</p>\r\n\r\n<br><p></p>'),
(9, 'Apa-Itu-Minat-?', 'Apa Itu Minat?', 'Artikel_STIFIn_.jpg', '<p>\r\n\r\n</p><p>Minat Pada Kamus Besar Bahasa Indonesia (KBBI), artinya kecenderungan hati yang tinggi terhadap sesuatu, bisa juga diartikan sebagai gairah atau keinginan. Bahas Inggris, minat sering digambarkan dengan kata “interets” atau “passion””. Interest, bermakna suatu perasaan ingin memperhatikan dan penasaran akan sesuatu hal. Passion, bermakna minat yang kuat atau gairah atau perasaan yang kuat atau antusiasme terhadap objek.</p><p><br>Minat merupakan ketertarikan akan sesuatu yang berasal dari dorongan dalam hati atau bakat, dorongan dalam diri sendiri bukan karena paksaan orang lain. Minat yang di miliki seseorang adalah hasil dari proses pemikirian, perasaan (emosi), dan pembelajaran, sehingga menimbulkan suatu keinginan untuk lebih menguasai objek atau kegiatan tertentu.</p><p><br>Minat anak akan terlihat pada usia pra sekolah sampai dengan usia sekolah. Pada usia prasekolah anak mulai mengeksplorasi hal-hal baru disekitarnya dan memiliki inisiatif untuk mempelajari hal baru tersebut. Sedangkan pada usia sekolah, anak telah berfokus pada penyelesaian tugas sekolah (PR). Terkadang hal ini juga akan membangun ketertarikan anak pada suatu objek atau tugas yang diselesaikannya.</p><p><br>DATANGNYA MINAT</p><p><br>Minat bisa lahir dari dalam diri si anak dan juga karena pengaruh lingkungan.</p><p><br>1. Minat yang lahir karena dorongan dari dalam diri si anak. Dorongan ini dipengaruhi dari bakat alami yang telah diberikan Allah (given) kepada anak. Biasanya minat yang tumbuh dari dalam diri anak, sulit baginya menjelaskan mengapa ia menyukai sesuatu. “kenapa ya? yaa. suka aja, demikian jawabnya jika ditanya. Minat ini kadang juga disebut Minat “bawaan lahir”, minat karena adanya dorongan dari bawaan bakat alami si anak.</p><p><br>2. Minat yang lahir karena pengaruh lingkungan. Lingkungan keluarga, dorongan dari orang tua, tempat tinggal atau teman sepermainan. Misalnya seorang anak yang minat berjualan (berdagang), karena ia lahir di lingkungan pedagang, ayah ibunya pedagang, dan tinggalnya dekat dari pasar (pajak). Oleh karena itu diperlukan Tes Bakat untuk mengetahui apakah ia memiliki bakat alami (genetik) berdagang atau tidak. Jika dari hasil tes bakat anak memiliki bakat dagang, maka minat anak bisa diteruskan.</p><p><br>Minat anak yang terbaik adalah jika berasal dari Bakat Alaminya anak. Sebab ini akan menjadikan anak lebih mampu untuk konsisten dalam melakukannya. Lebih senang tanpa ada beban. Jika anda ingin mengetahui minat sejati anak anda maka lakukanlah Tes Bakat dengan menggunakan Fingerprint Tes STIFIn.</p>\r\n\r\n<br><p></p>'),
(10, 'Tes-STIFIn-Baru-Tes-IQ', 'Tes STIFIn Baru Tes IQ', 'Artikel_STIFIn_(1).jpg', '<p>\r\n\r\n</p><p>Tes STIFIn apakah perlu ? Apakah kita memerlukan Tes IQ ? Pasti jawabannya adalah perlu. Tapi, sesungguhnya ada yang terlebih dahulu perlu dilakukan sebelum Tes IQ yaitu Tes STIFIn. Kenapa? Karena Tes IQ adalah Mengukur Kapasitas untuk mengetahui seseorang berada di peringkat yang mana, peringkat buruk, menengah ataupun baik, maka itu di ukur menggunakan Tes IQ. Sedangkan untuk mengetahui Jenis Kecerdasan seseorang maka itu diukur menggunakan Tes STIFIn. Jadi sesungguhnya adalah melakukan Tes STIFIn terlebih dahulu itu lebih diperlukan sebelum Tes IQ. Setelah mengetahui apa Jenis Kecerdasannya, maka baru kemudian bisa diikuti dengan Tes IQ untuk mengukur kapasitasnya.</p><p><br>Tes STIFIn sebagai sebuah alat tes memilki tiga keunggulan, yaitu 1. Simple, 2. Akurat dan 3. Aplikatif.</p><p><br>1. SIMPLE</p><p>Tes STIFIn simple karena hasil Tes STIFIn ini kesimpulannya clear, dia jelas, dia menentukan satu jenis kecerdasan tunggal yang direkomendasikan untuk menjadi panduan di dalam pembinaan. Yang satu itu adalah tingal melihat dari hasil tesnya, satu diantara 5 Mesin Kecerdasan atau satu diantara 9 Personality Genetic. Tidak ada Mesin Kecerdasan (MK) yang keenam dan tidak ada Personality Genetic (PG) yang kesepuluh.</p><p><br>[pull_quote_center]Jadi simple, setiap orang tinggal ditentukan apa salah satu diantara 5 Mesin Kecerdasan atau 9 Personality Genetic nya. [/pull_quote_center]</p><p><br>Mengapa disebut Personality Genetic ? Karena ini adalah jenis personality yang tidak akan berubah mulai dari lahir sampai kemudian meninggal, meskipun jenis genetiknya adalah genetik non hereditary, tapi dia permanen, menetap sebagai sebuah personality. Personality Genetik ada 9 menurut Konsep STIFIn.</p><p><br>2. AKURAT</p><p>Tes STIFIn mengapa disebut akurat ? Kita bisa melihat dari hasil risetnya bahwa validitasnya sangat baik, realibilitasnya juga sangat tinggi. Ketika tes ini diulang, maka kecenderungan hasilnya adalah sama, diantara 343 orang yang di tes ulang setelah sebulan hanya ada 4 orang yang berubah. Jadi, itu artinya persentasinya kecil sekali. Sejelek-jelek atau seburuk-buruknya adalah pada angka 5% yang kemungkinan bisa berubah.</p><p><br>Tes STIFIn mengapa disebut validitasnya tinggi ?</p><p>Karena rata-rata orang yang di Tes STIFIn, kebanyakan orang yang di tes merasa bahwa “kesimpulan dari hasil Tes STIFIn nya ini “ Gue Banget ”. Mereka merasa cocok sekali dengan penjelasan hasil Tes STIFIn ini. Bahkan mereka terkadang melihat ada hal-hal yang tersembunyi selama ini, yang dirinya belum tahu, hasil Tes STIFIn ini bisa mengungkapkan bahwa sesungguhnya seseorang memiliki potensi-potensi tertentu yang tersembunyi.</p><p><br>3. APLIKATIF</p><p>Tes STIFIn mengapa di sebut Aplikatif ? Karena kalau sudah mengikuti Tes STIFIn, Insya Allah dia akan menjadi lebih mudah untuk menemukan jati dirinya, men set-up arah tujuannya atau goal settingnya. Sehingga kemudian dia dapat membuat program yang sesuai, program 10 tahunanlah, 5 tahunanlah, tahunanlah, bulananlah, menjadi lebih mudah, karena dengan STIFIn menjadi amat sangat jelas, dia mesti pergi kemana tanpa harus bingung, tanpa harus galau. Jadi itulah yang menjadi keunggulan Tes STIFIn.</p><p><br>Tes STIFIn Sebagai Sebuah Konsep</p><p>Jika diatas dijelaskan STIFIn sebagai Alat Tes. Maka sebagai sebuah konsep, STIFIn adalah sistematis bahkan solutif. Berbagai macam persoalan sangat mudah dianalisa menggunakan pisau skema STIFIn, kalau ini sebagai pisau bedah maka Skema STIFIn pisau bedah yang tajam, sistematis yang kemudian solutif. Sehingga setiap perkara bisa dilihat dengan mudah menggunakan kaca mata STIFIn.</p><p><br>Sebagai contoh misalnya dalam bidang pendidikan, ekonomi, sosial dan politik, parenting, pasutri, skema STIFIn bisa berbicara untuk banyak hal dan menjadi mudah menyelesaikan setiap persoalan dengan menggunakan sudut pandang ataupun menggunakan STIFIn.</p><p><br>Tes STIFIn Sebagai Riset</p><p>Tes STIFIn sebagai riset ini sudah dilakukan sejak 13 tahun yang lalu, yaitu sejalan dengan berdirinya company Kubik Trainning &amp; Consultancy, sebagai sebuah company training yang didirikan bersama kawan-kawan. Setiap client yang menjadi client training Kubik, maka dia akan selalu di awali dengan melakukan Tes STIFIn. Dulu awal-awalnya menggunakan pensil dan paper tet, sekarang sudah menggunakan finger print.</p><p><br>Jadi sebagai sebuah riset, ini berkembang dari awal mulanya ditemukan empat Mesin Kecerdasan (STIF) dan kemudian berkembang menjadi lima Mesin Kecerdasan menjadi STIFIn. Dan ini sudah final.</p>\r\n\r\n<br><p></p>'),
(11, 'Wajib-Tau-Ilmu-Daktilopski-Nih-Sebelum-Mengenal-STIFIn!', 'Wajib Tau Ilmu Daktilopski Nih Sebelum Mengenal STIFIn!', 'Artikel_STIFIn_(2)1.jpg', '<h3>\r\n\r\n\r\n\r\nDermatoglyphic (Ilmu Sidik Jari)&nbsp;</h3><p>\r\n\r\nDermatoglyphic atau Ilmu tentang sidik jari sudah berkembang ratusan tahun yang lalu dalam berbagai bidang seperti dunia militer (peperangan) dan kepolisian (kriminalitas, detektif, inteligen), perusahaan/organisasi (rekrutmen), dan berbagai dunia terapan (seperti pemasaran, olahraga, kesenian dll). Sebenarnya, penelitian-penelitian yang menunjukkan bahwa sidik jari dapat digunakan mengidentifikasi perkembangan otak, telah berlangsung sejak jaman Plato.</p><p>Dermatoglyphic (ilmu sidik jari) mempunyai dasar ilmu pengetahuan yang kuat karena didukung penelitian sejak 300 tahun lalu. Para peneliti menemukan, bahwa sidik jari memiliki kode genetik yang secara ilmiah dapat dihubungkan dengan sel otak dan jenis kecerdasan/kepribadian seseorang yaitu ilmu Daktilopski. Beberapa peneliti tentak sidik jari diantaranya :</p><p></p><ul><li>Govard Bidloo (1685)</li><li>Marcello Malpighi (1686)</li><li>J.C.A. Mayer (1788)</li><li>John E. Purkinje (1823)</li><li>Dr. Henry Faulds’ (1880)</li><li>Francis Galton (1892)</li><li>Harris Hawthorne Wilder (1897)</li><li>Noel Jaquin (1958)</li><li>Beryl B. Hutchinson (1967)</li><li>Beverly C. Jaegers (1974)</li><li>John E.Purkinje (1823) Profesor Anatomi di Universitas Breslau, yang pertama kali mengklasifikasikan pola sidik jari menjadi sembilan kategori</li><li>Ahli neuroscience pemenang hadiah Nobel Tahun 1986 Dr. Rita Levi asal German, pada awalnya hanya mengembangkan ilmu sidik jari untuk keperluan militer dan diperluas kegunaannya untuk mempersiapkan tim olimpiade Rusia &amp; China. Sekarang telah digunakan meluas untuk kepentingan publik.</li></ul><h3>Daktilopski atau Ilmu Sidik Jari untuk Mengenali Identitas Orang<br></h3>\r\n\r\n<strong></strong>\r\n\r\nDaktilopski berasal dari bahasa Yunani yaitu Dactilos (garis-garis jari) dan Scopeen (mengamati). Sejak ribuan tahun sebelum masehi kesadaran manusia sudah mengenal adanya garis papilair pada jari. Hal ini terbukti adanya peninggalan sejarah kerajaan Inive di Babilonia, yaitu orang Indian tentang ukiran kasar bentuk sidik jari pada goa batu ditepi danau KEjimkcijik Nova Scovia di Teluk Mexico. Pada abad VII (650 m) di China penggunaan sidik jari sudah diwajibkan dengan  undang-undang untuk dicantumkan dalam dokumen perceraian, surat jual-beli dengan membubuhkan sidik jari<br><strong><br></strong><p></p><p><strong>Penelitian Para Ilmuwan</strong><br></p><strong>\r\n\r\n<p></p></strong><br><ul><li>Tahun 1686 Prof. Marcello Malpighi, di Univ. Bologna melihat adanya garis-garis bentuk : spiral dan loop</li><li>Tahun 1823 prof. Johanes E Purkinye, di Univ. Breslau : golongkan garis – garis papil menjadi 9 ( sembilan ) jenis.</li><li>Tahun 1864 Dr. Hihemiah Grew, mengamati garis – garis pada ujung jari dengan gun mikroskop.</li><li>Tahun 1903 di penjara Leavan Worth Kansas, ditemukan kesamaan data antropometry, potret wajah dari 2 orang negro bernama Will West dan Willian West tetapi sidik jari berbeda</li></ul>\r\n\r\n<h3><strong>Beberapa Pendapat Terkait Ketidaksamaan dan Sifat Permanen Sidik Jari</strong></h3><p></p><ul><li>Mr. H,A, Asquith, seorang ahli statistic di Amerika Serikat terjadinya kesamaan detail antara satu sidik jari dengan yang lain adalah 1 orang dalam 54 milyard, kalau dihitung dengan ukuran waktu terjadinya 2.660.337 abad.</li><li>William Jenings, anggota Franklin Institute Philadelpia, mengambil sidik jarinya sendiri pada umur 27 th (1887) kemudian bandingkan dengan sidik jari setelah umur 77 th ternyata tidak terjadi perubahan.&nbsp;</li><li>Surat kabar London News of The World th 1937 menyediakan hadiah  $1000 bagi mereka yang mempunyai sidik jari sama dengan sidik jari yang disayembarakan.</li><li>Tahun 1958, Sir William James Hersohel, seorang pembesar inggris di Bengal ( Indian ) mempergunakan sidik jari jempol yang diterapkan untuk identifikasi seorang terhukum.&nbsp;</li><li>Tahun 1892, Sir Francis Galton menulis buku fingerprints  dengan mengatakan bahwa sidik jari tidak sama dan tidak berubah semasa hidup dan dapat dirumus.</li><li>Tahun 1901 sejak ditemukan ilmu Daktilopski , maka sistem anthrometry yang diciptakan oleh Alponse Bertilion (th 1860) tidak digunakan dalam kepolisian karena mempunyai beberapa kelemahan yakni selain bisa terjadi kesalahan pengukuran juga tidak tepat diterapkan terhadap orang yang belum dewasa dan administrasinya sangat rumit.</li><li>Tahun 1901, Sir Edward Richard Henry kembangkan sistem Galton menjadi Galton-Henry.</li><li>Tahun 1914 sistem Galton Henry mulai dikembangkan di indonesia.</li><li>Tahun 1960, sistem ini resmi digunakan oleh Polri</li></ul><p></p><h3><strong>Beberapa Istilah Dalam Sidik Jari</strong></h3><p><strong></strong><b>Fokus Point</b></p><p></p><ul><li>Delta?(outer termius) titik fokus luar</li><li>Core?(inter minus) titik fokus dalam</li></ul><p></p><p></p><p></p><p></p><p><b>Type Linnes (Garis Pokok Lukisan)</b></p><p>Dua garis sidik jari yang letaknya paling dalam pada permulaannya berjalan sejajar, kemudian bersama-sama memisah, memeluk atau akan memeluk garis pokok lukisan sidik jari.</p><p><b>Patern Area (Pokok Lukisan)</b></p><p>Daerah / ruangan putih yang dikelilingi oleh garis tipe lines untuk tempat lukisan garis sidik jari yang diperlukan guna perumusan (terutama pada golongan loop dan whorl).</p><p><b>Delta</b></p><p>Titik / garis yang terdapat pada pusat perpisahan garis tipe linnes</p><p><b>Core</b></p><p>Suatu titik tengah yang terdapat / terletak pada garis sidik jari loop yang terdalam dan terjauh dari delta.<br><br><b>Apendage (Garis Sambungan)</b></p><p>Garis yang merusak garis sidik jari bila appendage tersebut menempati atau berada di suatu garis sidik jari, maka garis yang ditempati itu spoi (mati)<br><br><b>Inter Locking Loop</b><br>Dua garis loop yang terdalam yang saling memasuki / memotong satu sama lai</p><strong><br>\r\n\r\n</strong><p></p>\r\n\r\n<br><p></p><strong><p></p><br>\r\n\r\n<br></strong><p></p><p>\r\n\r\n<br></p>');

-- --------------------------------------------------------

--
-- Table structure for table `tb_cabang`
--

CREATE TABLE `tb_cabang` (
  `id` int(11) NOT NULL,
  `provinsi` varchar(255) DEFAULT NULL,
  `nama_cabang` varchar(255) DEFAULT NULL,
  `alamat` text,
  `jam_operasional` varchar(255) DEFAULT NULL,
  `no_hp` varchar(255) DEFAULT NULL,
  `maps` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_cabang`
--

INSERT INTO `tb_cabang` (`id`, `provinsi`, `nama_cabang`, `alamat`, `jam_operasional`, `no_hp`, `maps`, `website`) VALUES
(1, 'Banten', 'STIFIn Cabang Kota Tangerang Selatan', 'Jl. Setu Indah Raya No.16, RT.10 / RW.4, Setu, Kec. Cipayung, Kota Jakarta Timur, Daerah Khusus Ibukota Jakarta 13880', '08-00 - 21-00 WIB', 'https://wa.me/+6282288882779', 'https://g.co/kgs/kXWC4A', 'https://stifinfamily.com/'),
(2, 'Jawa Timur', 'STIFIn Cabang Kabupaten Jember', 'Jl. Pangandaran No.49, Plinggan, Antirogo, Kec. Sumbersari, Kabupaten Jember, Jawa Timur 68125', '00.00 - 24.00 WIB', 'https://wa.me/6285102412400', 'https://goo.gl/maps/g7cAmn9UzoLvhZGz5', 'https://pesantrennuris.net/'),
(3, 'Sumatera Utara', 'STIFIn Cabang Kota Binjai', 'Jl. Setia Budi Pasar 1 No.78D Tanjung Sari Medan Selayang Sumatera Utara Indonesia 20132', '09.00 - 17.00 WIB', 'https://wa.me/628116566677', 'https://goo.gl/maps/fd3rycnoCwkR8CwH8', 'https://stifinbrain.com/'),
(4, 'Jawa Tengah', 'STIFIn Cabang Kota Semarang', 'Jl. Raya Hankam  Ring Rudal Puskopau no 73 RT5 Jati Rahayu  kec. Pondok Melati  Kota Bekasi 17414 RW07 ', '08-00 - 21-00 WIB', 'https://wa.me/6287868504676', 'https://goo.gl/maps/zBTAREgtziaUMvsj6', 'https://stifinsemarang.com/'),
(5, 'Jawa Barat', 'STIFIn Cabang Kabupaten Bekasi', 'Perumahan Kemang Ifi Graha jl jakarta Blok A9 No 8A Kec.Jatiasih Kel.Jatirasa Bekasi Jawa barat 17424', '08.00-17.00 WIB', 'https://wa.me/6288809901661', 'https://goo.gl/maps/tXo5N3aPwQK2', 'https://stifinbekasi.com/'),
(6, 'Dki Jakarta', 'STIFIn Cabang Jakarta Utara', 'Jl. Patria Jaya VI No. 171 Bekasi', ' 08.30-17.30 WIB', 'https://wa.me/6289608513218', 'https://g.co/kgs/KVnMoC', 'https://www.stifinaction.com/'),
(7, 'Banten', 'STIFIn Cabang Kota Tangerang', 'Jl. H. Daud No.45 RT 02 RW 03, Sukabumi Selatan, Kebon Jeruk, Jakarta Barat', '08.00 - 17.00 WIB', 'https://wa.me/62895331643115', 'https://goo.gl/maps/7TcMJAaJtzuZB6T4A', 'https://www.tesstifin.com/tangerang/'),
(8, 'Dki Jakarta', 'STIFIn Cabang Jakarta Selatan', 'Jl Raya Pabuaran No.26B kel Jatiranggon, kec Jatisampurna, Bekasi', '09.00 - 17.00 WIB', 'https://wa.me/6285719281008', 'https://g.co/kgs/6pkQ74', 'https://www.stifincenter.com/'),
(9, 'Bali', 'STIFIn Kota Denpasar', 'Gunung Putri perumahan bukit Golf Riverside', '09.00 - 17.00 WIB', 'https://wa.me/6281284074727', 'https://maps.app.goo.gl/yfVvDFeNp6a6wQJv8', 'https://iknowyou.id/'),
(10, 'Banten', 'STIFIn Cabang Kota Serang', 'Jl. Baru Ciomas, Tembong Sawo RT 03 RW 02, Cipocok Jaya, Kota Serang', '00.00 - 24.00 WIB', 'https://wa.me/62887873242907', 'https://goo.gl/maps/wpSSamAQraiheaVA9', 'http://stifinbanten.blogspot.com/'),
(11, 'Singapore', 'STIFIn Cabang Singapore', 'Kedai Kafilah Jl. Raya Jati Makmur No.124, RT.004/RW.007, Jatimakmur, Kec. Pd. Gede, Kota Bks, Jawa Barat 17413 (Indonesia) - Alamat Madani Institute STIFIn Singapore Office PL, 218 Changi Road, PKMS Bldg, #02-03, Singapura 419737 ( Singapura) ', '09.00 - 17.00 WIB', 'https://wa.me/6287873242907', 'https://maps.app.goo.gl/VxWk2H43N14nbnHTA', 'http://stifinasia.com'),
(12, 'Sumatera Utara', 'STIFIn Cabang Kota Medan', 'Jl. Garu II A No.44D, Harjosari I, Kec. Medan Amplas, Kota Medan, Sumatera Utara 20147', '09.00 - 17.00 WIB', 'https://wa.me/6285260366356', 'https://maps.app.goo.gl/CoXPLHYKaipZtxwr5?g_st=iw', 'https://www.instagram.com/stifin.medan/'),
(13, 'Dki Jakarta', 'STIFIn Cabang Jakarta Timur', 'The Habibie Center, Jl. Kemang Selatan No.98 Kemang, Jaksel 12560', '09.00 - 17.00 WIB', 'https://wa.me/6281802219090', 'https://maps.app.goo.gl/UbDXH7x6dDuXGR2P8', 'https://instagram.com/the_jackteam'),
(14, 'Sulawesi Selatan', 'STIFIn Cabang Kota Makassar', 'Jl Perintis Kemerdekaan Ruko Puri Kencana Sari Blok A No 35 Tamalanrea Makassar ', '09.00 - 17.00 WIB', 'https://wa.me/6281342606133', 'https://maps.app.goo.gl/wATxjgzwQCPz3irGA', 'https://www.instagram.com/stifinfirdauslife/'),
(15, 'Jawa Timur', 'STIFIn Cabang Kota Malang ', 'Jalan ikan tombro samping futsal SM Sudimoro malang', '08.00 - 16.00 WIB', 'https://wa.me/628123305574', 'https://www.google.com/maps/dir/-6.4210282,106.9112866/rumah+cerdas+malang/@-7.0373896,105.3016043,6z/data=!3m1!4b1!4m9!4m8!1m1!4e1!1m5!1m1!1s0x2dd629c2cd9127b3:0x6abae9ecfe529cc4!2m2!1d112.6302708!2d-7.932476', 'https://www.rumahcerdas.id/'),
(16, 'Dki Jakarta', 'STIFIn Cabang Kota Kuningan ', 'Jl. RE Martadinata No. 109 RT 05/01 Kelurahan Ciporang, Kec./Kab. Kuningan...', '09.00 - 17.00 WIB', 'https://wa.me/6281808183785', 'https://www.google.com/maps/place/6%C2%B058\'11.2%22S+108%C2%B030\'05.3%22E/@-6.96978,108.50146,15z/data=!4m4!3m3!8m2!3d-6.96978!4d108.50146?hl=id-ID&gl=US', 'https://www.instagram.com/stifinkuningan/'),
(17, 'Jawa Barat', 'STIFIn Cabang Kabupaten Bandung Barat', 'Bojong Koneng Makmur  Tengah 11 Bandung 40125', '09.00 - 17.00 WIB', 'https://wa.me/628132180916', 'https://maps.app.goo.gl/N7sM4fpm1DUz75xv5', 'stifintest.com'),
(18, 'Jawa Barat', 'STIFIn Cabang Kota Depok', 'Wakaf tower ruko graha Depok mas blok A no.17-18 jalan.Arif Rahman Hakim no .3 ', '09.00 - 17.00 WIB', 'https://wa.me/6281213513131', 'https://maps.app.goo.gl/Agqitf4r5rMj9nHc8', 'www.stifindepok.com'),
(19, 'Malaysia', 'STIFIn Cabang Kota Selangor', '207A, Jalan 8/1, Seksyen 8, Bandar Baru Bangi, 43650 Bangi Selangor Malaysia', '09.00 - 17.00 WIB', 'https://wa.me/+60139115114', '2.9629804, 101.7584177', 'http://stifinmalaysia.com/'),
(20, 'Banten', 'STIFIn cabang Kabupaten Tangerang', 'Komplek karang tengah permai blok tq 3 ', '09.00 - 17.00 WIB', 'https://wa.me/6288298652208', 'https://maps.app.goo.gl/gqwk9ToJnsk2Z89c7', 'https://biolinky.co/stifinamazing'),
(21, 'Malaysia', 'STIFIn Cabang Kota Terengganu', 'Tingkat Atas , MBKT 375E Jalan Sultan Omar 20300 Kuala Terengganu Terengganu Malaysia', '09.00 - 17.00 WIB', 'https://wa.me/+60103787865', 'https://maps.app.goo.gl/brKws9eRDDg35FTE8?g_st=iw', 'www.stifin.com.my'),
(22, 'Kalimantan Timur', 'STIFIn Cabang Kalimantan Timur', 'Kota Bontang', '09.00 - 17.00 WIB', 'https://wa.me/6285261979191', '-', '-'),
(23, 'Di Yogyakarta', 'STIFIn Cabang Yogyakarta', 'Grand Dhika City Lifestyle Jatiwarna Tower Emerald RE-09 Bekasi', '09.00 - 17.00 WIB', 'https://wa.me/6281371555168', 'https://goo.gl/maps/3Btt1EBYJq9Eyj9Q6', 'www.stifinvestment.com'),
(24, 'Jawa Barat', 'STIFIn Cabang Kota Bekasi', 'Jl. Babarsari No.1, Tambak Bayan, Caturtunggal, Kec. Depok, Kabupaten Sleman, Daerah Istimewa Yogyakarta 55281', '09.00 - 17.00 WIB', 'https://wa.me/6281371555168', 'https://goo.gl/maps/HtZeddaAkJRbkwqJ8', 'www.stifinkita45.com');

-- --------------------------------------------------------

--
-- Table structure for table `tb_cabang_section`
--

CREATE TABLE `tb_cabang_section` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `deskripsi` text,
  `gambar` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_cabang_section`
--

INSERT INTO `tb_cabang_section` (`id`, `judul`, `deskripsi`, `gambar`) VALUES
(4, 'Menjadi Cabang STIFIn', 'Cabang STIFIn adalah perorangan atau badan hukum yang membeli paket cabang STIFIn yang diajukan untuk 1 (satu) Kota/Kab di Seluruh Indonesia. Cabang tersebut diberi hak penuh untuk mengelola area di Kota/Kab tersebut mulai dari rekrutmen Promotor STIFIn dan menyelenggarakan Workshop STIFIn. Setiap Kota/Kab hanya ada 1 (satu) cabang. ', 'Artikel_STIFIn_1.png');

-- --------------------------------------------------------

--
-- Table structure for table `tb_dc_about_cabang`
--

CREATE TABLE `tb_dc_about_cabang` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `deskripsi` text,
  `gambar` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_dc_about_cabang`
--

INSERT INTO `tb_dc_about_cabang` (`id`, `judul`, `deskripsi`, `gambar`) VALUES
(5, 'Apa itu Cabang STIFIn?', '<p>Cabang STIFIn adalah perorangan atau badan hukum yang membeli paket cabang STIFIn yang diajukan untuk 1 (satu) Kota/Kab di Seluruh Indonesia. Cabang tersebut diberi hak penuh untuk mengelola area di Kota/Kab. tersebut, di setiap Kota/Kab hanya ada 1 (satu) cabang.<br></p>', '51.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_dc_alur_tes`
--

CREATE TABLE `tb_dc_alur_tes` (
  `id` int(11) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `deskripsi` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_dc_alur_tes`
--

INSERT INTO `tb_dc_alur_tes` (`id`, `icon`, `judul`, `deskripsi`) VALUES
(4, 'fa-book', 'Pendaftaran', '<p></p><div><div>Yayasan STIFIn Menerima Hibah dari Cabang</div></div><p></p>'),
(5, 'fa-sitemap', 'Kedua', '<p></p><div><div>Cabang STIFIn Menjual Voucher Seharga @250.000 ke Promotor</div></div><p></p>'),
(6, 'fa-users', 'Ketiga', '<p></p><div><div>Promotor Menjual Tes STIFIn Seharga @500.000 ke Klien</div></div><p></p>'),
(7, 'fa-comments', 'Keempat', '<p></p><div><div>Klien Membayar Seharga @500.000 dan Mendapatkan Penjelasan Hasil Tes STIFIn dari Promotor</div></div><p></p>');

-- --------------------------------------------------------

--
-- Table structure for table `tb_dc_pilihan_cabang`
--

CREATE TABLE `tb_dc_pilihan_cabang` (
  `id` int(11) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `deskripsi` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_dc_pilihan_cabang`
--

INSERT INTO `tb_dc_pilihan_cabang` (`id`, `icon`, `judul`, `deskripsi`) VALUES
(5, 'fa-book', 'CABANG REGULER (Berlaku 1 Tahun)', '<p></p><ul><li>Cabang Reguler STIFIn merupakan cabang STIFIn yang memiliki masa berlaku hanya 1 tahun dan akan mendapatkan voucher STIFIn sejumlah 1600 voucher. Jika Cabang ingin dilanjutkan maka harus refill (membeli voucher lagi) dalam keadaan voucher sebelumnya habis ataupun tidak habis.</li><li>Cabang Reguler bisa mengadakan workshop dan merekrut promotor selama status cabang aktif.</li></ul><p></p>'),
(6, 'fa-user', 'CABANG PERMANEN (Tanpa Batas Waktu)', '<p></p><ul><li>Cabang Permanen STIFIn merupakan cabang STIFIn yang memiliki masa berlaku tanpa batas waktu dan akan mendapatkan voucher STIFIn sejumlah 4800 voucher. Jika voucher cabang telah habis maka harus refill (membeli voucher lagi). Setelah mencoba bisnis selama setahun dan penjualan voucher cabang tidak mencapai 500 voucher, maka voucher tersisa dapat dikembalikan tanpa potongan apapun.</li><li>Cabang Permanen bisa mengadakan workshop dan merekrut promotor selama status cabang aktif.</li></ul><br><p></p>'),
(7, 'fa-book', 'CABANG KHUSUS (Berupa Konsorsium & Tanpa Batas Waktu)', '<p></p><ul><li>Cabang Khusus STIFIn merupakan cabang STIFIn yang diberlakukan kepada 10 cabang baru yang membentuk konsorsium.</li><li>Cabang Khusus akan mendapatkan voucher secara gratis selamanya, dengan mekanisme yang diatur. Namun voucher tersebut dijual kepada promotor dengan harga mutlak 150 ribu per voucher.</li><ul><li>Jika menjalankan sendiri maka income 150 ribu dari pembelian promotor per voucher diambil semuanya oleh investor.<br></li><li>Jika bisnis diserahkan kepada kami, maka yang 150 ribu dibagi sbb: investor duduk manis mendapatkan 50 ribu, direktur konsorsium mendapatkan 20 ribu, dan kepala cabang mendapatkan 80 ribu dari setiap penjualan voucher tes. Dengan cara ini direktur konsorsium harus memastikan target 3600 tes per tahun bisa tercapai oleh kepala cabang. Sehingga bagi investor modal kembali dalam 3 tahun, sejak tahun ketiga panen 180 juta per tahun.<br><br></li></ul><li>Investor diberi 2 pilihan: menjalankan sendiri bisnisnya atau dipercayakan pada kami.<br></li></ul><p></p>');

-- --------------------------------------------------------

--
-- Table structure for table `tb_dc_popup`
--

CREATE TABLE `tb_dc_popup` (
  `id` int(11) NOT NULL,
  `gambar` varchar(255) DEFAULT NULL,
  `waktu` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_dc_popup`
--

INSERT INTO `tb_dc_popup` (`id`, `gambar`, `waktu`) VALUES
(4, 'Infografis_Investasi_5M.jpg', '2023-04-02'),
(5, 'Poster_Kecab_100_jt_.jpg', '2023-04-02');

-- --------------------------------------------------------

--
-- Table structure for table `tb_dc_potensi`
--

CREATE TABLE `tb_dc_potensi` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `deskripsi` text,
  `gambar` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_dc_potensi`
--

INSERT INTO `tb_dc_potensi` (`id`, `judul`, `deskripsi`, `gambar`) VALUES
(5, 'Potensi Usaha Cabang STIFIn?', '<p>Tes STIFIn mulai dari awal berdirinya 2006 sudah ada Setengah Juta orang yang merasakan manfaatnya atau baru sekitar 1% dari 273,52 juta lebih jiwa penduduk Indonesia (Databoks, 2023).</p><p>Katakanlah, secara nasional hanya 25% saja sebagai target, artinya masih ada sekitar 68.380.000 penduduk yang belum tes. Ini menunjukkan peluang bisnis yang sangat besar dan potensial untuk para pengusaha di luar Jakarta.</p><p>Kami menawarkan kesempatan usaha di bidang Jasa Pengembangan SDM/Pendidikan, dengan membuka Cabang STIFIn di 500 lebih kota/kabupaten diseluruh Indonesia. Keunggulan Produk STIFIn :</p><p></p><ul><li>Produk tidak basi, berumur panjang, sampai kapanpun dapat diperjualbelikan.</li><li>Produk berupa Tes STIFIn yang memberikan banyak manfaat bagi penggunanya pada area pasar yang sangat luas. Produk merupakan kebutuhan dasar setiap orang. Sudah terbukti lebih dari 25 tahun.</li><li>Investasi hanya sebesar belanja produk (berupa voucher tes) tidak memerlukan biaya-biaya lain.</li></ul><p></p>', 'Cabang_dan_promotor.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_dc_sumber_profit`
--

CREATE TABLE `tb_dc_sumber_profit` (
  `id` int(11) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `deskripsi` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_dc_sumber_profit`
--

INSERT INTO `tb_dc_sumber_profit` (`id`, `icon`, `judul`, `deskripsi`) VALUES
(5, 'fa-user', 'Voucher Tes STIFIn', '<p>Voucher Tes STIFIn merupakan produk utama cabang sehingga profit terbesar cabang berasal dari penjualan voucher Tes STIFIn kepada promotornya.</p>'),
(6, 'fa-cog', 'Konsultasi', '<p>Konsultasi merupakan produk sampingan ketika cabang menerima kerjasama dengan salah satu instansi pemerintah/non pemerintah, sekolah atau instansi lainnya. Pihak yang berhak memberikan konsultasi adalah pihak yang memiliki&nbsp;<i>Lisensi Solver STIFIn&nbsp;</i>oleh karena itu Cabang sangat disarankan selain memiliki Promotor juga memiliki Solver dan Trainer STIFIn.&nbsp;</p>'),
(7, 'fa fa-cog', 'Seminar, Training dan Workshop', '<p>\r\n\r\nSeminar, Training dan Workshop merupakan juga produk sampingan. Workshop STIFIn akan diadakan Cabang ketika Cabang ingin menambah promotor atau rekrutmen promotor. Training akan diadakan Cabang ketika Cabang mendapatkan klien/customer suatu instansi. Sedangkan Seminar akan diadakan ketika Cabang ingin mempolerkan <i>Branding Cabang.&nbsp;</i>Pihak yang berhak memberikan training adalah pihak yang memiliki <i>Lisensi Trainer STIFIn </i>oleh karena itu Cabang sangat disarankan selain memiliki Promotor juga memiliki Solver dan Trainer STIFIn. \r\n\r\n<br></p>'),
(8, 'fa fa-user', 'Buku dan Merchandise', '<p>Setiap Cabang STIFIn sangat diperbolehkan untuk ikut mempromosikan Buku-Buku STIFIn kepada para Pegiat STIFIn di Cabangnya dan berhak membuat dan memperjualbelikan merchandise Cabang dengan tujuan untuk meningkatkan Branding Cabang.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `tb_download_aplikasi`
--

CREATE TABLE `tb_download_aplikasi` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `deskripsi` text,
  `icon` varchar(100) NOT NULL,
  `file` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_download_aplikasi`
--

INSERT INTO `tb_download_aplikasi` (`id`, `judul`, `deskripsi`, `icon`, `file`) VALUES
(11, 'Drive GR Finger', 'GR Finger adalah Driver Aplikasi Promotor STIFIn, Driver digunakan apabila Scanner Menyala, tapi tidak berfungsi', 'fa-hand-o-up', 'GrFinger.rar'),
(12, 'Drive U Are U', 'UareU adalah Driver untuk Scanner FingerPrint, Driver ini digunakan apabila Lampu Scanner tidak menyala', 'fa-cog', 'UareU.rar'),
(13, 'Reset Password Promotor', 'SFT_reset.rar adalah Aplikasi ini digunakan untuk me-RESET Password Promotor, menjadi \"admin\" dan hanya dapat dipakai sekali saja', 'fa-undo', 'SFP_Reset.rar'),
(14, 'Aplikasi Promotor V 11.0.0', 'Aplikasi ini digunakan untuk Promotor baru atau Promotor Lama yg di Instal Ulang', 'fa-shopping-cart', 'SetupPromotor_V11_0_0.zip');

-- --------------------------------------------------------

--
-- Table structure for table `tb_download_audio`
--

CREATE TABLE `tb_download_audio` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `audio` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_download_audio`
--

INSERT INTO `tb_download_audio` (`id`, `judul`, `audio`) VALUES
(1, 'audio 1', 'Christina_Perri_-_Jar_of_Hearts.mp3'),
(2, 'audio 2', 'Christina_Perri_-_Jar_of_Hearts1.mp3');

-- --------------------------------------------------------

--
-- Table structure for table `tb_download_materi`
--

CREATE TABLE `tb_download_materi` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `deskripsi` varchar(255) DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_download_materi`
--

INSERT INTO `tb_download_materi` (`id`, `judul`, `deskripsi`, `file`) VALUES
(3, '9 Personaliti Genetik', 'Baru Mengenal STIFIn?\r\nWajib Baca Buku Ini!', 'Ebook9PG.pdf'),
(4, 'Penjelasan Singkat Hasil Tes STIFIn ', 'Simple dan Mudah Dipahami!\r\nRingkasan Penjelasan Hasil Tes STIFIn ', 'Penjelasan_Hasil_Tes_STIFIn.pdf'),
(5, 'STIFIn Personality 1.0', 'Mengenali Karakter dan Potensi dengan STIFIn Personality', 'STIFIn_Personality_1_0.pdf'),
(6, 'Flyer X-Banner Tes STIFIn', 'Toolskit Marketing Bisnis STIFIn yang WAJIB Anda Miliki!', 'Flyer_Tes_STIFIn_10x21_-_A4_dapat_3_-_Bolak_Balik_.pdf'),
(7, 'Brosur Tes STIFIn ', 'Toolskit Marketing Bisnis STIFIn ', 'Brosur_Tes_STIFIn_.pdf');

-- --------------------------------------------------------

--
-- Table structure for table `tb_download_video`
--

CREATE TABLE `tb_download_video` (
  `id` int(11) NOT NULL,
  `link_embed` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_download_video`
--

INSERT INTO `tb_download_video` (`id`, `link_embed`) VALUES
(4, 'SNn_2iZe1ls'),
(5, 'F9_5Oond1yM'),
(6, 'Sr2S6CXX8w0'),
(7, 'ooXaeWvyT8A'),
(8, 'uTbHq0tRDc0'),
(9, 'ucepKk8JkX4'),
(10, 'WMSo-buuDkc'),
(11, '3i4Pp73-yus'),
(12, 'erLS14vhQmI');

-- --------------------------------------------------------

--
-- Table structure for table `tb_dp_about`
--

CREATE TABLE `tb_dp_about` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `deskripsi` text,
  `gambar` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_dp_about`
--

INSERT INTO `tb_dp_about` (`id`, `judul`, `deskripsi`, `gambar`) VALUES
(6, 'Apa itu Promotor ?', '<p>Promotor STIFIn merupakan salah satu profesi STIFIn yang sangat populer dengan dibekali pemahaman terkait konsep STIFIn sehingga sangat mumpuni ketika berhubungan dengan klien Tes STIFIn. <br></p>', 'Artikel_STIFIn_2.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_dp_benefit`
--

CREATE TABLE `tb_dp_benefit` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `deskripsi` text,
  `gambar` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_dp_benefit`
--

INSERT INTO `tb_dp_benefit` (`id`, `judul`, `deskripsi`, `gambar`) VALUES
(6, 'Benefit Menjadi Promotor', '<p>Promotor STIFIn akan mendapatkan keuntungan utama berupa profit dari penjualan Tes STIFIn dan sesi private penjelasan hasil Tes STIFIn. Selain itu Promotor akan mendapatkan relasi baru dan menambah pengalaman karena bertemu banyak orang dengan&nbsp;<i>background story&nbsp;</i>yang berbeda-beda.<br></p>', 'Artikel_STIFIn_(1)2.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_dp_roadmap`
--

CREATE TABLE `tb_dp_roadmap` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `konten` text,
  `tanggal_roadmap` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_dp_roadmap`
--

INSERT INTO `tb_dp_roadmap` (`id`, `judul`, `konten`, `tanggal_roadmap`) VALUES
(22, NULL, '<h5>WSL 1</h5><h3><i>Find The True You - Gue Banget</i><br></h3><p>Workshop STIFIn Level 1 sebagai materi dasar yang membahas tentang 5 (lima) Mesin Kecerdasan dan 9 (sembilan) Personaliti Genetik. Workshop ini akan menguak jati diri Anda yang sebenarnya, sehingga akan terasa Ini Gue Banget. Pelaksanaan WSL 1 berlangsung selama 1 hari.<br></p>', '2023-04-01 09:52:53'),
(23, NULL, '<h5>WSL 2</h5><h3><i>Licensed Promotor STIFIn - Tentang Loe, Gue Ahlinya</i><br></h3><p>Workshop STIFIn Level 2 merupakan workshop yang diperuntukkan bagi Anda yang ingin mengetahui lebih dalam tentang diri Anda dan orang lain, maka workshop ini wajib Anda ikuti sehingga Anda bisa mengambil Peluang Bisnis STIFIn dengan menjadi Licensed Promotor STIFIn. Pelaksanaan WSL 2 berlangsung selama 1 hari.<br></p>', '2023-04-01 09:53:34'),
(24, NULL, '<h3><i>Scanner + ID</i><br></h3><p>Pembelian scanner dan ID dilakukan melalui cabang STIFIn<br></p>', '2023-04-01 09:54:20'),
(25, NULL, '<h3><i>Laptop</i></h3><p>Aplikasi tes STIFIn dapat diunduh atau didownload di website stifin.com versi 11.0 kemudian diinstal di laptop promotor dengan panduan yang telah diberikan.<br></p>', '2023-04-01 09:54:36'),
(26, NULL, '<h3><i>Ready</i></h3><p>Promotor STIFIn siap untuk melakukan pengetesan STIFIn dan menebar manfaat SuksesMulia<br></p>', '2023-04-01 09:54:54');

-- --------------------------------------------------------

--
-- Table structure for table `tb_feeling`
--

CREATE TABLE `tb_feeling` (
  `id` int(11) NOT NULL,
  `gambar` varchar(255) DEFAULT NULL,
  `konten` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_feeling`
--

INSERT INTO `tb_feeling` (`id`, `gambar`, `konten`) VALUES
(4, '172.png', '<h1>Feeling introvert (Fi) vs Feeling extrovert (Fe)</h1>'),
(5, 'Artikel_STIFIn_(10)2.jpg', '<p>\r\n\r\n</p><h3>Feeling introvert (Fi)</h3>\r\n<p>Fi adalah singkatan dari Feeling introvert. Jika huruf F berdiri sendiri merupakan identitas sebagai Mesin Kecerdasan (MK). Menurut konsep STIFIn, ragam Mesin Kecerdasan hanya ada lima, dan F adalah salah satu diantara 5 MK tersebut. Identitas Mesin Kecerdasan berubah menjadi kepribadian ketika MK digandengkan dengan jenis kemudi di belakangnya. Jenis kemudi kecerdasan hanya ada dua, yaitu i (introvert) dan e (extrovert). Dengan demikian, Fi sudah menjadi identitas kepribadian. F ditulis dengan huruf besar karena pengaruhnya sebagai MK lebih besar dari i yang ditulis dengan huruf kecil yang berperan hanya sebagai kemudi kecerdasan.  <br></p><h4><ul><li>Sistem Operasi Otak</li></ul></h4><p>Pengertian sederhana dari Feeling introvert adalah jenis kepribadian yang berbasiskan kecerdasan emosi atau perasaan yang proses kerjanya dikemudikan dari dalam dirinya menuju ke luar dirinya. Sistem operasi pada tipe Fi berada di belahan otak bagian bawah di sebelah kanan atau disebut sebagai limbik kanan. Pada limbik kanan tersebut yang menjadi kemudi kecerdasan dari tipe ini berada di lapisan putih yang letaknya di bagian dalam. Limbik kanan putih itulah yang menjadi sistem operasi tipe Fi. Lapisan yang berwarna putih memiliki tekstur otak yang lebih padat karena mengandung sel otak lebih banyak. Kerapatan yang lebih tinggi dibandingkan dengan lapisan bagian luar tersebut membuatkemudi kecerdasan bergerak<br>dari  dalam  ke  luar.  Hal  ini menyebabkan ‘tuan yang punya badan’ menjadi seolah  tidak berhenti untuk tebar pesona atau memancarkan kharismanya sebagai pemimpin.<br>\r\n\r\n<b></b></p><blockquote><b>Tipe Ii : Otak kanan putih, Dalam ke luar, Motorik kasar, Astenis</b></blockquote><p></p><p></p><p></p><ul><li><b>Tipologi Fisik </b></li></ul><b></b>Mesin Kecerdasan Feeling (F) sesungguhnya identik dengan pernafasan. Tipe ini memiliki kapasitas pernafasan yang besar. Kapasitas pernafasan yang kuat tersebut disebabkan tipe F memiliki mesin yang besar, yaitu berupa jantung dan paru-paru yang besar. Dengan mesin yang besar, tipe ini dapat memompa oksigen yang lebih banyak ke dalam darah, terlebih lagi tipe F dengan kemudi introvert (i). <br>  <br>Tipe Fi ditunjang oleh ketersediaan baterai (charger) yang ada di dalam dirinya, sehingga menyebabkan tipe ini seperti memiliki kekuatan emosi yang lebih kuat. Bentuk (konstitusi) fisik tipe F secara umum disebut sebagai displastik, karena mesin yang besar tersebut didukung oleh tulang yang kecil sehingga bawaannya terasa berat.<div><br></div><div>Dengan mesin besar, maka bentuk badan dari tipe Fi terlihat lebar dan tebal. Akibatnya, bagian punggung dari tipe ini seperti lebih sulit digerakkan (disebut displastik). Pada tipe Fi yang memiliki charger baterai di dalam, maka daya pancar emosi tipe ini pun lebih kuat. Secara fisik tipe ini dapat menggunakan pernafasan dalam yang bersumber dari paru-parunya. Oleh karena itu, kekuatan pernafasan dari mesin yang besar tersebut dapat digunakan untuk jenis olahraga yang memerlukan stamina jarak jauh dan jangka panjang seperti lari marathon.<p></p><p></p><p></p><ul><li><b>Sifat Khas</b></li></ul>Jika menggunakan sudut pandang dunia psikologi (aliran perilaku) kepribadian Fi mesti memiliki sifat perilaku khas yang dapat dibuktikan dan diukur yang berbeda dari delapan kepribadian yang lain. Terdapat sepuluh item yang bisa dibuktikan keberadaannya dan bisa diukur secara psikometrik. <br><br>Menurut konsep STIFIn, ke sepuluh item tersebut menjadi kepribadian tetap yang tidak akan berubah dan akan selalu eksis seiring dengan penambahan umurnya. Sepuluh (10) sifat yang tetap tersebut adalah:  <br><ul><li>Idealistic</li><li>Leader</li><li>Kindful</li><li>Reflective</li><li>Convincing</li><li>Promoter</li><li>Diplomatic</li><li>Friendship-appeal</li></ul><p>Sebagai pribadi yang utuh, tipe Fi memiliki sisi-sisi diametral sebagai berikut: semangat tinggi namun dibalik itu ingin dimanja dan diperhatikan, perkataannya halus dan lembut namun terkadang bisa juga menyengat, pemimpin yang berkharisma namun kebiasaaan buruknya mudah tersinggung. </p><p>Selain itu, tipe ini suka menolong namun menghitung nilai balas budi, komunikator yang mempengaruhi orang lain namun komitmen terhadap ajarannya lemah, mampu berempati namun terkadang lamban beraksi. Oleh karena itu, tipe Fi perlu waspada dengan kelemahannya dan berusaha mengekploitasi kelebihannya. Biasanya jika kelebihannya bergerak membaik maka secara otomatis kelemahan dari tipe ini akan tertutup dengan sendirinya.<br></p><ul><li><b>Kelebihan</b></li></ul><p></p><p>Kepribadian   dari tipe Fi  memiliki  kekhasan  karena memiliki  kemampuan emosi yang  melebihi delapan jenis kepribadianyanglain.Kelebihan ini dapat disepadankan dengan kecerdasan emosi atau disebut Dengan kekuatan pernafasan yang panjang, tipe ini memiliki potensi kekuatan mental yang lebih baik, mampu mendengar bahasa hati orang lain lebih lama. Tipe ini bahkan mampu menjaga perasaan dan berempati lebih baik dari orang lain. Selain itu, tipe F umumnya seperti memiliki kadar cinta yang lebih banyak, baik untuk mencintai ataupun dicintai. Pancaran kekuatan emosinya menjadikan tipe ini disukai oleh banyak orang (populer). Kepopuleran dari tipe ini merupakan kelebihannya. Dalam hal pengelolaan keuangan, tipe Fi terlihat sangat bossy (seperti juragan). Ia mudah menolong orang lain dengan harapan itu akan memperkuat posisinya sebagai pemimpin.<br></p><p>\r\n\r\n</p><blockquote><i><b>Tipe Fi : Emotional Quotient (EQ), Kemampuan emapti, Populer</b></i></blockquote>\r\n\r\n<p></p><div><i><ul><li><b>Kemistri</b><br></li></ul></i>Jalan  keberhasilan  dari  tipe  Fi  ditentukan  oleh kemampuannya  membuat  platform   untuk memperkuat pengaruh.   Dengan platform  itulah sosok bertipe Fi akan memiliki posisi tawar yang tinggi. Penting untuk digaris bawahi bahwa platform yang dibentuk oleh tipe ini seringkali platform yang diikat secara emosional.</div><div><br></div><div>Oleh karena itu, sejak awal setup platform dari tipe Fi tersebut   harus didisain secara masak-masak agar dapat mewadahi visi yang diperjuangkannya. Tentu saja platform ini akan mudah runtuh jika tidak diberi energi positif yang maksimal. Apalagi jika platform tersebut senantiasa berubah-ubah demi kepentingan pragmatis. <br> </div><div><br></div><div>Padahal, membangun platform visioner itu memakan masa yang panjang. Meskipun tipe Fi memiliki kemistri cinta, ia mesti mengikat kemistri cinta itu dengan sesuatu yang berdampak jangka panjang. Dalam menggunakan kekuatan kharismanya, tipe ini menebarkan pengaruh  terutama dalam hal visi untuk mendapatkan cinta dari pengikutnya. Oleh karena itu, visi tersebut mesti diberi jiwa (sentuhan emosi) supaya lebih banyak orang yang terikat dengan kepemimpinannya.</div><div>\r\n\r\n<b><i><blockquote>Tipe Fi : Membuat PLATFORM, Menjaring CINTA, Leader</blockquote></i></b></div><div><ul><li>\r\n\r\n<b>Peranan </b></li></ul>  Dengan  fungsi  kepala  kanan yang bagus, tipe I lebih menyukai pekerjaan-pekerjaan berkelas yang membutuhkan kualitas yang bersumber  dari  sumberdaya kreativitas yang dimilikinya. Jika   digabungkan dengan kepala kanan  akan membuat tipe I lebih menyukai pekerjaan-pekerjaan necis yang tidak kotor oleh keringat. Hal itu juga yang membuat tipe I memilih peran sebagai sutradara di belakang layar yang memberii ide dan inspirasi kepada para pemain yang berada di lapangan atau di panggung.<br><br>Jika tipe I dikemudikan dari dalam menjadi tipe Ii- akan muncul sifat ingin serba sempurna. Hal itu membuat tipe Ii menyukai peran sebagai seorang penggagas yang menginisiasi (memulakan) pekerjaan dengan target melakukan perubahan atau memberi nilai tambah pada pekerjaannya. Secara umum, kemampuan menangkap bahasa langit atau memprediksi kejadian membuat tipe I berani mengambil risiko. Hal inilah yang membuat tipe ini sangat berminat menjadi pengusaha atau pebisnis yang sejatinya berani menghadapi risiko gagal.</div><div><ul><li><b>Target dan Harapan</b></li></ul>Dengan modal kepemimpinan tipe Fi yang kharismatik dan terpancar dari dalam, keseharian dari tipe ini selalu menebarkan pesona. Ketika kepemimpinan tipe Fi diterima oleh lingkungan sekitar,  maka hal tersebut   sesuai dengan pengharapannya. Aktualisasi diri dari tipe ini terjadi jika ia memiliki fungsi kepemimpinan. Bahkan jika tidak mendapatkannya, tipe ini akan terus akan mencari jalan leadership untuk kemudian dikukuhkan sebagai pemimpin.</div><div><br></div><div> Dengan modal kepemimpinan inilah tipe Fi menawarkan idealisme baru. Biasanya tipe Fi memang merupakan seorang ideolog yang memiliki pandangan visioner. Sumber ideologi tipe ini datang dari kematangan emosinya yang sejalan dengan jiwa perjuangannya. Kekuatan emosi dari tipe ini yaitu memahami kedalaman jiwa dari masyarakat, yang perlu diperjuangkan oleh dirinya. Hal inilah yang membuat tipe Fi menargetkan visi tertentu untuk diperjuangkan bersama.</div><div><br></div><div>Salah satu jalan untuk memelihara pengaruh dari tipe Fi ialah dengan berkontribusi pada pengikutnya. Sebagai konsekuensi dari kasih sayang kepada pengikutnya, tipe ini kerap kali mengulurkan tangannya untuk membantu pengikutnya.<br></div><div>\r\n\r\n<i><b><blockquote>Tipe Fi : Idealisme/visi baru, Leading, Kemampuan leadership</blockquote></b></i></div><div><p></p><p></p><ul><li>&nbsp;<b>Arah Merek</b></li></ul>Kekuatan kepemimpinan dari tipe Fi sekaligus menjadi merek bagi dirinya. Tipe ini diberi merek sebagai sosok yang memiliki kepemimpinan yang baik. Untuk memiliki merek seperti ini, tipe Fi tidak harus secara formal memimpin suatu organisasi tertentu, namun secara informal pun, merek kepemimpinan tersebut melekat pada dirinya. <br> <br>Jika tipe Fi tidak memiliki merek kepemimpinan, maka ia seperti tercerabut dari akar. Hal ini dapat diartikan bahwa sumber kekuatan utama dari tipe ini dalam memancarkan emosi kasih sayang ke masyarakatnya, macet.<br><ul><li><b>Cara Belajar</b></li></ul><b></b><p>Proses belajar yang baik bagi tipe Fi adalah menjadi pendengar yang baik meskipun ia begitu tergoda untuk berbicara. Tipe Fi suka ngomong dan menebar pesona dengan omongannya. Namun tipe ini akan belajar lebih banyak jika ia mendengar. Maka ketika hadir di dalam kelas, ia cukup berkonsentrasi mendengarkan penjelasan dari gurunya. <br></p><p>Jika perlu, penjelasan tersebut direkam dengan MP3 dan didengarkan berulangulang, sampai tipe ini mendapatkan ‘feel’nya. Bagi tipe Fi, memang sulit berkonsentrasi dalam durasi yang lama. <br></p><p>Tipe ini sering terbawa pada suasana emosinya. Hasil rekaman yang didengarkan ulang itu membuat tipe ini mendapatkan gambaran secara keseluruhan. Pendek kata, tipe F pada umumnya memang harus belajar menggunakan telinganya. Motivasi belajar dari tipe Fi akan naik seiring dengan mood. Namun begitu, jika tipe ini sedang mood, maka seperti tidak ada yang dapat menghentikannya.</p><p>Jika sedang semangat akan terlihat sekali, namun jika sedang malas, maka tipe ini akan susah untuk memulai kembali. Oleh karena itu, perlu ditempelkan sentuhan emosi setiap kali tipe ini merasa sedang kehilangan mood. Motivasi yang naik turun tergantung kondisi emosi sesaat. Permainan perasaan untuk memelihara tingkat motivasi dari tipe F memang membutuhkan kesabaran yang luar biasa.<br></p><ul><li><b>Pilihan Sekolah/ Profesi</b></li></ul>Prioritas utama jika ingin memilih jurusan atau profesi untuk tipe Fi sebaiknya diarahkan pada satu diantara empat pilihan industri berikut ini, yaitu : politik, kepemerintahan, hukum, kesenian. Selanjutnya pilihan lain dari jurusan atau profesi yang sesuai untuk tipe Fi sebagai berikut: psikolog, komunikasi, diplomat, humas, promotor, investor, negarawan, trainer/public speaker, administrasi negara, seniman, budayawan, hubungan internasional, salesman, inspirator, motivator, psikiater, counselor, ideolog, personalia, lawyer, birokrat, aktivis, atlit maraton, dll. <br> <br> Di masa depan, pilihan profesi akan semakin beragam. Namun jika ingin memilih jurusan ataupun profesi patokan bagi tipe Fi adalah mempertimbangkan empat kata kuncinya, yaitu: <b>merasakan, pernafasan, memimpin, dan dicintai. </b>Artinya, jurusan atau profesi yang dipilih oleh tipe ini perlu didominasi   oleh unsur-unsur yang membutuhkan keterlibatan emosi (termasuk kemampuan berkomunikasi dari   hati ke hati), secara fisik memerlukan nafas yang panjang atau semangat yang tinggi sehingga bisa bekerja atau bertanding secara marathon, serta jurusan/profesi yang melibatkan kharisma kepemimpinan, serta dapat mengeluarkan jurus-jurus tebar pesona supaya tipe ini dicintai lebih banyak orang.</div><div>\r\n\r\n<blockquote><b><i>Industri Tipe Fi : Politik, Kepemerintahan, Hukum, Kesenian</i></b></blockquote> <br><br></div>\r\n\r\n</div>\r\n\r\n<br><p></p>'),
(6, 'Artikel_STIFIn_(11).jpg', '<p>\r\n\r\n</p><h3>Feeling extrovert (Fe)</h3>\r\n<p>Fe adalah singkatan dari Feeling extrovert. Jika huruf F berdiri sendiri merupakan identitas sebagai Mesin Kecerdasan (MK). Menurut konsep STIFIn, ragam Mesin Kecerdasan hanya ada lima, dan F adalah salah satu diantara 5 MK tersebut. Identitas Mesin Kecerdasan berubah menjadi kepribadian ketika MK digandengkan dengan jenis kemudi di belakangnya. Jenis kemudi kecerdasan hanya ada dua, yaitu i (introvert) dan e (extrovert). Dengan demikian, Fe sudah menjadi identitas kepribadian. F ditulis dengan huruf besar karena pengaruhnya sebagai MK lebih besar dari e yang ditulis dengan huruf kecil yang berperan hanya sebagai kemudi kecerdasan. <br></p><h4><ul><li>Sistem Operasi Otak</li></ul></h4><p>Pengertian sederhana dari Feeling extrovert adalah jenis kepribadian yang berbasiskan kecerdasan emosi atau perasaan yang proses kerjanya dikemudikan dari luar dirinya menuju ke dalam dirinya. Sistem operasi pada tipe Fe berada&nbsp;di belahan otak bagian bawah di sebelah kanan atau disebut sebagai limbik kanan. Pada limbik kanan tersebut yang menjadi kemudi kecerdasan dari tipe ini berada di lapisan abu-abu yang letaknya di bagian luar atau permukaan otak. Limbik kanan abu-abu itulah yang menjadi sistem operasi tipe Fe.</p><p>Lapisan yang berwarna abu-abu memiliki tekstur otak yang lebih longgar karena mengandung sel otak lebih sedikit. Kerapatan yang lebih rendah dibandingkan dengan lapisan bagian dalam tersebut membuat kemudi kecerdasan bergerak dari luar ke dalam. Hal ini menyebabkan ‘tuan yang punya badan’ lebih malas untuk bergerak melakukan aktivitas kecuali ada pengaruh luar yang membangkitkan mood untuk bersosialisasi.</p><p></p><ul><li><b>Tipologi Fisik</b></li></ul>Mesin Kecerdasan Feeling (F) sesungguhnya identik dengan pernafasan. Tipe  ini memiliki<br>kapasitas pernafasan yang besar. Kapasitas pernafasan yang kuat tersebut disebabkan tipe F memiliki mesin yang besar, yaitu berupa jantung dan paru-paru yang besar.&nbsp;<p></p><p>Dengan mesin yang besar ini, tipe F dapat memompa oksigen yang lebih banyak ke dalam darah. Tipe Fe lebih bergantung kepada suplai baterai (charger) yang datang dari&nbsp;luar dirinya. Mesin yang besar tersebut harus dinyalakan dari luar. Begitu mesin tersebut menyala, maka akan beraksi sedikit menggebu-gebu.</p><p><b></b>Bentuk (konstitusi) fisik tipe F secara umum disebut sebagai displastik, artinya mesin yang besar tersebut didukung oleh tulang yang kecil sehingga bawaannya terasa berat. Dengan mesin yang besar, maka tipe ini memiliki bentuk badan yang terlihat lebar dan tebal. Akibatnya bagian punggung dari tipe ini lebih sulit digerakkan (disebut displastik). Pada tipe Fe mesin yang besar tersebut ditopang oleh tulang yang lebih kecil. Tinggi badan tipe Fe lebih pendek, sehingga lebih terkesan seperti pendekar (pendek dan kekar). <br> <br>Secara fisik tipe ini dapat menggunakan pernafasan dalam yang bersumber dari paru-parunya. Namun karena charger baterainya bersumber dari luar, maka tipe Fe memiliki kapasitas pernafasannya lebih dangkal. Kekuatan pernafasan karena mesin yang besar tersebut dapat digunakan untuk jenis olahraga yang memerlukan stamina jarak jauh dan jangka panjang seperti lari marathon.<br></p><div><p></p><p></p><p></p><ul><li><b>Sifat Khas</b></li></ul>Jika menggunakan sudut pandang dunia psikologi (aliran perilaku) kepribadian Fe mesti memiliki sifat perilaku khas yang dapat dibuktikan dan diukur yang berbeda dari delapan kepribadian yang lain. Terdapat sepuluh item yang bisa dibuktikan keberadaannya dan bisa diukur secara psikometrik.<br><br>Menurut konsep STIFIn, ke sepuluh item tersebut menjadi kepribadian tetap yang tidak akan berubah dan akan selalu eksis seiring dengan penambahan umurnya. Sepuluh (10) sifat yang tetap tersebut adalah:  <br><ul><li>Emphathy</li><li>Tolerant</li><li>Communicative</li><li>Listener</li><li>Symphathetic</li><li>Persuasive</li><li>Affectionate</li><li>Enable&nbsp;</li><li>Considerate</li><li>Guilding</li></ul><p>Sebagai pribadi yang utuh, tipe Fe memiliki sisi-sisi diametral sebagai berikut: pendengar seperti \'sponge’ namun sebenarnya berhati kenyal, peduli orang lain meski abai terhadap satu per satu persoalan orang lain, selalu ada misi baru namun terlalu mudah menyerah. <br> <br>Selain itu, jika sedang bersih hati, tipe Fe mudah mendapat ilmu tinggi; akan tetapi tipe ini justru jarang membersihkan hatinya, memiliki kepasrahan yang tinggi namun sebenarnya sikap itu datang dari malas berpikir dan menggampangkan banyak hal. Oleh karena itu, tipe ini perlu waspada dengan kelemahannya dan berusaha mengekploitasi kelebihannya. Biasanya jika kelebihannya bergerak membaik maka secara otomatis kelemahan dari tipe ini akan tertutup dengan sendirinya. <br></p><ul><li><b>Kelebihan</b></li></ul><p></p><p>Kekhasan dari kepribadian tipe Fe yaitu memiliki kemampuan sosial yang melebihi delapan jenis kepribadian yang lain. Kelebihan ini dapat disepadankan dengan kecerdasan sosial atau disebut <b>Soc-Q (Social Quotient).</b>&nbsp;Dengan kekuatan pernafasan yang panjang, tipe ini memiliki potensi kekuatan mental yang lebih baik, mampu mendengar bahasa hati orang lain lebih lama. Tipe ini bahkan mampu menjaga perasaan dan berempati lebih baik dari orang lain. Selain itu, tipe F umumnya seperti memiliki kadar cinta yang lebih banyak, baik untuk mencintai ataupun dicintai.</p><p>Namun khusus pada tipe Fe, kemampuan daya jangkau pernafasan atau mental untuk berbagi perasaan bersifat lebih meluas. Tipe ini dapat menjangkau gabungan berbagai event jarak menengah atau memberi perhatian kepada lebih banyak orang.&nbsp;Kelebihan  dari  tipe  Fe  terletak  pada  kemampuan membesarkan (magnifying) orang lain. Pada fase berikutnya,&nbsp;tipe ini menikmati pengaruhnya sebagai king-maker atau owner dari  usaha yang dirintisnya. Selain  itu, dalam mengelola keuangan, tipe Fe berani melangkah mengambil risiko yang besar<br></p><p>\r\n\r\n</p><blockquote><i><b>Tipe Fe : Social Quotient (SQ), Magnifying, Mencari KADER, Mengejar CINTA</b></i></blockquote>\r\n\r\n<p></p><div><i><ul><li><b>Kemistri</b></li></ul></i>Jalur keberhasilan dari tipe Fe terletak pada sejumlah kader yang telah berhasil digemblengnya sebagai wadah untuk mengaktualisasikan panggilan jiwanya. Mengkader orang hingga berhasil mengeluarkan energi positif yang besar. Kemistri cinta yang datang menghampiri tipe ini bersamaan dengan kedatangan sumber energinya, mesti disambut dengan suka cita. </div><div><br></div><div>Jika banyak orang yang mengharapkan digembleng oleh tipe Fe, (didatangi cinta) maka tipe ini harus merespon secara positif (menerima cinta) agar pada akhirnya siklus cinta tersebut membesar dengan sendirinya. Letak keberhasilan dari tipe ini justru sejalan dengan membesarnya siklus cinta. Bagi tipe Fe, siklus cinta tersebut berarti seberapa banyak kader yang telah sukses dibina olehnya.<br></div><div><ul><li>\r\n\r\n<b>Peranan </b></li></ul>Kematangan emosi tipe Fe disalurkan dengan melakukankaderisasi kepada orang-orang yang disayanginya. Fokus daritipe ini adalah menjadi coach bagi orang lain, menggemblengorang-orang yang disayanginya. Penyaluran emosi seperti inimendorong tipe ini menjadi seperti king-maker: mencetak orangsupaya menjadi yang terbaik, kalau perlu sampai menjadi <br>raja (pada bidang tersebut).&nbsp;Dengan kematangan emosi yang bagus, tipe Fe lebih suka<br>memberi jalan kepada orang lain untuk terjun memimpin,&nbsp;sementara  dirinya  sendiri  lebih <br>senang duduk di belakang layar sebagai perintis atau pemilik. Tipe Ini menyukai menjadi tokoh tetapi jenis tokoh yang di balik layar. <br></div><div><br></div><div>Panggilan  alamiah  tipe  Fe&nbsp;tingginya  dengan  bimbingannya. Ketika menggembleng, tipe ini memang memberi sebanyak yang ia bisa, bahkan terkesan menggebu-gebu. Pada akhirnya tipe ini akan puas jika kadernya berhasil meraih mimpinya. <br></div><div><ul><li><b>Target dan Harapan</b></li></ul></div><div>Target pribadi dari tipe Fe adalah mencetak orang, selalu berorientasi kepada orang. Jiwa sosial tipe ini memang cukup tinggi, ia selalu memiliki kepedulian untuk memperbaiki kemampuan SDM (Sumber Daya Manusia) dari masyarakatnya. Panggilan jiwa sebagai mentor atau coach (pelatih) memberi makna yang besar dalam hidup tipe Fe. Tipe ini merasa telah mengaktualisasi diri jika ada diantara kaderkadernya yang berhasil meraih mimpinya.<br> <br>Secara kepakaran pun tipe Fe terpanggil untuk menjadi coach, bahkan bermimpi menjadi coach yang termahal bayarannya. Meskipun begitu, sering kali tipe ini mengorbankan transaksi pembayaran upah jika aspek penggemblengan SDM yang ditawarkan terasa lebih<br>menantang. Panggilan sosial tipe ini untuk mengangkat SDM ditempatkan lebih tinggi dibanding transaksi komersialnya. <br> <br>Pengaruh sosial sebagai baterai dari tipe Fe terkadang membuat tipe ini menjadi begitu subjektif ketika mengukur sesuatu. Bahkan ketika tipe ini menjalankan usaha, proses kalkulasi bisnis terkadang terabaikan. Tipe ini begitu menggebu-gebu memperjuangkan apa yang disukainya. <br> <br>Argumentasi dari tipe Fe sering kali membawa-bawa pertimbangan sosial, meski hal ini memang disukainya. Letak kesukaan tersebut didasarkan pada ukuran keberadaan tipe ini secara sosial. Artinya, antara kepentingan pribadi tipe ini di mata sosial dengan kepentingan sosial yang murni, dapat beririsan. <br></div><div><br></div><div>Ketika sudah suka, tipe Fe akan membuat keputusan yang nekat dan berani ambil risiko, sehingga kurang hati-hati mempertimbangkan bahwa pada akhirnya bisa jadi ia akan menjadi korban demi kepentingan orang lain. Seperti dalam hal mengelola keuangan, tipe ini memang terkadang terlalu berani mengambil risiko yang terlalu besar, sementara dibalik keputusan tersebut masih banyak unsur ketidakpastian.<br></div><div><p></p><p></p><ul><li>&nbsp;<b>Arah Merek</b></li></ul>Kegetolan tipe Fe membangun SDM menciptakan merek bagi dirinya sebagai orang yang dapat dipercaya untuk membangun tim pemenang dan organisasi terbaik. Tipe ini justru merasa gagal jika pribadinya yang dialamatkan terhadap keberhasilan organisasi. <br> <br>Tipe Fe akan merasa berhasil jika tim dan organisasinya menjadi yang terbaik di bidangnya dan bahkan sudah bisa dilepas sendiri menjalankan hasil tempaannya. Panggilan jiwa dari tipe ini selalu menginginkan karya tempaan, yaitu hasil gemblengannya tertanam dalam pada tim dan organisasi yang digemblengnya. <br><ul><li><b>Cara Belajar</b></li></ul><b></b><p>Proses belajar yang sesuai bagi tipe Fe adalah mendiskusikan mata pelajaran dengan guru/teman sambil memperbanyak item yang diulang secara verbal. Bagaimanapu, tipe F secara umum harus belajar menggunakan telinga. Tipe ini harus menjadi pendengar yang baik. Namun bagi tipe Fe, proses komunikasi yang interaktif lebih disukai berhubung&nbsp;charger baterainya ada di luar. Dengan demikian, diskusi menjadi pilihan terbaik bagi tipe Fe untuk belajar. <br> <br>Motivasi belajar dari tipe Fe akan terjaga jika ia dipuji oleh orang lain, apalagi oleh teman seumurnya. Ibarat seperti perempuan yang ingin belajar, maka tipe ini merasa lebih nyaman jika didampingi oleh orang lain, apalagi jika orang yang mendampingi adalah orang yang begitu<br>berarti baginya. Menyediakan pendamping bagi tipe Fe sekaligus merupakan upaya untuk meningkatkan mood belajar. <br></p><ul><li><b>Pilihan Sekolah/ Profesi</b></li></ul>Prioritas utama jika ingin memilih jurusan atau profesi untuk tipe Fe sebaiknya diarahkan pada satu diantara empat pilihan industri berikut ini, yaitu psikologi, komunikasi, diplomasi, humas/promosi. <br> <br>Selanjutnya pilihan lain dari jurusan atau profesi yang sesuai untuk tipe Fe sebagai berikut: politisi, eksekutif, lawyer, kesenian, investor, negarawan, trainer/public speaker, administrasi negara, seniman, budayawan, hubungan internasional, salesman,inspirator, motivator, psikiater, counselor, ideolog, personalia,birokrat, aktivis, atlit maraton, dll. <br> <br>Di masa depan, pilihan profesi akan semakin beragam. Namun jika ingin memilih jurusan ataupun profesi patokan&nbsp;bagi tipe Fe adalah mempertimbangkan empat kata kuncinya, yaitu: merasakan, pernafasan, memimpin, dan mencintai. Artinya, jurusan atau profesi yang dipilih tipe Fe didominasi oleh unsur-unsur yang membutuhkan keterlibatan emosi (termasuk&nbsp;kemampuan berkomunikasi dari hati ke hati), secara fisik memerlukan nafas yang panjang atau semangat yang  tinggi sehingga bisa bekerja atau bertanding secara  marathon, serta melibatkan kharisma dan kemampuan kepemimpinan.&nbsp;Selain  itu,  jurusan  atau profesi yang mampu menangkap simpati atau cinta orang lain (pengikut), sehingga para pengikut atau pengagum dari tipe ini tidak merasa cinta bertepuk sebelah tangan. <br></div><div>\r\n\r\n<blockquote><b><i>Industri Tipe Fi : Psikologi, Komunikasi, Diplomasi, Humas/promosi</i></b></blockquote><br></div>\r\n\r\n</div>\r\n\r\n<br><p></p>');

-- --------------------------------------------------------

--
-- Table structure for table `tb_footer`
--

CREATE TABLE `tb_footer` (
  `id` int(11) NOT NULL,
  `tagline` varchar(255) DEFAULT NULL,
  `alamat` text,
  `telp` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `tw` varchar(100) NOT NULL,
  `fb` varchar(100) NOT NULL,
  `yt` varchar(100) NOT NULL,
  `lk` varchar(100) NOT NULL,
  `ig` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_footer`
--

INSERT INTO `tb_footer` (`id`, `tagline`, `alamat`, `telp`, `email`, `tw`, `fb`, `yt`, `lk`, `ig`) VALUES
(1, 'Kami akan membersamai Anda dalam menemukan versi terbaik Anda dan Lingkungan Anda dalam menuju Jalan yang SuksesMulia', 'Cabang STIFIn tersebar di seluruh wilayah Indonesia dan beberapa mancanegara (Malaysia dan Singapore)', ' +6281283101051', 'yayasanstifin@gmail.com', 'https://twitter.com/STIFInInstitute', 'https://www.facebook.com/stifininstituteofficial', ' https://www.youtube.com/@STIFInInstitute', 'https://www.linkedin.com/in/stifin-institute-b1b77b167/', 'https://www.instagram.com/stifininstitute/');

-- --------------------------------------------------------

--
-- Table structure for table `tb_hero_section`
--

CREATE TABLE `tb_hero_section` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `deskripsi` text,
  `gambar` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_hero_section`
--

INSERT INTO `tb_hero_section` (`id`, `judul`, `deskripsi`, `gambar`) VALUES
(7, 'STIFIn Personality Genetic Intelligence', ' Jalan menuju SuksesMulia dengan menempatkan diri pada lingkungan yang sesuai dengan Versi Terbaik Anda', 'bg-stifin3.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_insting`
--

CREATE TABLE `tb_insting` (
  `id` int(11) NOT NULL,
  `gambar` varchar(255) DEFAULT NULL,
  `konten` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_insting`
--

INSERT INTO `tb_insting` (`id`, `gambar`, `konten`) VALUES
(3, '182.png', '<h1>Insting (In)</h1>'),
(4, 'Artikel_STIFIn_(12)1.jpg', '<p>\r\n\r\n</p><h3>Insting (In)</h3>\r\n<p>In adalah singkatan dari Insting. Menggunakan dua huruf supaya tidak sama dengan singkatan Intuiting. Jika dua huruf In bergandengan, merupakan identitas sebagai Mesin Kecerdasan (MK). Menurut konsep STIFIn, ragam Mesin Kecerdasan hanya ada lima, dan In adalah salah satu diantara 5 MK tersebut. Pada MK In tidak memiliki kemudi kecerdasan, baik i (introvert) ataupun e (extrovert). Hal ini disebabkan secara fisik otak tengah yang menjadi milik In memang tidak memiliki lapisan berwarna abu-abu dan putih. Dengan demikian, In selain berperan sebagai Mesin Kecerdasan, juga merupakan kepribadian genetik. <br></p><h4><ul><li>Sistem Operasi Otak</li></ul></h4><b></b><b></b><p><b></b>Pengertian sederhana dari Insting adalah jenis kecerdasan atau kepribadian genetik yang berbasiskan kecerdasan naluri atau indera ketujuh. Proses kerja otak dari tipe In dikemudikan secara otomatis karena tidak memiliki kemudi. Sistem operasi pada tipe In berada di belahan otak tengah. Otak tengah menyangga keempat belahan otak sama baiknya. Jika otak tengah kuat, maka pada tingkat tertentu dapat membantu memperkuat keempat belahan otak lainnya. Terutama karena otak tengah menjadi hubungan bagi keempat belahan otak lainnya sehingga proses koordinasi, harmonisasi, dan keseimbangan otak dapat berjalan secara baik. <br> <br>Akibat tidak memiliki kemudi, otak tengah akan memberi reaksi spontan pada setiap stimulus yang masuk. Meski syaraf terlebih dahulu tiba di otak tengah sebelum menyebar kepada empat belahan otak lainnya, namun bukan berarti keempat belahan otak tersebut tidak dilibatkan ketika otak tengah ingin merespon sesuatu. Proses koordinasi respon holistik oleh seluruh otak terjadi walaupun tanpa kemudi. Hal tersebut ditunjukkan oleh kemampuan otak tengah yang merespon secara holistik pada setiap respon yang datang.<br><b></b></p><p></p><p></p><ul><li><b>Tipologi Fisik </b></li></ul><b></b>Mesin Kecerdasan In sesungguhnya identik dengan reflek. Mereka memiliki reflek yang cepat. Reflek yang bersumber dari hasil pengolahan yang holistik ini membuat tipe In dalam kesehariannya mudah beradaptasi.  Tipe  ini  seperti  dapat mengakses sama  baiknya kepada empat belahan otak yang lain. Bahkan secara fisik pun, tipe In memiliki kemampuan adaptasi fisik yang baik terhadap lingkungan. Pada cuaca lingkungan dan mental seperti tipe Sensing, ia akan menyesuaikan seperti tipe Sensing juga. Demikian seterusnya dengan ketiga belahan otak lainnya.<div><br></div><div>Tipe In umumnya memiliki bentuk badan yang datar (stenis), ditandai dengan garis bahu yang lurus rata ke samping. Dengan bentuk badan yang datar, ditambah leher yang pendek dengan posisi otak tengah yang paling dekat dengan tulang belakang, serta pada saat yang sama otak tengah tersebut menyangga keempat belahan otak sama baiknya, maka hal tersebut menjadikan tipe ini mempunyai fungsi tubuh yang serba bisa. Fungsi keserba-bisaan dari tipe ini yang digabung dengan kekuatan reflek, membuat tipe In seolah-olah akan survive diletakkan di lingkungan seperti apapun atau diterjunkan pada profesi apapun.<br><p></p><p>\r\n\r\n<b></b></p><blockquote><b><i>Tipe In: Otak tengah, Tidak memiliki kemudi kecerdasan, Reflek, Stenis</i></b></blockquote><p></p><p></p><p></p><ul><li><b>Sifat Khas</b></li></ul><b>\r\n\r\n</b>Jika menggunakan sudut pandang dunia psikologi (aliran perilaku), kepribadian dari tipe In mesti memiliki sifat perilaku khas yang dapat dibuktikan dan diukur yang berbeda dari delapan kepribadian yang lain. Terdapat sepuluh item yang bisa dibuktikan keberadaannya dan bisa diukur secara psikometrik. <br> <p></p><p>Menurut konsep STIFIn, kesepuluh item tersebut menjadi kepribadian tetap bagi tipe Si, yang tidak akan berubah dan akan selalu eksis seiring dengan penambahan umur. Sepuluh (10) sifat yang tetap tersebut adalah:</p><p></p><ul><li>Balanced</li><li>Compromising</li><li>Peaceful </li><li>Resourceful</li><li>Simple</li><li>Forgiving</li><li>Occupied</li><li>Flowing</li><li>Smooth</li><li>Intermediary</li></ul> <br>Sebagai pribadi yang utuh, tipe In memiliki sisi-sisi diametral sebagai berikut: spiritualis yang hebat namun juga individu yang galak dan temperamental, nalurinya tajam dan pandai meramal namun peragu dan tidak punya prinsip, isi kepala lengkap sebagai generalis namun membuatnya tanggung tidak tuntas, selalu ingin tenang dan bahagia tapi kurang assertive.</div><div><br></div><div>Selainitu,tipeinisangatresponsif,cepat,dan komprehensif, akan tetapi dalam kontektualitas sebatas <i>ad hock</i> dan kurang jangka panjang, jujur dan polos namun seringkali naif. Oleh karena itu, tipe ini perlu waspada dengan kelemahannya dan berusaha mengekploitasi kelebihannya. Biasanya jika kelebihannya bergerak membaik maka secara otomatis kelemahan dari tipe ini akan tertutup dengan sendirinya.<br>\r\n\r\n<ul><li><b>Kelebihan</b></li></ul><b>\r\n\r\n</b>Kepribadian dari tipe In memiliki kekhasan yaitu dalam hal kemampuan berkorban bagi kepentingan yang lebih besar melebihi delapan jenis kepribadian yang lain.  Kelebihan ini dapat  disepadankan  dengan  kecerdasan  berkorban  atau disebut AQ (Altruist Quotient).  <br><br>Jika tipe ini bisa bekerja secara tenang dan damai, maka salah satu kemampuannya yang   lain akan perform, yaitu memiliki mata ketiga dalam   melihat terhadap setiap peristiwa.  Hasil pengamatan mata ketiga tersebut antara lain: dapat melihat hikmah yang tersembunyi dibalik setiap kejadian, dapat memaknai secara spiritual terhadap setiap kejadian, atau memperoleh informasi penting dari indera ketujuh (naluri) tipe ini.</div><div><br></div><div>Dalam hal pengelolaan uang, tipe In merasa sedih jika tidak bisa menolong orang lain yang memerlukannya. Tabiat tipe ini terhadap uang adalah selalu ingin menolong orang. Akibatnya, uang habis bukan untuk diri sendiri melainkan untuk menolong orang lain.<br><p></p><p></p><blockquote><b><i>Tipe In : Altruist Quotient (MQ), Indra ketujuh (naluri), Insight </i></b></blockquote><i><ul><li><b>Kemistri</b><br></li></ul></i>Kemistri alamiah dari tipe In adalah selalu bahagia. Ia hanya merasa nyaman hidup dalam suasana yang penuh kebahagiaan dan jauh dari masalah-masalah. Jika kebahagiaan itu berhasil didapatkannya, maka tipe ini mulai berperan untuk memultitaskingkan pengabdiannya. <br> <br>Namun sebaliknya, tipe In tidak akan bahagia jika ia tidak memiliki peran apa-apa. Keberhasilan hidup dari tipe ini ditandai dengan keberperanan yang bermakna bagi sesama. Hal itu jugalah yang menjadi sumber kebahagiaan dari tipe ini. Semakin besar keberperanan, semakin banyak \'kepake’, maka tipe In semakin bahagia.<br><ul><li><b>Peranan</b><br></li></ul>Faktor serba bisa dan daya adaptasi fisik serta adaptasi kecerdasan yang tinggi menyebabkan panggilan jiwa dari tipe In selalu ingin berperan sebagai mitra (partner) bagi tipe lainnya. Meskipun harus menjadi orang nomor dua dalam kemitraan tersebut, tipe ini tidak terlalu mempermasalahkan. Yang penting bagi tipe ini yaitu dapat menjadi mitra bagi tipe lainnya di level yang lebih baik. Hal tersebut tidak akan terjadi pada tipe kepribadian yang lain. Keempat tipe lainnya selalu ingin menjadi nomor satu dalam setiap kemitraan.<br><p></p><ul><li><b>Target dan Harapan</b></li></ul>Dalam menjalankan fungsi kemitraanm tipe Insting memasang target menghasilkan pertumbuhan dari masa ke masa, meskipun kecil atau sedikit yang penting ada pertumbuhan. Sebenarnya tipe ini tidak suka dengan langkah-langkah revolusioner karena hanya akan meningkatkan suhu kerja yang akan membuat suasana tegang. Tipe In menyukai suasana damai dan tenteram tanpa konflik. Termasuk alasan tipe ini terpanggil untuk menjadi orang kedua adalah: ia tidak ingin menjadi sasaran tembak jika menjadi orang nomor satu. <br> <br>Harapan terbesar dari tipe In yaitu ingin selalu memberikan kontribusi pada lingkungan sosial. Tipe ini merasa hampa jika tidak menolong atau berkontribusi terhadap  lingkungannya. Hal yang paling  menyakitkan bagi tipe In  adalah  ketika  ia  ditolak oleh lingkungannya. Kemampuan adaptasi dari tipe ini membawanya untuk berkontribusi lebih banyak kepada lingkungan. </div><div>\r\n\r\n<blockquote><b><i>Tipe In : Selalu bahagia, Patner, Contributing, Menghasilkan pertumbuhan, Tingkat keberperanan</i></b></blockquote> <p></p><p></p><ul><li>&nbsp;<b>Arah Merek </b></li></ul>Ketulusan dari tipe In untuk selalu menolong dan berkontribusi, membuat pihak lain secara cepat memberikan merek sesuai dengan tingkat keberperannya yang tinggi. Tipe ini menyadari bahwa jika tidak berperan dimana-mana, ia akan kehilangan makna hidupnya. Kuantitas keberperanan dari tipe ini cukup tinggi. Tipe ini seolah-olah tertarik kesana kemari oleh lingkungannya. <br> <br>Dibalik kuantitas keberperanan yang tinggi, tantangan bagi tipe In agar semua peranan tersebut dapat ditangani dengan baik, sehingga kualitas keberperanannya juga tinggi. Merek diri tipe In akan terangkat jika kuantitas dan kualitas keberperanan semuanya tinggi.<br><br><ul><li><b>Cara Belajar</b></li></ul><b></b><p>Proses belajar pada tipe In sangat berbeda dengan kedelapan kepribadian yang lain. Tipe kepribadian yang lain cenderung induktif proses belajanya, berangkat dari detail kemudian disimpulkan secara umum. Sedangkan tipe In cenderung menggunakan pola belajar deduktif: ketahui dulu kesimpulan baru kemudian diteruskan pada rincian. Oleh karena itu, pada setiap buku yang dibaca, tipe ini selalu merangkai dulu mencari kesimpulan, baru kemudian diuraikan detailnya. Sambil belajar, tipe In dapat dibantu dengan suasana yang damai dan tenteram dengan dukungan musik latar yang lembut. </p><p>Cara membangkitkan motivasi dari tipe In adalah dengan jalan menghilangkan segala macam tekanan yang menimpanya. Selesaikan satu per satu hingga akhirnya tipe ini merasa lega dan tidak lagi punya trauma masa lalu. Setelah itu, baru membimbing tipe ini dengan pendekatan scaffolding: dibimbing dari dekat supaya bisa naik tangga satu per satu. <br> <br><br></p><ul><li><b>Pilihan Sekolah/ Profesi</b><br></li></ul><b></b><p>Prioritas utama jika ingin memilih jurusan atau profesi yang sesuai untuk tipe In sebaiknya diarahkan pada satu diantara lima pilihan industri berikut ini, yaitu musik/performance, jasa, culinary, agama/budaya/charity. Selanjutnya pilihan lain dari jurusan atau profesi yang sesuai untuk tipe In sebagai berikut: pembalap, entertainer, agamawan, aktivis, musik, chef, jurnalis, aktivis, pengetahuan umum, performance, spiritualis, birokrat, pelayan masyarakat, mediator, tangan kanan untuk semua posisi dan semua industri, kolumnis, generalis, presenter serba-bisa, atlit serba-bisa, dll.</p><p>Di masa depan, pilihan profesi akan semakin beragam. Namun jika ingin memilih jurusan ataupun profesi patokan bagi tipe In adalah mempertimbangkan empat kata kuncinya, yaitu: <i>merangkai, reflek, berkorban, dan otomatis. </i>Artinya, jurusan atau profesi yang dipilih tipe In didominasi oleh hal-hal yang bersifat umum sebagai hasil rangkaian atau gabungan dari berbagai  disiplin ilmu dan profesi, memerlukan reaksi spontan hasil reflek yang cepat, mengandung unsur-unsu berkorban untuk kepentingan yang lebih besar, serta bekerja secara otomatis tanpa perlu dipikir ulang. Dengan  demikian,  tipe  ini  lebih cenderung sesuai dalam bidang-bidang kemanusiaan.</p><p><i><b></b></i></p><blockquote><i><b>Industri Tipe In : Musik/performance, Jasa, Culinary, Agama/budaya/charity</b></i></blockquote><i><br></i> <br><b><br></b><br><b><br></b><br><b><br></b><br><b>  \r\n\r\n</b>\r\n\r\n<p></p><p><br><b><br></b></p><p></p><b></b> <br><br><br></div>\r\n\r\n<br><p></p>');

-- --------------------------------------------------------

--
-- Table structure for table `tb_intuiting`
--

CREATE TABLE `tb_intuiting` (
  `id` int(11) NOT NULL,
  `gambar` varchar(255) DEFAULT NULL,
  `konten` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_intuiting`
--

INSERT INTO `tb_intuiting` (`id`, `gambar`, `konten`) VALUES
(2, '163.png', '<p></p><h4></h4>\r\n\r\n<h1>Intuiting (I)</h1><p></p>'),
(3, 'Artikel_STIFIn_(8)1.jpg', '<p>\r\n\r\n</p><h3></h3>\r\n\r\n<h3>Intuiting introvert (Ii)</h3>\r\n<p>Ii adalah singkatan dari Intuiting introvert.  Jika huruf I berdiri sendiri merupakan identitas sebagai Mesin Kecerdasan (MK). Menurut konsep STIFIn, ragam Mesin Kecerdasan hanya ada lima, dan I adalah salah satu diantara 5 MK tersebut. Identitas Mesin Kecerdasan berubah menjadi kepribadian ketika MK digandengkan dengan jenis kemudi di belakangnya. Jenis kemudi kecerdasan hanya ada dua, yaitu i (introvert) dan e (extrovert). Dengan demikian, Ii sudah menjadi identitas kepribadian. I ditulis dengan huruf besar karena pengaruhnya sebagai MK lebih besar dari i yang ditulis dengan huruf kecil yang berperan hanya sebagai kemudi kecerdasan. <br></p><h4><ul><li>Sistem Operasi Otak</li></ul></h4><p>Pengertian sederhana dari Intuiting introvert adalah jenis kepribadian yang berbasiskan kecerdasan indera keenam (intuisi) yang proses kerjanya dikemudikan dari dalam dirinya menuju ke luar dirinya. Sistem operasi pada tipe Ii berada di belahan otak bagian atas di sebelah kanan atau disebut sebagai otak besar kanan atau diringkas otak kanan. Pada otak kanan tersebut yang menjadi kemudi kecerdasan dari tipe ini berada di lapisan putih yang letaknya di bagian dalam. Otak kanan putih itulah yang menjadi sistem operasi tipe Ii. <br> <br>Lapisan yang berwarna putih memiliki tekstur otak yang lebih padat karena mengandung sel otak lebih banyak. Kerapatan yang lebih tinggi dibandingkan dengan lapisan bagian luar tersebut membuat kemudi kecerdasan bergerak dari dalam ke luar. Hal ini menyebabkan ‘tuan yang punya badan’ ketika memiliki ide yang bagus seperti diberi mesin roket untuk terbang ke angkasa bersama ide tersebut. <br></p><p></p><p></p><ul><li><b>Tipologi Fisik </b></li></ul><b></b>Mesin Kecerdasan Intuiting (I) sesungguhnya identik dengan perut panjang. Mereka memiliki perut yang panjang dimana di dalamnya terdapat usus yang panjang. Perut panjang tersebut rupanya merupakan ciri-ciri dari tipe I yang mengolah makanan menjadi tenaga anaerobik dan disimpan dalam otot yang berwarna putih. <br><p>Tenaga anaerobik yang terdapat pada otot putih ini merupakan tenaga yang tidak stabil dan meledak-ledak. Itulah mengapa jenis tenaga ini akan bagus untuk melakukan sprint dengan  kecepatan  tinggi.  Tenaga anaerobik merupakan tenaga yang eksplosif. Tentu saja otot putih ini mesti sering digunakan supaya otot berkembang dan memiliki kapasitas menyimpan tenaga anaerobik yang lebih banyak.<br></p><p>Tipe Ii terlebih lagi karena ditunjang oleh ketersediaan baterai (charger) yang ada di dalam dirinya menyebabkan tipe Ii seperti memiliki daya ledak yang kuat. Selain memiliki bentuk (konstitusi) fisik yang astenis (jangkung) tebal, orang Ii juga memiliki tubuh sebagai penunjang bagi kepalanya untuk pergi terbang jauh bersama gagasannya. Oleh karena itu, pada tipe I secara umum sering kali memiliki bentuk bahu yang miring, seolah-olah ibarat roket yang dipandu oleh kepalanya, khususnya otak kanannya. <br> <br>Dalam proses penceranaan, makanan secara rakus diolah oleh perut panjang dan disimpan pada otot putih dari tipe I sebagai tenaga anaerobik. Tenaga anaerobik ini memiliki kemampuan untuk melakukan sprint. Bergerak secara eksplosif namun kemudian cepat habis. </p><p>Pada tipe I dengan kemudi introvert (i), hal tersebut masih bisa didorong hingga mampu mencapai jarak menengah, namun tetap tidak bisa digunakan untuk jarak jauh. Penggunaan untuk jarak jauh hanya mungkin jika memakai tenaga aerobik. Itulah mengapa tipe I kurang suka main otot. Tipe ini tahu bahwa pada dirinya hanya tersedia lebih banyak otot putih dan kurang memiliki otot merah yang mengandung tenaga aerobik. Tipe ini lebih suka menggunakan kepala, namun jenis kepala sebelah kanan yang tidak membuat merengut seperti kepala sebelah kiri. Kepala kanan tipe ini justru lebih nyeleneh. <br> <br></p><p></p><p>\r\n\r\n<b></b></p><blockquote><b><i>Tipe Ii : Otak kanan putih, Dalam ke luar, Motorik kasar, Astenis </i></b></blockquote><p></p><p></p><p></p><ul><li><b>Sifat Khas</b></li></ul>Jika menggunakan sudut pandang dunia psikologi (aliran perilaku) kepribadian dari tipe Ii mesti memiliki sifat perilaku khas yang dapat dibuktikan dan diukur yang berbeda dari delapan kepribadian yang lain. Terdapat sepuluh item yang bisa dibuktikan keberadaannya dan bisa diukur secara psikometrik.  <br> <br>Menurut konsep STIFIn, ke sepuluh item tersebut menjadi kepribadian tetap yang tidak akan berubah dan akan selalu eksis seiring dengan penambahan umurnya. Sepuluh (10) sifat yang tetap tersebut adalah:  <br><ul><li>Learner</li><li>Assertive</li><li>Perfectionist</li><li>Scholar</li><li>Hard to please</li><li>Proud</li><li>Optimistic</li><li>Deep</li><li>Insistent</li><li>Capable Selling</li></ul>Sebagai pribadi yang utuh, tipe Ii memiliki sisi-sisi diametral sebagai berikut: berpikir positif namun sekaligus mengkhayal ala ‘time tunnel’ seolah-olah semua bisa terjadi, menyenangkan sebagai mitra bisnis namun tidak suka bicara pribadi, percaya diri sangat tinggi namun memacu ‘mesin’nya terlalu cepat seolah tanpa rem.<br><p></p><p>Selain itu, tipe ini terlihat atraktif dan estetik namun terkadang melewati zamannya, mahir membuat konsep dan sekaligus menguasai pekerjaan hilirnya namun menjadi terlalu masa-bodoh dengan lingkungannya, terbuka dengan perbedaan pendapat namun tetap keras kepala dengan keyakinannya.<br></p><p>Oleh karena itu, tipe ini perlu waspada dengan kelemahannya dan berusaha mengekploitasi kelebihannya. Biasanya jika kelebihannya bergerak membaik maka secara otomatis kelemahan dari tipe ini akan tertutup dengan sendirinya. <br></p><ul><li><b>Kelebihan</b></li></ul><p></p><p>Kepribadian dari tipe Ii ini memiliki kekhasan karena memiliki kemampuan kreativitas dan intuisi yang melebihi delapan jenis kepribadian yang lain. Kelebihan ini dapat disepadankan dengan kecerdasan kreatif atau disebut CQ (Creativity Quotient). </p><p>Tipe Ii memiliki kelebihan otomatis yang berfungsi dalam cara kerjanya yang terbiasa mencari kualitas. Kualitas tersebut diciptakan dari pangkal, sehingga membuat tipe Ii sangat getol mencari penemuan-penemuan atau penciptaan-penciptaan baru. <br></p><p>Kelebihan tipe Ii justru terletak pada orientasinya untuk mengadakan sesuatu yang sebelumnya belum ada. Tipe ini mencari kepuasan. Jika karya ciptaannya kemudian diakui sebagai penemuan baru, maka hal tersebut menjadi kepuasan yang tak terhingga bagi tipe ini. Demikian juga tabiatnya terhadap uang, tipe Ii akan membelanjakan dan menginvestasikan uangnya untuk segala sesuatu yang dapat meningkatkan kualitas dirinya atau karya ciptaannya, agar ia memperoleh kepuasan secara berkelas.<br></p><p>\r\n\r\n</p><blockquote><i><b>Tipe Ii : Creativity Quotient (Cq), Kualitas, Penemuan (invention)</b></i></blockquote>\r\n\r\n<p></p><div><i><ul><li><b>Kemistri</b><br></li></ul></i>Disain keberhasilan hidup seseorang dengan tipe Ii ditandai dengan kemampuan menjalankan program untuk menginkubasi ciptaannya. Tipe ini tidak pernah kehabisan ide karena radar vertikalnya diberi roket dari dalam. Secara umum, Mesin Kecerdasan Intuiting memiliki kemistri kata yang berupa ilmu dan gagasan. Bagi tipe ini, ilmu dan gagasan tersebut akan begitu mudah dimunculkan. Namun ide saja tidak cukup. Ide tersebut harus diterbangkan setinggi-tingginya.<br></div><div><br></div><div>Proses menerbangkan ide dimulai dengan cara mematangkan ide, lalu dibuatkan infrastuktur untuk menerbangkan ide tersebut. Wujud konkrit mematangkan ide dan mempersiapkan infrastrukturnya yaitu, jika ide tersebut telah mewujud menjadi program dengan segala pranatanya. Semakin banyak program yang bisa digarap tipe Ii untuk merealisasikan mimpi dan gagasannya, tipe ini semakin menemukan jati dirinya.</div><div><ul><li>\r\n\r\n<b>Peranan\r\n\r\n&nbsp;</b></li></ul>  Dengan  fungsi  kepala  kanan yang bagus, tipe I lebih menyukai pekerjaan-pekerjaan berkelas yang membutuhkan kualitas yang bersumber  dari  sumberdaya kreativitas yang dimilikinya. Jika   digabungkan dengan kepala kanan  akan membuat tipe I lebih menyukai pekerjaan-pekerjaan necis yang tidak kotor oleh keringat. Hal itu juga yang membuat tipe I memilih peran sebagai sutradara di belakang layar yang memberii ide dan inspirasi kepada para pemain yang berada di lapangan atau di panggung.<br><br>Jika tipe I dikemudikan dari dalam menjadi tipe Ii- akan muncul sifat ingin serba sempurna. Hal itu membuat tipe Ii menyukai peran sebagai seorang penggagas yang menginisiasi (memulakan) pekerjaan dengan target melakukan perubahan atau memberi nilai tambah pada pekerjaannya. Secara umum, kemampuan menangkap bahasa langit atau memprediksi kejadian membuat tipe I berani mengambil risiko. Hal inilah yang membuat tipe ini sangat berminat menjadi pengusaha atau pebisnis yang sejatinya berani menghadapi risiko gagal. <br></div><div><blockquote><b><i>Tipe Ii : Menjalankan PROGRAM, Menerbangkan KATA, Inisiator</i></b></blockquote></div><div><ul><li><b>Target dan Harapan</b></li></ul>Kesungguhan untuk menciptakan sesuatu yang baru menjadikan tipe Ii hanya akan puas jika menghasilkan sesuatu yang signifikan (berbeda secara nyata). Tipe ini berharap dapat selalu mencipta sesuatu yang baru sehingga pada akhirnya secara akumulatif, ia berhasil mengubah keadaan dirinya atau lingkungannya secara signifikan. <br></div><div>\r\n\r\n<i><b><blockquote>Tipe Ii : Hasil yang signifikan, Creating, Kualitas kerja/margin tinggi</blockquote></b></i></div><div><p></p><p></p><ul><li>&nbsp;<b>Arah Merek</b></li></ul>Dengan kemampuan untuk selalu mencipta, memberi  nilai ambah, menghasilkan perubahan yang signifikan, dan menjaga kesempurnaan,  tipe  Ii memiliki merek yang melekat pada dirinya sebagai  sosok yang memiliki kualitas kerja yang tinggi.<br><br>Tipe Ii tidak mementingkan kuantitas (volume) tapi justru sangat mementingkan kualitas (menghasilkan marjin yang besar). Sehingga, kesungguhan untuk menjaga kualitas kerja menjadi jaminan bagi orang lain yang ingin bekerjasama dengan tipe ini.<br><ul><li><b>Cara Belajar</b></li></ul><b></b><p>Dalam proses belajar, tipe Ii selalu berfokus untuk memahami konsep. Upaya memahami konsep tersebut tidak mudah, maka tipe ini perlu dibantu dengan ilustrasi, grafis, dan film, yang akan memudahkan baginya untuk memahami konsep dari setiap pelajaran. Selain itu, proses belajar dari tipe ini juga dapat ditransfer dari bahasa tubuh si pengajar. Tipe Ii akan menyukai dosen atau guru yang ekspresif dalam berkomunikasi  baik  dari  aspek  konten  pilihan  kata ataupun   dari   cara   penyampaiannya. Konten pelajaran yang disukai tipe ini adalah konten yang dapat menggugah keingintahuan atau memberi inspirasi baru baginya. Tipe ini juga menyukai cerita-cerita petualangan yang fiktif karena hal itu akan membuka cakrawala fantasinya.</p><p>Pemberian motivasi bagi tipe Ii cukup dengan ditantang melihat masa depan yang lebih baik. Tipe Ii memiliki optimisme yang kuat dan juga keras kepala untuk memperjuangkan kemauannya. Dengan memvisualkan ‘big picture’ tentang masa<br>depannya, maka akan lebih mudah bagi tipe ini untuk membangun sendiri rute keberhasilannya. Demikian pula dalam kegiatan belajar, tipe ini akan lahap membaca buku jika sudah melihat manfaat bagi dirinya.<br></p><ul><li><b>Pilihan Sekolah/ Profesi</b></li></ul>Prioritas utama jika ingin memilih jurusan atau profesi untuk tipe Ii sebaiknya diarahkan pada satu diantara empat pilihan industri berikut ini, yaitu : <i>marketing/periklanan, lifestyle/mode, penerbangan, agroforestry. <br></i>Selanjutnya pilihan lain dari jurusan atau profesi yang sesuai untuk tipe Ii sebagai berikut: pengusaha/investor, bisnis pendidikan/pelatihan, penulis sastra, cinematrografis, detektif, seniman, pengusaha, peneliti sains murni, fisikawan quantum, pencipta lagu, reengineering, proyeksi, peramal bisnis, pemain saham, investor, filosof, pakar pembelajaran/pemodul, event organizer, produsen, disainer, ahli kreatif berbagai bidang, arsitek, sutradara, pelukis abstrak, pesulap, peramal, pialang saham, atlit (spinter/pertandingan), pelatih olahraga pertandingan, penyembuh, cenayang, dll.  <i><br> </i></div><div><br></div><div>Di masa depan, pilihan profesi akan semakin beragam. Namun jika ingin memilih jurusan ataupun profesi patokan bagi tipe Ii adalah mempertimbangkan empat kata kuncinya, yaitu: <b>mengarang, perut panjang, perubahan, dan murni</b>. Artinya, jurusan atau profesi yang dipilihnya didominasi oleh unsur-unsur yang memerlukan kreativitas, mengandalkan tenaga anaerobik (karena perutnya panjang) atau tenaga tidak stabil yang melompat-lompat dan meledak-ledak, serta jenis jurusan atau profesi yang bernapaskan perubahan signifikan, radikal, dan memiliki ciri mencuri kemurnian ide atau menerbangkan ide menjulang ke langit yang tinggi. <br><p><b><br></b></p></div>\r\n\r\n\r\n\r\n\r\n\r\n<div><b><br></b><p></p></div><p></p>'),
(4, 'Artikel_STIFIn_(9)1.jpg', '<p>\r\n\r\n</p><h3>Intuiting extrovert (Ie)</h3><p>Ie adalah singkatan dari Intuiting extrovert.  Jika huruf I berdiri sendiri merupakan identitas sebagai Mesin Kecerdasan (MK). Menurut konsep STIFIn, ragam Mesin Kecerdasan hanya ada lima, dan I adalah salah satu diantara 5 MK tersebut. Identitas Mesin Kecerdasan berubah menjadi kepribadian ketika MK digandengkan dengan jenis kemudi di belakangnya. Jenis kemudi kecerdasan hanya ada dua, yaitu i (introvert) dan e (extrovert).  Dengan demikian, Ie sudah menjadi identitas kepribadian. I ditulis dengan huruf besar karena pengaruhnya sebagai MK lebih besar dari e yang ditulis dengan huruf kecil yang berperan hanya sebagai kemudi kecerdasan.<br></p><h4><ul><li>Sistem Operasi Otak</li></ul></h4><p>Pengertian sederhana dari Intuiting extrovert adalah jenis kepribadian yang berbasiskan kecerdasan indera keenam (intuisi) yang proses kerjanya dikemudikan dari luar dirinya menuju ke dalam dirinya. Sistem operasi pada tipe Ie berada di belahan otak bagian atas di sebelah kanan atau disebut sebagai otak besar kanan atau diringkas otak kanan. Pada otak kanan tersebut yang menjadi kemudi kecerdasan dari tipe ini berada di lapisan abu-abu yang letaknya di bagian luar atau permukaan otak. Otak kanan abu-abu itulah yang menjadi sistem operasi tipe Ie. <br></p><p>Lapisan yang berwarna abu-abu memiliki tekstur otak yang lebih longgar karena mengandung sel otak lebih sedikit. Kerapatan yang lebih rendah dibandingkan dengan lapisan bagian dalam tersebut membuat kemudi kecerdasan bergerak dari luar ke dalam. Hal ini menyebabkan ‘tuan yang punya badan’ ketika memiliki ide yang bagus dibawa mendarat ke bumi agar bisa lebih diterima oleh masyarakat atau lingkungannya.</p><p></p><p></p><ul><li><b>Tipologi Fisik </b></li></ul><b></b>Mesin Kecerdasan Intuiting (I) sesungguhnya identik dengan perut panjang. Mereka memiliki perut  yang  panjang  dimana  di dalamnya terdapat usus yang panjang. Perut panjang tersebut rupanya merupakan ciri-ciri dari tipe I yang mengolah makanan menjadi  tenaga  anaerobik dan disimpan dalam otot yang berwarna putih.<br> <br>Tenaga anaerobik yang terdapat pada otot putih ini merupakan tenaga yang tidak stabil dan meledak-ledak. Itulah mengapa jenis tenaga ini akan bagus untuk melakukan sprint dengan kecepatan tinggi. Tenaga anaerobik merupakan tenaga yang eksplosif. Tentu saja otot putih ini mesti sering digunakan supaya otot berkembang dan memiliki kapasitas menyimpan tenaga anaerobik yang lebih banyak.<p></p><p>Tipe Ie menjadi lebih tidak stabil karena ketersediaan baterai (charger) yang ada di luar dirinya menyebabkan tipe ini seperti memiliki daya ledak yang eksplosif seketika. Selain memiliki bentuk (konstitusi) fisik yang astenis (jangkung) dan tipis, tipe Ie juga memiliki kepala yang lebih pipih mengarah ke belakang sehingga memudahkan menyerap aspirasi horizontal tanpa kehilangan orientasi vertikalnya. Hal ini dapat diibaratkan seperti alien yang mendarat ke bumi. Selain itu, pada umumnya tipe I memiliki bentuk bahu yang miring, seolah-olah ibarat roket yang dipandu oleh kepalanya, khususnya otak kanannya.</p><p>Dalam proses pencernaan, makanan secara rakus diolah oleh perut panjang dan disimpan pada otot putih dari tipe I sebagai tenaga anaerobik. Tenaga anaerobik ini memiliki kemampuan untuk melakukan sprint, yaitu bergerak secara eksplosif namun kemudian cepat habis. Pada tipe I dengan kemudi extrovert (e) bahkan daya eksplosif tersebut menjadi cepat habis karena sumber energinya datang dari luar dan memang tidak memiliki kemampuan untuk menempuh jarak jauh. Penggunaan untuk jarak jauh hanya mungkin jika menggunakan tenaga aerobik.</p><p> <br>Itulah mengapa orang I kurang suka main otot. Tipe ini tahu bahwa pada dirinya hanya tersedia lebih banyak otot putih dan kurang memiliki otot merah yang mengandung tenaga aerobik. Tipe I lebih suka menggunakan kepala, namun jenis kepala sebelah kanan yang tidak membuat merengut seperti kepala sebelah kiri, kepala kanan justru lebih nyeleneh.<br></p><p></p><p>\r\n\r\n<b></b></p><blockquote><b><i>Tipe Ie : Otak kana abu-abu, Luar ke dalam, Motorik halus, dan Astenis</i></b></blockquote><p></p><p></p><p></p><ul><li><b>Sifat Khas</b></li></ul>Jika menggunakan sudut pandang dunia psikologi (aliran perilaku) kepribadian Ie mesti memiliki sifat perilaku khas yang dapat dibuktikan dan diukur yang berbeda dari delapan kepribadian yang lain. Terdapat sepuluh item yang bisa dibuktikan keberadaannya dan bisa diukur secara psikometrik. <br><br>Menurut konsep STIFIn, ke sepuluh item tersebut menjadi kepribadian tetap yang tidak akan berubah dan akan selalu eksis seiring dengan penambahan umurnya. Sepuluh (10) sifat yang tetap tersebut adalah:  <br><ul><li>Inspiring</li><li>Solver</li><li>Branchmaker</li><li>Expressive</li><li>Respectful</li><li>Assemblies</li><li>&nbsp;Detective</li><li>Selective</li><li>Diesel</li><li>Romantic</li></ul>Sebagai pribadi  yang  utuh,  tipe  Ie  memiliki  sisi- sisi diametral sebagai berikut: ibarat detektif yang hebat menginvestigasi maka ia harus berjarak dengan lingkungannya, memuja misi pribadinya namun enggan terbebani dengan hubungan yang mendalam, intuisi, dan kreativitasnya meluas namun ada kalanya tipe ini lari secara asketis atau melakukan pesta pelampiasan. <br><p>Selain itu, tipe Ie dapat meng-assembling ide-ide besar dengan idenya sendiri untuk menjadi lebih bernilai namun sayangnya ia bergantung pada mastermind-nya, selalu ingin perubahan secara kontekstual dan progresif namun tidak menyadari api permusuhan yang menentangnya. Oleh karena itu, tipe ini perlu waspada dengan kelemahannya dan berusaha mengekploitasi kelebihannya. Biasanya jika kelebihannya bergerak membaik maka secara otomatis kelemahan dari tipe ini akan tertutup dengan sendirinya. <br></p><ul><li><b>Kelebihan</b></li></ul><p></p><p>Kepribadian dari tipe Ie ini memiliki kekhasan karena memiliki kemampuan kreativitas meruang (spatial) yang melebihi delapan jenis kepribadian yang lain. Kelebihan ini dapat disepadankan dengan kecerdasan spatial atau disebut Spa-Q (Spatial Quotient).<br></p><p>Tipe Ie memiliki kelebihan yang berfungsi otomatis dalam cara kerjanya, yaitu terbiasa berinovasi diantara ruang gerak yang terbuka di sekitar lingkungannya. Inovasi berarti menciptakan peluang baru dalam berbisnis. Kelebihan dari tipe ini dalam mempertemukan inovasi baru dengan kehendak kekinian yang boleh jadi masih tersimpan dalam bentuk potensi, menyebabkan ia menjadi pembaharu inovatif yang lebih diterima masyarakat. <br> <br>Meskipun demikian, tipe Ie tidak menyerah begitu saja dengan kemauan pasar, akan tetapi mengajak pasar kepada sesuatu yang baru. Pada saat yang sama, tipe ini mampu mengukur dan membawa orientasi pasar pergi sejauh yang masih mampu dijangkau. Pada bagian itulah tipe Ie mampu meng-assembling ide murni menjadi ide pasar. Tipe ini mampu meng-assembling berbagai ide untuk dirakit ulang menjadi ide baru yang berorientasi pasar. <br> <br> Dalam hal mengelola keuangan, tipe Ie merasa ada sesuatu yang salah jika tidak punya uang untuk berinvestasi pada kemajuan dirinya.   Selalu ada alokasi untuk membeli masa depannya.  Ia selalu berpikir kemajuan yang dibelanjakan dari sekarang.<br></p><p>\r\n\r\n</p><blockquote><i><b>Tipe Ie : Spatial Quotient, Inovasi, Assembler</b></i></blockquote>\r\n\r\n<p></p><div><i><ul><li><b>Kemistri</b><br></li></ul></i>Jalur keberhasilan tipe Ie adalah jalur kata. Karena kemistri kata akan selalu melekat kepadanya. Kata yang berupa ilmu dan gagasan merupakan panggilan jiwa dari tipe ini. Seseorang dengan tipe Ie memiliki kemampuan mendatangkan kata karena memiliki radar untuk menangkap kata. Begitu juga sebaliknya, kata juga akan lebih suka datang kepada tipe ini. Kata telah menjadi pasangan hidup bagi tipe Ie. <br> <br>Akan tetapi, kata yang diperoleh tipe ini harus didaratkan di bumi dengan jalan mengolahnya menjadi ide yang membumi dan dapat diwujudkan sesuai keperluan pasar. Oleh karena itu, tingkat keberhasilan mendaratkan kata bagi tipe ini yaitu jika kata tersebut dapat diolah menjadi aktivitas bisnis. Wujud keberhasilan bagi tipe Ie adalah jika idenya dapat ditransformasi menjadi bisnis. Aktivitas bisnis ini sesungguhnya merupakan wadah yang tidak hanya berfungsi untuk mengkonkritkan idenya, tetapi juga untuk  mengkapitalisasi  asetnya. Semakin banyak tipe ini memiliki ide dan aset diri yang dikapitalisasi menjadi  aktivitas  bisnis,  maka semakin cemerlang hidupnya.  \r\n\r\n<blockquote><b><i>Tipe Ie : Memiliki BISNIS, Mendaratkan KATA, Reformer</i></b></blockquote>\r\n\r\n</div><div><ul><li>\r\n\r\n<b>Peranan\r\n\r\n&nbsp;</b></li></ul>  Dengan fungsi kepala kanan yang bagus, tipe I lebih menyukai pekerjaan-pekerjaan berkelas yang membutuhkan kualitas yang bersumber dari sumberdaya kreativitas yang dimilikinya.</div><div><br></div><div>Jika digabungkan dengan kecerdasan yang berbasiskan indera keenam, maka fungsi kepala kanan akan membuat tipe I lebih menyukai pekerjaan-pekerjaan necis yang tidak kotor oleh keringat. Hal itu juga yang membuat tipe I memilih peran sebagai sutradara di belakang layar, yang memberi ide dan inspirasi kepada para pemain di lapangan atau di panggung.</div><div><br></div><div>Jika tipe I dikemudikan dari luar-menjadi tipe Ie-akan muncul sifat ingin selalu berinovasi dengan jenis inovasi yang bisa diterima oleh lingkungannya. Hal tersebut membuat tipe Ie menyukai peran sebagai seorang reformer (pembaharu) yang membawa ide murni sebagai sumber perubahan, namun di-assembling dengan selera pasar. <br> &nbsp;<br>Kemampuan  tipe  ini  menangkap  bahasa  langit  atau memprediksi  kejadian,  menangkap  ide,  dan  kemudian dibenturkan dengan kenyataan yang ada di bumi, menjadikan tipe Ie lebih yakin dalam berbisnis. Selain itu, tipe Ie memiliki daya assembling untuk mempertemukan antara  kemurnian dan  kenyataan, sehingga menjadikan tipe ini lebih berani mengambil risiko.  Hal itulah yang  membuat  tipe  Ie  sangat berminat menjadi pengusaha atau pebisnis   yang   sejatinya   berani menghadapi risiko gagal.<br></div><div><blockquote><b><i>\r\n\r\nTipe Ie : Berpikir maju/progresif, Pencipta margin keuntungan, Assembling ide</i></b></blockquote></div><div><ul><li><b>Target dan Harapan</b></li></ul>Kegemaran tipe Ie untuk berinovasi, memerlukan ruang gerak yang leluasa untuk mematangkan idenya. Tipe ini akan merasa hampa jika tidak bisa mencipta sesuatu untuk lingkungannya.  <br></div><div><br></div><div>Harapan dari tipe Ie yaitu ingin selalu bertumpu pada kesempatan untuk mencipta dan berinovasi sehingga ruang bisnis di sekitarnya berhasil diwarnainya dengan aneka pembaharuan. <br></div><div><p></p><p></p><ul><li>&nbsp;<b>Arah Merek</b></li></ul>Ukuran keberhasilan tipe Ie terletak pada bisnis yang dijalankannya. Basis bisnis tipe ini bukanlah datang dari kerja keras melainkan kerja kreatif yang menghasilkan nilai tambah yang besar. Sehingga wajar jika karya cipta tersebut dijual pada pasar berkelas dengan harga tinggi. Otomatis tipe ini akan memiliki marjin yang tinggi. <br> <br>Dengan kemampuan kreativitas, diolah menjadi inovasi, dimatangkan dengan proses assembling, baik ide-ide lain ataupun di-assembling dengan ekspektasi pasar, maka akhirnya sosok dari tipe Ie sangat layak diberi merek sebagai pencipta marjin keuntungan bisnis. <br><br></div><div>Tipe Ie dapat ditantang untuk memenangkan persaingan di pasar ‘lautan merah’ yang berdarah-darah. Selain itu, tipe ini memiliki kemampuan menciptakan peta persaingan baru di pasar ‘lautan biru’ yang belum terjamah. Naluri bisnis tipe Ie memiliki karakter dan optimisme yang kuat untuk bermain di pasar berkelas. Tipe ini yakin bahwa kreativitas, inovasi, dan kemampuan assemblingnya membuatnya pantas mendapatkan marjin yang tinggi.<br><ul><li><b>Cara Belajar</b></li></ul><p>Proses belajar dari tipe Ie cenderung lebih cepat dari usianya. Dalam proses belajarnya, tipe ini justru selalu mencoba mencari tema di balik buku yang dibacanya. Selain itu, tipe ini akan mampu menemukan konsep yang tersembunyi dari apa yang dipelajarinya, melebihi kemampuan jenisjenis kecerdasan yang lain. Oleh karena itu, jika ingin membuat tipe Ie belajar dengan baik, maka ia harus dipermudah dalam merumuskan tema yang sedang dipelajari. &nbsp;</p><p>Supaya kemampuan kreatif dari tipe Ie menyala, maka perlu difasilitasi dengan peraga bongkar pasang. Kecerdasan spasial dari tipe ini akan merekam hal tersebut menjadi pelajaran yang kreatif.  <br> <br>Tipe   Ie   mampu   membumi   dan beradaptasi, sehingga ia juga perlu diberikan cara belajar sebagaimana kebanyakan orang, yaitu dengan melatih soal untuk mendapatkan pengalaman yang terpola dalam pikirannya.</p><p>Untuk memotivasi tipe Ie haruslah seseorang yang lebih kreatif dari dirinya. Yang diperlukan oleh tipe ini memang sesuatu yang sukar dan mahal, yaitu ruang gerak sesuai dengan minatnya. Sementara minat dari tipe Ie adalah di bidang kreatif yang senantiasa berubah. <br> <br>Menciptakan ruang gerak yang seperti itu terkadang tidak terdapat di sekolah dan di tempat kursus. Seringkali orang tua harus mendisain sendiri untuk memastikan minat dari tipe Ie tergembleng secara maksimal dan naik dari satu tahap ke tahap yang lain tanpa ia kehilangan semangatnya.<br></p><p></p><ul><li>\r\n\r\n<b>Pilihan Sekolah/ Profesi</b></li></ul>Prioritas  utama jika  ingin memilih jurusan atau profesi untuk tipe Ie sebaiknya diarahkan pada satu diantara lima pilihan industri berikut ini yaitu kewirausahaan/ investasi, pendidikan/pelatihan, sastra, cinematrografi, spionase/ kepolisian.  <br><b><br></b>Selanjutnya pilihan lain dari jurusan/profesi yang sesuai untuk tipe Ie sebagai berikut:  marketer/iklan agency, lifestyle/mode, bisnis-penerbangan, bisnis agro-forestry, seniman, pengusaha, peneliti sains murni, fisikawan quantum, pencipta lagu, reengineering, proyeksi, peramal bisnis, pemain saham, investor, filosof, pakar pembelajaran/pemodul, event organizer, produsen, disainer, ahli kreatif berbagai bidang, arsitek, sutradara, pelukis abstrak, pesulap, peramal, pialang saham, atlit (spinter/pertandingan), pelatih olahraga pertandingan, penyembuh, cenayang, dll. <br> <b><br></b>Di masa depan, pilihan profesi akan semakin beragam. Namun jika ingin memilih jurusan ataupun profesi patokan bagi tipe Ie adalah mempertimbangkan empat kata kuncinya, yaitu: <b>mengarang, perut panjang, perubahan, dan rakitan. </b>Artinya, jurusan atau profesi yang dipilihnya didominasi <br>oleh unsur-unsur yang memerlukan kreativitas, mengandalkan tenaga anaerobik (karena perutnya panjang) atau tenaga tidak stabil yang melompat-lompat dan meledakledak, jenis jurusan atau profesi yang memiliki jiwa perubahan, dan memiliki ciri merakit atau mendaratkan ide brilian yang datang dari langit untuk didaratkan ke bumi sesuai keadaan masyarakat atau selera pasar. <br> <b><br><br></b><br></div>\r\n\r\n<br><p></p>');

-- --------------------------------------------------------

--
-- Table structure for table `tb_jenjang`
--

CREATE TABLE `tb_jenjang` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `konten` text,
  `tahapan` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_jenjang`
--

INSERT INTO `tb_jenjang` (`id`, `judul`, `konten`, `tahapan`) VALUES
(1, 'WSL 1', '<p>\r\n\r\n</p><h1><i></i>WSL1<i></i></h1><h2><i><b>Find The True You - Gue Banget</b></i><br></h2><p>Workshop STIFIn Level 1 sebagai materi dasar yang membahas tentang 5 (lima) Mesin Kecerdasan dan 9 (sembilan) Personaliti Genetik. Workshop ini akan menguak jati diri Anda yang sebenarnya, sehingga akan terasa Ini Gue Banget. Pelaksanaan WSL 1 berlangsung selama 1 hari.</p>\r\n\r\n<p></p>', 'Tahapan 2'),
(2, 'WSL 2', '<p>\r\n\r\n</p><h1><i><b></b></i><b>WSL 2</b><i><b></b></i></h1><h2><i><b>Licensed Promotor STIFIn - Tentang Loe, Gue Ahlinya</b></i><br></h2><p>Workshop STIFIn Level 2 merupakan workshop yang diperuntukkan bagi Anda yang ingin mengetahui lebih dalam tentang diri Anda dan orang lain, maka workshop ini wajib Anda ikuti sehingga Anda bisa mengambil Peluang Bisnis STIFIn dengan menjadi Licensed Promotor STIFIn. Pelaksanaan WSL 2 berlangsung selama 1 hari.</p>\r\n\r\n<p></p>', 'Tahapan 1'),
(3, 'Licensed_Promotor STIFIn ', '<h1>Licensed Promotor STIFIn&nbsp;</h1><h2><b><i>Bridging The Concept of STIFIn to Universe</i></b><br></h2><p><br></p><p>\r\n\r\n</p><p>Tools pelengkap untuk kamu menjadi promotor STIFIn dan sangat penting yaitu laptop, ID Promotor dan scanner.</p><p>Sekarang Anda siap untuk menjadi <i>&nbsp;Bridging The Concept of STIFIn to Universe</i></p>\r\n\r\n<br><p></p>', 'Tahapan 3');

-- --------------------------------------------------------

--
-- Table structure for table `tb_keunggulan`
--

CREATE TABLE `tb_keunggulan` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `deskripsi` text,
  `icon` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_keunggulan`
--

INSERT INTO `tb_keunggulan` (`id`, `judul`, `deskripsi`, `icon`) VALUES
(1, 'SIMPLE', 'Konsep STIFIn terbagi menjadi 5 Mesin Kecerdasan dan 9 Personaliti Genetik. FOKUS-SATU-HEBAT ', 'fa-dot-circle-o'),
(2, 'AKURAT', 'Konsep STIFIn mengindentifikasi sistem operasi atau software otak manusia.  Tingkat validitas dan reabilitas sebesar 95%. 1X SEUMUR HIDUP', 'fa-bullseye'),
(3, 'APLIKATIF', 'Bersifat berkolerasi dengan berbagai aspek kehidupan atau MULTI-ANGLE-FIELD.', 'fa-puzzle-piece');

-- --------------------------------------------------------

--
-- Table structure for table `tb_konsep_stifin`
--

CREATE TABLE `tb_konsep_stifin` (
  `id` int(11) NOT NULL,
  `gambar` varchar(255) DEFAULT NULL,
  `konten` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_konsep_stifin`
--

INSERT INTO `tb_konsep_stifin` (`id`, `gambar`, `konten`) VALUES
(7, 'Logo_STIFIn_Genetic_Intelligence_-_general_bg_putih2.png', '<p>\r\n\r\n</p><h3><b></b>Apa&nbsp;itu STIFIn?<b></b></h3><p>STIFIn sendiri merupakan konsep tentang fungsi otak dominan yang menjadi sistem operasi pada otak manusia (Sensing, Thinking, Intuiting, Feeling dan Insting). STIFIn juga merupakan metode untuk mengetahui jenis kecerdasan dan personaliti seseorang melalui Tes STIFIn.&nbsp;</p><p>Konsep STIFIn sudah dibangun oleh Farid Poniman sejak tahun 1999. Riset dan kajiannya sudah dilakukan lebih dari 20 tahun. Penerapan konsep STIFIn mulai digunakan ketika Farid Poniman bersama Indrawan Nugroho dan Jamil Azzaini mendirikan lembaga training yang bernama KUBIK Leadership (<a target=\"_blank\" rel=\"nofollow\" href=\"http://www.kubik.co.id)\">www.kubik.co.id)</a>. Lembaga training tersebut setiap memulai program trainingnya terlebih dahulu memetakan peserta sesuai dengan jenis kecerdasannya. Sebagai konsep, STIFIn dikembangkan terus menerus hingga mencapai kekukuhan ketika mematenkan formula 5 MK (Mesin Kecerdasan) dan 9 PG (Personaliti Genetik).</p><p>Yayasan STIFIn beruntung sekali karena memiliki Konsep STIFIn dan alat Tes STIFIn. Sejak mulai diperkenalkan kepada masyarakat pada Juni 2009, Tes STIFIn telah membantu melakukan pengetesan kepada hampir 500.000 orang (update 25 Maret 2023) dan secara keseluruhan peserta tes merasa bahwa alat Tes STIFIn memiliki tingkat akurasi yang tinggi. Hal ini juga didukung oleh pembuktian tim riset independen dari Malaysia yang dilakukan oleh Prof. Dr. Mohammed Zin Nurdin (pakar psikometrik) bersama dua doktor lainnya yaitu Dr. Suhaimi Mohamad (pakar personaliti) dan Dr. Wan Sulaiman (pakar personaliti) menunjukkan bahwa validitas dan reliabilitas Tes STIFIn yang tinggi. Keunggulan berikutnya tentang Konsep STIFIn yang ternyata memang bisa digunakan sangat aplikatif dalam berbagai bidang dan disiplin ilmu. Ini menjadi keuntungan besar bagi Yayasan STIFIn karena memiliki alat tes dan konsep yang Simple, Akurat, dan Aplikatif.</p><p>Sebagai bentuk aplikasinya, Konsep STIFIn sudah dipraktikan dalam menghafal Al Quran yang diterapkan oleh Rumah Quran STIFIn (RQS) lebih dari 20 angkatan menggunakan Metode Tahfidz STIFIn. Menggunakan Metode Tahfidz STIFIn, santri kami bisa menghafal Al-Quran 30 Juz dengan tingkat mutqin 15 Juz hanya dalam waktu 7 bulan.</p><p>Tes&nbsp;STIFIn&nbsp;adalah&nbsp;tes&nbsp;yang&nbsp;dilakukan&nbsp;dengan&nbsp;cara&nbsp;menscan&nbsp;kesepuluh&nbsp;ujung&nbsp;jari&nbsp;Anda&nbsp;(kurang&nbsp; lebih&nbsp;dalam&nbsp;waktu&nbsp;1- 2&nbsp;menit).&nbsp;Sidik&nbsp;jari&nbsp;yang&nbsp;membawa&nbsp;informasi&nbsp;tentang&nbsp;komposisi&nbsp;susuna&nbsp;syaraf&nbsp;tersebut&nbsp;kemudian&nbsp;dianalisa&nbsp;dan&nbsp;dihubungkan&nbsp;dengan&nbsp;belahan&nbsp;otak&nbsp;tertentu&nbsp;yang&nbsp;dominan&nbsp;berperan&nbsp;sebagai&nbsp;sistem&nbsp;operasi&nbsp;dan&nbsp;sekaligus&nbsp;menjadi&nbsp;mesin&nbsp;kecerdasan&nbsp;Anda.&nbsp;\r\n\r\n</p><h3>Mengapa Anda Perlu Tes STIFIn?</h3><ol><li>Pengalaman para orang dewasa mengatakan terlalu banyak ‘biaya kebodohan’ dengan terlalu banyak melakukan uji coba dalam hidup ini. Tes Stifin ini adalah panduan untuk menghilangkan biaya kebodohan tersebut, sehingga kita tidak buang umur dan buang uang. Sejak awal kita sudah tahu mesti pergi kemana dan bagaimana cara terbaiknya.</li><li>Revolusi hidup yang paling baik bukan dengan mengacak-ngacak cara hidup anda, melainkan dengan mensyukuri apa ‘harta karun’ dalam diri kita yang diberikan oleh Tuhan. Setelah tes Stifin, anda akan tahu bagaimana caranya berilmu, bersyukur, dan bersabar melalui ‘harta karun’ diri anda. Ubahlah nasib anda melalui jalur Tuhan, jalur ‘karpet merah’ yang telah diberikan kepada anda. Itulah revolusi hidup yang sesungguhnya.</li><li>Setelah tes Stifin, anda akan menemukan cetak biru hidup anda. Hal tersebut bukanlah vonis atau ramalan keberhasilan tetapi jalur tempat anda mengikhtiarkan kucuran keringat demi keberhasilan di depan mata. Energi yang difokuskan kepada satu maksud akan menciptakan momentum keberhasilan.</li><li>Untuk menjadi outliers (sosok yang sangat jarang) seperti tulisannya Malcolm Gladwell, maka anda harus telah memulai profesinya lebih dini dan menanam 10 ribu jam untuk deliberate-practice membangun profesi pilihan. Tes Stifin membantu anak anda menemukan profesi pilihan sejak dini dan sekaligus mengarahkan bagaimana menikmati 10 ribu jam tersebut.</li><li>Orang berbakat bisa gagal, jika ia mengingkari atau tidak tahu bakatnya. Salah asuhan terhadap bakat adalah ketidak-harmonisan dengan habitat anda. Jika yang ada diabaikan, dan yang tidak ada mau diadakan sama dengan memutar jarum jam hidup anda secara terbalik. Percayalah, anda akan dipenuhi dengan riwayat kegagalan.</li></ol><br>\r\n\r\n<p></p><p><br></p><div><p></p></div>');

-- --------------------------------------------------------

--
-- Table structure for table `tb_merchandise`
--

CREATE TABLE `tb_merchandise` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `deskripsi` varchar(255) DEFAULT NULL,
  `gambar` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_merchandise`
--

INSERT INTO `tb_merchandise` (`id`, `judul`, `deskripsi`, `gambar`) VALUES
(5, 'STIFIn Pancarona', 'Rp 200.000 /pcs', '1.png'),
(6, 'STIFIn Personality ', 'Rp 150.000 /pcs', '2.png'),
(7, 'Gue Banget', 'Rp 125.000/pcs', '31.png'),
(8, 'STIFIn Couple', 'Rp 200.000/pcs', '41.png'),
(9, 'QnA STIFIn ', 'Rp 150.000/pcs', '51.png'),
(10, 'STIFIn Profesi', 'Rp 200.000/pcs', 'Cover_Buku_STIFIn_.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_partners`
--

CREATE TABLE `tb_partners` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_partners`
--

INSERT INTO `tb_partners` (`id`, `nama`, `logo`) VALUES
(10, 'Kubik Leadership', 'Untitled_design.jpg'),
(11, 'Hydro Water Solution', '10.jpg'),
(12, 'Diebold', '9_(2).jpg'),
(13, 'Sharp', '11.jpg'),
(14, 'Holcim', '7_(2).jpg'),
(15, 'CIMB Niaga', '8_(2).jpg'),
(16, 'United Tractor', '6_(2).jpg'),
(17, 'BPBD DKI Jakarta', '8.jpg'),
(18, 'Kementrian Dalam Negeri', '9.jpg'),
(19, 'Fakultas Sosial & Politik UI', '5_(2).jpg'),
(20, 'Binus University', '4_(2).jpg'),
(21, 'Politeknik LP3I', '7.jpg'),
(22, 'Perbanas Institute', '6.jpg'),
(23, 'Universitas Muhammadiyah Prof. Dr. HAMKA', '5.jpg'),
(24, 'Akaademi Trainer', '3_(2).jpg'),
(25, 'Bimba', '41.jpg'),
(26, 'SD Islam Terpadu Ar-Ridho', '2.jpg'),
(27, 'Sekolah Isalm Terpadu Citra Az-Zahra', '31.jpg'),
(28, 'LIPI', '2_(2).jpg'),
(29, 'UNISMA', '12.jpg'),
(30, 'Waroeng Steak & Shake', '1_(2).jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_promotor_section`
--

CREATE TABLE `tb_promotor_section` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `deskripsi` text,
  `gambar` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_promotor_section`
--

INSERT INTO `tb_promotor_section` (`id`, `judul`, `deskripsi`, `gambar`) VALUES
(1, 'Menjadi Promotor STIFIn', 'Promotor STIFIn merupakan salah satu profesi STIFIn yang sangat POPULER dengan menjadi tangan STIFIn pertama dalam mengenalkan dan mempopulerkan Konsep STIFIn kepada masyarakat luas melalui TES STIFIn (Tes mengenali karakter dan potensi diri sebenarnya).', 'Artikel_STIFIn_(1)1.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_provinsi`
--

CREATE TABLE `tb_provinsi` (
  `id` varchar(2) NOT NULL,
  `nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tb_provinsi`
--

INSERT INTO `tb_provinsi` (`id`, `nama`) VALUES
('11', 'Aceh'),
('12', 'Sumatera Utara'),
('13', 'Sumatera Barat'),
('14', 'Riau'),
('15', 'Jambi'),
('16', 'Sumatera Selatan'),
('17', 'Bengkulu'),
('18', 'Lampung'),
('19', 'Kepulauan Bangka Belitung'),
('21', 'Kepulauan Riau'),
('31', 'Dki Jakarta'),
('32', 'Jawa Barat'),
('33', 'Jawa Tengah'),
('34', 'Di Yogyakarta'),
('35', 'Jawa Timur'),
('36', 'Banten'),
('51', 'Bali'),
('52', 'Nusa Tenggara Barat'),
('53', 'Nusa Tenggara Timur'),
('61', 'Kalimantan Barat'),
('62', 'Kalimantan Tengah'),
('63', 'Kalimantan Selatan'),
('64', 'Kalimantan Timur'),
('65', 'Kalimantan Utara'),
('71', 'Sulawesi Utara'),
('72', 'Sulawesi Tengah'),
('73', 'Sulawesi Selatan'),
('74', 'Sulawesi Tenggara'),
('75', 'Gorontalo'),
('76', 'Sulawesi Barat'),
('81', 'Maluku'),
('82', 'Maluku Utara'),
('91', 'Papua Barat'),
('94', 'Papua'),
('98', 'Malaysia'),
('99', 'Singapore');

-- --------------------------------------------------------

--
-- Table structure for table `tb_sejarah`
--

CREATE TABLE `tb_sejarah` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `konten` text,
  `tanggal_sejarah` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_sejarah`
--

INSERT INTO `tb_sejarah` (`id`, `judul`, `konten`, `tanggal_sejarah`) VALUES
(13, 'Kelahiran Stifin', '<h1><img alt=\"\" src=\"https://imgbox.io/ib/1XnEaW57vE\">1999</h1><h2><b></b><i>Kubik Leadership</i><b></b><br></h2><p><b></b>Konsep awal mula STIFIn yang ditulis menjadi buku dan dijadikan perusahaan yang bergerak di pengembangan sumber daya manusia.</p>', '0000-00-00 00:00:00'),
(14, '2', '<h1>2006</h1><h2><i>Konsep STIFIn&nbsp;</i><br></h2><p>Merupakan pengembangan dari <b>konsep STIF </b>dan disempurnaan menjadi STIFIn<br></p>', '0000-00-00 00:00:00'),
(15, NULL, '<h1>2009</h1><h2><i><b>STIFIn Menjadi PT</b></i><br></h2><p><i>Awalnya STIFIn menjadi sebuah PT<br></i></p>', NULL),
(16, NULL, '<p>\r\n\r\n</p><h1>2012</h1><h2><b><i>Rumah Qur\'an STIFIn</i></b></h2><p><i>Sebagai CSR dan bukti bahwa STIFIn aplikatif</i></p>\r\n\r\n<p></p>', NULL),
(17, NULL, '<p>\r\n\r\n</p><h1>2014</h1><h2><b><i>PT Menjadi Yayasan</i></b></h2><p><i>\r\n\r\n</i></p><p><i>PT berubah menjadi Yayasan STIFIn <br></i></p><i>\r\n\r\n</i><p></p>\r\n\r\n<p></p>', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tb_sensing`
--

CREATE TABLE `tb_sensing` (
  `id` int(11) NOT NULL,
  `gambar` varchar(255) DEFAULT NULL,
  `konten` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_sensing`
--

INSERT INTO `tb_sensing` (`id`, `gambar`, `konten`) VALUES
(3, '143.png', '<p></p><h1>Sensing (S)</h1><p></p>'),
(4, 'Artikel_STIFIn_(4)1.jpg', '<p>\r\n\r\n</p><h3></h3>\r\n\r\n<h3>Sensing introvert (Si)</h3>\r\n<p>\r\n\r\nSI adalah singkatan dari Sensing introvert. \r\n\r\n.  Jika huruf S berdiri sendiri merupakan identitas sebagai Mesin Kecerdasan (MK). Menurut konsep STIFIn, ragam Mesin Kecerdasan hanya ada lima, dan S adalah salah satu diantara 5 MK tersebut. Identitas Mesin Kecerdasan berubah menjadi kepribadian ketika MK digandengkan dengan jenis kemudi di belakangnya. Jenis kemudi kecerdasan hanya ada dua, yaitu i (introvert) dan e (extrovert). Dengan demikian Si sudah menjadi identitas kepribadian. S ditulis dengan huruf besar karena pengaruhnya sebagai MK lebih besar dari i yang ditulis dengan huruf kecil yang berperan hanya sebagai kemudi kecerdasan. <br></p><h4><ul><li>Sistem Operasi Otak</li></ul></h4><b></b><b></b><p><b></b>Pengertian sederhana dari Sensing introvert adalah jenis kepribadian yang berbasiskan kecerdasan lima-indera yang proses kerjanya dikemudikan dari dalam dirinya menuju ke luar dirinya. Sistem operasi pada tipe Si berada di belahan otak bagian bawah di sebelah kiri atau disebut sebagai limbik kiri.<b></b></p><p></p><p></p><ul><li><b>Tipologi Fisik </b></li></ul><b></b>Mesin Kecerdasan Sensing (S) sesungguhnya identik dengan otot. Mereka memiliki otot yang kuat. Otot yang kuat itu disebabkan karena tipe S memiliki otot merah tempat menyimpan tenaga aerobik. Tentu saja otot itu mesti sering digunakan supaya otot berkembang menyimpan tenaga aerobik yang lebih banyak. Tipe Si terlebih lagi karena ditunjang oleh ketersediaan baterai (charger) yang ada di dalam dirinya menyebabkan Si seperti memiliki tenaga yang kuat. <p></p><p>Selain memiliki bentuk (konstitusi) fisik yang atletis besar (bongsor), tipe Si juga memiliki kemahiran fisik yang ditunjang oleh motorik kasar. Otot memiliki kemampuan   mengingat yang disimpan  pada  bagian  sel  otot yang disebut dengan myelin. Otot yang terlatih memiliki myelin yang lebih berharga.  Kelebihan tipe S justru terletak di harga myelinnya yang lebih terlatih. </p><p></p><p>\r\n\r\n<b></b></p><blockquote><b><i>Tipe Si : Limbik kiri putih, Dalam ke Luar, Motorik Kasar, Atletis</i></b></blockquote><p></p><p></p><p></p><ul><li><b>Sifat Khas</b></li></ul><b>\r\n\r\n</b>Jika menggunakan sudut pandang dunia psikologi (aliran perilaku), kepribadian dari tipe Si mesti memiliki sifat perilaku khas yang dapat dibuktikan dan diukur yang berbeda dari delapan kepribadian yang lain. Terdapat sepuluh item yang bisa dibuktikan keberadaannya dan bisa diukur secara psikometrik.<p></p><p>Menurut konsep STIFIn, kesepuluh item tersebut menjadi kepribadian tetap bagi tipe Si, yang tidak akan berubah dan akan selalu eksis seiring dengan penambahan umur. Sepuluh (10) sifat yang tetap tersebut adalah:</p><p></p><ul><li>Persistent</li><li>Detailed</li><li>Recorder</li><li>Discipline</li><li>Careful</li><li>Spirited</li><li>Encyclopedic</li><li>Workaholic</li><li>Timeful</li><li>Indifferent</li></ul>Sebagai pribadi yang utuh, tipe Si memiliki sisi-sisi diametral sebagai berikut: penuh ambisi namun mudah patah, penurut namun muncul rasa persaingan,berjiwa pesiar namun ingin selalu didampingi sparring-nya, menjadi kamus atas segala problematika namun terkadang ‘blank’, cenderung kronologis dan fakta-sentris meski terkadang seperti bola liar dan tiba-tiba meyakini telepati, pelakon yang percaya diri namun terkadang menjadi pencemas, memang ulet dan tersusun namun disertai dengan bawel seperti Mr. Printilan. <br> <br>Oleh karena itu, tipe Si perlu waspada dengan kelemahannya dan berusaha mengekploitasi kelebihannya. Biasanya jika kelebihannya bergerak membaik, maka secara otomatis kelemahan dari tipe ini tertutup dengan sendirinya.<br>\r\n\r\n<ul><li><b>Kelebihan</b></li></ul><b>\r\n\r\n</b>Kepribadian dari tipe Si ini memiliki kekhasan karena memiliki kemampuan mengingat yang melebihi delapan jenis kepribadian yang lain. Kelebihan ini dapat disepadankan dengan<br>kecerdasan ingatan atau disebut MQ (Memory Quotient).<br><br>Tipe  Si  memiliki  kelebihan  yang  otomatis  berfungsi dalam cara kerjanya yang terbiasa efisien. Memperlakukanpekerjaannya dengan  output  per  input  yang  bagus. Pekerjaannya mesti efisien. Cara membelanjakan pengeluaran begitu hemat. Termasuk dalam berbagi pun, tipe  Si  sangat<br>hemat.Daripada disuruh berbagi terlalu besar, tipe Si  memilih membantu  dengan cara  lain yang mengeluarkan tenaganya. Tipe ini lebih senang menolong. <p></p><p></p><blockquote><b><i>Tipe Si : Memori Quotient (MQ), Efisien dan Hemat </i></b></blockquote><i><ul><li><b>Kemistri</b><br></li></ul></i>Tipe Si sesungguhnya calon orang kaya, karena memiliki kemistri terhadap harta. Hanya saja untuk mendapatkannya, tipe Si harus mengejar harta tersebut dengan keuletannya. Jika ingin berhasil meraih harta dengan baik maka tipe Si harus mencari panggung untuk meningkatkan produktivitasnya.<i> <br><b><br></b></i>Keuletan tipe Si bukan dilakukan secara membabi-buta, melainkan sejak dini menset-up panggung agar dirinya dapat tampil untuk memamerkan kebolehannya. Untuk mendapatkan panggung tersebut, ia mesti menata dari bawah sampai ia akhirnya mencapai level tertinggi dari permainan panggungnya.<i> <br><b></b> </i><br>Panggung disini tidak selalu berarti panggung pertunjukan, meskipun itu memang menjadi keutamaannya, namun panggung ini bisa dengan makna yang lebih luas. Termasuk di dalamnya panggung bisnis. Misalnya: keberhasilan mempertontonkan kemahiran pengelolaan uang nvestor dengan pulangan investasi (ROI) yang aman dan mengagumkan.<br>&nbsp; <br>Bisnis investasi   dijadikan   panggungnya   melalui Kemampuan pengelolaan keuangan yang efisien dan terpercaya. Sehingga setelah dipertontonkan, membuat  banyak  pihak tertarik untuk mempercayakan investasi kepadanya. Sebut saja karena tipe Si ini punya panggung berupa bank atau lembaga keuangan.<br><ul><li><b>Peranan</b><br></li></ul>Dengan fungsi otot yang kuat, Mesin Kecerdasan Sensing (S) lebih senang dengan jenis pekerjaan yang memerlukan keuletan. Tipe ini memiliki stamina yang lebih hebat dibandingkan dengan tipe lainnya. Kemudian digabungkan dengan kecerdasan yang berbasiskan panca indera, maka fungsi otot tersebut akan membuat tipe S menyukai pekerjaan berkeringat. Hal itu juga yang membuat tipe S memilih peran sebagai pemain yang berada di lapangan atau di panggung, dibandingkan dengan menjadi orang di belakang layar.<p></p><p>Jika tipe S dikemudikan dari dalam -menjadikan tipe Si akan muncul sifat yang lebih percaya diri. Hal itu membuat tipe Si menyukai sebagai orang yang tampil dan berperan sebagai pemain (player), satu diantara berbagai aktivitas yang ditekuninya. Seperti menjadi atlit, penyanyi, artis, pekerja, atau kalau pun menjadi pengusaha, maka ia akan ikut turut tangan menangani bisnisnya.</p><p></p><ul><li><b>Target dan Harapan </b></li></ul> Berkat rajin dan tidak mudah lelah, tipe Si memiliki jumlah koneksi hubungan sosial yang banyak. Jumlah   relasi yang dikunjungi  atau dihubungi tiap harinya lebih  banyak  dibanding <br>tipe yang lain. Tipe Si memiliki target mendapatkan koneksi dari semua haluan yang diperlukan untuk menyelesaikan tugas atau telah dilakukannya, tipe Si berharap dapat panen memetik<br>hasilnya (yielding).<p></p><p></p><ul><li>&nbsp;<b>Arah Merek </b></li></ul>Pengulangan aktivitas yang konsisten mengakibatkan tipe Si memiliki merek yang menempel pada dirinya yaitu sebagai orang yang sukses menghasilkan kuantitas yang besar. Merek tipe Si dikenal sebagai orang yang selalu berjaya memperbesar skala volume kinerjanya. Misalnya: jika memilih bisnis transportasi, tipe Si selalu berhasil memperbanyak penumpang. Jika ia menjadi ahli bahasa, maka jumlah perbendaharaan kata yang berhasil direkam jumlahnya begitu mengagumkan.<br><ul><li><b>Cara Belajar</b></li></ul><b></b>Seseorang dengan tipe Si jika ingin memperbaiki cara belajarnya, maka lakukandengan merekam perbendaharaan istilah ataukata-kata (vocabulary) baru. Setiap istilah ataupelajaran diulang-ulang dengan membahasakan kembali dengan cara yang bervariasi. Selain itu, belajar menggunakan alat peraga juga penting karena penampakan secara visual akan memperbanyak rekaman informasi yang menempel pada ingatan tipe Si. Bahkan alat peraga tersebut perlu dimainkan agar mendapat pengalaman tersendiri.<b><br><br></b>Mencoba dan mengalami langsung merupakan proses belajar yang paling efektif bagi tipe Si. Bentuk-bentuk latihan yang mengulang merupakan cara yang sangat baik untuk memfungsikan myelin-nya. Semakin sering dilatih, semakin banyak myelin yang berkembang dan terbentuk, kemahiran tipe ini akan semakin meningkat.<p></p><p>Belajar sambil bergerak justru akan membuat tipe Si menjadi lebih nyaman dan dapat memperpanjang masa belajar jika melakukannya sambil bergerak. Bagi tipe Si, cara yang paling jitu untuk membangkitkan motivasi untuk belajar dan bekerja yaitu jika diberi sparring (teman berlatih yang sekaligus menjadi pesaingnya), karena tipe ini sangat memerlukan ‘tarikan nyata’. Tipe Si tidak bisa dimotivasi menggunakan pesaing imajiner, karena kecerdasan inderawinya hanya akan bekerja jika pesaingnya nyata. Semakin tipe ini ingin ‘naik kelas’, semakin tinggi pula level sparring yang mesti dihadirkan. <br></p><ul><li><b>Pilihan Sekolah/ Profesi</b><br></li></ul><b></b>Prioritas utama jika ingin memilih jurusan atau profesiuntuk tipe Si sebaiknya diarahkan pada satu diantara limapilihan industri berikut ini, yaitu industri keuangan, bahasa,transportasi, perdagangan, hiburan.<b> <br> <br></b>Selanjutnya, pilihan lain dari jurusan atau profesi yangsesuai untuk tipe Si sebagai berikut: ekonom, atlit, tentara,bisnis perhotelan, sejarawan, bankir, pertanahan, dokter,jurnalis, keartisan, manufacturing, pilot, pramugari, model, aktor/artis, salesman, perkebunan, pertanian, peternakan,administrasi, sekretaris, pustakawan,operator, pegawai/staff,penyanyi, pekerja pabrik, security, pelukis naturalis,fotografer, cameraman, presenter, penari, pekerja lapangandalam berbagai sektor, dan lain-lain.<b> <br><br></b>Di masa depan, pilihan profesi akan semakin beragam. Namun jika ingin memilih jurusan ataupun profesi, patokan bagi tipe Si adalah mempertimbangkan empat kata kunci-nya, yaitu: <b><i>mengingat, otot, rajin, dan tergerak.</i></b><p></p><p> <i></i>Artinya, jurusan atau profesi yang sebaiknya dipilih oleh tipe Si didominasi oleh unsur-unsur yang memerlukandayaingat,memerlukan otot, dan mengandalkan faktor rajin untuk menguasainya, serta tergerak dengan sendirinya karena tipe Si memang terpanggil di jurusan atau profesi tersebut.<i><br><b></b></i></p><blockquote><i><b>Industri Tipe Si : Keuangan, Bahasa, Transportasi, Perdagangan dan Hiburan</b></i></blockquote> <br><b><br></b><br><b><br></b><br><b><br></b><br><b>  \r\n\r\n</b>\r\n\r\n<p></p><p><br><b><br></b></p><p></p><b></b> <br><br><br>\r\n\r\n<br><p></p>'),
(5, 'Artikel_STIFIn_(5)1.jpg', '<p>\r\n\r\n</p><h3>Sensing introvert (Si)</h3>Se adalah singkatan dari Sensing extrovert.  Jika huruf S berdiri sendiri merupakan identitas sebagai Mesin Kecerdasan (MK). Menurut konsep STIFIn, ragam Mesin Kecerdasan hanya ada lima, dan S adalah salah satu diantara 5 MK tersebut. Identitas Mesin Kecerdasan berubah menjadi kepribadian ketika MK digandengkan dengan jenis kemudi di belakangnya. Jenis kemudi kecerdasan hanya ada dua, yaitu i (introvert) dan e (extrovert). Dengan demikian, Se sudah menjadi identitas kepribadian. S ditulis dengan huruf besar karena pengaruhnya sebagai MK lebih besar dari e yang ditulis dengan huruf kecil yang berperan hanya sebagai kemudi kecerdasan.<p></p><h4><ul><li>Sistem Operasi Otak</li></ul></h4><b></b><b></b><p><b></b>Pengertian sederhana dari Sensing extrovert adalah jenis kepribadian yang berbasiskan kecerdasan lima-indera yang proses kerjanya dikemudikan dari luar dirinya menuju ke dalam dirinya. Sistem operasi pada tipe Se berada di belahan otak bagian bawah di sebelah kiri atau disebut sebagai limbik kiri. Pada limbik kiri tersebut yang menjadi kemudi kecerdasan dari tipe ini berada di lapisan abu-abu yang letaknya di bagian luar atau permukaan otak. Limbik kiri abu-abu itulah yang menjadi sistem operasi tipe Se. </p><p>Lapisan yang berwarna abu-abu memiliki tekstur otak yang lebih renggang karena mengandung sel otak yang lebih sedikit. Kerapatan yang lebih rendah dibandingkan dengan lapisan bagian dalam tersebut membuat kemudi kecerdasan bergerak dari luar ke dalam. Hal ini menyebabkan ‘tuan yang punya badan’ menjadi seolah lebih malas untuk bergerak karena sumber bioritmiknya tergantung pemicu dari luar. <br> <b></b></p><p></p><p></p><ul><li><b>Tipologi Fisik </b></li></ul><b></b>Mesin kecerdasan Sensing (S) sesungguhnya identik dengan otot. Mereka memiliki otot yang kuat. Otot yang kuat itu disebabkan karena tipe S memiliki otot merah tempat menyimpan tenaga aerobik. Tentu saja otot itu mesti sering digunakan supaya otot berkembang menyimpan tenaga aerobik yang lebih banyak. Tipe Se  menyimpan  potensi tenaga yang besar namun sangat tergantung dengan ketersediaan baterai  (charger)  yang justru adadi  luar  dirinya  menyebabkan  Se cenderung seperti diesel yang memulai dengan lambat tapi lama-kelamaan akan semakin kuat. <p></p><p><b></b></p><blockquote><b><i>Tipe Se : Limbik kiri abu-abu, Dari luar ke dalam, Motorik halus, Atletis</i></b></blockquote><p><i></i>Bentuk (konstitusi) fisiknya atletis tetapi cenderung berukuran mungil namun ditunjang oleh keberadaan motorik halus sehingga memiliki kemampuan fleksibilitas fisik yang<br>mengagumkan.</p><p>Otot memiliki kemampuan mengingat yang disimpan pada bagian sel otot yang disebut dengan myelin. Otot yang terlatih memiliki myelin yang lebih berharga. Kelebihan tipe S justru terletak di harga myelin-nya yang lebih terlatih.<i><b></b> <br></i></p><p></p><p></p><p></p><ul><li><b>Sifat Khas</b></li></ul><b></b>Jika menggunakan sudut pandang dunia psikologi (aliran perilaku), kepribadian dari tipe Se mesti memiliki sifat perilaku khas yang dapat dibuktikan dan diukur yang berbeda dari delapan kepribadian yang lain. Terdapat sepuluh item yang bisa dibuktikan keberadaannya dan bisa diukur secara psikometrik. <p></p><p>Menurut konsep STIFIn, kesepuluh item tersebut menjadi kepribadian tetap bagi tipe Si, yang tidak akan berubah dan akan selalu eksis seiring dengan penambahan umur. Sepuluh (10) sifat yang tetap tersebut adalah:</p><p></p><ul><li>Persistent</li><li>Detailed</li><li>Adventurous</li><li>Playful </li><li>Demonstrative</li><li>Generous</li><li>Repetitious</li><li>Tune in order</li><li>Show offs</li><li>Inoffensive </li><li>Endurance</li><li>Experience</li></ul>Sebagai pribadi yang utuh, tipe Se memiliki sisi-sisi diametral sebagai berikut: tahan banting seperti laki-laki tetapi manja seperti perempuan, exposure petualangannya luas namun internalisasi kedewasaan lambat, seperti pemberani padahal sebenarnya kerdil, menjadi pendamping yang mudah disenangkan namun tidak mudah dibuat jatuh cinta. <p></p><p>Selain itu, tipe Se memiliki pembawaan yang terkesan lembut padahal suaranya sering melengking, susah memulai kerja tetapi jika sudah mulai kerja determinasinya kuat, dermawan tapi boros sebagai penikmat, mengharapkan kepastian tetapi cepat merasa tersudut dan kemudian kabur. Oleh karena itu, tipe ini perlu waspada dengan kelemahannya dan berusaha mengekploitasi kelebihannya. Biasanya jika kelebihannya bergerak membaik, maka secara otomatis kelemahan dari tipe ini akan tertutup dengan sendirinya.</p><p></p><ul><li><b>Kelebihan</b></li></ul><b></b>Kepribadian dari tipe Se ini memiliki kekhasan karena memiliki kemampuan fleksibilitas dan kekuatan otot yang melebihi delapan jenis kepribadian yang lain. Kelebihan ini dapat disepadankan dengan kecerdasan fisik atau disebut PQ (Physical Quotient). <br><br>Tipe Se memiliki kelebihan yang cenderung lebih dermawan. Juga di sisi lain dalam membelanjakan utuk dirinya cenedrung lebih boros. Dengan kedermawanannya, terutama untuk membelanjakan orang lain, akan membuat tipe ini kerapkali mendapatkan momentum bisnis yang bagus. Tipe ini memperlakukan pekerjaannya sebagai aktivitas yang menyenangkan, bahkan hidupnya pun cenderung ingin bersenang-senang.  <p></p><p></p><blockquote><b><i>Tipe Se : Mencari LADANG, Dikejar HARTA, dan Frontliner</i></b></blockquote><p><b></b>Oleh karena itu, tipe ini menjadi pembelanja yang boros karena cenderung ingin menikmati hidup. Jika menolong orang, cukup dengan memberi uang. Tipe Se merasa tanggung jawabnya selesai jika sudah menangani masalah dengan memberi uang. <b><i><br></i></b></p><i><ul><li><b>Kemistri</b><br></li></ul></i>Tipe Se sesungguhnya calon orang kaya, karena memiliki kemistri terhadap harta. Hanya saja, untuk mendapatkannya tipe Se harus mampu menangkap setiap peluang yang datang. Jika ingin berhasil meraih harta dengan baik maka tipe Se harus mencari ladang untuk menanam uangnya. <br> <i><br></i>Keuletan tipe Se untuk memanfaatkan setiap kesempatan yang datang ibarat berladang secara musiman. Kejelian tipe ini untuk menangkap setiap peluang yang datang membuat ia begitu cekatan jika berbisnis dari proyek ke proyek. Ladang itu biasanya lebih berupa aset fisik, properti, franchise, atau infrastruktur yang dikelola untuk menghasilkan pemasukan uang secara rutin.<br><ul><li><b>Peranan</b><br></li></ul>Dengan fungsi otot yang kuat, Mesin Kecerdasan Sensing (S)  lebih  senang  dengan  jenis pekerjaan yang   memerlukan keuletan. Tipe ini memiliki stamina yang  lebih hebat dibandingkan dengan  tipe lainnya.  Kemudian digabungkan dengan kecerdasanyang berbasiskan panca indera,maka fungsi otot akan membuat tipe S menyukai pekerjaan  berkeringat. Hal itu juga yang membuat tipe S memilih peran sebagai pasukan paling depan ketika berada di lapangan atau di panggung dibandingkan dengan menjadi<br>orang di belakang layar. <br>  <br>Jika tipe S nya dikemudikan dari luar ke dalam menjadi tipe Se akan muncul sifat lebih pemalu dan pencemas, namun berani mencoba sekaligus memanfaatkan potensi tenaganya yang sedia ada. Bahkan pada urusan untuk tampil ke depan jika ia sudah ditempa dengan latihan yang disiplin dan pengalaman justru ia merasa paling berhak untuk berada di barisan paling depan, meskipun awalnya pemalu.<p></p><p>Tipe Se merasa tidak punya pilihan harus berada di depan karena selain lebih sederhana juga lebih fun (menyenangkan). Tipe Se sanggup untuk bekerja keras agar dapat terpilih sebagai barisan paling depan. Selain sanggup mengerjakan pekerjaan rutin, tipe ini juga bersedia difungsikan sebagai frontliner dalam suatu pekerjaan. Termasuk ketika menekuni profesi seperti menjadi atlit, penyanyi, artis, pekerja, atau kalau pun menjadi pengusaha, maka tipe ini lebih merasa yakin dengan pengalamannya sehingga ia akan menangani bisnisnya memulai dari paling depan.</p><p></p><p></p><ul><li><b>Target dan Harapan </b></li></ul>Kestabilan tipe Se dalam bekerja dan selalu terhubung dengan lingkungannya (karena <br>lingkungan justru menjadi chargernya) serta selalu membeli dukungan lingkungan dengan kedermawanannya maka akhirnya membuat tipe Se memiliki banyak peluang bisnis<br>yang menjadi momentum terbaik untuk dimanfaatkan. <br> <br>Kebiasaannya selalu mencari peluang baru terkadang tergelincir menjadi sosok yang lebih oportunistik. Namun baginya peluang bisnis harus diambil sebagai momentum untuk menghasilkan pendapatan (generate income). Hal inilah yang menyebabkan tipe Se seperti cenderung bergerak dari satu proyek ke proyek lainnya. <br> <p></p><p></p><ul><li>&nbsp;<b>Arah Merek </b></li></ul>Akibat dari rutinitas yang dilakukan dan minat yang besar untuk menangkap peluang, membuat tipe Se memiliki merek yang menempel pada dirinya yaitu sebagai sosok yang menjadi jaminan sukses di setiap peluang kerjasama. Merek tipe Se dikenal dengan merek dirinya yang menjadi jaminan terselenggaranya kerjasama dengannya. <br> <br>Jika terdapat investor yang ingin bekerja sama dengannya, maka apa yang tertanam di benak investor tersebut adalah bahwa pribadi dari tipe Se ini dianggap sudah punya pengalaman dan track record yang bagus untuk menjalankan investasi tersebut. <br><ul><li><b>Cara Belajar</b></li></ul><p></p><p>Cara tipe Se dalam mempelajari sesuatu dilakukan dengan menghafal bacaan. Supaya bacaan tersebut lebih mudah dikuasai, tipe ini harus ikut menggerakkan tangannya untuk menandai bacaan-bacaan yang dianggap penting. Tipe Se memiliki kemampuan merekam secara visual yang mengagumkan. Merekam peristiwa menjadi kelebihan dari tipe ini, apalagi ketika ia ingin mempertontonkan kebolehannya. Urutan peristiwa secara detail dapat direkam dengan seksama. Oleh karena itu, cara belajar dengan menggunakan alat peraga secara visual akan menjadi keutamaan baginya.</p><p>Namun faktor yang paling membuat tipe Se menguasai pelajaran yaitu jika ia  mengulang ulang mengerjakan latihan soal atau memecahkan masalah. Pengalaman tipe ini dalam melakukan sesuatu akan selalu menjadi bekal keberhasilannya. Mencoba langsung dan mengalami sendiri merupakan proses belajar yang paling efektif bagi tipe Se. Bentuk bentuk pengulangan latihan merupakan cara yang sangat baik untuk memfungsikan myelinnya. Semakin sering dilatih, semakin banyak myelin yang berkembang, maka kemahiran dari tipe ini akan semakin meningkat. Dengan berbekal kemampuan fisik yang berbasiskan myelin, memberikan terpaan pengalaman yang spesifik akan menjadikan tipe Se memiliki tarif yang tinggi.</p><p>Cara yang paling jitu untuk mendorong tipe Se adalah dengan memberikan insentif berupa sesuatu yang nyata (tangible). Insentif ini harus dirancang sedemikian rupa supaya terukur. Artinya jika insentif tersebut diberikan dalam jumlah yang sedikit sudah dapat memotivasi tipe ini, kenapa harus banyak-banyak. Proses motivasi pada tipe Se <br>ini sesungguhnya mudah karena cukup dengan memberikan materi. Namun yang paling penting, perlu diukur jumlah dan waktunya yang tepat. <br></p><ul><li><b>Pilihan Sekolah/ Profesi</b><br></li></ul><b></b><p>Prioritas utama jika ingin memilih jurusan atau profesi untuk tipe Se sebaiknya diarahkan pada satu diantara lima pilihan industri berikut ini, yaitu ekonomi, sport, kemiliteran, perhotelan, sejarah. Selanjutnya pilihan lain dari jurusan atau profesi yang sesuai untuk tipe Se sebagai berikut: ahli keuangan, ahli bahasa, bisnis transportasi, pedagang, entertainer, bankir, pertanahan, dokter, jurnalis, keartisan, manufacturing, pilot, pramugari,model, aktor/artis, salesman, perkebunan, pertanian, peternakan, administrasi, sekretaris, pustakawan, operator, pegawai/staff, penyanyi, pekerja pabrik, security, pelukis naturalis, fotografer, cameraman, presenter, penari, pekerja lapangan dalam berbagai sektor, dll.</p><p> Di masa depan, pilihan profesi akan semakin beragam.  Namun jika ingin memilih jurusan ataupun profesi patokan bagi tipe Se adalah mempertimbangkan  empat kata kuncinya, yaitu: <b><i>mengingat, otot, rajin, dan tercetak.</i></b><br></p><p><i></i>Artinya, jurusan atau profesi yang dipilihnya didominasioleh unsur-unsur yang memerlukan daya ingat, memerlukanotot, dan mengandalkan faktor rajin untuk menguasainya, serta mudah membentuk dirinya ibarat mencetak lilin oleh jurusanatau profesi yang dipilihnya, karena tipe Se memang sangat <i>bergantung pada tempaan lingkungannya.</i></p><p><i><b></b></i></p><blockquote></blockquote><blockquote><i><b>Industri Tipe Se : Ekonomi, Sport, Kemiliteran, Perhotelan</b></i></blockquote><i><blockquote></blockquote><b> </b></i><p></p>\r\n\r\n\r\n\r\n<br><p></p>');

-- --------------------------------------------------------

--
-- Table structure for table `tb_slider`
--

CREATE TABLE `tb_slider` (
  `id` int(11) NOT NULL,
  `teks` text,
  `gambar` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_slider`
--

INSERT INTO `tb_slider` (`id`, `teks`, `gambar`) VALUES
(1, 'Genetic Intelligence', '1_(1).png'),
(2, 'Kompilasi 3 Teori Besar para Ahli Landasan Konsep STIFIn', '4.png'),
(3, '5 Mesin Kecerdasan dan 9 Personaliti Genetik STIFIn ', '3_Opsi_desain_XBanner_STIFIn_80x160cm_(1).jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_solver_about`
--

CREATE TABLE `tb_solver_about` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `deskripsi` text,
  `gambar` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_solver_about`
--

INSERT INTO `tb_solver_about` (`id`, `judul`, `deskripsi`, `gambar`) VALUES
(3, 'Seputar Solver STIFIn', '<p>Tes STIFIn dilakukan dengan cara men-scan kesepuluh ujung jari Anda, mengambil waktu tidak lebih dari 1 menit. Sidik jari yang membawa informasi tentang komposisi susunan syaraf tersebut kemudian dianalisa dan dihubungkan dengan belahan otak tertentu yang dominan berperan sebagai sistem-operasi dan sekaligus menjadi mesin kecerdasan anda.</p>', '5_(2)1.png');

-- --------------------------------------------------------

--
-- Table structure for table `tb_solver_people`
--

CREATE TABLE `tb_solver_people` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `branding` varchar(255) DEFAULT NULL,
  `rating` varchar(255) DEFAULT NULL,
  `deskripsi` text,
  `gambar` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_solver_people`
--

INSERT INTO `tb_solver_people` (`id`, `nama`, `branding`, `rating`, `deskripsi`, `gambar`) VALUES
(1, 'Heri Irawan', 'UI/UX Designer', '4 and a half', '<p><b></b></p>Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.<br><p></p><p></p>', 't12.jpg'),
(4, 'Rizki Aditya', 'Marketing', '3 and a half', '<p>Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.<br></p>', 't13.jpg'),
(5, 'Anggela Pangraung12', 'Head Marketing', '4 and a half', '<p>Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.<br></p>', 't110.jpg'),
(9, 'Andi Jaya', 'Developer', '4', '<p>Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.<br></p>', 't111.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_solver_roadmap`
--

CREATE TABLE `tb_solver_roadmap` (
  `id` int(11) NOT NULL,
  `konten` text,
  `waktu` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_solver_roadmap`
--

INSERT INTO `tb_solver_roadmap` (`id`, `konten`, `waktu`) VALUES
(5, '<p>12312321213123123123</p>', '2023-04-09'),
(6, '<p>221</p>', '2023-04-09');

-- --------------------------------------------------------

--
-- Table structure for table `tb_stifin_institute`
--

CREATE TABLE `tb_stifin_institute` (
  `id` int(11) NOT NULL,
  `konten` text,
  `gambar` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_stifin_institute`
--

INSERT INTO `tb_stifin_institute` (`id`, `konten`, `gambar`) VALUES
(3, '<p>\r\n\r\n</p><b><h2><br></h2><h2>About</h2></b><p></p><p>Lembaga dibawah yang diberikan mandat oleh Yayasan STIFIn untuk mengembangkan keilmuwan STIFIn dan mempopulerkan konsep STIFIn melalui&nbsp;workshop - workshop resmi STIFIn yang diadakan oleh STIFIn Institute dan Cabang STIFIn. Adapun workshop-workshop STIFIn yang dipopulerkan antara lain Worskhop STIFIn Level (WSL)1, WSL2 (Promotor), WSTematik, TFT, WSL3 (Solver), dan WSL4 (Konsultan). Dimana WSL1 merupakan perkenalan konsep STIFIn, WSL2 sebagai syarat untuk menjadi Promotor STIFIn, SPT untuk mencetak Trainer STIFIn, WSL3 merupakan syarat untuk menjadi Solver, WSL4 syarat untuk menjadi Konsultan STIFIn, dan WSTematik syarat untuk menjadi Trainer STIFIn. Dalam rangka mempopulerkan Konsep STIFIn, STIFIn Institute diharapkan bisa melakukan kampanye/sosialisasi dalam berbagai platform medsos seperti Instagram, Facebook, Youtube, hingga Podcast.</p><b><p>\r\n\r\n</p><br><h2>Visi </h2><p></p></b><p>Konsep STIFIn diterima, diaplikasikan dan membantu meningkatkan kualitas masyarakat Indonesia hingga dunia, dan dikenal sebagai konsep Personal Development (pengembangan diri) asli Indonesia yang simpel, akurat dan aplikatif.</p><br><h2>Misi </h2><ol><li><b></b>Menjadi Konsultan Konseptual dan Pendamping penerapan Konsep STIFIn</li><li>Mengadakan event dan kegiatan publik sebagai sarana publikasi dan edukasi&nbsp;tentang Konsep STIFIn</li><li>Menjadi Best Practice dan rujukan aplikasi Konsep STIFIn dalam peningkatan kualitas hidup para pegiat STIFIn</li><li>Digitalisasi Konsep STIFIn</li></ol><br><b><h2>Tujuan</h2><p></p></b><p>Menjadi Institusi Pendidikan Konsep STIFIn bagi seluruh STIFIners</p><b><p> </p></b><br><br><p></p><p><br></p>\r\n\r\n<br><p></p>', 'logo_STIFIn_Institute_Update_Hijau1.png');

-- --------------------------------------------------------

--
-- Table structure for table `tb_stifin_rqs`
--

CREATE TABLE `tb_stifin_rqs` (
  `id` int(11) NOT NULL,
  `konten` text,
  `gambar` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_stifin_rqs`
--

INSERT INTO `tb_stifin_rqs` (`id`, `konten`, `gambar`) VALUES
(5, '<p>\r\n\r\n<b></b></p><p></p><h2></h2>\r\n\r\n<b><p></p></b><p></p>\r\n\r\n<b><p></p></b><h2><br></h2><h2>About </h2><p>Progam penggemblengan menghafal Al-Quran 30 Juz dalam waktu 7 (tujuh) bulan dengan menggunakan Metode Tahfizd STIFIn dan Metode RQS, wajib mukim/berasrama dan full beasiswa (GRATIS) . Dengan adanya program RQS membuat masyarakat melek bahwa menghafal Al-Quran ternyata bisa dilakukan dengan cepat dan mudah, jika menggunakan Metode Tahfizd STIFIn dan Metode RQS.</p><p><br></p><h2>Visi&nbsp;</h2><p>Menaikkan spiritualitas umat Islam dengan menjamurkan para penghafal Al-Quran di merata tempat.</p><br><h2>Misi&nbsp;</h2><p>Mencetak 3000 penghafal Al-Quran setiap tahun, dengan standar hafal 30 Juz mutqin (hafal lancar) 15 Juz dalam waktu 7 (tujuh) bulan.</p><br><h2>Komitmen&nbsp;</h2><p>Memberikan beasiswa full kepada seluruh santri RQS.</p><br>\r\n\r\n<br><p></p>', 'Logo_RQS_PNG1.png');

-- --------------------------------------------------------

--
-- Table structure for table `tb_testimonial`
--

CREATE TABLE `tb_testimonial` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `pekerjaan` varchar(255) DEFAULT NULL,
  `testimoni` text,
  `gambar` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_testimonial`
--

INSERT INTO `tb_testimonial` (`id`, `nama`, `pekerjaan`, `testimoni`, `gambar`) VALUES
(5, 'Subiakto Priosoedarsono', 'Pakar Branding', 'Konsep STIFIn dengan Branding, apa hubungannya? \r\nAda hubungannya ternyata, jadi untuk membangun personal brand, STIFIn membantu mengidentifikasi Brand DNA dan produk Anda. Dalam hal produk, STIFIn akan membantu untuk mengidentifikasi Brand Personality dari produk Anda, seperti tampilan logo, packaging atau iklan produk Anda.', 'subiakto.jpg'),
(6, 'Kris Pujiatni', 'Psikolog, Dosen Psikologi Fakultas UMS', 'Banyak cara dalam memahami diri sendiri dan orang lain. Salah satunya dengan konsep STIFIn, kita akan lebih mengerti dan memahami diri sendiri dan orang lain serta kerjasama dalam keluarga dan dunia kerja menjadi lebih baik.', 'Kris-Pujiatni_-Psikologi.png'),
(7, 'Prof. Dr. Arief Rachman, M. Pd', 'Praktisi Pendidikan ', 'Jika sebelumnya sidik jari hanya digunakan untuk melengkapi identitas, kepentingan kepolisian, dan sebagai penanda kehadiran seseorang, ternyata analisis sidik jari STIFIn juga dapat digunakan untuk membaca potensi terpendam setiap anak. Dengan demikian, setiap anak akan mendapatkan hak sesuai dengan kecenderungan yang mereka miliki. Jangan paksa anak melukis, padahal ia senang menari. Mengetahui potensi anak sejak dini merupakan sebuah keharusan agar anak mendapat perlakuan yang berbeda dan khusus. Dengan melalui analisis sidik jari ini semakin membuka pikiran kita bahwa setiap anak dilahirkan dengan kecerdasan dan potensinya masing-masing.', 'Prof__Dr__Arief_Rachman2.jpg'),
(8, 'Melly Goeslaw', 'Penulis Lagu dan Penyanyi ', 'I recommended banget untuk semuanya, khususnya untuk anak-anak kita. Tes, deh melalui STIFIn! Thank you STIFIn, Aku jadi tau karakter anak-anak aku, terus bagaimana cara menanggulangi dan berkomunikasi baik dengan mereka. Cocok dan pas banget buat anak-anak kita.', 'melly_goeslow.jpg'),
(9, 'Arifia Maulida', 'Miss Indonesia Favorit 2020', 'Setelah mengikuti tes STIFIn, saya dapat mengenal dan memahami diri saya lebih dalam, menentukan metode dan pola belajar yang terbaik untuk diri saya, serta memahami segala kelebihan dan kekurangan diri.\r\n\r\nTak hanya itu, STIFIn juga memberikan gambaran akan profesi, bidang studi, hubungan intrapersonal yang cocok bagi masing-masing karakter. Tentunya STIFIn membantu saya dalam menentukan langkah di masa yang akan datang, dan juga investasi yang baik untuk tiap individu.', 'Arifia-Maulida.png'),
(10, 'Prof. Dr. Kuswandi, S.U., M. Phill., Apt', 'Guru Besar UGM, Pakar Genetika, Pemerhati Pendidikan ', 'Hasil tes STIFIn ini sangat cocok dengan saya. Penjelasannya sangat ilmiah, detail dan praktis untuk saya pahami. Jadi menurut saya, tes ini sangat penting dilakukan sedini mungkin agar kita sebagai orang tua tidak salah dalam mengarahkan anak untuk memilih jalur pendidikan dan profesi yang sesuai dengan bakat dan potensinya. Sehingga anak-anak menemukan cara belajar yang menyenangkan.', '76Prof_-Dr_-M_-Kuswandi-Tirtodiharjo-S_U_-M_Phil__-Apt.jpg'),
(11, 'Helvy Tiana Rosa', 'Penulis, Dosen dan Produser Film', 'STIFIn membuat saya memahami diri sendiri dan orang lain. Bahkan saya menerapkan konsep STIFIn ini dalam mengembangkan karakter tokoh dalam cerpen, novel dan film yang saya buat. ', 'Helvy_Tiana.jpg'),
(12, 'Prof. Dr. Kumaidi, MA', 'Ahli Psikometrik, Dosen UMS', 'Saya telah membaca laporan riset yang dipimpin . Dr. Mohammed Zin Nordin dan timnya tentang Tes STIFIn. Meke telah melakukan riset dengan baik. ', 'Prof_-Dr_-Kumaidi2C-MA-Ahli-Psikometrik.png');

-- --------------------------------------------------------

--
-- Table structure for table `tb_thingking`
--

CREATE TABLE `tb_thingking` (
  `id` int(11) NOT NULL,
  `gambar` varchar(255) DEFAULT NULL,
  `konten` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_thingking`
--

INSERT INTO `tb_thingking` (`id`, `gambar`, `konten`) VALUES
(2, '152.png', '<p></p><h4></h4>\r\n\r\n<h1>Thinking (T)</h1><div><p></p></div>'),
(3, 'Artikel_STIFIn_(6)1.jpg', '<p>\r\n\r\n</p><h3></h3>\r\n\r\n<h3>Thinking introvert (Ti)</h3>\r\n<p>Ti adalah singkatan dari Thinking introvert. Jika huruf T berdiri sendiri merupakan identitas sebagai Mesin Kecerdasan (MK). Menurut konsep STIFIn, ragam Mesin Kecerdasan hanya ada lima, dan T adalah salah satu diantara 5 MK tersebut. Identitas Mesin Kecerdasan berubah menjadi kepribadian ketika MK digandengkan dengan enis kemudi di belakangnya. Jenis kemudi kecerdasan hanya ada dua, yaitu i (introvert) dan e (extrovert). Dengan demikian, Ti sudah menjadi identitas kepribadian. T ditulis dengan huruf besar karena pengaruhnya sebagai MK lebih besar dari i yang ditulis dengan huruf kecil yang berperan hanya sebagai kemudi kecerdasan.  <br></p><h4><ul><li>Sistem Operasi Otak</li></ul></h4><p>Pengertian sederhana dari Thinking introvert adalah jenis kepribadian yang berbasiskan kecerdasan logika yang proses kerjanya dikemudikan dari dalam dirinya menuju ke luar dirinya. Sistem operasi pada tipe Ti berada di belahan otak bagian atas di sebelah kiri atau disebut sebagai otak besar kiri atau diringkas otak kiri. Pada otak kiri tersebut yang menjadi kemudi kecerdasan dari tipe ini berada di lapisan putih yang letaknya di bagian dalam. Otak kiri putih itulah yang menjadi sistem operasi tipe Ti. <br> <br>Lapisan yang berwarna putih memiliki tekstur otak yang lebih padat karena mengandung sel otak lebih banyak. Kerapatan yang lebih tinggi dibandingkan dengan lapisan bagian luar tersebut membuat kemudi kecerdasan bergerak dari dalam ke luar. Hal ini menyebabkan ‘tuan yang punya badan\' menjadi membiayai sendiri keperluan untuk memutar kepalanya. Energi yang datang untuk mengolah otak kiri datang dalam diri tipe Ti sendiri. <b></b></p><p></p><p></p><ul><li><b>Tipologi Fisik </b></li></ul><b></b>Mesin kecerdasan Thinking (T) sesungguhnya identik dengan tulang. Mereka memiliki tulang yang besar dan kuat. Dengan bentuk tubuh (konstitusi) yang piknis, terlihat unsur tulang secara proporsional menjadi dominan. Disebut piknis karena ukuran badan volumenya lebih kecil dibanding tipe lain sementara volume tulangnya paling dominan dibanding tipe lain.<br>  <p></p><p>Akibatnya, tulang kerangka menyangga beban yang lebih ringan sehingga mudah bergerak kesana-kemari.  Itulah mengapa disebut piknis. Terlebih lagi tipe Ti ditunjang oleh ketersediaan baterai (charger) yang ada di dalam dirinya, sehingga menyebabkan tipe Ti ini seperti memiliki tulang yang bertenaga. Meskipun proporsi tulang lebih dominan dibanding ukuran badannya, namun secara umum Mesin Kecerdasan T malas bergerak. Padahal seharusnya MK ini mudah bergerak. Hal ini disebabkan tenaga yang tersedia dari MK ini cenderung disedot oleh kepala. Penggunaan energi oleh kepala dapat memakan energi yang besar sehingga menjadikan MK ini malas bergerak.<br>  </p><p></p><p>\r\n\r\n<b></b></p><blockquote><b><i>Tipe Ti : Otak kiri putih, Dalam ke luar, Motorik kasar, Piknik</i></b></blockquote><p></p><p></p><p></p><ul><li><b>Sifat Khas</b></li></ul>Jika menggunakan sudut pandang dunia psikologi (aliran perilaku), kepribadian dari Ti mesti memiliki sifat perilaku khas yang dapat dibuktikan dan diukur yang berbeda dari delapan kepribadian yang lain. Terdapat sepuluh item yang bisa dibuktikan keberadaannya dan bisa diukur secara psikometrik. <br> <br>Menurut konsep STIFIn, kesepuluh item tersebut menjadi kepribadian tetap yang tidak akan berubah dan akan selalu eksis seiring dengan penambahan umurnya. Sepuluh (10) sifat yang tetap tersebut adalah: <br><p></p><ul><li>Expert</li><li>On time</li><li>Scheduled</li><li>Independent</li><li>Focus</li><li>Through</li><li>Mechanistic</li><li>Prudent</li><li>Responsible</li><li>Scheme</li></ul>&nbsp;Sebagai pribadi yang utuh, tipe Ti memiliki sisi-sisi diametral sebagai berikut: sangat logis namun jika mentok malah bergantung pada faktor x, mengelola secara hebat namun penakut untuk keluar dari zonanya, di balik kemandiriannya tipe ini menyimpan rahasia dan ‘masking’, sebenarnya ia agresif tapi juga ingin ‘diladeni’. <br> <br>Selain itu, tipe Ti mengadili secara hitam-putih, namun mudah diprovokasi, jeli dan objektif namun terkadang gagal menangkap kontekstualitas gambaran besarnya, menjadi mesin profit yang mahir namun sering terjebak oleh hal-hal sepele. <br> <br>Oleh karena itu, tipe ini perlu waspada dengan kelemahannya<br>dan berusaha mengekploitasi kelebihannya. Biasanya jika<br>kelebihannya bergerak membaik, maka secara otomatis<br>kelemahan dari tipe ini akan tertutup dengan sendirinya. <br>\r\n\r\n<ul><li><b>Kelebihan</b></li></ul><b>\r\n\r\n</b>Kepribadian dari tipe Ti memiliki kekhasan karena memiliki kemampuan menalar secara mendalam dalam wujud penguasaan teknologi, mesin, dan mekanika yang melebihi delapan jenis kepribadian yang lain. Kelebihan ini dapat disepadankan dengan kecerdasan teknik  atau  disebut  TQ (Technical Quotient). <br> <br>Tipe Ti memiliki kelebihan yang otomatis berfungsi dalam cara  kerjanya yang terbiasa efektif. Tipe ini memperlakukan pekerjaannya dengan output per standar yang harus tinggi. Pekerjaan tipe ini mesti efektif dan menghasilkan kepastian yang tinggi pula. Pada akhirnya tipe Ti akan mengelola proses dan menata sistem sehingga pada akhirnya aktivitas berjalan dengan cara yang penuh kepastian.<div><br></div><div>Cara membelanjakan pengeluaran dari tipe Ti juga harus efektif. Termasuk dalam berbagi pun, tipe ini sangat berhitung. Tipe ini lebih suka diminta menyumbang pikiran dibandingkan mengeluarkan uang jika dalam pertimbangannya dianggap tidak efektif.</div><div> <p></p><p></p><blockquote><b><i>Tipe Ti : Technical Quotient (TQ), Efektif, Defensif</i></b></blockquote><i><ul><li><b>Kemistri</b><br></li></ul></i>Dengan kekuatan pada spesialisasi, kepakaran, kemampuan otak kiri, kemandirian, karakter yang seperti tulang (atau ibarat besi yang kokoh), bahkan keberadaan tulang yang lebih dominan dibanding ukuran badan, menjadikan orang Ti akan jauh lebih sukses kalau ia memiliki tapak untuk menekuni aktivitasnya. <i><br><b></b> </i><br>Tipe Ti jangan menjalankan usaha yang tidak memiliki tapak/markas/kantor/bengkel permanen. Hal tersebut akan menyebabkan usahanya tidak stabil dan tidak terstruktur. Tipe ini harus memulai dari lokasi tertentu dan menumbuh kembangkan<br>hingga besar. Lokasi  itulah  tapak yang akan  membuat  tipe  Ti  meraih tahta sebagai   kekuasaannya.</div><div><br></div><div>Kebesaran tapaknya akan sejalan dengan kebesaran tahtanya atau kekuasaannya. Pada dasarnya, tipe ini memang menjadi orang yang paling bertahta, sepanjang ia berusaha mencari dimulai dari tapaknya. &nbsp;</div><div>\r\n\r\n<b><blockquote><i>Tipe Ti : Memiliki TAPAK, Mengejar TAHTA, Pakar/Expert</i></blockquote></b><ul><li><b>Peranan</b><br></li></ul><b></b>Dengan fungsi kepala yang dominan, tipe T lebih senang jenis pekerjaan yang memerlukan berpikir keras sehingga dapat menyelesaikan masalah hingga tuntas. Ia dapat menyusun serangkaian logika sebagai metode untuk menyelesaikan masalah. <br> <br>Hal inilah yang membuat tipe T dianggap bertangan dingin. Setiap masalah yang menjadi tanggung jawabnya dapat diselesaikan dengan baik. Kemudian jika kemampuan kepala tersebut digabungkan dengan karakter tulang yang kokoh maka akan melahirkan kepribadian yang teguh dan keras kepala. <br> <br>Jika tipe T dikemudikan dari dalam ke luar- menjadi tipe Ti akan muncul sifat yang lebih mandiri. Hal itu membuat tipe ini menyukai peranan sebagai orang yang menguasai masalah dan dapat bekerja sendiri seperti seorang pakar. <br> <br>Kepakaran dari tipe ini menjadi lebih spesifik, misalnya seperti seorang spesialis. Hal ini disebabkan cara kerja kemudinya yang datang dari dalam ke luar dirinya sehingga ia memiliki mesin pendorong sendiri untuk menekuni atau menggali masalah secara khusus dan mendalam<b><br></b><ul><li><b>Target dan Harapan </b></li></ul>Target  kerja  dari  tipe  Ti adalah menghasilkan kestabilan dalam  penuh  kepastian. Kemampuan pengelolaan tipe Ti  sangat bagus. Ia  mampu mengkalkulasi segala macam unsur yang  akan menimbulkan kesalahan yang diperkirakan mengganggu kerjanya. Dan mencari jalan   keluar setiap masalah  dengan belajar kepada informasi <br>yang direkamnya. Pengharapan dari tipe ini terletak pada kemampuan pengelolaannya yang efektif, menjalankan tugas sehingga ia semakin layak diberi otoritas yang lebih besar.</div><div>\r\n\r\n<i><b><blockquote>Tipe Ti : Ingin kepastian, Managing, Jalur kepakaran/expertise</blockquote></b></i></div><div><p></p><p></p><ul><li>&nbsp;<b>Arah Merek</b></li></ul>Bagi tipe Ti, kepakaran (expertise) menjadi merek dirinya. Dengan kepakarannya, tipe ini akan ‘kepake’ kemana-mana. Tarif per jam dari tipe ini akan menjadi semakin mahal karena kepakaran tersebut. Masyarakat luas atau pasar pada umumnya akan menyebut nama tipe ini ketika mencari pakar di bidang tertentu. <br><ul><li><b>Cara Belajar</b></li></ul><b></b><p>Pada umumnya, tipe Ti tidak mengalami masalah dalam hal belajar. Pada umumnya, materi pelajaran memerlukan cara kerja otak yang menalar, berhitung, dan menstrukturkan. Pada tipe ini, ia sudah terbiasa menalar bacaan untuk mendapatkan logika isi dan intisari bacaannya. Otak kiri dari tipe Ti selalu memerlukan ‘fooding’ dengan cara berpikir, atau pada dasarnya ia suka berpikir, baik diminta ataupun tidak diminta. Hasil akhirnya menyebabkan tipe Ti menjadi orang yang paling lahap membaca buku pelajaran dan sekaligus menjadi orang yang tingkat penguasaannya paling tinggi terhadap isi pelajaran. <br> <br>Meskipun tidak dimotivasi, tipe Ti sudah dengan sendirinya memiliki kemandirian untuk belajar. Tetapi untuk meningkatkan atau memelihara motivasinya, tipe ini dapat didorong dengan cara memberikan rekognisi dari orang yang dihormatinya. Rekognisi berbeda dengan pujian. Rekognisi lebih menyerupai sebagai penghargaan atau pengakuan bahwa pada dirinya ada kemajuan dan diberikan oleh orang yang dihormatinya, seperti ibu-bapaknya atau gurunya atau seniornya atau bahkan dari lawan yang diseganinya.<br></p><ul><li><b>Pilihan Sekolah/ Profesi</b></li></ul>Prioritas utama jika ingin memilih jurusan atau profesi untuk tipe Ti sebaiknya diarahkan pada satu diantara lima pilihan industri berikut ini, yaitu riset dan teknologi, informasi teknologi, pertambangan, konstruksi, kesehatan, garmen. <br> <br>Selanjutnya pilihan lain dari jurusan atau profesi yang sesuai untuk tipe Ti sebagai berikut: manajer/eksekutif/yudikatif, manufactur, bisnis properti, peternakan, peneliti, fabrikan, perminyakan, pengeboran, programmer, dokter, apoteker, ekonom, manajer, dosen/guru, property,insinyur, fisikawan, kimiawan,konsultan managemen, teknokrat, birokrat, pajak, produsen, quality control, auditor, ahli strategi, pelatih bola, pembuat kebijakan, system analyst, ahli rekayasa, developer, atlit (pertandingan), dll.  <br><p><i></i>Di masa depan, pilihan profesi akan semakin beragam.  Namun jika ingin memilih jurusan ataupun profesi patokan bagi tipe Ti adalah mempertimbangkan empat kata kuncinya, yaitu: <b><i>menalar, tulang, mandiri</i></b><br> <br><i></i>Artinya, jurusan atau profesi yang dipilihnya didominasi oleh unsur-unsur yang memerlukan daya nalar, mengandalkan pendirian yang kuat atau bahkan profesi yang betul-betul mengandalkan tulang dalam pengertian sesungguhnya (seperti atlit balap sepeda, taekwondo, lari, renang khusus gaya dada, dsb)dan dapat memanfaatkan kemandiriannya dalam bekerja, serta bidang-bidang yang memerlukan kedalaman atau spesialisasi seperti insinyur atau teknokrat.</p><p><b></b></p><blockquote><b><i>Industri Tipe Ti : Riset dan Tenknologi, Informasi Teknologi, Pertambangan, Konstruksi, Kesehatan, Garmen</i></b></blockquote><b> </b><br> <br><b><br></b><br><b><br></b><br><b><br></b><br><b>  \r\n\r\n</b>\r\n\r\n<p></p><p><br><b><br></b></p><p></p><b></b> <br><br><br></div>\r\n\r\n<br><p></p>'),
(4, 'Artikel_STIFIn_(7)1.jpg', '<p>\r\n\r\n</p><h3>Thinking extrovert (Te)</h3>\r\n<p>Te adalah singkatan dari Thinking extrovert.  Jika huruf T berdiri sendiri merupakan identitas sebagai Mesin Kecerdasan (MK). Menurut konsep STIFIn, ragam Mesin Kecerdasan hanya ada lima, dan T adalah salah satu diantara 5 MK tersebut. Identitas Mesin Kecerdasan berubah menjadi kepribadian ketika MK digandengkan dengan jenis kemudi di belakangnya. Jenis kemudi kecerdasan hanya ada dua, yaitu i (introvert) dan e (extrovert). Dengan demikian, Te sudah menjadi identitas kepribadian. T ditulis dengan huruf besar karena pengaruhnya sebagai MK lebih besar dari e yang ditulis dengan huruf kecil yang berperan hanya sebagai kemudi kecerdasan.   <br></p><h4><ul><li>Sistem Operasi Otak</li></ul></h4><p>Pengertian sederhana dari Thinking extrovert adalah jenis kepribadian yang berbasiskan kecerdasan logika yang proses kerjanya dikemudikan dari luar  dirinya menuju ke dalam dirinya. Sistem operasi pada tipe Te berada di belahan otak bagian atas  di sebelah kiri atau disebut sebagai otak besar kiri atau diringkas otak kiri. Pada otak kiri tersebut yang menjadi kemudi kecerdasan dari tipe ini, berada di lapisan abu-abu yang letaknya di bagian luar atau permukaan otak. Otak kiri abu-abu itulah yang menjadi sistem operasi tipe Te.</p> Lapisan yang berwarna abu-abu dari tipe Te memiliki tekstur otak yang lebih longgar karena mengandung sel otak lebih sedikit. Kerapatan yang lebih rendah dibandingkan dengan lapisan bagian dalam tersebut membuat kemudi kecerdasan bergerak dari luar ke dalam. <br><br><p></p><p>Hal ini menyebabkan ‘tuan yang punya badan’ menjadi lebih beradaptasi dengan lingkungan luar, karena dari lingkuangan luar itulah tipe Te mendapatkan energi untuk memutar kepalanya. Energi yang datang untuk mengolah otak kiri tipe ini berasal dari luar dirinya. <br></p><p></p><p></p><ul><li><b>Tipologi Fisik </b></li></ul><b></b>Mesin kecerdasan Thinking (T) sesungguhnya identik dengan tulang. Mereka memiliki tulang yang besar dan kuat. Dengan bentuk tubuh (konstitusi) yang piknis terlihat unsur tulang secara proporsional menjadi dominan. Disebut ringan sehingga mudah bergerak piknis karena ukuran badan dar tipe  ini  volumenya  lebih  kecil dibanding tipe lain, sementara volume tulangnya paling domina dibanding tipe lain. <br><p>Akibatnya, tulang  kerangka menyangga beban yang  lebih kesana-kemari. Itulah mengapa disebut piknis. Tipe Te tidak memiliki baterai (charger) di dalam dirinya dan justru ia menghadirkan dari luar. Hal inilah yang menyebabkan Te seperti memiliki tulang yang kurang tenaga.<br></p><p>Meskipun proporsi tulang lebih dominan dibanding ukuran badannya, namun secara umum Mesin Kecerdasan T malas bergerak. Padahal seharusnya MK ini mudah bergerak. Hal ini disebabkan tenaga yang tersedia dari MK ini cenderung disedot oleh kepala. Penggunaan energi oleh kepala dapat memakan energi yang besar sehingga menjadikan MK ini malas bergerak.<br>  </p><p></p><p>\r\n\r\n<b></b></p><blockquote><b><i>Tipe Te : Otak kiri abu-abu, Luar ke dalam, Motorik halus dan Piknis</i></b></blockquote><p></p><p></p><p></p><ul><li><b>Sifat Khas</b></li></ul>Jika menggunakan sudut pandang dunia psikologi (aliran perilaku) kepribadian dari tipe Te mesti memiliki sifat perilaku khas yang dapat dibuktikan dan diukur yang berbeda dari delapan kepribadian yang lain. Terdapat sepuluh item yang bisa dibuktikan keberadaannya dan bisa diukur secara psikometrik. <br> <br>Menurut konsep STIFIn, kesepuluh item tersebut menjadi kepribadian tetap yang tidak akan berubah dan akan selalu eksis seiring dengan penambahan umurnya. Sepuluh (10) sifat yang tetap tersebut adalah: <br><ul><li>Thoughtful</li><li>Analytical </li><li>Competitive</li><li>Reserved</li><li>Planner</li><li>Positive</li><li>Argumentative</li><li>Forceful</li><li>Formal </li><li>Justice</li></ul>Sebagai pribadi yang utuh, tipe Te memiliki sisi-sisi diametral sebagai berikut: menyukai kemenangan, seperti seorang penakluk namun kurang stamina untuk terus menerus bertanding, kebenaran empirik hanya datang dari pengalaman dirinya namun tidak suka kebenaran dari nasihat orang lain, pandai mengakumulasi keuntungan dengan coverage yang luas, namun kurang mahir dalam membuat prioritas jangka panjang. <br><br><p></p><p>Selain itu, peranan dari tipe Te sangat sirkulatif di semua entitasnya, hanya sayang kurang pandai membaca aspirasi, memiliki siklus hidup yang dinamis namun terlalu normatif. Oleh karena itu, tipe ini perlu waspada dengan kelemahannya dan berusaha mengeksploitasi kelebihannya. Biasanya jika kelebihannya bergerak membaik, maka secara otomatis kelemahan dari tipe ini akan tertutup dengan sendirinya.<br>\r\n\r\n</p><ul><li><b>Kelebihan</b></li></ul><b>\r\n\r\n</b>Kepribadian dari tipe Te  memiliki  kekhasan karena memiliki kemampuan  menalar secara  meluas  dalam bentuk pengendalian manajemen dan logika yang lebih efektif yang melebihi delapan (8) jenis kepribadian yang lain. <p></p><p>Tipe Te pandai mengumpulkan kekuatan untuk menghasilkan kemenangan bagi diri dan organisasinya. Bagi tipe ini, organisasi akan dikelola seperti pabrik, tentara, birokrat, atau atasan-bawahan karena otoritas sepenuhnya berada di tangannya. Cara organisasi yang seperti inilah yang kemudian membuatnya berhasil dalam melipatgandakan hasil atau produksi.  Pendek kata, tipe Te memiliki kelebihan secara alamiah yang tercermin dari cara kerjanya yang selalu mencari<br>peluang untuk melipatgandakan. Tipe ini memperlakukan pekerjaannya dengan mengendalikan proses secara tepat, yaitu proses yang menimbulkan produktivitas pelipatgandaan. <br> <br>Hal tersebut sejalan dengan tabiatnya terhadap uang. Tipe Te adalah tipe yang paling mampu mengelola keuangan dengan cara mengakumulasi atau mengumpulkan. Bagi tipe ini, cara membelanjakan pengeluaran sebenarnya sangat rasional namun punya kemampuan taktis jika ia melihat ada peluang untuk mengakumulasi keuntungan.</p><div><i><ul><li><b>Kemistri</b><br></li></ul></i>Dengan kemampuan pengendalian terhadap span of control yang besar, kemampuan logika umum, kemampuan dalam melipatgandakan hasil, karakter yang seperti tulang (atau ibarat besi yang kokoh), dan keberadaan tulang yang lebih dominan dibanding ukuran badan, menjadikan tipe Te akan jauh lebih sukses jika ia memiliki pabrik untuk menjalankan ‘mesinnya’. Tipe Te perlu lebih mengkonkritkan usahanya dalam wujud yang nyata, misalnya berbentuk pabrik, sehingga mesin pelipatgandaannya dapat berjalan dengan baik. </div><div><br></div><div>Tipe Te memiliki kemampuan untuk meraih tahta dimanapun jika segala macam unsur sumberdaya yang ada di bawah kekuasaannya dikelola secara mekanistik seolah-olah seperti pabrik. Jika memiliki pabrik (dalam pengertian yang sesungguhnya), maka tipe ini akan memiliki kedudukan di masyarakat sebagai tokoh yang layak diberi tahta yang lebih tinggi. <br><br>Kebesaran pabrik tersebut akan sejalan dengan kebesaran tahta atau kekuasaan dari tipe Te. Pada dasarnya tipe Te memang menjadi orang yang paling dicari tahta. Hal ini disebabkan pada diri tipe ini, melekat kharisma pemegang otoritas. Jika diberi amanah, tipe ini akan menjalankannya dengan baik.</div><div><blockquote><b><i>Tipe Te : Memiliki PABRIK, Dikejar TAHTA, Komandan (chief)</i></b><br></blockquote></div><div><ul><li><b>Peranan</b><br></li></ul><b></b>Dengan fungsi kepala yang dominan, MK Thinking (T) lebih senang dengan jenis pekerjaan yang memerlukan berpikir keras sehingga dapat menyelesaikan masalah hingga tuntas. Tipe ini dapat menyusun serangkaian logika sebagai metode<br>untuk menyelesaikan masalah.   <br> <br>Hal itulah yang menjadikan tipe  T dianggap  bertangan dingin. Setiap  masalah  yang menjadi tanggungjawabnya, dapat diselesaikan dengan baik. Jika kemampuan kepala tersebut digabungkan  dengan  karakter tulang yang kokoh, maka akan melahirkan tipe T dengan kepribadian yang teguh dan keras kepala. <br> <br>Jika tipe ini dikemudikan dari luar ke dalam -menjadi tipe Te-akan muncul sifat-sifat utama yang adil, objektif, dan menerima argumentasi logika. Hal itu membuat tipe Te menyukai peran layaknya seorang komandan yang memimpin dengan otoritas penuh untuk memastikan keadilan dan logika dapat berjalan secara efektif. Peranan tersebut diikuti dengan kemampuan pengelolaan dan pengendalian organisasi yang baik dari tipe Te.<b><br></b><ul><li><b>Target dan Harapan </b></li></ul> <br>Bagi tipe Te, kemampuan mengendalikan proses produksi menjadi jalan sukses untuk memperoleh kekuasaan. Ia akan menjadi sangat terpukul jika pada akhirnya tidak diberi otoritas untuk mengambil keputusan. Target dari tipe ini adalah mendapatkan otoritas penuh. Dengan otoritas tersebut ia dapat mengendalikan secara penuh jalannya organisasi. Tipe ini akan mengendalikan organisasi (kemampuan managerialship) dengan memberikan instruksi, pengarahan, dan briefing-briefing. <br></div><div>\r\n\r\n<i><b><blockquote>Tipe Te : Mendapatkan kekuasaan (authority), Controlling, Kemampuan manajerialship yang efektif</blockquote></b></i></div><div><p></p><p></p><ul><li>&nbsp;<b>Arah Merek</b></li></ul>Tipe Te Memiliki kemampuan kepemimpinan melalui Managerialship yang sangat bagus. Bahkan tipe ini diberi merk karena kemampuan managerialshipmya yang efektif. Jika ada pertarungan untuk menunjukkan kemampuan pengelolaan  dan pengendalian organisasi, agar organisasinya dapat selalu naik kelas karena selalu menciptakan produksi yang berlipat ganda, maka tipe Te merupakan sosok yang akan memenangkannya. Selain tipe Te memang memiliki kemampuan managerial yang sistematis dan efektif, secara alamiah tipe ini juga haus kekuasaan.<br><ul><li><b>Cara Belajar</b></li></ul><b></b><p>Pada umumnya, tipe Te tidak mengalami masalah dalam hal belajar.   Pada umumnya, materi pelajaran memerlukan cara kerja otak yang menalar, berhitung, dan  menstrukturkan. Pada  tipe  Te,  ia sudah  terbiasa  menalar  bacaan  untuk mendapatkan  logika  dengan  membuat struktur dan skema yang memudahkan.  <br>Hal ini disebabkan tipe Te sebenarnyatidak ingin terlalu jelimet. Otak kiri dari tipe Te selalu memerlukan ‘fooding’ dengan cara berpikir atau pada dasarnya ia suka berpikir, baik diminta ataupun tidak diminta. Hasil akhirnya menyebabkan tipe Te menjadi orang yang paling berwawasan karena kumpulan buku yang dibacanya cukup lengkap. Meskipun penguasaan setiap topik dari buku tersebut tidak terlalu mendalam, namun tipe ini sudah mendapat struktur berpikir dari setiap bacaannya. <br></p><ul><li><b>Pilihan Sekolah/ Profesi</b></li></ul>Prioritas utama jika ingin memilih jurusan atau profesiuntuk tipe Te sebaiknya diarahkan pada satu diantara empat (4) pilihan industri berikut ini, yaitu manajemen/kepemerintahan,manufacturing, properti, peternakan. <br>  <br> <br>Selanjutnya  pilihan  lain  dari  jurusan  atau  profesi yang sesuai untuk tipe Te sebagai berikut: ristek, IT, bisnis pertambangan, ahli konstruksi, bisnis kesehatan, garmen, peneliti, fabrikan, perminyakan, pengeboran,  programmer,  dokter, apoteker, ekonomi, manajer, dosen/ guru, property, insinyur, fisikawan,  kimiawan,  konsultan managemen, teknokrat, birokrat, pajak, produsen, quality control, auditor, ahli strategi, pelatih bola,  pembuat  kebijakan, system analyst, ahli rekayasa, developer, atlit (pertandingan),dll. <br> <br>Di masa depan, pilihan profesi akan semakin beragam. Namun jika ingin memilih jurusan ataupun profesi patokan bagi tipe Te adalah mempertimbangkan empat kata kuncinya, yaitu: menalar, tulang, mandiri, dan meluas. <br> <br>Artinya, jurusan atau profesi yang dipilihnya didominasi oleh unsur-unsur yang memerlukan daya nalar, mengandalkan pendirian yang kuat atau bahkan profesi yang betul-betul mengandalkan tulang dalam pengertian sesungguhnya (seperti atlit balap sepeda, tae kwon do, lari, renang khusus gaya dada, dsb) dan dapat memanfaatkan kemandiriannya dalam bekerja, serta bidang-bidang yang memerlukan logika umum yang meluas seperti manajemen, menjadi komandan, atau birokrat. <br><p><b></b></p><blockquote><b><i>Industri Tipe Te : Manajemen/ Kepemimpinan, Manufacturing, Properti, Peternakan</i></b></blockquote><br> <br><b><br></b><b><br></b><p></p></div>\r\n\r\n\r\n\r\n<br><p></p>');

-- --------------------------------------------------------

--
-- Table structure for table `tb_trainer_about`
--

CREATE TABLE `tb_trainer_about` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `deskripsi` text,
  `gambar` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_trainer_about`
--

INSERT INTO `tb_trainer_about` (`id`, `judul`, `deskripsi`, `gambar`) VALUES
(2, 'Seputar Trainer STIFIn', '<p>Tes STIFIn dilakukan dengan cara men-scan kesepuluh ujung jari Anda, mengambil waktu tidak lebih dari 1 menit. Sidik jari yang membawa informasi tentang komposisi susunan syaraf tersebut kemudian dianalisa dan dihubungkan dengan belahan otak tertentu yang dominan berperan sebagai sistem-operasi dan sekaligus menjadi mesin kecerdasan anda.<br></p>', 'Artikel_STIFIn_(2)111.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_trainer_people`
--

CREATE TABLE `tb_trainer_people` (
  `id` int(11) NOT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `branding` varchar(255) DEFAULT NULL,
  `rating` varchar(255) DEFAULT NULL,
  `deskripsi` text,
  `gambar` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_trainer_people`
--

INSERT INTO `tb_trainer_people` (`id`, `nama`, `branding`, `rating`, `deskripsi`, `gambar`) VALUES
(1, 'Heri Irawan', 'UI/UX Designer', '4 and a half', '<p><b></b></p>Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.<br><p></p><p></p>', 't12.jpg'),
(4, 'Rizki Aditya', 'Marketing', '3 and a half', '<p>Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.<br></p>', 't13.jpg'),
(5, 'Anggela Pangraung', 'Head Marketing', '4 and a half', '<p>Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.<br></p>', 't14.jpg'),
(6, 'Stepani Belly', 'Marketing', '3', '<p>Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.<br></p>', 't15.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tb_trainer_roadmap`
--

CREATE TABLE `tb_trainer_roadmap` (
  `id` int(11) NOT NULL,
  `konten` text,
  `waktu` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_trainer_roadmap`
--

INSERT INTO `tb_trainer_roadmap` (`id`, `konten`, `waktu`) VALUES
(3, '<p>1213</p>', '2023-04-08'),
(4, '<p>31</p>', '2023-04-08');

-- --------------------------------------------------------

--
-- Table structure for table `tb_video`
--

CREATE TABLE `tb_video` (
  `id` int(11) NOT NULL,
  `link_embed` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_video`
--

INSERT INTO `tb_video` (`id`, `link_embed`) VALUES
(5, '5nsR8SkYdv0');

-- --------------------------------------------------------

--
-- Table structure for table `tb_workshop`
--

CREATE TABLE `tb_workshop` (
  `id` int(11) NOT NULL,
  `judul` varchar(255) DEFAULT NULL,
  `konten` text,
  `tanggal_workshop` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tb_workshop`
--

INSERT INTO `tb_workshop` (`id`, `judul`, `konten`, `tanggal_workshop`) VALUES
(1, 'WSL 1', '<h1>Workshop STIFIn Level 1&nbsp;</h1><h4><i><blockquote><small>Find The True You - Gue Banget</small></blockquote></i></h4><p>Workshop STIFIn Level 1 sebagai materi dasar yang membahas tentang 5 (lima) Mesin Kecerdasan dan 9 (sembilan) Personaliti Genetik. Workshop ini akan menguak jati diri Anda yang sebenarnya, sehingga akan terasa Ini <b><i>Gue Banget</i></b>. Pelaksanaan WSL 1 berlangsung selama 1 hari.</p><p><br></p>', '2023-03-25 08:30:00'),
(2, 'WST', '<p>\r\n\r\n</p><h1>Workshop STIFIn Tematik</h1><h4><i><blockquote><small>STIFIn Aplikatif - STIFIn Multi Angle Field</small></blockquote></i></h4><p>Workshop STIFIn akan menghantarkan Anda untuk makin memahami kedalaman dan ketajaman konsep STIFIn dalam membedah setiap bidang kehidupan. Ini merupakan bentuk aplikasi dari konsep STIFIn yang <b><i>Multi Angle Field</i></b>&nbsp;(bisa diaplikasikan disemua bidang kehidupan). Pelaksanaan WST berlangsung selama 1 hari.</p><br>\r\n\r\n<br><p></p>', '2023-03-25 20:30:00'),
(3, 'WSL 2', '<p>\r\n\r\n</p><h1>Workshop STIFIn Level 2</h1><h4><i><blockquote><small>Licensed Promotor STIFIn - Tentang Loe, ue Ahlinya</small></blockquote></i></h4><p>Workshop STIFIn Level 2 merupakan workshop yang diperuntukkan bagi Anda yang ingin mengetahui lebih dalam tentang diri Anda dan orang lain, maka workshop ini wajib Anda ikuti sehingga Anda bisa mengambil Peluang Bisnis STIFIn dengan menjadi <b><i>Licensed Promotor STIFIn</i></b>. Pelaksanaan WSL 2 berlangsung selama 1 hari.</p><br>\r\n\r\n<br><p></p>', '2023-03-25 08:30:00'),
(4, 'SPT', '<p>\r\n\r\n</p><h1>STIFIn Professional Trainer</h1><h4><i><blockquote><small>Licensed Trainer STIFIn - Gue Trainer, Puas Loe Pade</small></blockquote></i></h4><p>STIFIn Professional Trainer merupakan training yang akan mengantarkan Anda menjadi <b><i>Licensed Trainer STIFIn</i></b> yang berkarakter, expert dan berkualitas. Pelaksanaan SPT berlangsung selama 3 hari secara online untuk sesi menemukan materi andalan sesuai roles Anda dan 30 hari sesi mentoring bersama Master Trainer STIFIn.&nbsp;</p><br>\r\n\r\n<br><p></p>', '2023-03-25 08:30:00'),
(5, 'WSL 3', '<p>\r\n\r\n</p><h1></h1>\r\n\r\n<h1>Workshop STIFIn Level 3</h1><h4><i><blockquote><small>Licensed Solver STIFIn - Nyalakan Jalan Cahaya</small></blockquote></i></h4><p>Workshop STIFIn Level 3 merupakan workshop bersama Penemu STIFIn yang akan membedah lebih detail dan tajam setiap bagian dari Konsep STIFIn yang akan digunakan dalam proses Solving pada klien. Lulus WSL 3, maka diakui sebagai Licensed Solver STIFIn setelah mengikuti uji sertifikasi dari Asosiasi Solver STIFIn. Solver STIFIn akan membantu orang lain dalan menyalakan Jalan Cahaya untuk <i>problem solving</i> orang lain. Pelaksanaan WSL 3 berlangsung selama 28 hari selama 4 bulan.&nbsp;</p>\r\n\r\n<br><p></p>', '2023-03-25 08:30:00'),
(6, 'WSL 4', '<p>\r\n\r\n</p><h1>Workshop STIFIn Level 4</h1><h4><i><blockquote><small>Licensed Konsultan STIFIn&nbsp;</small></blockquote></i></h4><p>Workshop STIFIn Level 4 merupakan workshop bersama Penemu STIFIn yang akan membedah lebih detail dan tajam setiap bagian dari Konsep STIFIn, yang akan membantu Anda untuk menyelesaikan permasalahan pada Level Sosial dan Organisasi. Pelaksanaan WSL 4 berlangsung selama 3 bulan.&nbsp;</p>\r\n\r\n<br><p></p>', '2023-03-25 08:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `tb_yayasan_stifin`
--

CREATE TABLE `tb_yayasan_stifin` (
  `id` int(11) NOT NULL,
  `konten` text,
  `gambar` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tb_yayasan_stifin`
--

INSERT INTO `tb_yayasan_stifin` (`id`, `konten`, `gambar`) VALUES
(5, '<p>\r\n\r\n</p><h2>About</h2><p>Yayasan STIFIn merupakan yayasan yang bergerak dalam bidang sosial, pendidikan, dan keagamaan berdasarkan Konsep STIFIn.<br></p><p><b></b></p><b><p><br></p></b><p></p><h2>Visi :</h2><p>Menjadikan Konsep STIFIn sebagai konsep yang paling mendekati derivasi Al Quran, sehingga layak menjadi mainstream platform Pengembangan SDM hingga akhir zaman</p><br><h2>Misi :</h2><ol><li>Menjadikan alat Tes STIFIn sebagai alat tes yang menentukan jenis kecerdasan dan personaliti yang terbaik dibanding alat tes lainnya</li><li>Menjadikan Konsep STIFIn sebagai pemodelan, skema, algoritma terbaik dalam menjelaskan tentang manusia sebagai makhluk individu dan sebagai makhluk sosial.</li><li>Berperan besar dalam program-program kebermanfaatan yang memiliki dampak strategis dalam meningkatkan spiritualitas umat dan masyarakat.</li></ol>\r\n\r\n\r\n\r\n<br><p></p>', 'Yayasan_STIFIn_hijau_baru.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tb_about_promotor`
--
ALTER TABLE `tb_about_promotor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_about_us`
--
ALTER TABLE `tb_about_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_admin`
--
ALTER TABLE `tb_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_artikel`
--
ALTER TABLE `tb_artikel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_cabang`
--
ALTER TABLE `tb_cabang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_cabang_section`
--
ALTER TABLE `tb_cabang_section`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_dc_about_cabang`
--
ALTER TABLE `tb_dc_about_cabang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_dc_alur_tes`
--
ALTER TABLE `tb_dc_alur_tes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_dc_pilihan_cabang`
--
ALTER TABLE `tb_dc_pilihan_cabang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_dc_popup`
--
ALTER TABLE `tb_dc_popup`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_dc_potensi`
--
ALTER TABLE `tb_dc_potensi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_dc_sumber_profit`
--
ALTER TABLE `tb_dc_sumber_profit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_download_audio`
--
ALTER TABLE `tb_download_audio`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_download_materi`
--
ALTER TABLE `tb_download_materi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_download_video`
--
ALTER TABLE `tb_download_video`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_dp_about`
--
ALTER TABLE `tb_dp_about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_dp_benefit`
--
ALTER TABLE `tb_dp_benefit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_dp_roadmap`
--
ALTER TABLE `tb_dp_roadmap`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_feeling`
--
ALTER TABLE `tb_feeling`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_footer`
--
ALTER TABLE `tb_footer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_hero_section`
--
ALTER TABLE `tb_hero_section`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_insting`
--
ALTER TABLE `tb_insting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_intuiting`
--
ALTER TABLE `tb_intuiting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_jenjang`
--
ALTER TABLE `tb_jenjang`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_keunggulan`
--
ALTER TABLE `tb_keunggulan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_konsep_stifin`
--
ALTER TABLE `tb_konsep_stifin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_merchandise`
--
ALTER TABLE `tb_merchandise`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_partners`
--
ALTER TABLE `tb_partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_promotor_section`
--
ALTER TABLE `tb_promotor_section`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_provinsi`
--
ALTER TABLE `tb_provinsi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_sejarah`
--
ALTER TABLE `tb_sejarah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_sensing`
--
ALTER TABLE `tb_sensing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_solver_about`
--
ALTER TABLE `tb_solver_about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_solver_people`
--
ALTER TABLE `tb_solver_people`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_solver_roadmap`
--
ALTER TABLE `tb_solver_roadmap`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_stifin_institute`
--
ALTER TABLE `tb_stifin_institute`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_stifin_rqs`
--
ALTER TABLE `tb_stifin_rqs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_testimonial`
--
ALTER TABLE `tb_testimonial`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_thingking`
--
ALTER TABLE `tb_thingking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_trainer_about`
--
ALTER TABLE `tb_trainer_about`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_trainer_people`
--
ALTER TABLE `tb_trainer_people`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_trainer_roadmap`
--
ALTER TABLE `tb_trainer_roadmap`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_video`
--
ALTER TABLE `tb_video`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_workshop`
--
ALTER TABLE `tb_workshop`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tb_yayasan_stifin`
--
ALTER TABLE `tb_yayasan_stifin`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tb_about_promotor`
--
ALTER TABLE `tb_about_promotor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_about_us`
--
ALTER TABLE `tb_about_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_admin`
--
ALTER TABLE `tb_admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_artikel`
--
ALTER TABLE `tb_artikel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `tb_cabang`
--
ALTER TABLE `tb_cabang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `tb_cabang_section`
--
ALTER TABLE `tb_cabang_section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_dc_about_cabang`
--
ALTER TABLE `tb_dc_about_cabang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_dc_alur_tes`
--
ALTER TABLE `tb_dc_alur_tes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_dc_pilihan_cabang`
--
ALTER TABLE `tb_dc_pilihan_cabang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_dc_popup`
--
ALTER TABLE `tb_dc_popup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_dc_potensi`
--
ALTER TABLE `tb_dc_potensi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_dc_sumber_profit`
--
ALTER TABLE `tb_dc_sumber_profit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tb_download_audio`
--
ALTER TABLE `tb_download_audio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_download_materi`
--
ALTER TABLE `tb_download_materi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_download_video`
--
ALTER TABLE `tb_download_video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tb_dp_about`
--
ALTER TABLE `tb_dp_about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tb_dp_benefit`
--
ALTER TABLE `tb_dp_benefit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tb_dp_roadmap`
--
ALTER TABLE `tb_dp_roadmap`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `tb_feeling`
--
ALTER TABLE `tb_feeling`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tb_footer`
--
ALTER TABLE `tb_footer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_hero_section`
--
ALTER TABLE `tb_hero_section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_insting`
--
ALTER TABLE `tb_insting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_intuiting`
--
ALTER TABLE `tb_intuiting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_jenjang`
--
ALTER TABLE `tb_jenjang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_keunggulan`
--
ALTER TABLE `tb_keunggulan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_konsep_stifin`
--
ALTER TABLE `tb_konsep_stifin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_merchandise`
--
ALTER TABLE `tb_merchandise`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tb_partners`
--
ALTER TABLE `tb_partners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `tb_promotor_section`
--
ALTER TABLE `tb_promotor_section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tb_sejarah`
--
ALTER TABLE `tb_sejarah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `tb_sensing`
--
ALTER TABLE `tb_sensing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_solver_about`
--
ALTER TABLE `tb_solver_about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_solver_people`
--
ALTER TABLE `tb_solver_people`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `tb_solver_roadmap`
--
ALTER TABLE `tb_solver_roadmap`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tb_stifin_institute`
--
ALTER TABLE `tb_stifin_institute`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tb_stifin_rqs`
--
ALTER TABLE `tb_stifin_rqs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_testimonial`
--
ALTER TABLE `tb_testimonial`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `tb_thingking`
--
ALTER TABLE `tb_thingking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_trainer_about`
--
ALTER TABLE `tb_trainer_about`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tb_trainer_people`
--
ALTER TABLE `tb_trainer_people`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tb_trainer_roadmap`
--
ALTER TABLE `tb_trainer_roadmap`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tb_video`
--
ALTER TABLE `tb_video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tb_workshop`
--
ALTER TABLE `tb_workshop`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tb_yayasan_stifin`
--
ALTER TABLE `tb_yayasan_stifin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
